package com.oostajob;

import android.app.Application;

import com.flurry.android.FlurryAgent;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Admin on 26-Dec-15.
 */
public class OostaApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

      /*  // configure Flurry
        FlurryAgent.setLogEnabled(true);

        FlurryAgent.setContinueSessionMillis(System.currentTimeMillis());
        // init Flurry
        FlurryAgent.init(this, getString(R.string.flurry_analytics_key));*/
    }
}
