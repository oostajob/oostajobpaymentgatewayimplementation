package com.oostajob.navigationdrawer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.google.android.gms.maps.model.LatLng;
import com.oostajob.ConProfileActivity;
import com.oostajob.CusUpdateActivity;
import com.oostajob.LoginActivity;
import com.oostajob.R;
import com.oostajob.RegisterActivity;
import com.oostajob.SelectionActivity;
import com.oostajob.adapter.DrawerMenuAdapter;
import com.oostajob.global.Global;
import com.oostajob.model.LogoutModel;
import com.oostajob.model.MenuModel;
import com.oostajob.utils.CircleTransform;
import com.oostajob.utils.FetchAddress;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DrawerHomeFragment extends Fragment {

    private String USER_TYPE, NAME, PROFILE_PIC, USER_ID, LAT, LNG;
    private int PAGE;
    private View rootView;
    private DrawerHomeFragmentCallbacks mCallbacks;
    private View mFragmentContainerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView leftDrawerList;
    private DrawerMenuAdapter drawerMenuAdapter;
    private CustomTextView txtName, txtAddress;
    private CustomButton btnLogin, btnRegister, btnBack;
    private ImageView imgProfile;
    private RelativeLayout imgProfileLyt;
    private LinearLayout linLogout, menuLay;

    private int mCurrentSelectedPosition = 0;
    private ArrayList<MenuModel> list = new ArrayList<>();

    private APIService apiService;
    private ProgressDialog p;

    private FetchAddress fetchAddress;
    private InputMethodManager imm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        USER_TYPE = PrefConnect.readString(getActivity(), PrefConnect.USER_TYPE, "");
        NAME = PrefConnect.readString(getActivity(), PrefConnect.NAME, "");
        LAT = PrefConnect.readString(getActivity(), PrefConnect.LAT, "");
        LNG = PrefConnect.readString(getActivity(), PrefConnect.LNG
                , "");
        PROFILE_PIC = PrefConnect.readString(getActivity(), PrefConnect.PROFILE_PHOTO, "");
        apiService = RetrofitSingleton.createService(APIService.class);

        p = new ProgressDialog(getActivity());
        p.setMessage(getString(R.string.logout_msg));
        p.setIndeterminate(false);
        p.setCancelable(false);

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.drawer_list, container, false);
        init(rootView);
        if (USER_TYPE.equals("1")) {
            list = Global.getCustomerMenu(getActivity());
        } else if (USER_TYPE.equals("2")) {
            list = Global.getContractorMenu(getActivity());

        } else {
//            list = Global.getCustomerMenu(getActivity());
            menuLay.setVisibility(View.GONE);
        }
        drawerMenuAdapter = new DrawerMenuAdapter(getActivity(), list);
        leftDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawer(mFragmentContainerView);
                }

                mDrawerLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectItem(position);
                    }
                }, 300);
            }
        });
        leftDrawerList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        leftDrawerList.setAdapter(drawerMenuAdapter);
        leftDrawerList.setItemChecked(mCurrentSelectedPosition, true);
        return rootView;
    }

    private void init(View rootView) {
        leftDrawerList = (ListView) rootView.findViewById(R.id.menuListView);
        mDrawerLayout = (DrawerLayout) rootView.findViewById(R.id.drawerLayout);
        txtName = (CustomTextView) rootView.findViewById(R.id.txtName);
        txtAddress = (CustomTextView) rootView.findViewById(R.id.txtAddress);
        imgProfile = (ImageView) rootView.findViewById(R.id.imgProfile);
        imgProfileLyt = (RelativeLayout) rootView.findViewById(R.id.imgProfile_lyt);
        linLogout = (LinearLayout) rootView.findViewById(R.id.linLogout);
        btnLogin = (CustomButton) rootView.findViewById(R.id.btnLogin);
        btnBack = (CustomButton) rootView.findViewById(R.id.btnBack);
        btnRegister = (CustomButton) rootView.findViewById(R.id.btnRegister);
        menuLay = (LinearLayout) rootView.findViewById(R.id.menuLay);

        txtName.setText(NAME);
        String url = String.format("%s/%s", Global.BASE_URL, PROFILE_PIC);
        Picasso.with(getActivity()).load(url).transform(new CircleTransform()).placeholder(R.drawable.user).fit().into(imgProfile);
        setListener();

        try {
            LatLng latLng = new LatLng(Double.parseDouble(LAT), Double.parseDouble(LNG));
            fetchAddress = new FetchAddress(getActivity(), latLng);
            fetchAddress.getAddress();
            String address = fetchAddress.getLocality();
            if (TextUtils.isEmpty(address)) {
                address = TextUtils.isEmpty(fetchAddress.getPostalCode()) ? "" : fetchAddress.getPostalCode();
            } else if (!TextUtils.isEmpty(fetchAddress.getPostalCode())) {
                address = address + " - " + fetchAddress.getPostalCode();
            }
            txtAddress.setText(address);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        linLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearApplicationDataAndLogout();
            }
        });
        imgProfileLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (USER_TYPE.equals("1")) {
                    Intent upIntent = new Intent(getActivity(), CusUpdateActivity.class);
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(upIntent);
                } else {
                    Intent upIntent = new Intent(getActivity(), ConProfileActivity.class);
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(upIntent);
                }
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SelectionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    public void initDrawer(DrawerLayout drawerLayout, final Toolbar toolbar, int currentPosition) {
        mFragmentContainerView = getActivity().findViewById(R.id.navigation_drawer);
        mDrawerLayout = drawerLayout;

        drawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().supportInvalidateOptionsMenu();

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                toolbar.setTitle(getString(R.string.app_name));
                getActivity().supportInvalidateOptionsMenu();

            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });
        drawerLayout.setDrawerListener(drawerToggle);
        mCurrentSelectedPosition = currentPosition;
        selectItem(mCurrentSelectedPosition);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (rootView != null) {
            leftDrawerList.setItemChecked(position, true);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.global, menu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (DrawerHomeFragmentCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void clearApplicationDataAndLogout() {
        p.show();
        Call<LogoutModel> call = apiService.logout(USER_ID);
        call.enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    LogoutModel model = response.body();
                    if (model.getResult().equalsIgnoreCase("Success")) {
                        PrefConnect.clearAllPrefs(getActivity());
                        Intent intent = new Intent(getActivity(), RegisterActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    public static interface DrawerHomeFragmentCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }
}
