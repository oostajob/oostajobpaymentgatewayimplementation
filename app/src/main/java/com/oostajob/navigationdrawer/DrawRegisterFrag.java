package com.oostajob.navigationdrawer;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;

/**
 * Created by Admin on 26-06-2015.
 */
public class DrawRegisterFrag extends Fragment {

    public static final int GET_STARTED = 0;
    public static final int ABOUT = 1;
    public static final int CONTACT = 2;
    public static final int STORIES = 3;
    public static final int FAQ = 4;
    public static final int CUSTOMER = 5;
    public static final int CONTRACTOR = 6;

    private LinearLayout aboutLay, contactLay, storiesLay, faqLay, customerLay, contractorLay;
    private CustomTextView txtGetStarted;
    private View rootView;
    private Toolbar toolbar;
    private DrawRegisterFragCallbacks mCallbacks;
    private View mFragmentContainerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private int mCurrentSelectedPosition = GET_STARTED;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.drawer_fragment, container, false);
        init(rootView);
        setListener();
//        leftDrawerList.setItemChecked(mCurrentSelectedPosition, true);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectItem(mCurrentSelectedPosition);
    }

    private void init(View rootView) {
        mDrawerLayout = (DrawerLayout) rootView.findViewById(R.id.drawerLayout);
        aboutLay = (LinearLayout) rootView.findViewById(R.id.aboutLay);
        contactLay = (LinearLayout) rootView.findViewById(R.id.contactLay);
        storiesLay = (LinearLayout) rootView.findViewById(R.id.storiesLay);
        faqLay = (LinearLayout) rootView.findViewById(R.id.faqLay);
        customerLay = (LinearLayout) rootView.findViewById(R.id.customerLay);
        contractorLay = (LinearLayout) rootView.findViewById(R.id.contractorLay);
        txtGetStarted = (CustomTextView) rootView.findViewById(R.id.txtGetStarted);
    }

    private void setListener() {
        aboutLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(ABOUT);
            }
        });
        contactLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(CONTACT);
            }
        });
        storiesLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(STORIES);
            }
        });
        faqLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(FAQ);
            }
        });
        customerLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(CUSTOMER);
            }
        });
        contractorLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(CONTRACTOR);
            }
        });
        txtGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer(GET_STARTED);
            }
        });
    }

    private void closeDrawer(final int position) {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }

        mDrawerLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                selectItem(position);
            }
        }, 300);
    }

    public void initDrawer(DrawerLayout drawerLayout, final Toolbar toolbar, int currentPosition) {
        mFragmentContainerView = getActivity().findViewById(R.id.navigation_drawer);
        this.toolbar = toolbar;
        mDrawerLayout = drawerLayout;

        drawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().supportInvalidateOptionsMenu();

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                toolbar.setTitle("");
                getActivity().supportInvalidateOptionsMenu();

            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });
        drawerLayout.setDrawerListener(drawerToggle);
        mCurrentSelectedPosition = currentPosition;
        selectItem(mCurrentSelectedPosition);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (rootView != null) {
//            leftDrawerList.setItemChecked(position, true);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.global, menu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (DrawRegisterFragCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement DrawRegisterFragCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public static interface DrawRegisterFragCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }
}
