package com.oostajob.global;


import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.Toast;

import com.oostajob.R;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.model.MenuModel;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class Global {

    //Service URL
    public static final boolean CLIENT_INSTANCE = true;

    public static final String DEV_URL = "http://52.11.208.116";
    //Demo instance URL
    //public static final String DEV_URL = "http://52.34.43.229";
    public static final String CLIENT_URL = "http://52.35.35.214/";

    public static final String liveDomain = "http://oostajob.com/";

    public static final String stagingUrl = "http://54.245.208.103";

    public static final String NowServer = "http://52.41.134.73/";

    public static final String CODEWAVE_URL = "http://13.126.94.161/Oostajob/";

    public static final String BASE_URL = stagingUrl;
    public static final String PACKAGE = "com.oostajob";
    public static final String GOOGLE_API_URL = "https://maps.googleapis.com";
    public static final String SENDER_ID = "449041080274";
    public static final int CAMERA_CAPTURE = 1000;
    public static final int GALLERY_PICK = 1001;
    public static final int PICTURE_CROP = 1002;
    public static final int VIDEO_CAPTURE = 1003;
    public static final int GALLERY_PICK_VIDEO = 1004;
    public static final int CAMERA_CAPTURE_lICENCE = 2000;
    public static final int GALLERY_PICK_lICENCE = 2001;
    public static final int PICTURE_CROP_lICENCE = 2002;
    public static final int ADDRESS = 100;
    public static final int TEXT = 101;

    public static final double DEFAULT_LAT = 0;
    public static final double DEFAULT_LNG = 0;
    public static final String DEFAULT_ADDRESS = "5141 California Ave, Irvine, CA 92617, USA";

    public static final long INTERVAL = 5 * 1000;

    //    public static final String EMAIL_EXPRESSION = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
//    public static final String EMAIL_EXPRESSION = "^[^0-9][A-Z0-9._%+-]+@[^0-9][A-Z0-9.-]+\.[A-Z]{2,6}$";
//    public static final String EMAIL_EXPRESSION = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    public static final String EMAIL_EXPRESSION = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";

    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Uri generateTimeStampPhotoFileUri() {

        Uri photoFileUri = null;
        File outputDir = Global.getPhotoDirectory();
        if (outputDir != null) {
         /*   Time t = new Time();
            t.setToNow();*/
            photoFileUri = Uri.fromFile(outputDir);
        }
        return photoFileUri;
    }


    public static File generateTimeStampPhotoFile() {

        Uri photoFileUri = null;
        File outputDir = Global.getPhotoDirectory();
        if (outputDir != null) {
         /*   Time t = new Time();
            t.setToNow();*/
            photoFileUri = Uri.fromFile(outputDir);
        }
        return outputDir;
    }


    public static File getPhotoDirectory() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }


    public static File getVideoDirectory() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "MP4_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File video = null;
        try {
            video = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".mp4",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        return video;
    }

    public static void performCrop(Activity activity, Uri formUri, Uri toUri, int outputX, int outputY, int code) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(formUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", outputX);
            cropIntent.putExtra("outputY", outputY);
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, toUri);
            activity.startActivityForResult(cropIntent, code);
        } catch (ActivityNotFoundException e) {
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<MenuModel> getCustomerMenu(Activity activity) {
        ArrayList<MenuModel> list = new ArrayList<>();
        MenuModel model = new MenuModel(R.drawable.menu1, activity.getString(R.string.cMenu1));
        list.add(model);
        model = new MenuModel(R.drawable.menu2, activity.getString(R.string.cMenu2));
        list.add(model);

        model = new MenuModel(R.drawable.menu2, activity.getString(R.string.cMenu5));
        list.add(model);

        /*model = new MenuModel(R.drawable.menu3, activity.getString(R.string.cMenu3));
        list.add(model);*/
        model = new MenuModel(R.drawable.contact, activity.getString(R.string.cMenu4));
        list.add(model);
        return list;
    }

    public static ArrayList<MenuModel> getContractorMenu(Activity activity) {
        ArrayList<MenuModel> list = new ArrayList<>();
        MenuModel model = new MenuModel(R.drawable.menu4, activity.getString(R.string.coMenu1));
        list.add(model);
        model = new MenuModel(R.drawable.menu2, activity.getString(R.string.coMenu2));
        list.add(model);
        model = new MenuModel(R.drawable.menu5, activity.getString(R.string.coMenu3));
        list.add(model);
       /* model = new MenuModel(R.drawable.menu3, activity.getString(R.string.coMenu4));
        list.add(model);*/
        model = new MenuModel(R.drawable.menu6, activity.getString(R.string.coMenu5));
        list.add(model);
        model = new MenuModel(R.drawable.menu_bank, activity.getString(R.string.coMenu6));
        list.add(model);
        model = new MenuModel(R.drawable.contact, activity.getString(R.string.cMenu4));
        list.add(model);
        return list;
    }

    /**
     * Get Progress Dialog.
     *
     * @param context Application Context
     * @return new Progress Dialog
     */
    public static ProgressDialog initProgress(Context context) {
        ProgressDialog p = new ProgressDialog(context);
        p.setMessage(context.getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        return p;
    }

    /**
     * Dismiss the ProgressBar Dialog from the window.
     *
     * @param progressDialog New Progress Dialog Object
     */
    public static void dismissProgress(ProgressDialog progressDialog) {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveImage(Context context, Bitmap bitmap, Uri uri) {
        File file = new File(uri.getPath());
        FileOutputStream out = null;
        try {
            BitmapFactory.Options bounds = new BitmapFactory.Options();
            bounds.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getAbsolutePath(), bounds);

//            int rotationAngle = getCameraPhotoOrientation(context, uri, file.getAbsolutePath());
            int rotationAngle = 0;

            Matrix matrix = new Matrix();
            matrix.postRotate(rotationAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);


            out = new FileOutputStream(file);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static boolean isForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(Global.PACKAGE);
    }

    /*public static void doCrop(final Activity activity, final Uri fromUri, final int pictureCrop) {
        final ArrayList<CropOption> cropOptions = new ArrayList<>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image*//*");

        List<ResolveInfo> list = activity.getPackageManager().queryIntentActivities(intent, 0);

        for (ResolveInfo info : list) {
            String processName = info.activityInfo.processName;
            if (processName.equals("com.google.android.apps.photos")) {
                list.remove(info);
                break;
            }
        }
        int size = list.size();

        if (size == 0) {
            Toast.makeText(activity, "Can not find image crop app", Toast.LENGTH_LONG).show();

            return;
        } else {
            intent.setData(fromUri);

            intent.putExtra("outputX", 256);
            intent.putExtra("outputY", 256);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                activity.startActivityForResult(i, pictureCrop);
            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = activity.getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = activity.getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(activity, cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        activity.startActivityForResult(cropOptions.get(item).appIntent, pictureCrop);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (fromUri != null) {
                            activity.getContentResolver().delete(fromUri, null, null);
//                            fromUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }*/

    public static void showSimpleAlert(final Context context, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.app_name));
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(android.R.string.ok), onClickListener);
        builder.setCancelable(false);
        AlertDialog dialog = builder.show();
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.black));
    }

    public static void openAppDetailsScreen(final Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivity(intent);
    }

    public static boolean isUriEmpty(Uri uri) {
        return uri == null || Uri.EMPTY.equals(uri);
    }

    public static void doCrop(final Activity activity, final Uri fromUri, final Uri toUri, final int pictureCrop) {
        //String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;

        //destinationFileName += ".png";
        //destinationFileName += ".jpg";

        if (fromUri == null || Uri.EMPTY.equals(fromUri) || toUri == null || Uri.EMPTY.equals(toUri)) {
            Toast.makeText(activity, R.string.image_not_found, Toast.LENGTH_SHORT).show();
        }
        UCrop uCrop = UCrop.of(fromUri, toUri);
        uCrop = uCrop.withAspectRatio(1, 1);
        uCrop = advancedConfig(activity, uCrop);

        uCrop.start(activity, pictureCrop);
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public static void openCameraActivity(Activity activity, ImageListener listener, boolean isLicence) {
        Log.d("openCameraActivity", "" + isLicence);
        File mHighQualityImageUri = Global.generateTimeStampPhotoFile();
        try {
            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            Uri apkURI = FileProvider.getUriForFile(
                    activity,
                    activity.getApplicationContext()
                            .getPackageName() + ".provider", mHighQualityImageUri);
            captureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri);
            if (listener != null) {
                if (isLicence) {
                    //   listener.onImageSelected(mHighQualityImageUri, isLicence);
                    activity.startActivityForResult(captureIntent, Global.CAMERA_CAPTURE_lICENCE);
                } else {
                    // listener.onImageSelected(mHighQualityImageUri, isLicence);
                    activity.startActivityForResult(captureIntent, Global.CAMERA_CAPTURE);
                }
            }
        } catch (ActivityNotFoundException e) {
            if (activity != null) {
                Toast.makeText(activity, R.string.camera_not_found, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * To pick profile image from gallery
     */
    public static void pickImageFromGallery(Activity activity, boolean isLicense) {

        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            activity.startActivityForResult(
                    Intent.createChooser(intent,
                            activity.getString(R.string.add_photo)),
                    isLicense ? Global.GALLERY_PICK_lICENCE : Global.GALLERY_PICK);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            activity.startActivityForResult(intent,
                    isLicense ? Global.GALLERY_PICK_lICENCE : Global.GALLERY_PICK);
        }
    }


    /**
     * To pick image/Video from gallery
     */
    public static void pickVideoFromGallery(Activity activity) {

        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            activity.startActivityForResult(
                    Intent.createChooser(intent,
                            activity.getString(R.string.add_video)), Global.GALLERY_PICK_VIDEO);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("video/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            activity.startActivityForResult(intent, Global.GALLERY_PICK_VIDEO);
        }
    }


    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private static UCrop advancedConfig(final Activity activity, @NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setShowCropGrid(false);
        options.setHideBottomControls(true);
        // Color palette
        options.setToolbarColor(ContextCompat.getColor(activity, R.color.light_blue));
        options.setStatusBarColor(ContextCompat.getColor(activity, R.color.black_trans));
        options.setActiveWidgetColor(ContextCompat.getColor(activity, R.color.rose_Dark));
        options.setToolbarWidgetColor(ContextCompat.getColor(activity, R.color.white));
        options.setRootViewBackgroundColor(ContextCompat.getColor(activity, R.color.black_trans));
        //options.setCompressionQuality(mSeekBarQuality.getProgress());


        //options.setFreeStyleCropEnabled(mCheckBoxFreeStyleCrop.isChecked());

        /*
        If you want to configure how gestures work for all UCropActivity tabs

        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.ROTATE, UCropActivity.ALL);
        * */

        /*
        This sets max size for bitmap that will be decoded from source Uri.
        More size - more memory allocation, default implementation uses screen diagonal.

        options.setMaxBitmapSize(640);
        * */


       /*

        Tune everything (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧

        options.setMaxScaleMultiplier(5);
        options.setImageToCropBoundsAnimDuration(666);
        options.setDimmedLayerColor(Color.CYAN);
        options.setCircleDimmedLayer(true);
        options.setShowCropFrame(false);
        options.setCropGridStrokeWidth(20);
        options.setCropGridColor(Color.GREEN);
        options.setCropGridColumnCount(2);
        options.setCropGridRowCount(1);
        options.setToolbarCropDrawable(R.drawable.your_crop_icon);
        options.setToolbarCancelDrawable(R.drawable.your_cancel_icon);

        // Color palette
        options.setToolbarColor(ContextCompat.getColor(this, R.color.your_color_res));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.your_color_res));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.your_color_res));
        options.setToolbarWidgetColor(ContextCompat.getColor(this, R.color.your_color_res));
        options.setRootViewBackgroundColor(ContextCompat.getColor(this, R.color.your_color_res));

        // Aspect ratio options
        options.setAspectRatioOptions(1,
            new AspectRatio("WOW", 1, 2),
            new AspectRatio("MUCH", 3, 4),
            new AspectRatio("RATIO", CropImageView.DEFAULT_ASPECT_RATIO, CropImageView.DEFAULT_ASPECT_RATIO),
            new AspectRatio("SO", 16, 9),
            new AspectRatio("ASPECT", 1, 1));

       */

        return uCrop.withOptions(options);
    }


    // Get Date from String
    public static Date getDate(String time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static Date getDate(String time, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    //GEt Date as String in Different Formats
    public static String getDate(String dateTime, String fromPattern, String toPattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.applyPattern(fromPattern);
        try {
            Date date = dateFormat.parse(dateTime);
            dateFormat.setTimeZone(TimeZone.getDefault());
            dateFormat.applyPattern(toPattern);
            return dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    //Get Date as String in given pattern by Providing date
    public static String getDate(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.applyPattern(pattern);
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateNotUTC(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(pattern);
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //Perform Call
    public static void call(Context context, String number) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT) {
            Toast.makeText(context, "Insert SIM", Toast.LENGTH_LONG).show();
            return;
        }
        if (telephonyManager.getNetworkType() == TelephonyManager.NETWORK_TYPE_UNKNOWN) {
            Toast.makeText(context, "No Network", Toast.LENGTH_LONG).show();
            return;
        }
        try {
            if (number != null && !number.trim().equals("")) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(callIntent);
            } else {
                Toast.makeText(context, "Not valid number", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.d("Call :", e.getMessage());
        }
    }

    //Send Email
    public static void sendEmail(Context context, String email) {
        Log.i("Send email", "");
        String[] TO = {email};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
//        emailIntent.putExtra(Intent.EXTRA_CC, CC);
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
//        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finished sending email", "");
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_LONG).show();
        }
    }

    //Load Bitmap Efficiently
    public static Bitmap decodeFromResource(Resources res, int resId,
                                            int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeResource(res, resId, options);
        int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
        return scaled;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


    public static void setBackground(ImageView imgBack, Resources res, int drawable) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeResource(res, drawable, options);
        imgBack.setImageBitmap(bitmap);
    }
}

