package com.oostajob.payment.internal;


import com.oostajob.model.GetClientTokenWithCustomerID;
import com.oostajob.payment.models.ClientToken;
import com.oostajob.payment.models.Transaction;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface ApiClient {

    @POST("/api/index.php/getclientaccesstoken")
    Call<ClientToken> getClientToken(@Body GetClientTokenWithCustomerID getClientTokenWithCustomerID);
//    @GET("/client_token")
//    void getClientToken(@Query("customer_id") String customerId, @Query("merchant_account_id") String merchantAccountId, Callback<ClientToken> callback);

    @FormUrlEncoded
    @POST("/nonce/transaction")
    Call<Transaction> createTransaction(@Field("nonce") String nonce);

    @FormUrlEncoded
    @POST("/nonce/transaction")
    Call<Transaction> createTransaction(@Field("nonce") String nonce, @Field("merchant_account_id") String merchantAccountId);

    @FormUrlEncoded
    @POST("/nonce/transaction")
    Call<Transaction> createTransaction(@Field("nonce") String nonce, @Field("merchant_account_id") String merchantAccountId, @Field("three_d_secure_required") boolean requireThreeDSecure);
}
