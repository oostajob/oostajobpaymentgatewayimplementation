package com.oostajob.payment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientToken {

    @Expose
    public String result;
    //    @SerializedName("client_token")
    @SerializedName("clientToken")
    private String mClientToken;

    public String getClientToken() {
        return mClientToken;
    }

    public String getResult() {
        return result;
    }
}
