package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.Green.customfont.CustomRadioButton;
import com.Green.customfont.CustomTextView;
import com.oostajob.adapter.customer.ConHireAdapter;
import com.oostajob.adapter.customer.ConHireAdapter.HireListener;
import com.oostajob.adapter.customer.CusMessagesAdapter;
import com.oostajob.dialog.CancelOrSkipDia;
import com.oostajob.dialog.CancelOrSkipDia.SuccessListener;
import com.oostajob.dialog.CusCalendarDialog;
import com.oostajob.global.Global;
import com.oostajob.model.DiscussionlistModel;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobDiscussionReplyModel;
import com.oostajob.model.Result;
import com.oostajob.utils.BlurTransformation;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.oostajob.adapter.customer.CusMessagesAdapter.ReplyListener;

public class CusHireActivity extends AppCompatActivity implements ReplyListener, HireListener, SuccessListener {

    private String USER_ID;
    private ProgressDialog p;
    private Toolbar toolbar;
    private ImageView imageView, imgPlay, imgClose, imgMenuArrow, imgIcon, imgBack, imgEC;
    private FrameLayout videoLay;
    private VideoView videoView;
    private HorizontalScrollView scrollView;
    private LinearLayout scrollContent, menuLay, messageLay, contractLay;
    private CustomTextView txtCloses, txtMenuItem, txtTitle, txtConCount, txtZipCode, txtTime, txtDescription, txtMessage, txtContract;
    private ChipView chipView;
    private SlidingUpPanelLayout panelLayout;
    private ListView messageView;
    private CustomRadioButton radioMessage, radioContract;
    private APIService apiService;
    private GetCustDetailsModel.Details details;
    private MediaController mediaController;
    private CusMessagesAdapter adapter;
    private ConHireAdapter hireAdapter;
    private boolean showFirst = true;
    private ArrayList<DiscussionlistModel.DiscustionDetails> messageList;
    private ArrayList<GetCustDetailsModel.Contractorlist> hireList;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus_hire_detail);


        init();
        setUpToolbar();
        setListener();
        loadValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.imageView);
        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        imgMenuArrow = (ImageView) findViewById(R.id.imgMenuArrow);
        imgIcon = (ImageView) findViewById(R.id.imgIcon);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        videoLay = (FrameLayout) findViewById(R.id.videoLay);
        videoView = (VideoView) findViewById(R.id.videoView);
        scrollView = (HorizontalScrollView) findViewById(R.id.scrollView);
        scrollContent = (LinearLayout) findViewById(R.id.scrollContent);
        menuLay = (LinearLayout) findViewById(R.id.menuLay);
        txtMenuItem = (CustomTextView) findViewById(R.id.txtMenuItem);
        txtTitle = (CustomTextView) findViewById(R.id.txtTitle);
        txtConCount = (CustomTextView) findViewById(R.id.txtConCount);
        txtZipCode = (CustomTextView) findViewById(R.id.txtZipCode);
        txtTime = (CustomTextView) findViewById(R.id.txtTime);
        txtCloses = (CustomTextView) findViewById(R.id.txtCloses);
        txtDescription = (CustomTextView) findViewById(R.id.txtDescription);
        txtMessage = (CustomTextView) findViewById(R.id.txtMessage);
        messageView = (ListView) findViewById(R.id.messageView);
        chipView = (ChipView) findViewById(R.id.chipView);
        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.panelLayout);
        imgEC = (ImageView) findViewById(R.id.imgEC);
        messageLay = (LinearLayout) findViewById(R.id.messageLay);
        contractLay = (LinearLayout) findViewById(R.id.contractLay);
        radioMessage = (CustomRadioButton) findViewById(R.id.radioMessage);
        radioContract = (CustomRadioButton) findViewById(R.id.radioContract);
        txtContract = (CustomTextView) findViewById(R.id.txtContract);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        txtCloses.setVisibility(View.GONE);
        panelLayout.setScrollableView(messageView);
//        panelLayout.setTouchEnabled(false);

        p = new ProgressDialog(CusHireActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);

        mediaController = new MediaController(CusHireActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        menuLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(CusHireActivity.this, v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_cancel:
                                CancelOrSkipDia dialog = new CancelOrSkipDia();
                                dialog.setSuccessListener(CusHireActivity.this);

                                Bundle bundle = new Bundle();
                                bundle.putSerializable("jobPostID", details.getJobpostID());
                                bundle.putString("title", "cancel");
                                bundle.putBoolean("isCustomer", true);
                                dialog.setArguments(bundle);
                                dialog.show(getSupportFragmentManager(), "Cancel Dialog");
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.inflate(R.menu.actions);
                popup.show();
            }
        });
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPlay.setVisibility(View.GONE);
                videoView.start();
                scrollView.setVisibility(View.GONE);
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CusHireActivity.this, "Cannot load video", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                setMediaVisibility(View.GONE, View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        panelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {
                imgEC.setImageResource(R.drawable.up);
            }

            @Override
            public void onPanelExpanded(View panel) {
                imgEC.setImageResource(R.drawable.down);
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });
        imgEC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                else if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });
        messageLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioMessage.setChecked(true);
            }
        });
        contractLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioContract.setChecked(true);
            }
        });
        radioMessage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setMessageAdapter();
                }
            }
        });
        radioContract.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setContractorAdapter();
                }
            }
        });
        panelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {

            }

            @Override
            public void onPanelExpanded(View panel) {
                readMessages();
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });
    }

    private void readMessages() {
        p.show();
        Call<Result> call = apiService.readMessages(details.getJobpostID(), USER_ID);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    String res = response.body().getResult();
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });

    }


    private void setMessageAdapter() {
        if (messageList != null) {
            adapter = new CusMessagesAdapter(CusHireActivity.this, messageList, false);
            adapter.setOnReplyListener(CusHireActivity.this);
            messageView.setAdapter(adapter);
        } else {
            messageView.setAdapter(adapter);
        }
    }

    private void setContractorAdapter() {
        if (hireList != null) {
            hireAdapter = new ConHireAdapter(CusHireActivity.this, hireList);
            hireAdapter.setHireListener(this);
            messageView.setAdapter(hireAdapter);
        } else {
            messageView.setAdapter(adapter);
        }
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(CusHireActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void loadValues() {
        try {
            details = (GetCustDetailsModel.Details) getIntent().getExtras().getSerializable("details");
            assert details != null;
            // Load Background Image
            String IMG_URL = String.format("%s/%s", Global.BASE_URL, details.getJob_image());
            Picasso.with(this).load(IMG_URL).transform(new BlurTransformation(this, 25)).fit().into(imgBack);

            //Menu
            txtMenuItem.setText("Hire");

            //load Files
            loadFiles(details.getJobmedialist());

            toolbar.setTitle(details.getJobtypeName());
            //title
            String ICON_URL = String.format("%s/%s", Global.BASE_URL, details.getJobicon());
            Picasso.with(this).load(ICON_URL).fit().into(imgIcon);
            txtTitle.setText(details.getJobtypeName());

            //Contractor Count
            txtConCount.setText(details.getContractorcnt());

            //Zipcode
            txtZipCode.setText(details.getZipcode());

            //Remaining Hours
            Date date = Global.getDate(details.getJobpostTime());
            long difference = System.currentTimeMillis() - date.getTime();
            long hours = TimeUnit.MILLISECONDS.toHours(difference);
            if (hours == 0) {
                long minu = TimeUnit.MILLISECONDS.toMinutes(difference);
                if (minu > 1)
                    txtTime.setText(String.format("%d minutes ago", minu));
                else txtTime.setText(String.format("%d minute ago", minu));
            } else if (hours >= 24) {
                long days = TimeUnit.MILLISECONDS.toDays(difference);
                if (days > 1)
                    txtTime.setText(String.format("%d days ago", days));
                else txtTime.setText(String.format("%d day ago", days));
            } else if (hours < 24) {
                if (hours > 1)
                    txtTime.setText(String.format("%d hours ago", hours));
                else txtTime.setText(String.format("%d hour ago", hours));
            }

            //Tags
            setTags(details);

            //Description
            StringBuilder builder = new StringBuilder();
            builder.append(details.getDescription()).append("\n").append("\n");
            for (GetCustDetailsModel.JobAnwserlist answer : details.getJobanwserlist()) {
                if (answer.getTag_key().equalsIgnoreCase("2")) {
                    builder.append(answer.getQuest_name()).append("\n").append(answer.getAnswer()).append("\n");
                }
            }
            txtDescription.setText(builder.toString());

            loadMessages(details.getJobpostID());

            hireList = details.getContractorlist();
            txtContract.setText(String.format("Contractors(%s)", hireList.size()));

            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTags(GetCustDetailsModel.Details details) {
        List<Chip> chipList = new ArrayList<>();
        for (GetCustDetailsModel.JobAnwserlist answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        chipView.setLineSpacing(10);
        chipView.setChipSpacing(10);
        chipView.setChipLayoutRes(R.layout.chip_lay);
        chipView.setChipBackgroundRes(R.drawable.tag);
        chipView.setChipList(chipList);
    }

    private void loadFiles(ArrayList<GetCustDetailsModel.Jobmedialist> fileList) {

        scrollContent.removeAllViews();
        final LayoutInflater inflater = LayoutInflater.from(this);

        for (final GetCustDetailsModel.Jobmedialist model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.image_view, scrollContent, false);
            ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            if (model.getMediaType().equals("1")) {
                Picasso.with(this).load(url).fit().into(imgChosen);
            } else {
                Picasso.with(this).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (model.getMediaType().equals("1")) {
                        setMediaVisibility(View.VISIBLE, View.GONE);
                        Picasso.with(CusHireActivity.this).load(url).into(imageView);
                    } else {
                        setMediaVisibility(View.GONE, View.VISIBLE);
                        videoView.setVideoPath(url);
                        videoView.requestFocus();
                    }

                }
            });
            if (showFirst) {
                imgChosen.performClick();
                showFirst = false;
            }
            scrollContent.addView(view);
        }
    }

    private void setMediaVisibility(int arg0, int arg1) {
        imageView.setVisibility(arg0);
        videoLay.setVisibility(arg1);
    }

    private void loadMessages(String jobPostId) {
        p.show();
        Call<DiscussionlistModel.Response> call = apiService.discussionList(jobPostId);
        call.enqueue(new Callback<DiscussionlistModel.Response>() {
            @Override
            public void onResponse(Call<DiscussionlistModel.Response> call, Response<DiscussionlistModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    DiscussionlistModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        messageList = response1.getDiscustionDetails();
                        txtMessage.setText(String.format("Messages(%s)", details.getQuestion_mesage()));
                    }
                    radioContract.setChecked(true);
                }
            }

            @Override
            public void onFailure(Call<DiscussionlistModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void sendReply(DiscussionlistModel.DiscustionDetails details, String answer) {
        p.show();
        final String jobPostId = this.details.getJobpostID();
        final JobDiscussionReplyModel.Request request = new JobDiscussionReplyModel.Request(details.getDiscussionID(),
                USER_ID, jobPostId, answer);
        Call<DiscussionlistModel.Response> call = apiService.replyDiscussion(request);
        call.enqueue(new Callback<DiscussionlistModel.Response>() {
            @Override
            public void onResponse(Call<DiscussionlistModel.Response> call, Response<DiscussionlistModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    DiscussionlistModel.Response response1 = response.body();
                    if (response.body().getResult().equalsIgnoreCase("Success")) {
                        loadMessages(jobPostId);
                    }
                }

            }

            @Override
            public void onFailure(Call<DiscussionlistModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onHireClick(GetCustDetailsModel.Contractorlist item) {
        Bundle bundle = new Bundle();
        bundle.putString("postId", details.getJobpostID());
        bundle.putString("userId", item.getUserID());
        bundle.putInt("bidAmount", Integer.parseInt(item.bidAmount));
      //  bundle.putString("contractor_id",item.userID);
        CusCalendarDialog dialog = new CusCalendarDialog();
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "Hire Dialog");
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, ConInfoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("details", details);
        intent.putExtra("position", position);
        startActivity(intent);
    }

    @Override
    public void onSuccess() {
        Intent intent = new Intent(CusHireActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("fragPosition", 1);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
