package com.oostajob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.Green.customfont.CustomButton;
import com.oostajob.global.Global;
import com.oostajob.utils.PrefConnect;

public class SelectionActivity extends AppCompatActivity {

    private CustomButton btnService, btnContractor;
    private ImageView imgBack;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        init();
        setUpToolBar();
        setListener();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnService = (CustomButton) findViewById(R.id.btnService);
        btnContractor = (CustomButton) findViewById(R.id.btnContractor);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setListener() {
        btnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectionActivity.this, CusRegActivity.class);
                if (PrefConnect.readString(SelectionActivity.this, PrefConnect.SKIP, "").equalsIgnoreCase("skip_login")) {
                    /*intent.putExtra("subTitle", getIntent().getStringExtra("subTitle"));
                    intent.putExtra("imgUrl", getIntent().getStringExtra("imgUrl"));
                    intent.putExtra("jobTypeId", getIntent().getStringExtra("jobTypeId"));*/
                    intent.putExtra("list", getIntent().getSerializableExtra("list"));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnContractor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectionActivity.this, ConRegActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
