package com.oostajob.callbacks;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Admin on 09-10-2015.
 */
public class PlacesTextWatcher implements TextWatcher {
    TextWatcher textWatcher;

    public PlacesTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (textWatcher != null) {
            textWatcher.afterTextChanged(s);
        }
    }

    public interface TextWatcher {
        public void afterTextChanged(Editable s);
    }
}
