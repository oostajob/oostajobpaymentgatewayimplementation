package com.oostajob.callbacks;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;

/**
 * Created by Admin on 09-10-2015.
 */
public class PlacesResultCallback implements ResultCallback<PlaceBuffer> {

    ResultCallback callback;

    public PlacesResultCallback(ResultCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onResult(PlaceBuffer places) {
        if (callback != null) {
            callback.onResult(places);
        }
    }

    public interface ResultCallback {
        public void onResult(PlaceBuffer places);
    }
}
