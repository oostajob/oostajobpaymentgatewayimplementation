package com.oostajob.callbacks;

import android.net.Uri;

/**
 * Created on 07-12-2017.
 */

public interface ImageListener {
    void onImageSelected(Uri uri, boolean isLicense);
}
