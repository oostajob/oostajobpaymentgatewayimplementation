package com.oostajob.webservice;

import com.oostajob.model.AddNewFile;
import com.oostajob.model.AddRatingModel;
import com.oostajob.model.BidAmountModel;
import com.oostajob.model.BidCancelModel;
import com.oostajob.model.CalanderListModel;
import com.oostajob.model.CancelJobByCustomer;
import com.oostajob.model.ChangePasswordModel;
import com.oostajob.model.CityDetailsModel;
import com.oostajob.model.ComplaintsModel;
import com.oostajob.model.ConBankDetails;
import com.oostajob.model.ConBankInputModel;
import com.oostajob.model.ConComplaintsModel;
import com.oostajob.model.ConEarningsResponse;
import com.oostajob.model.ConJobListModel;
import com.oostajob.model.ConPaymentBillingHistoryResponse;
import com.oostajob.model.ConPaymentHistoryJobResponse;
import com.oostajob.model.ConRatingModel;
import com.oostajob.model.ConRegModel;
import com.oostajob.model.ConReviewModel;
import com.oostajob.model.ConUpdateModel;
import com.oostajob.model.ContactUs;
import com.oostajob.model.ContractorJobList;
import com.oostajob.model.ContractorJobListModel;
import com.oostajob.model.CusRegModel;
import com.oostajob.model.CustomerPaymentJobHistory;
import com.oostajob.model.CustomerUpdateModel;
import com.oostajob.model.DiscussionlistModel;
import com.oostajob.model.ForgotPasswordModel;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.GetJobHiredModel;
import com.oostajob.model.InboxModel;
import com.oostajob.model.JobCompleteByConSideResponse;
import com.oostajob.model.JobCompletedByConInput;
import com.oostajob.model.JobDiscussionModel;
import com.oostajob.model.JobDiscussionReplyModel;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.JobSubTypeModel;
import com.oostajob.model.JobTypeModel;
import com.oostajob.model.LogInModel;
import com.oostajob.model.LogoutModel;
import com.oostajob.model.MakeSubscriptionModel;
import com.oostajob.model.Nonce;
import com.oostajob.model.NotifyAddress;
import com.oostajob.model.PayableAmountInput;
import com.oostajob.model.PayableAmountResponse;
import com.oostajob.model.PaymentListModel;
import com.oostajob.model.PaymentTransactionInputResponse;
import com.oostajob.model.PaymentTransactionResults;
import com.oostajob.model.PostJobModel;
import com.oostajob.model.ResendEmailModel;
import com.oostajob.model.Result;
import com.oostajob.model.TimeZone;
import com.oostajob.model.UpdateCalenderModel;
import com.oostajob.model.Urls;
import com.oostajob.model.VerificationProcessModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface APIService {

    @GET("/api/index.php/urllinks")
    Call<Urls> getUrls();

    //https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331766000&language=es&key=
    @GET("/maps/api/timezone/json")
    Call<TimeZone> getTimeZone(@Query("location") String location, @Query("timestamp") long timestamp,@Query("key") String key);

    //Customer Profile URl
//    @Multipart
//    @POST("/api/index.php/profileimg")
//    Call<> cusProfileUpload(@Part("profile") TypedFile file, Callback<Upload> response);

    //Customer Registration
    //@POST("/api/index.php/customerregister")
    @POST("/api/index.php/andriod_customerregister")
    Call<CusRegModel.Response> cusRegister(@Body CusRegModel.Request request);

    //CUstomer Update
    @PUT("/api/index.php/customerupdate")
    Call<CustomerUpdateModel.Response> cusUpdate(@Body CustomerUpdateModel.Request request);


    //Contractor  Registration
    //@POST("//api/index.php/contractorregister")
    @POST("/api/index.php/andriod_contractorregister")
    Call<ConRegModel.Response> conRegister(@Body ConRegModel.Request request);

//    //contractor profile img and licence img upload
//    @Multipart
//    @POST("/api/index.php/license")
//    Call<> conProfileUpload(@Part("profile") TypedFile file, @Part("license") TypedFile lic_file, Callback<Upload> response);

    //Contractor Update
    @PUT("/api/index.php/contractorupdate")
    Call<ConUpdateModel.Response> conUpdate(@Body ConUpdateModel.Request request);

    //Customer and contractor login
    @POST("/api/index.php/login")
    Call<LogInModel.Response> logIn(@Body LogInModel.Request request);

    //Logout
    @GET("/api/index.php/userlogout/{userId}/1")
    Call<LogoutModel> logout(@Path("userId") String userId);

    //forgot password
    @POST("/api/index.php/forgetpassword")
    Call<ForgotPasswordModel.Response> forgotPassword(@Body ForgotPasswordModel.Request request);

    //Resend activation mail /api/index.php/resendmail/6(userid)
    @GET("/api/index.php/resendmail/{userID}")
    Call<ResendEmailModel.Response> resendMail(@Path("userID") String userID);

    //register exptertise at
    @GET("/api/index.php/getjobtype")
    Call<JobTypeModel> getExpertiseAt();

    //contractor status
    @GET("/api/index.php/verificationstatus/{userID}")
    Call<VerificationProcessModel.Response> verifyMail(@Path("userID") String userID);

    // send bid amount
    @POST("/api/index.php/jobbidding")
    Call<BidAmountModel.Response> bidJob(@Body BidAmountModel.Request request);


    //get city details
    @GET("api/index.php/city")
    Call<CityDetailsModel.Response> getCityDetails();

    //get jobsubtype details
    @GET("/api/index.php/getsubjobtype/{jobtype_id}")
    Call<JobSubTypeModel.Response> getJobSubType(@Path("jobtype_id") String jobtype_id);

    //get jobsubtype Char details
    @GET("/api/index.php/getjobsubtypechar/{subTypeId}")
    Call<JobSubTypeCharModel.Response> getJobSubTypeChar(@Path("subTypeId") String subTypeId);

    //post job
    @POST("/api/index.php/postjob")
    Call<PostJobModel.Response> postJob(@Body PostJobModel.Request request);

    //contractor job list model
    @POST("/api/index.php/getcontractorjobs/{userid}")
    Call<ConJobListModel.Response> conJobList(@Path("userid") String userid, @Body ConJobListModel.Request request);

    //job bidding cancel
    @GET("/api/index.php/canceljob/{userid}/{jobid}")
    Call<BidCancelModel.Response> bidCancel(@Path("userid") String userid, @Path("jobid") String jobid);

    //JOB DISCUSSION LIST
    @GET("/api/index.php/getforumlist/{jobid}")
    Call<DiscussionlistModel.Response> discussionList(@Path("jobid") String jobId);

    //JOB Discussion reply
    @POST("/api/index.php/replydiscusstion")
    Call<DiscussionlistModel.Response> replyDiscussion(@Body JobDiscussionReplyModel.Request request);

    //add rating
    @POST("/api/index.php/addrating")
    Call<AddRatingModel.Response> addRating(@Body AddRatingModel.Request request);


    //Get customer Job List
    @GET("/api/index.php/getcustomerjoblist/{customerID}")
    Call<GetCustDetailsModel.Response> getCustomerJobDetails(@Path("customerID") String customerID);

    @GET("/api/index.php/getcontractorjoblist/{id}")
    Call<ContractorJobList> getContractorJobList(@Path("id") String id);


    // post complaints
    @POST("/api/index.php/addcomplients")
    Call<ComplaintsModel.Response> postComplaints(@Body ComplaintsModel.Request request);

    //get Job List for Contractor
    @POST("/api/index.php/getcontractorjobs/{userID}")
    Call<ContractorJobListModel.Response> jobListContractor(@Path("userID") String userID, @Body ContractorJobListModel.Request request);

    // job discussion
    @POST("/api/index.php/postdiscussion")
    Call<JobDiscussionModel.Response> postDiscussion(@Body JobDiscussionModel.Request request);

    //get contractor rating
    @GET("/api/index.php/getrating/{contractorid}")
    Call<ConReviewModel.Response> getReviews(@Path("contractorid") String contractorid);

    //get contractor rating for Particular jobtype
    @GET("/api/index.php/getjobrating/{userID}/{jobID}")
    Call<ConRatingModel.Response> conRatParticularJT(@Path("userID") String userID, @Path("jobID") String jobID);

    //get Contrator Complaints
    @GET("/api/index.php/getcomplients/{userID}")
    Call<ConComplaintsModel.Response> conComplaints(@Path("userID") String userID);

    //MAKE SUBSCRIPTION
    @POST("/api/index.php/paymentconfigre")
    Call<MakeSubscriptionModel.Response> makeSubscription(@Body MakeSubscriptionModel.Request request);

    //GET PAYMENT LIST
    @GET("/api/index.php/getpayment/{contractorid}")
    Call<PaymentListModel.Response> getPaymentList(@Path("contractorid") String contractorid);

    // updatate calender
    @POST("/api/index.php/updatecalander/{id}")
    Call<UpdateCalenderModel.Response> updateCalender(@Path("id") String id, @Body UpdateCalenderModel.Request request);

    //get job hired
    @POST("/api/index.php/jobhired")
    Call<GetJobHiredModel.Response> hireContractor(@Body GetJobHiredModel.Request request);

    //get calander list
    @GET("/api/index.php/getcalender/{contractorID}")
    Call<CalanderListModel.Response> getCalendar(@Path("contractorID") String contractorID);

    @POST("/api/index.php/changepassword ")
    Call<ChangePasswordModel.Response> changePassword(@Body ChangePasswordModel.Request request);

    @GET("/api/index.php/getinboxlist/{id}")
    Call<InboxModel> getInbox(@Path("id") String id);

    @GET("/api/index.php/removeInbox/{id}")
    Call<Result> removeInbox(@Path("id") String id);

    @POST("/api/index.php/contactusinsert")
    Call<ContactUs.Response> contactus(@Body ContactUs.Request request);


    @GET("/api/index.php/skipcntractor/{userId}/{jobPostId}")
    Call<Result> skipJob(@Path("userId") String userId, @Path("jobPostId") String jobPostId);

    @GET("/api/index.php/cancelcustomer/{userId}/{jobPostId}")
    Call<Result> cancelJob(@Path("userId") String userId, @Path("jobPostId") String jobPostId);

    @POST("/api/index.php/getemailzipcode")
    Call<NotifyAddress.Result> notifyService(@Body NotifyAddress request);

    @POST("/api/index.php/checkout")
    Call<Nonce.NonceResult> sendNonce(@Body Nonce nonce);

    @POST("/api/index.php/AddImageVideoPostjob")
    Call<AddNewFile.Result> addFilesToJob(@Body AddNewFile file);

    //http://52.35.35.214//api/index.php/getnotificationCunt/385/149
    @GET("/api/index.php/getnotificationCunt/{josbpost_id}/{user_id}")
    Call<Result> readMessages(@Path("josbpost_id") String josbpost_id, @Path("user_id") String user_id);


    @POST("/api/index.php/addBankDetails")
    Call<ConRegModel.ContractorBankDetailsResponse> changeBankDetails(@Body ConBankInputModel contractorBankDetailsInput);


    @POST("/payableAmount")
    Call<PayableAmountResponse> payableAmount(@Body PayableAmountInput payableAmountInput);

    @POST("/payAmount")
    Call<PaymentTransactionResults> transctionStatus(@Body PaymentTransactionInputResponse paymentTransactionInputResponse);

    @POST("/api/index.php/makeJobCompleted")
    Call<JobCompleteByConSideResponse> jobcompletedByCon(@Body JobCompletedByConInput JobCompletedByConInput);

    @POST("/api/index.php/cancelAJob")
    Call<JobCompleteByConSideResponse> jobCancelByCustomer(@Body CancelJobByCustomer cancelJobByCustomer);

   /* @POST("api/index.php/myEarnings")
    Call<ConEarningsResponse> conEarnings(@Body ConEarningsInput conEarningsInput);*/


    @GET("api/index.php/myPaymentHistory/{id}")
    Call<CustomerPaymentJobHistory> getCustomerJobHistory(@Path("id") String id);

    @GET("api/index.php/contractorEarnings/{id}")
    Call<ConEarningsResponse> getContractorEarnings(@Path("id") String id);

    //http://52.41.134.73/api/index.php/contractorPaymentHistory/887

    @GET("api/index.php/contractorPaymentHistory/{id}")
    Call<ConPaymentBillingHistoryResponse> getConJobHistory(@Path("id") String id);

    @GET("api/index.php/getContractorBankDetails/{id}")
    Call<ConBankDetails> getBankDetailsForContractor(@Path("id") String id);


}
