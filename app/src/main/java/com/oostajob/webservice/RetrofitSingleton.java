package com.oostajob.webservice;


import com.oostajob.global.Global;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSingleton {

    public static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Global.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder builderGoogle =
            new Retrofit.Builder()
                    .baseUrl(Global.GOOGLE_API_URL)
                    .addConverterFactory(GsonConverterFactory.create());


    public static <T> T createService(Class<T> serviceClass) {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        httpClient.retryOnConnectionFailure(true);
        httpClient.connectTimeout(5, TimeUnit.MINUTES);
        httpClient.readTimeout(5, TimeUnit.MINUTES);

        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createGoogleService(Class<S> serviceClass) {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        httpClient.retryOnConnectionFailure(true);
        httpClient.connectTimeout(1, TimeUnit.MINUTES);
        httpClient.readTimeout(2, TimeUnit.MINUTES);

        Retrofit retrofit = builderGoogle.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}
