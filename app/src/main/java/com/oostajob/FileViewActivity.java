package com.oostajob;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.oostajob.global.Global;
import com.squareup.picasso.Picasso;

import java.io.File;

public class FileViewActivity extends AppCompatActivity implements MediaPlayer.OnBufferingUpdateListener {

    private String FILE_TYPE = "", FILE_PATH = "";
    private Toolbar toolbar;
    private ImageView image;
    private VideoView video;
    private ImageView imgPlay;
    private FrameLayout vidLay;
    private ProgressBar progressBar;
    private MediaController mediaController;
    private MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_view);


        FILE_TYPE = getIntent().getStringExtra("fileType");
        FILE_PATH = getIntent().getStringExtra("filePath");
        init();
        setUpToolBar();
        setListener();
        loadValues();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        image = (ImageView) findViewById(R.id.image);
        video = (VideoView) findViewById(R.id.video);
        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        vidLay = (FrameLayout) findViewById(R.id.vidLay);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mediaController = new MediaController(this);
        mediaController.setAnchorView(video);
        video.setMediaController(mediaController);
    }


    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPlay.setVisibility(View.GONE);
                video.start();
            }
        });
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                player = mp;
                player.setOnBufferingUpdateListener(FileViewActivity.this);
                progressBar.setVisibility(View.GONE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(FileViewActivity.this, "Cannot load video", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    private void loadValues() {
        if (FILE_TYPE.equals("1")) {
            setVisibility(View.VISIBLE, View.GONE);
            if (FILE_PATH.startsWith("http")) {
                Picasso.with(this).load(FILE_PATH).into(image);
            } else {
                File file = new File(FILE_PATH);
                Picasso.with(this).load(file).into(image);
            }

        } else {
            setVisibility(View.GONE, View.VISIBLE);
            video.setVideoPath(FILE_PATH);
        }
    }

    private void setVisibility(int arg0, int arg1) {
        image.setVisibility(arg0);
        vidLay.setVisibility(arg1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (video.isPlaying()) {
            video.pause();
            video.stopPlayback();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
       /* if (percent != 100) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
*/
    }
}
