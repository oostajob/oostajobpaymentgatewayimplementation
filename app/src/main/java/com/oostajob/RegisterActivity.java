package com.oostajob;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.oostajob.fragment.AboutFragment;
import com.oostajob.fragment.ContactFragment;
import com.oostajob.fragment.FAQFragment;
import com.oostajob.fragment.StoriesFragment;
import com.oostajob.fragment.WorksFragment;
import com.oostajob.global.Global;
import com.oostajob.model.Urls;
import com.oostajob.navigationdrawer.DrawRegisterFrag;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.oostajob.global.Global.SENDER_ID;


public class RegisterActivity extends AppCompatActivity implements DrawRegisterFrag.DrawRegisterFragCallbacks {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private final static int PERMISSION_REQUEST = 8888;
    private static final String TAG = "RegisterActivity";
    private APIService apiService;
    private Toolbar toolbar;
    private ImageView skipLogin;
    private DrawRegisterFrag drawerFragment;
    private String mTitle;
    private ProgressDialog p;

    GoogleCloudMessaging gcm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();
        setListener();
        setUrls();

        if (checkPlayServices()) {
            String regId = getRegistrationId(getApplicationContext());
            if (regId.equals("")) {
                //registerInBackGround();
                RegisterGCMAccount();
            } else {
                Log.d("Already Registered", regId);
            }
        } else {
            Log.d("Play service Error:",
                    "No valid Google Play Services APK found");
        }
        drawerFragment = (DrawRegisterFrag)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        setSupportActionBar(toolbar);
        int fragPosition = getIntent().getIntExtra("fragPosition", 0);
        drawerFragment.initDrawer((DrawerLayout) findViewById(R.id.drawerLayout), toolbar, fragPosition);

        checkPermission();
    }

    private void init() {
        apiService = RetrofitSingleton.createService(APIService.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        skipLogin = (ImageView) findViewById(R.id.skipLogin);

        p = Global.initProgress(this);
    }

    private void setListener() {
        skipLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        if (!drawerFragment.isDrawerOpen()) {
            onRestoreTitle();
        }

        return true;
    }

    private void onRestoreTitle() {
        toolbar.setTitle(mTitle);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        WorksFragment fragment = new WorksFragment();
        Bundle bundle = new Bundle();
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position) {
            case DrawRegisterFrag.ABOUT:
                fragmentManager.beginTransaction().replace(R.id.container, new AboutFragment()).commit();
                setSkipLoginVisibility(View.GONE);
                break;

            case DrawRegisterFrag.CONTACT:
                fragmentManager.beginTransaction().replace(R.id.container, new ContactFragment()).commit();
                setSkipLoginVisibility(View.GONE);
                break;

            case DrawRegisterFrag.STORIES:
                fragmentManager.beginTransaction().replace(R.id.container, new StoriesFragment()).commit();
                setSkipLoginVisibility(View.GONE);
                break;

            case DrawRegisterFrag.FAQ:
                fragmentManager.beginTransaction().replace(R.id.container, new FAQFragment()).commit();
                setSkipLoginVisibility(View.GONE);
                break;

            case DrawRegisterFrag.CUSTOMER:
                bundle.putInt("position", 1);
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                setSkipLoginVisibility(View.VISIBLE);
                break;

            case DrawRegisterFrag.CONTRACTOR:
                bundle.putInt("position", 2);
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                setSkipLoginVisibility(View.VISIBLE);
                break;

            case DrawRegisterFrag.GET_STARTED:
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                setSkipLoginVisibility(View.VISIBLE);
                break;

        }
    }

    public void setSkipLoginVisibility(int visibility) {
        skipLogin.setVisibility(visibility);
    }

    public void onAttached(int position) {
        switch (position) {
            case DrawRegisterFrag.ABOUT:
                mTitle = "About us";
                break;
            case DrawRegisterFrag.CONTACT:
                mTitle = "Contact us";
                break;
            case DrawRegisterFrag.STORIES:
                mTitle = "Customer Stories";
                break;
            case DrawRegisterFrag.FAQ:
                mTitle = "FAQ";
                break;
            case DrawRegisterFrag.CUSTOMER:
                mTitle = "";
                break;
            case DrawRegisterFrag.CONTRACTOR:
                mTitle = "";
                break;
            case DrawRegisterFrag.GET_STARTED:
                mTitle = "";
                break;
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                // Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        String registrationId = PrefConnect.readString(RegisterActivity.this, PrefConnect.GCM_TOKEN, "");
        if (registrationId.equals("")) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = PrefConnect.readInteger(RegisterActivity.this, PrefConnect.APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackGround() {
        new AsyncTask<String, Void, String>() {

            String token;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                p.show();
            }

            @Override
            protected String doInBackground(String... params) {
                InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                try {
                    token = instanceID.getToken(SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("Token :", token);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Global.dismissProgress(p);
                if (!TextUtils.isEmpty(token)) {
                    PrefConnect.writeString(RegisterActivity.this, PrefConnect.GCM_TOKEN, token);
                    PrefConnect.writeInteger(RegisterActivity.this, PrefConnect.APP_VERSION, getAppVersion(getApplicationContext()));
                }
            }
        }.execute();
    }

    private void setUrls() {
        // Set Urls for The First Time
        Call<Urls> call = apiService.getUrls();
        call.enqueue(new Callback<Urls>() {
            @Override
            public void onResponse(Call<Urls> call, Response<Urls> response) {
                if (response.isSuccessful()) {
                    Urls urls = response.body();
                    if (urls.getResult().equalsIgnoreCase("Success")) {
                        PrefConnect.writeString(RegisterActivity.this, PrefConnect.ABOUT_US, urls.getAboutus());
                        PrefConnect.writeString(RegisterActivity.this, PrefConnect.FAQ, urls.getFAQ());
                        PrefConnect.writeString(RegisterActivity.this, PrefConnect.CUSTOMER_TERMS, urls.getCustomerTerms());
                        PrefConnect.writeString(RegisterActivity.this, PrefConnect.CONTRACTOR_TERMS, urls.getContractorTerms());
                        PrefConnect.writeString(RegisterActivity.this, PrefConnect.CUSTOMER_PAYMENT_TERMS, urls.getCustomerpaymentsterms());
                        PrefConnect.writeString(RegisterActivity.this, PrefConnect.CONTRACTOR_PAYMENT_TERMS, urls.getContractorpayments());

                    }
                }

            }

            @Override
            public void onFailure(Call<Urls> call, Throwable t) {

            }
        });
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            ArrayList<String> permissionList = new ArrayList<>();
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.CAMERA);
            }
            if (checkSelfPermission(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.GET_ACCOUNTS);
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.RECORD_AUDIO);
            }
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.CALL_PHONE);
            }
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (permissionList.size() > 0) {
                String[] permissionArray = new String[permissionList.size()];
                permissionList.toArray(permissionArray);
                requestPermissions(permissionArray, PERMISSION_REQUEST);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST:
                for (int permissionResult : grantResults) {
                    if (permissionResult == PackageManager.PERMISSION_DENIED) {
                        Global.showSimpleAlert(this, getString(R.string.permissions_mandatory), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        break;
                    }
                }
                break;
        }
    }

    private void RegisterGCMAccount() {

        // Check Shared Preferences
        // userPreferences = getSharedPreferences("LawMatters", MODE_PRIVATE);
        // gcm_regid = userPreferences.getString("gcm_regid", null);

        // Register with GCM
        // if (gcm_regid == null)// check preference here, enter this block if no
        // regid is found

        final String token = null;
        {

            // Toast.makeText(getApplicationContext(), "GCM Not Yet Registered",
            // Toast.LENGTH_LONG).show();
            int gcmavailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (gcmavailable != ConnectionResult.SUCCESS) {
                Log.d("Naveen", "GCM Unavailable");
                if (GooglePlayServicesUtil.isUserRecoverableError(gcmavailable)) {
                    // Toast.makeText(getApplicationContext(),
                    // "This device does support GCM but needs sw update",
                    // Toast.LENGTH_LONG).show();
                    GooglePlayServicesUtil.getErrorDialog(gcmavailable, this, 9000).show();
                } else {
                    Log.d("Naveen", "This device is not supported");
                    // Toast.makeText(getApplicationContext(),
                    // "This device does not support GCM",
                    // Toast.LENGTH_LONG).show();
                    // make appropriate entry in shared prefernce to disable
                    // push messages
                    // finish();
                }
            } else {

                // Toast.makeText(getApplicationContext(),
                // "This device supports GCM", Toast.LENGTH_LONG).show();
                gcm = GoogleCloudMessaging.getInstance(this);
                // Toast.makeText(getApplicationContext(),
                // "Registering with Google Servers.... \n Please wait. ",
                // Toast.LENGTH_LONG).show();
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        String regid = "null";
                        try {
                            if (gcm == null) {
                                gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                            }
                            regid = gcm.register(SENDER_ID);// Change this
                            // token=regid;
                            // to use
                            // official
                            // api key
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            regid = "null";
                        }
                        return regid;
                    }

                    @Override
                    protected void onPostExecute(final String regid) {
                        if (regid.equals("null")) {
                            //  Toast.makeText(getApplicationContext(), "GCM registration failed. Not able to get notifications", Toast.LENGTH_LONG).show();
                        } else {
                            // Toast.makeText(getApplicationContext(),
                            // "Registration Successful",
                            // Toast.LENGTH_LONG).show();
                            // +regid
                            /*userPreferences.edit().putString("gcm_regid", regid).commit();
                            gcm_reg_id = regid;

                            Log.d("Naveen1", "The GCM ID is: " + gcm_reg_id);*/
                            // Toast.makeText(getApplicationContext(),
                            // "Server Response: "+gcm_reg_id,
                            // Toast.LENGTH_LONG).show();
                            // login();

                            if (!TextUtils.isEmpty(regid)) {
                                PrefConnect.writeString(RegisterActivity.this, PrefConnect.GCM_TOKEN, regid);
                                PrefConnect.writeInteger(RegisterActivity.this, PrefConnect.APP_VERSION, getAppVersion(getApplicationContext()));
                            }
                        }
                    }
                }.execute(null, null, null);
            }

        }
     /*   else
        {
           *//* Log.d("Naveen", "The GCM ID is: " + gcm_regid);
            // Toast.makeText(getApplicationContext(),
            // "Gcm Already Registered: "+gcm_regid, Toast.LENGTH_LONG).show();
            gcm_reg_id = gcm_regid;*//*
        }*/

    }

}
