package com.oostajob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oostajob.dialog.FileAttach;
import com.oostajob.video.CameraHelper;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("deprecation")
public class VideoActivity extends AppCompatActivity implements FileAttach.SuccessListener {


    private static final long DURATION = 120000L; //  2 minutes 120 secs * 1000
    private static final long INTERVAL = 1000L; // 1 second
    private static final String TAG = "Recorder";
    private Camera mCamera;
    private SurfaceView mPreview;
    private MediaRecorder mMediaRecorder;
    private boolean isRecording = false;
    private ImageView captureButton;
    private TextView txtTimer;
    private Timer timer;
    private File file;
    private String angle = "Landscape";

    private Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

//        hideStatusBar();
        init();
        checkOrientation();

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    private void init() {
        timer = new Timer(DURATION, INTERVAL);
        mPreview = (SurfaceView) findViewById(R.id.surface_view);
        captureButton = (ImageView) findViewById(R.id.button_capture);
        txtTimer = (TextView) findViewById(R.id.txtTimer);
        bundle = new Bundle();
        bundle.putString("title", getString(R.string.do_you_want_to_include_this_video));
    }

    public void onCaptureClick(View view) {
        if (isRecording) {
            timer.cancelTimer();
        } else {
            new MediaPrepareTask().execute(null, null, null);
        }
    }

    private void setCaptureButtonText(int id) {
        captureButton.setImageResource(id);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            timer.cancel();
            releaseResources();
        } catch (Exception e) {

        }
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.stop();  // stop the recording
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
            mCamera.lock();
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private boolean prepareVideoRecorder() throws Exception {
        mCamera = CameraHelper.getDefaultCameraInstance();
        if (mCamera == null) {
            mCamera = CameraHelper.getDefaultFrontFacingCameraInstance();
        }


        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
        Camera.Size optimalSize = CameraHelper.getOptimalPreviewSize(mSupportedPreviewSizes,
                mPreview.getWidth(), mPreview.getHeight());

        // Use the same size for recording profile.
        CamcorderProfile profile = null;
        try {
            profile = CamcorderProfile.get(0, CamcorderProfile.QUALITY_LOW);
            profile.videoFrameWidth = optimalSize.width;
            profile.videoFrameHeight = optimalSize.height;
            // likewise for the camera object itself.
            parameters.setPreviewSize(profile.videoFrameWidth, profile.videoFrameHeight);
        } catch (Exception e) {
            // likewise for the camera object itself.
            parameters.setPreviewSize(optimalSize.width, optimalSize.height);
            e.printStackTrace();
        }
        mCamera.setParameters(parameters);
        try {
            // Requires API level 11+, For backward compatibility use {@link setPreviewDisplay}
            // with {@link SurfaceView}
            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (IOException e) {
            Log.e(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
            return false;
        }


        // BEGIN_INCLUDE (configure_media_recorder)
        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        try {
            mMediaRecorder.setProfile(profile);
        } catch (Exception ignored) {

        }

        // Step 4: Set output file
        file = CameraHelper.getOutputMediaFile(CameraHelper.MEDIA_TYPE_VIDEO);
        mMediaRecorder.setOutputFile(file.toString());

        // END_INCLUDE (configure_media_recorder)

        // Step 5: Prepare configured MediaRecorder
        try {
            if (angle.equalsIgnoreCase("Landscape")) {
                mMediaRecorder.setOrientationHint(0);
            } else {
                mMediaRecorder.setOrientationHint(90);
            }
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    public void releaseResources() {
        txtTimer.setText("00:00");
        releaseMediaRecorder(); // release the MediaRecorder object
        setCaptureButtonText(R.drawable.record);
        isRecording = false;
        releaseCamera();
    }

    @Override
    public void attach() {
        Intent intent = new Intent();
        intent.setData(Uri.fromFile(file));
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Asynchronous task for preparing the {@link MediaRecorder} since it's a long blocking
     * operation.
     */
    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            // initialize video camera
            try {
                if (prepareVideoRecorder()) {
                    // Camera is available and unlocked, MediaRecorder is prepared,
                    // now you can start recording
                    mMediaRecorder.start();

                    isRecording = true;

                    timer.start();
                } else {
                    // prepare didn't work, release the camera
                    releaseMediaRecorder();
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                VideoActivity.this.finish();
            }
            // inform the user that recording has started
            setCaptureButtonText(R.drawable.stop);

        }
    }

    public class Timer extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public Timer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            String ms = String.format(Locale.getDefault(), "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
            System.out.println(ms);
            txtTimer.setText(ms);
        }

        @Override
        public void onFinish() {
            releaseResources();
            FileAttach attach = new FileAttach();
            attach.setArguments(bundle);
            attach.setSuccessListener(VideoActivity.this);
            attach.show(getSupportFragmentManager(), "File Attach");
        }

        public void cancelTimer() {
            cancel();
            Intent intent = new Intent();
            try {
                releaseResources();
                FileAttach attach = new FileAttach();
                attach.setArguments(bundle);
                attach.setSuccessListener(VideoActivity.this);
                attach.show(getSupportFragmentManager(), "File Attach");
            } catch (Exception e) {
                Toast.makeText(VideoActivity.this, "Unable to capture media", Toast.LENGTH_LONG).show();
                if (file != null && file.exists()) {
                    file.delete();
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
            }
        }
    }

    public void checkOrientation() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(new SensorEventListener() {
            int orientation = -1;

            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.values[1] < 6.5 && event.values[1] > -6.5) {
                    if (orientation != 1) {
                        Log.d("Sensor", "Landscape");
                        angle = "Landscape";
                    }
                    orientation = 1;
                } else {
                    if (orientation != 0) {
                        Log.d("Sensor", "Portrait");
                        angle = "Portrait";
                    }
                    orientation = 0;
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                // TODO Auto-generated method stub

            }
        }, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
    }
}
