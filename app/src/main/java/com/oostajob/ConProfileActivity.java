package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.Green.customfont.CustomTextView;
import com.oostajob.adapter.contractor.ConReviewAdapter;
import com.oostajob.adapter.contractor.SpinnerAdapter;
import com.oostajob.global.Global;
import com.oostajob.model.ConReviewModel;
import com.oostajob.model.JobTypeModel;
import com.oostajob.utils.CircleTransform;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConProfileActivity extends AppCompatActivity {

    private View viewIndLeft, viewIndRight;
    private APIService apiService;
    private ProgressDialog p;
    private ImageView imgProfile;
    private LinearLayout detLay, reviewLay;
    private Spinner spinFilter;
    private SlidingUpPanelLayout panelLayout;
    private ListView reviewsView;
    private CustomTextView txtName, txtExpertise, txtHourly, txtAbout, txtReviews, txtAddress, txtEmail, txtPhone;
    private Toolbar toolbar;
    private String USER_ID, NAME, EMAIL, PHONE, ADDRESS, PASSWORD, PROFILE, ZIP_CODE, LAT, LNG, EXPERTISE, SSN, HOURLY_RATE, LICENSE;
    private int[] color = new int[2];
    private Animation fadeIn;
    private SpinnerAdapter spinnerAdapter;
    private ConReviewAdapter reviewAdapter;
    private ArrayList<ConReviewModel.RatingDetails> ratingDetailList = new ArrayList<>();
    private ImageView imgBack;
    private AppCompatRatingBar ratingBar;
    private ImageView imgEC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_profile);

        fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);

        color[0] = getResources().getColor(R.color.white);
        color[1] = getResources().getColor(R.color.blue);
        init();
        setUpToolbar();
        setListener();
        loadValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            case R.id.action_edit:
                gotoUpdateActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.panelLayout);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        txtName = (CustomTextView) findViewById(R.id.txtName);
        txtExpertise = (CustomTextView) findViewById(R.id.txtExpertise);
        txtHourly = (CustomTextView) findViewById(R.id.txtHourly);
        txtAbout = (CustomTextView) findViewById(R.id.txtAbout);
        txtReviews = (CustomTextView) findViewById(R.id.txtReviews);
        txtAddress = (CustomTextView) findViewById(R.id.txtAddress);
        txtEmail = (CustomTextView) findViewById(R.id.txtEmail);
        txtPhone = (CustomTextView) findViewById(R.id.txtPhone);
        viewIndLeft = (View) findViewById(R.id.viewIndLeft);
        viewIndRight = (View) findViewById(R.id.viewIndRight);
        detLay = (LinearLayout) findViewById(R.id.detLay);
        reviewLay = (LinearLayout) findViewById(R.id.reviewLay);
        spinFilter = (Spinner) findViewById(R.id.spinFilter);
        reviewsView = (ListView) findViewById(R.id.reviewsView);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        ratingBar = (AppCompatRatingBar) findViewById(R.id.ratingBar);
        imgEC = (ImageView) findViewById(R.id.imgEC);

        p = new ProgressDialog(this);
        p.setIndeterminate(false);
        p.setMessage(getString(R.string.loading));
        p.setCancelable(false);
        apiService = RetrofitSingleton.createService(APIService.class);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }


    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIndicator(color[1], color[0]);
                setVisibility(View.VISIBLE, View.GONE);
            }
        });
        txtReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIndicator(color[0], color[1]);
                setVisibility(View.GONE, View.VISIBLE);
            }
        });
        panelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {
                imgEC.setImageResource(R.drawable.up);
            }

            @Override
            public void onPanelExpanded(View panel) {
                imgEC.setImageResource(R.drawable.down);
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });
        imgEC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                else if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });
        spinFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String select = spinnerAdapter.getItem(position).getJob_name();
                ArrayList<ConReviewModel.RatingDetails> list = new ArrayList<ConReviewModel.RatingDetails>();
                if (select.equalsIgnoreCase("All")) {
                    setReviewAdapter(ratingDetailList);
                } else {
                    for (ConReviewModel.RatingDetails ratingDetails : ratingDetailList) {
                        if (ratingDetails != null && ratingDetails.getJobtypeName() != null) {
                            if (ratingDetails.getJobtypeName().equalsIgnoreCase(select)) {
                                list.add(ratingDetails);
                            }
                        }
                    }
                    setReviewAdapter(list);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setIndicator(int color1, int color2) {
        viewIndLeft.setBackgroundColor(color1);
        viewIndRight.setBackgroundColor(color2);
    }

    private void setVisibility(int arg0, int arg1) {
        if (arg1 == View.VISIBLE) {
            reviewLay.startAnimation(fadeIn);
        } else if (arg0 == View.VISIBLE) {
            detLay.startAnimation(fadeIn);
        }
        detLay.setVisibility(arg0);
        reviewLay.setVisibility(arg1);
    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(ConProfileActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void gotoUpdateActivity() {
        Intent homeIntent = new Intent(ConProfileActivity.this, ConUpdateActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    private void loadValues() {
        USER_ID = PrefConnect.readString(ConProfileActivity.this, PrefConnect.USER_ID, "");
        EMAIL = PrefConnect.readString(ConProfileActivity.this, PrefConnect.EMAIL, "");
        PASSWORD = PrefConnect.readString(ConProfileActivity.this, PrefConnect.EMAIL, "");
        NAME = PrefConnect.readString(ConProfileActivity.this, PrefConnect.NAME, "");
        ADDRESS = PrefConnect.readString(ConProfileActivity.this, PrefConnect.ADDRESS, "");
        PHONE = PrefConnect.readString(ConProfileActivity.this, PrefConnect.PHONE, "");
        ZIP_CODE = PrefConnect.readString(ConProfileActivity.this, PrefConnect.ZIPCODE, "");
        LAT = PrefConnect.readString(ConProfileActivity.this, PrefConnect.LAT, "");
        LNG = PrefConnect.readString(ConProfileActivity.this, PrefConnect.LNG, "");
        SSN = PrefConnect.readString(ConProfileActivity.this, PrefConnect.SSN, "");
        EXPERTISE = PrefConnect.readString(ConProfileActivity.this, PrefConnect.EXPERTISE_AT, "");
        //HOURLY_RATE = PrefConnect.readString(ConProfileActivity.this, PrefConnect.HOURLY_RATE, "");

        Uri pUri = Uri.parse(PrefConnect.readString(ConProfileActivity.this, PrefConnect.PROFILE_PHOTO, ""));
        PROFILE = pUri.getLastPathSegment();
        Uri lUri = Uri.parse(PrefConnect.readString(ConProfileActivity.this, PrefConnect.LICENSE, ""));
        LICENSE = lUri.getLastPathSegment();

        txtName.setText(NAME);
        //txtHourly.setText(String.format("Hourly Rates:$%s/hr", HOURLY_RATE));

        txtAddress.setText(ADDRESS);
        txtEmail.setText(EMAIL);
        txtPhone.setText(PHONE);

        String pUrl = String.format("%s/%s", Global.BASE_URL, pUri.toString());
        Picasso.with(this).load(pUrl).placeholder(R.drawable.user).transform(new CircleTransform()).fit().into(imgProfile);
        setExpertise();

        txtAbout.performClick();
    }

    private void setExpertise() {
        p.show();
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                if (response.isSuccessful()) {
                    JobTypeModel model = response.body();
                    ArrayList<String> expertiseList = new ArrayList<>();
                    ArrayList<JobTypeModel.JobtypeDetails> list = new ArrayList<>();
                    if (model.getResult().equalsIgnoreCase("success")) {
                        ArrayList<JobTypeModel.JobtypeDetails> jobTypesList = model.getJobtypeDetails();
                        String[] arr = EXPERTISE.split(",");
                        for (String anArr : arr) {
                            for (int j = 0; j < jobTypesList.size(); j++) {
                                if (jobTypesList.get(j).getJobtype_id().equals(anArr.trim())) {
                                    expertiseList.add(jobTypesList.get(j).getJob_name());
                                    list.add(jobTypesList.get(j));
                                }
                            }
                        }

                        if (expertiseList.size() > 1) {
                            JobTypeModel.JobtypeDetails details = new JobTypeModel.JobtypeDetails("", "All", "", "");
                            list.add(0, details);
                        }

                        spinnerAdapter = new SpinnerAdapter(ConProfileActivity.this, list);
                        spinFilter.setAdapter(spinnerAdapter);

                        String tempVar = expertiseList.toString().replace("[", "").replace("]", "");
                        txtExpertise.setText(tempVar);
                        loadReviews();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void loadReviews() {
        if (!p.isShowing()) p.show();
        Call<ConReviewModel.Response> call = apiService.getReviews(USER_ID);
        call.enqueue(new Callback<ConReviewModel.Response>() {
            @Override
            public void onResponse(Call<ConReviewModel.Response> call, Response<ConReviewModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ConReviewModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        ratingDetailList = response1.getRatingDetails();
                        setReviewAdapter(ratingDetailList);

                        ConReviewModel.Earn earn = response1.getEarningDetails();
                        ratingBar.setRating(Float.parseFloat(earn.getTotalRating()));

                        //txtEarn.setText(String.format("$%s", earn.getTotalEarning()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ConReviewModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


    private void setReviewAdapter(ArrayList<ConReviewModel.RatingDetails> list) {
        reviewAdapter = new ConReviewAdapter(ConProfileActivity.this, list);
        reviewsView.setAdapter(reviewAdapter);
    }
}
