package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.global.Global;
import com.oostajob.model.NotifyAddress;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ZipCodeActivity extends AppCompatActivity {

    private CustomTextView txtZipCode;
    private CustomEditText edtEmail;
    private CustomButton btnSend;
    private String ZIP_CODE;
    private ImageView imgBack;
    private APIService apiService;
    private ProgressDialog p;
    private String goTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zip_code);

        getFromIntent();
        init();
        setListener();
    }

    private void getFromIntent() {
        ZIP_CODE = getIntent().getStringExtra("zipCode");
        goTo = getIntent().getStringExtra("goto");
    }

    private void init() {
        txtZipCode = (CustomTextView) findViewById(R.id.txtZipCode);
        edtEmail = (CustomEditText) findViewById(R.id.edtEmail);
        btnSend = (CustomButton) findViewById(R.id.btnSend);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtZipCode.setText(String.format("ZIP Code %s", ZIP_CODE));
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);

        apiService = RetrofitSingleton.createService(APIService.class);

        p = new ProgressDialog(ZipCodeActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
    }

    private void setListener() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    notifyService();
                }
            }
        });
    }

    private void notifyService() {
        p.show();
        NotifyAddress address = new NotifyAddress("", ZIP_CODE, edtEmail.getText().toString(), "");
        Call<NotifyAddress.Result> call = apiService.notifyService(address);
        call.enqueue(new Callback<NotifyAddress.Result>() {
            @Override
            public void onResponse(Call<NotifyAddress.Result> call, Response<NotifyAddress.Result> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    NotifyAddress.Result result = response.body();
                    if (result.getResult().equalsIgnoreCase("success")) {
                        Toast.makeText(ZipCodeActivity.this, "We will get back to you when service is available in your location.", Toast.LENGTH_LONG).show();
                        if (goTo.equals("home")) {
                            gotoHome();
                        } else {
                            finish();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<NotifyAddress.Result> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(ZipCodeActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    public boolean isValidated() {
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            Toast.makeText(ZipCodeActivity.this, "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtEmail.getText().toString())) {
            Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtEmail.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(ZipCodeActivity.this, "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }
}
