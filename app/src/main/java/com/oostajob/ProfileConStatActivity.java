package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.oostajob.global.Global;
import com.oostajob.model.LogoutModel;
import com.oostajob.model.VerificationProcessModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileConStatActivity extends AppCompatActivity {

    private String userId;
    private ImageView backnav;
    private APIService apiService;
    private ProgressDialog p;

    private Handler mHandler = null;
    private Call<VerificationProcessModel.Response> mVerificationCall;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_con_status);
        apiService = RetrofitSingleton.createService(APIService.class);
        init();
        setListener();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startHandlerThread();
    }

    public void startHandlerThread() {
        HandlerThread mHandlerThread = new HandlerThread("verifyAdminApproveHandlerThread");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
        mHandler.postDelayed(mRunnable, Global.INTERVAL);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mVerificationCall != null)
                mVerificationCall.cancel();
            verifyAdminApprove();
            mHandler.postDelayed(this, Global.INTERVAL); // repeating
        }
    };

    private void verifyAdminApprove() {
        apiService = RetrofitSingleton.createService(APIService.class);
        mVerificationCall = apiService.verifyMail(userId);
        mVerificationCall.enqueue(new Callback<VerificationProcessModel.Response>() {
            @Override
            public void onResponse(Call<VerificationProcessModel.Response> call, Response<VerificationProcessModel.Response> response) {
                if (response.isSuccessful()) {
                    VerificationProcessModel.Response response1 = response.body();
                    if (response1 != null && response1.getResult().equalsIgnoreCase("Success")) {
                        if (response1.getEmailStatus().equalsIgnoreCase("Active") &&
                                response1.getProfileStatus().equalsIgnoreCase("Verified")) {
                            //PrefConnect.writeInteger(getApplicationContext(), PrefConnect.PAGE, -1);
                            //startHomeActivity();

                            activatedBankDetailsContractor();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<VerificationProcessModel.Response> call, Throwable t) {

            }
        });
    }

    private void setListener() {
        backnav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.equalsIgnoreCase("")) {
                    PrefConnect.writeString(ProfileConStatActivity.this, PrefConnect.USER_ID, "");
                    PrefConnect.writeString(ProfileConStatActivity.this, PrefConnect.USER_TYPE, "");
                    PrefConnect.writeInteger(ProfileConStatActivity.this, PrefConnect.PAGE, 0);
                    Intent intent = new Intent(ProfileConStatActivity.this, RegisterActivity.class);
                    intent.putExtra("fragPosition", 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    p.show();
                    Call<LogoutModel> call = apiService.logout(userId);
                    call.enqueue(new Callback<LogoutModel>() {
                        @Override
                        public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                LogoutModel model = response.body();
                                if (model != null && model.getResult().equalsIgnoreCase("Success")) {
                                    PrefConnect.clearAllPrefs(ProfileConStatActivity.this);
                                    Intent intent = new Intent(ProfileConStatActivity.this, RegisterActivity.class);
                                    intent.putExtra("fragPosition", 0);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<LogoutModel> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });
                }
            }
        });
    }

    private void init() {
        ImageView imgBack = (ImageView) findViewById(R.id.imgBack);
        backnav = (ImageView) findViewById(R.id.backnav);

        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);

        userId = PrefConnect.readString(getApplicationContext(), PrefConnect.USER_ID, "");
        p = Global.initProgress(this);
    }


    private void activatedBankDetailsContractor() {
        mHandler.removeCallbacks(mRunnable);
        PrefConnect.writeInteger(getApplicationContext(), PrefConnect.PAGE, 3);
        Intent intent = new Intent(getApplicationContext(), ConBankDetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
