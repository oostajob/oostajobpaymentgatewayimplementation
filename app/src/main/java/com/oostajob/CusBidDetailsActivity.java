package com.oostajob;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.VideoView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.google.android.gms.maps.model.LatLng;
import com.oostajob.adapter.customer.CusMessagesAdapter.ReplyListener;
import com.oostajob.adapter.customer.CustomerJobsAdapter;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.dialog.CancelOrSkipDia;
import com.oostajob.dialog.CancelOrSkipDia.SuccessListener;
import com.oostajob.dialog.FileDialog;
import com.oostajob.global.Global;
import com.oostajob.model.AddNewFile;
import com.oostajob.model.DiscussionlistModel;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobDiscussionReplyModel;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.Result;
import com.oostajob.utils.BlurTransformation;
import com.oostajob.utils.CircleTransform;
import com.oostajob.utils.FetchAddress;
import com.oostajob.utils.FilePath;
import com.oostajob.utils.PrefConnect;
import com.oostajob.video.TrimVideo;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CusBidDetailsActivity extends AppCompatActivity implements ReplyListener, SuccessListener, ImageListener {

    private int CURRENT_ITEM = 0;
    private String USER_ID;
    private ProgressDialog p;
    private Toolbar toolbar;
    private ImageView imageView, imgPlay, imgClose, imgMenuArrow, imgIcon, imgBack, imgRateArrow, imgProfile;
    private FrameLayout videoLay;
    private VideoView videoView;
    private HorizontalScrollView scrollView;
    private LinearLayout scrollContent, menuLay, bottomLay, timeLay, msgLayout;
    private CustomTextView txtMenuItem, txtTitle, txtConCount, txtZipCode, txtTime, txtDescription, txtMessage, txtName, txtAddress,
            txtDateTime, txtEmptyView, txtCloses;
    private ChipView chipView;
    private SlidingUpPanelLayout panelLayout;
    //    private ListView messageView;
    private ScrollView msgScroll;
    private APIService apiService;
    private GetCustDetailsModel.Details details;
    private MediaController mediaController;
    //    private CusMessagesAdapter adapter;
    private CustomButton btnEmail, btnCall;
    private RatingBar ratingBar;
    private boolean showFirst = true;
    private Uri fileUri;
    private Boolean upload;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus_bid_detail);

        if (savedInstanceState != null) {
            fileUri = Uri.parse(savedInstanceState.getString("fileUri", ""));
        }

        init();
        setUpToolbar();
        setListener();
        loadValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!Global.isUriEmpty(fileUri))
            outState.putString("fileUri", fileUri.toString());
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.imageView);
        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        imgMenuArrow = (ImageView) findViewById(R.id.imgMenuArrow);
        imgIcon = (ImageView) findViewById(R.id.imgIcon);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgRateArrow = (ImageView) findViewById(R.id.imgRateArrow);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        videoLay = (FrameLayout) findViewById(R.id.videoLay);
        videoView = (VideoView) findViewById(R.id.videoView);
        scrollView = (HorizontalScrollView) findViewById(R.id.scrollView);
        scrollContent = (LinearLayout) findViewById(R.id.scrollContent);
        timeLay = (LinearLayout) findViewById(R.id.timeLay);
        menuLay = (LinearLayout) findViewById(R.id.menuLay);
        bottomLay = (LinearLayout) findViewById(R.id.bottomLay);
        msgLayout = (LinearLayout) findViewById(R.id.msgLay);
        txtMenuItem = (CustomTextView) findViewById(R.id.txtMenuItem);
        txtTitle = (CustomTextView) findViewById(R.id.txtTitle);
        txtConCount = (CustomTextView) findViewById(R.id.txtConCount);
        txtZipCode = (CustomTextView) findViewById(R.id.txtZipCode);
        txtTime = (CustomTextView) findViewById(R.id.txtTime);
        txtCloses = (CustomTextView) findViewById(R.id.txtCloses);
        txtDescription = (CustomTextView) findViewById(R.id.txtDescription);
        txtMessage = (CustomTextView) findViewById(R.id.txtMessage);
        txtName = (CustomTextView) findViewById(R.id.txtName);
        txtAddress = (CustomTextView) findViewById(R.id.txtAddress);
        //messageView = (ListView) findViewById(R.id.messageView);
        msgScroll = (ScrollView) findViewById(R.id.msgScroll);
        chipView = (ChipView) findViewById(R.id.chipView);
        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.panelLayout);
        btnEmail = (CustomButton) findViewById(R.id.btnEmail);
        btnCall = (CustomButton) findViewById(R.id.btnCall);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        txtDateTime = (CustomTextView) findViewById(R.id.txtDateTime);
        txtEmptyView = (CustomTextView) findViewById(R.id.txtEmptyView);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //messageView.setEmptyView(txtEmptyView);
        panelLayout.setScrollableView(msgScroll);


        p = new ProgressDialog(CusBidDetailsActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);

        mediaController = new MediaController(CusBidDetailsActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        menuLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CURRENT_ITEM == CustomerJobsAdapter.BIDDING) {
                    PopupMenu popup = new PopupMenu(CusBidDetailsActivity.this, v);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_cancel:
                                    CancelOrSkipDia dialog = new CancelOrSkipDia();
                                    dialog.setSuccessListener(CusBidDetailsActivity.this);

                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("jobPostID", details.getJobpostID());
                                    bundle.putString("title", "cancel");
                                    bundle.putBoolean("isCustomer", true);
                                    dialog.setArguments(bundle);
                                    dialog.show(getSupportFragmentManager(), "Cancel Dialog");
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.inflate(R.menu.actions);
                    popup.show();
                }
               /* if (CURRENT_ITEM == CustomerJobsAdapter.RATE) {
                    Intent intent = new Intent(CusBidDetailsActivity.this, FeedBackActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("contractor", details);
                    startActivity(intent);
                }*/
            }
        });
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPlay.setVisibility(View.GONE);
                videoView.start();
                scrollView.setVisibility(View.GONE);
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CusBidDetailsActivity.this, "Cannot load video", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                setMediaVisibility(View.GONE, View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        imgRateArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(CusBidDetailsActivity.this, FeedBackActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("contractor", details);
                startActivity(intent);*/
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.sendEmail(CusBidDetailsActivity.this, details.getContractorDetails().getEmail());
            }
        });
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.call(CusBidDetailsActivity.this, details.getContractorDetails().getPhone());
            }
        });
        panelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {

            }

            @Override
            public void onPanelExpanded(View panel) {
                readMessages();
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });
    }


    private void gotoHome() {
        Intent homeIntent = new Intent(CusBidDetailsActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void loadValues() {
        try {
            details = (GetCustDetailsModel.Details) getIntent().getExtras().getSerializable("details");
            CURRENT_ITEM = getIntent().getExtras().getInt("currentItem", 0);
            assert details != null;
            // Load Background Image
            String IMG_URL = String.format("%s/%s", Global.BASE_URL, details.getJob_image());
            Picasso.with(this).load(IMG_URL).transform(new BlurTransformation(this, 25)).fit().into(imgBack);

            //Menu
            txtMenuItem.setText(getMenuItemText());

            //load Files
            loadFiles(details.getJobmedialist());

            toolbar.setTitle(details.getJobtypeName());
            //title
            String ICON_URL = String.format("%s/%s", Global.BASE_URL, details.getJobicon());
            Picasso.with(this).load(ICON_URL).fit().into(imgIcon);
            txtTitle.setText(details.getJobtypeName());

            //Contractor Count
            txtConCount.setText(details.getContractorcnt());

            //Zipcode
            txtZipCode.setText(details.getZipcode());

            //Remaining Hours
            Date date = Global.getDate(details.getJobpostTime());
            long difference = System.currentTimeMillis() - date.getTime();
            long hours = TimeUnit.MILLISECONDS.toHours(difference);
            if (hours == 0) {
                long minu = TimeUnit.MILLISECONDS.toMinutes(difference);
                if (minu > 1)
                    txtTime.setText(String.format("%d minutes ago", minu));
                else txtTime.setText(String.format("%d minute ago", minu));
            } else if (hours >= 24) {
                long days = TimeUnit.MILLISECONDS.toDays(difference);
                if (days > 1)
                    txtTime.setText(String.format("%d days ago", days));
                else txtTime.setText(String.format("%d day ago", days));
            } else if (hours < 24) {
                if (hours > 1)
                    txtTime.setText(String.format("%d hours ago", hours));
                else txtTime.setText(String.format("%d hour ago", hours));
            }

            //Tags
            setTags(details);

            //Description
            StringBuilder builder = new StringBuilder();
            builder.append(details.getDescription()).append("\n").append("\n");
            for (GetCustDetailsModel.JobAnwserlist answer : details.getJobanwserlist()) {
                if (answer.getTag_key().equalsIgnoreCase("2")) {
                    builder.append(answer.getQuest_name()).append("\n").append(answer.getAnswer()).append("\n");
                }
            }
            txtDescription.setText(builder.toString());
            closeTimeVisiblity();

            loadMessages(details.getJobpostID());

            setBottomVisibility();

            setBottomContent();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeTimeVisiblity() {
        switch (CURRENT_ITEM) {
            case CustomerJobsAdapter.BIDDING:
                txtCloses.setText(String.format("Wait %s to hire", details.getRemaining_time()));
                break;
            case CustomerJobsAdapter.HIRE:
                txtCloses.setVisibility(View.GONE);
                break;
            case CustomerJobsAdapter.RATE:
                txtCloses.setVisibility(View.GONE);
                break;
            case CustomerJobsAdapter.CLOSE:
                txtCloses.setVisibility(View.GONE);
                break;

        }
    }

    private void setBottomVisibility() {
        switch (CURRENT_ITEM) {
            case CustomerJobsAdapter.BIDDING:
                bottomLay.setVisibility(View.GONE);
                break;
            case CustomerJobsAdapter.HIRE:
                ratingBar.setVisibility(View.GONE);
                txtCloses.setVisibility(View.GONE);
                break;
            case CustomerJobsAdapter.RATE:
                timeLay.setVisibility(View.VISIBLE);
                txtCloses.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);
                break;
            case CustomerJobsAdapter.CLOSE:
                txtCloses.setVisibility(View.GONE);
                imgRateArrow.setVisibility(View.GONE);
                timeLay.setVisibility(View.VISIBLE);
                ratingBar.setVisibility(View.VISIBLE);
                break;

        }
    }

    private void setBottomContent() {
        GetCustDetailsModel.ContractorDetails customer = details.getContractorDetails();
        String PROF_URL = String.format("%s/%s", Global.BASE_URL, customer.getProfilePhoto());
        Picasso.with(this).load(PROF_URL).transform(new CircleTransform()).placeholder(R.drawable.contrac).into(imgProfile);
        txtName.setText(customer.getUsername());

        btnEmail.setText(customer.getEmail());
        btnCall.setText(customer.getPhone());

        LatLng latLng = new LatLng(Double.parseDouble(customer.getLat()), Double.parseDouble(customer.getLng()));
        FetchAddress fetchAddress = new FetchAddress(this, latLng);
        fetchAddress.getAddress();
        txtAddress.setText(String.format("%s - %s", fetchAddress.getLocality(), fetchAddress.getPostalCode()));

        switch (CURRENT_ITEM) {
            case CustomerJobsAdapter.RATE:
                ratingBar.setVisibility(View.GONE);
                if ((customer.getMeetingDate() == null && customer.getMeetingTime() == null) || customer.getMeetingDate().startsWith("0000")) {
                    txtDateTime.setText("Meeting not scheduled");
                } else {
                    String dateTime = String.format("%s, %s", Global.getDate(customer.getMeetingDate(), "yyyy-MM-dd", "MM/dd/yyyy"), customer.getMeetingTime());
                    txtDateTime.setText(dateTime);
                }
                break;
            case CustomerJobsAdapter.CLOSE:
                if ((customer.getMeetingDate() == null && customer.getMeetingTime() == null) || customer.getMeetingDate().startsWith("0000")) {
                    txtDateTime.setText("Meeting not scheduled");
                } else {
                    String dateTime = String.format("%s, %s", Global.getDate(customer.getMeetingDate(), "yyyy-MM-dd", "MM/dd/yyyy"), customer.getMeetingTime());
                    txtDateTime.setText(dateTime);
                }
                ratingBar.setRating(Float.parseFloat(customer.getTotalRating()));
                break;
        }
    }

    private String getMenuItemText() {
        switch (CURRENT_ITEM) {
            case CustomerJobsAdapter.BIDDING:
                return "Bidding";
            case CustomerJobsAdapter.RATE:
                imgMenuArrow.setVisibility(View.GONE);
                return "Rate Now";
            case CustomerJobsAdapter.CLOSE:
                imgMenuArrow.setVisibility(View.GONE);
                return "Closed";
            default:
                return "";
        }
    }

    private void setTags(GetCustDetailsModel.Details details) {
        List<Chip> chipList = new ArrayList<>();
        for (GetCustDetailsModel.JobAnwserlist answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        chipView.setLineSpacing(10);
        chipView.setChipSpacing(10);
        chipView.setChipLayoutRes(R.layout.chip_lay);
        chipView.setChipBackgroundRes(R.drawable.tag);
        chipView.setChipList(chipList);
    }

    private void loadFiles(ArrayList<GetCustDetailsModel.Jobmedialist> fileList) {

        scrollContent.removeAllViews();
        final LayoutInflater inflater = LayoutInflater.from(this);

        for (final GetCustDetailsModel.Jobmedialist model : fileList) {
            if (model.getMediaContent().startsWith("public/assets/uploads/jobpost")) {
                final String url = Global.BASE_URL + "/" + model.getMediaContent();
                View view = inflater.inflate(R.layout.image_view, scrollContent, false);
                ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
                if (model.getMediaType().equals("1")) {
                    Picasso.with(this).load(url).fit().into(imgChosen);
                } else {
                    Picasso.with(this).load(R.drawable.play).fit().into(imgChosen);
                }
                imgChosen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (model.getMediaType().equals("1")) {
                            setMediaVisibility(View.VISIBLE, View.GONE);
                            Picasso.with(CusBidDetailsActivity.this).load(url).into(imageView);
                        } else {
                            setMediaVisibility(View.GONE, View.VISIBLE);
                            videoView.setVideoPath(url);
                            videoView.requestFocus();
                        }

                    }
                });
                if (showFirst) {
                    imgChosen.performClick();
                    showFirst = false;
                }
                scrollContent.addView(view);
            } else {
                if (CURRENT_ITEM == CustomerJobsAdapter.BIDDING) {
                    loadEditFiles(model);
                }
            }
        }

        if (CURRENT_ITEM == CustomerJobsAdapter.BIDDING) {
            // To Add More Files
            View addView = inflater.inflate(R.layout.image_plus_up, scrollContent, false);
            ImageView imgAdd = (ImageView) addView.findViewById(R.id.imgAdd);
            final ImageView imgUpload = (ImageView) addView.findViewById(R.id.imgUpload);
            imgUploadVisible(imgUpload);
            imgAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openFileDialog();
                }
            });
            imgUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getUploadCount(details.getJobmedialist()) > 0) {
                        new AddFileTask().execute();
                    } else {
                        Toast.makeText(CusBidDetailsActivity.this, "Choose file to upload", Toast.LENGTH_LONG).show();
                    }
                }
            });

            scrollContent.addView(addView, scrollContent.getChildCount());
        }

    }

    public void imgUploadVisible(View imgUpload) {
        if (getUploadCount(details.getJobmedialist()) > 0) {
            imgUpload.setVisibility(View.VISIBLE);
        } else {
            imgUpload.setVisibility(View.GONE);
        }
    }

    private int getUploadCount(ArrayList<GetCustDetailsModel.Jobmedialist> fileList) {
        int i = 0;
        for (GetCustDetailsModel.Jobmedialist model : fileList) {
            if (TextUtils.isEmpty(model.getMediaContent())) {
                i = i + 1;
            }
        }
        return i;
    }

    private void loadEditFiles(final GetCustDetailsModel.Jobmedialist model) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.item_sub_image, scrollContent, false);
        ImageView imgClose = (ImageView) view.findViewById(R.id.imgClose);
        ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
        if (model.getMediaType().equals("1")) {
            imgChosen.setImageURI(Uri.fromFile(model.getFile()));
        } else {
            Picasso.with(this).load(R.drawable.play).fit().into(imgChosen);
        }
        imgChosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CusBidDetailsActivity.this, FileViewActivity.class);
                intent.putExtra("fileType", model.getMediaType());
                intent.putExtra("filePath", model.getFile().getPath());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeImage(model);
            }
        });
        scrollContent.addView(view);
        scrollView.fullScroll(View.FOCUS_RIGHT);
    }

    public void removeImage(GetCustDetailsModel.Jobmedialist model) {
        ArrayList<GetCustDetailsModel.Jobmedialist> fileList = details.getJobmedialist();
        fileList.remove(model);
        details.setJobmedialist(fileList);
        loadFiles(details.getJobmedialist());
    }

    private void openFileDialog() {
        FileDialog dialog = new FileDialog();
        dialog.setFileListener(CusBidDetailsActivity.this);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                dialog.show(getSupportFragmentManager(), "File");
            } else {
                Toast.makeText(this, "Enable Camera Permission in Settings -> Apps -> OOstaJob -> Permissions.", Toast.LENGTH_LONG).show();
            }
        } else {
            dialog.show(getSupportFragmentManager(), "File");
        }
    }

    private void setMediaVisibility(int arg0, int arg1) {
        imageView.setVisibility(arg0);
        videoLay.setVisibility(arg1);
    }

    private void loadMessages(String jobPostId) {
        p.show();
        Call<DiscussionlistModel.Response> call = apiService.discussionList(jobPostId);
        call.enqueue(new Callback<DiscussionlistModel.Response>() {
            @Override
            public void onResponse(Call<DiscussionlistModel.Response> call, Response<DiscussionlistModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    DiscussionlistModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        ArrayList<DiscussionlistModel.DiscustionDetails> discussion = response1.getDiscustionDetails();
                        switch (CURRENT_ITEM) {
                            case CustomerJobsAdapter.BIDDING:
                                messageLayout(discussion, true);
                                //adapter = new CusMessagesAdapter(CusBidDetailsActivity.this, response1.getDiscustionDetails(), true);
                                break;
                            case CustomerJobsAdapter.RATE:
                            case CustomerJobsAdapter.CLOSE:
                                messageLayout(discussion, false);
                                //adapter = new CusMessagesAdapter(CusBidDetailsActivity.this, response1.getDiscustionDetails(), false);
                                break;
                        }

                        // adapter.setOnReplyListener(CusBidDetailsActivity.this);
                        // messageView.setAdapter(adapter);
                        txtMessage.setText(String.format("Messages (%s)", details.getQuestion_mesage()));
                    }
                    if (response1.getResult().equalsIgnoreCase("Failed")) {
                        msgScroll.setVisibility(View.GONE);
                        txtEmptyView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<DiscussionlistModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void messageLayout(ArrayList<DiscussionlistModel.DiscustionDetails> discussion, boolean showReply) {

        msgLayout.removeAllViews();
        LayoutInflater inflater = getLayoutInflater();
        final ReplyListener listener;

        for (int i = 0; i < discussion.size(); i++) {
            final DiscussionlistModel.DiscustionDetails details = discussion.get(i);
            View convertView = inflater.inflate(R.layout.item_customer_messages, msgLayout, false);
            ImageView imgConProfile = (ImageView) convertView.findViewById(R.id.imgConProfile);
            LinearLayout replyLay = (LinearLayout) convertView.findViewById(R.id.replyLay);
            final LinearLayout replySendLay = (LinearLayout) convertView.findViewById(R.id.replySendLay);
            CustomTextView txtConName = (CustomTextView) convertView.findViewById(R.id.txtConName);
            CustomTextView txtConTime = (CustomTextView) convertView.findViewById(R.id.txtConTime);
            CustomTextView txtConText = (CustomTextView) convertView.findViewById(R.id.txtConText);
            CustomTextView txtReply = (CustomTextView) convertView.findViewById(R.id.txtReply);
            CustomTextView txtReplyTime = (CustomTextView) convertView.findViewById(R.id.txtReplyTime);
            final CustomButton btnReply = (CustomButton) convertView.findViewById(R.id.btnReply);
            CustomButton btnSend = (CustomButton) convertView.findViewById(R.id.btnSend);
            final CustomEditText edtReply = (CustomEditText) convertView.findViewById(R.id.edtReply);

            String CON_URL = String.format("%s/%s", Global.BASE_URL, details.getContractor_profile());
            Picasso.with(this).load(CON_URL).transform(new CircleTransform()).fit().into(imgConProfile);
            txtConName.setText(details.getContractor_name());
            txtConTime.setText(Global.getDate(details.getQuestionTime(), Global.SERVER_DATE_FORMAT,
                    "MM/dd/yyyy, hh:mm aa"));
            txtConText.setText(details.getQuestionName());

            if (!TextUtils.isEmpty(details.getAnwser())) {
                replyLay.setVisibility(View.VISIBLE);
                txtReply.setText(details.getAnwser());
                txtReplyTime.setText(Global.getDate(details.getRespondTime(), Global.SERVER_DATE_FORMAT,
                        "MM/dd/yyyy, hh:mm aa"));
            } else {
                replyLay.setVisibility(View.GONE);
            }

            if (showReply) {
                btnReply.setVisibility(View.VISIBLE);
            } else {
                btnReply.setVisibility(View.GONE);
            }
            btnReply.setEnabled(true);
            replySendLay.setVisibility(View.GONE);
            //setListener(details);

            btnReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replySendLay.setVisibility(View.VISIBLE);
                    btnReply.setEnabled(false);
                    edtReply.requestFocus();
                }
            });
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendReply(details, edtReply.getText().toString());
                }
            });
            msgLayout.addView(convertView);
        }
    }

    private void readMessages() {
        p.show();
        Call<Result> call = apiService.readMessages(details.getJobpostID(), USER_ID);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    String res = response.body().getResult();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }
        super.onBackPressed();
    }

    public void sendReply(DiscussionlistModel.DiscustionDetails details, String answer) {
        p.show();
        final String jobPostId = this.details.getJobpostID();
        final JobDiscussionReplyModel.Request request = new JobDiscussionReplyModel.Request(details.getDiscussionID(),
                USER_ID, jobPostId, answer);
        Call<DiscussionlistModel.Response> call = apiService.replyDiscussion(request);
        call.enqueue(new Callback<DiscussionlistModel.Response>() {
            @Override
            public void onResponse(Call<DiscussionlistModel.Response> call, Response<DiscussionlistModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    DiscussionlistModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        loadMessages(jobPostId);
                    }
                }
            }

            @Override
            public void onFailure(Call<DiscussionlistModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onSuccess() {
        Intent intent = new Intent(CusBidDetailsActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("fragPosition", 1);
        startActivity(intent);
    }

    @Override
    public void onImageSelected(Uri uri, boolean isLicense) {
        fileUri = uri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inDither = true;
        JobSubTypeCharModel.JobsubtypecharDetails details;
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    try {
                        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                        Global.saveImage(CusBidDetailsActivity.this, bitmap, fileUri);
                        addFiles("1", FilePath.getPath(getApplicationContext(), fileUri));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK:
                    try {
                        Uri selectedImageUri = data.getData();
                        if (!Global.isUriEmpty(selectedImageUri)) {
                            //String type = Global.getMimeType(FilePath.getPath(getApplicationContext(), selectedImageUri));
                            String type = getContentResolver().getType(selectedImageUri);
                            if (!TextUtils.isEmpty(type)) {
                                if (type.contains("image")) {
                                    fileUri = Uri.fromFile(Global.getPhotoDirectory());
                                    Bitmap bitmap = BitmapFactory.decodeFile(FilePath.getPath(getApplicationContext(), selectedImageUri)
                                            , options);
                                    Global.saveImage(CusBidDetailsActivity.this, bitmap, fileUri);
                                    addFiles("1", FilePath.getPath(getApplicationContext(), fileUri));
                                } else if (type.contains("video")) {
                                    fileUri = Uri.fromFile(Global.getVideoDirectory());
//                                  addFiles("2", FilePath.getPath(getApplicationContext(), selectedImageUri));
                                    new TrimTask().execute(selectedImageUri, fileUri);
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.VIDEO_CAPTURE:
                    fileUri = data.getData();
                    addFiles("2", FilePath.getPath(getApplicationContext(), fileUri));
                    break;
            }
        }
    }

    private void addFiles(String type, String filePath) {
        File file = new File(filePath);
        ArrayList<GetCustDetailsModel.Jobmedialist> fileList = details.getJobmedialist();
        if (fileList.size() < 25) {
            if (type.equals("1")) {
                if (getFileCount(type, fileList) < 20) {
                    GetCustDetailsModel.Jobmedialist model = new GetCustDetailsModel.Jobmedialist(type, "", file);
                    fileList.add(model);
                    details.setJobmedialist(fileList);
                    loadFiles(fileList);
                } else
                    Toast.makeText(CusBidDetailsActivity.this, "The Photo count is limited to 20", Toast.LENGTH_LONG).show();
            } else if (type.equals("2")) {
                if (getFileCount(type, fileList) < 5) {
                    GetCustDetailsModel.Jobmedialist model = new GetCustDetailsModel.Jobmedialist(type, "", file);
                    fileList.add(model);
                    details.setJobmedialist(fileList);
                    loadFiles(fileList);
                } else
                    Toast.makeText(CusBidDetailsActivity.this, "The Video count is limited to 5", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(CusBidDetailsActivity.this, "Attachment limit Reached", Toast.LENGTH_LONG).show();
        }
    }

    private int getFileCount(String type, ArrayList<GetCustDetailsModel.Jobmedialist> fileList) {
        int i = 0;
        for (GetCustDetailsModel.Jobmedialist model : fileList) {
            if (model.getMediaType().equals(type)) {
                i = i + 1;
            }
        }
        return i;
    }

    public class TrimTask extends AsyncTask<Uri, Void, String> {

        String fromPath, toPath;

        @Override
        protected void onPreExecute() {
            p.show();
        }

        @Override
        protected String doInBackground(Uri... params) {
            fromPath = FilePath.getPath(CusBidDetailsActivity.this, params[0]);
            toPath = FilePath.getPath(CusBidDetailsActivity.this, params[1]);
            try {
                TrimVideo.trimVideo(fromPath, toPath);
                return "1";
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Global.dismissProgress(p);
            if (!TextUtils.isEmpty(s)) {
                addFiles("2", toPath);
            }
        }
    }

    public class AddFileTask extends AsyncTask<String, Void, Boolean> {
        HttpEntity resEntity;
        String url = Global.BASE_URL + "api/index.php/postjobimgupload";

        @Override
        protected void onPreExecute() {
            p.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                MultipartEntity reqEntity = getMultiPart();
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                final String response_str = EntityUtils.toString(resEntity);
                if (resEntity != null) {
                    Log.i("RESPONSE", response_str);
                    return true;
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                Toast.makeText(getApplicationContext(), "Upload Complete", Toast.LENGTH_LONG).show();
                postData();
            } else {
                Global.dismissProgress(p);
                Toast.makeText(getApplicationContext(), "Upload InComplete", Toast.LENGTH_LONG).show();
            }
        }

        public MultipartEntity getMultiPart() {
            MultipartEntity reqEntity = new MultipartEntity();
            ArrayList<GetCustDetailsModel.Jobmedialist> fileList = details.getJobmedialist();
            int i = 1;
            for (GetCustDetailsModel.Jobmedialist media : fileList) {
                if (TextUtils.isEmpty(media.getMediaContent())) {
                    FileBody fileBody = new FileBody(media.getFile());
                    String key = String.format("postjob%s", i);
                    reqEntity.addPart(key, fileBody);
                    i = i + 1;
                }
            }
            return reqEntity;
        }

        private void postData() {

            ArrayList<AddNewFile.Media> mediaList = new ArrayList<>();
            for (GetCustDetailsModel.Jobmedialist jobmedialist : details.getJobmedialist()) {
                if (TextUtils.isEmpty(jobmedialist.getMediaContent())) {
                    AddNewFile.Media media = new AddNewFile.Media(jobmedialist.getMediaType(), jobmedialist.getFile().getName());
                    mediaList.add(media);
                }
            }

            AddNewFile addNewFile = new AddNewFile(USER_ID, details.getJobpostID(), mediaList);
            Call<AddNewFile.Result> call = apiService.addFilesToJob(addNewFile);
            call.enqueue(new Callback<AddNewFile.Result>() {
                @Override
                public void onResponse(Call<AddNewFile.Result> call, Response<AddNewFile.Result> response) {
                    Global.dismissProgress(p);
                    if (response.isSuccessful()) {
                        AddNewFile.Result result = response.body();
                        if (result.getResult().equalsIgnoreCase("Success")) {
                            Toast.makeText(CusBidDetailsActivity.this, "New Files Added", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(CusBidDetailsActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("fragPosition", 1);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddNewFile.Result> call, Throwable t) {
                    Global.dismissProgress(p);
                }
            });
        }
    }
}
