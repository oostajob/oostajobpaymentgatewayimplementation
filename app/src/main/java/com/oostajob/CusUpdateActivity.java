package com.oostajob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.dialog.PasswordChangeFragment;
import com.oostajob.dialog.PhotoDialog;
import com.oostajob.global.Global;
import com.oostajob.model.CustomerUpdateModel;
import com.oostajob.model.TimeZone;
import com.oostajob.utils.FilePath;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CusUpdateActivity extends AppCompatActivity implements AutoTextWatcher.TextWatcher, ImageListener {

    private Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
    private CustomButton btnRegister;
    private Toolbar toolbar;
    private ImageView imgProfile, name_add, email_add, address_add, phone_add;
    private CustomEditText edtName, edtEmail, edtPhone;
    private TextView edtAddress;
    private APIService apiService, googleService;

    private ImageView imgBack;

    private String USER_ID, NAME, EMAIL, PHONE, ADDRESS, PASSWORD, PROFILE, ZIP_CODE, LAT = "0.0", LNG = "0.0";

    //Picture uri
    private Uri selectedPicUri;
    private Uri croppedPicUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus_update);
        apiService = RetrofitSingleton.createService(APIService.class);
        googleService = RetrofitSingleton.createGoogleService(APIService.class);
        if (savedInstanceState != null) {
            selectedPicUri = Uri.parse(savedInstanceState.getString("selectedPicUri", ""));
            croppedPicUri = Uri.parse(savedInstanceState.getString("croppedPicUri", ""));
        }
        init();
        setUpToolBar();
        setListener();

        loadPrefValues();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edtName = (CustomEditText) findViewById(R.id.edtName);
        edtEmail = (CustomEditText) findViewById(R.id.edtEmail);
        edtAddress = (TextView) findViewById(R.id.edtAddress);
        edtPhone = (CustomEditText) findViewById(R.id.edtPhone);
        name_add = (ImageView) findViewById(R.id.name_add);
        email_add = (ImageView) findViewById(R.id.email_add);
        address_add = (ImageView) findViewById(R.id.address_add);
        phone_add = (ImageView) findViewById(R.id.phone_add);
        btnRegister = (CustomButton) findViewById(R.id.btnRegister);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        if (!Global.isUriEmpty(croppedPicUri)) {
            imgProfile.setImageURI(croppedPicUri);
        }

        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (!Global.isUriEmpty(selectedPicUri))
            outState.putString("selectedPicUri", selectedPicUri.toString());
        if (!Global.isUriEmpty(croppedPicUri))
            outState.putString("croppedPicUri", croppedPicUri.toString());
        super.onSaveInstanceState(outState);
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void loadPrefValues() {
        USER_ID = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.USER_ID, "");
        EMAIL = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.EMAIL, "");
        PASSWORD = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.EMAIL, "");
        NAME = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.NAME, "");
        ADDRESS = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.ADDRESS, "");
        PHONE = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.PHONE, "");
        ZIP_CODE = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.ZIPCODE, "");
        LAT = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.LAT, "");
        LNG = PrefConnect.readString(CusUpdateActivity.this, PrefConnect.LNG, "");

        croppedPicUri = Uri.parse(PrefConnect.readString(CusUpdateActivity.this, PrefConnect.PROFILE_PHOTO, ""));
        PROFILE = croppedPicUri.getLastPathSegment();

        edtName.setText(NAME);
        edtEmail.setText(EMAIL);
        edtAddress.setText(ADDRESS);
        edtPhone.setText(PHONE);
        if (!Global.isUriEmpty(croppedPicUri)) {
            String url = String.format("%s/%s", Global.BASE_URL, croppedPicUri.toString());
            Picasso.with(this).load(url)/*.placeholder(R.drawable.user)*/.fit().into(imgProfile);// removing placeholder , since there is already a placeholder image
        }
        setTickMark(address_add);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_password, menu);
        CustomTextView textView = (CustomTextView) menu.findItem(R.id.action_password).getActionView();
        textView.setText(R.string.change_password);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordChangeFragment fragment = new PasswordChangeFragment();
                fragment.show(getSupportFragmentManager(), "Change Password");
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_password:
                PasswordChangeFragment fragment = new PasswordChangeFragment();
                fragment.show(getSupportFragmentManager(), "Change Password");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(CusUpdateActivity.this, SelectAddrActivity.class);
                mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mapIntent.putExtra("title", "Select Address");
                mapIntent.putExtra("goto", "");
                mapIntent.putExtra("lat", Double.parseDouble(LAT));
                mapIntent.putExtra("lng", Double.parseDouble(LNG));
                startActivityForResult(mapIntent, Global.ADDRESS);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    new UpdateTask().execute();
                }

            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString("title", "Picture");
                if (!Global.isUriEmpty(croppedPicUri) || !TextUtils.isEmpty(PROFILE)) {
                    args.putBoolean("isRemove", true);
                }
                photoDialog.setArguments(args);
                photoDialog.setCancelable(false);
                photoDialog.setImageListener(CusUpdateActivity.this);
                photoDialog.show(getSupportFragmentManager(), "Photo Dialog");
            }
        });


        edtName.addTextChangedListener(new AutoTextWatcher(R.id.edtName, this));
        edtEmail.addTextChangedListener(new AutoTextWatcher(R.id.edtEmail, this));
        edtPhone.addTextChangedListener(new AutoTextWatcher(R.id.edtPhone, this));
    }


    @Override
    public void afterTextChanged(int view, Editable s) {
        switch (view) {
            case R.id.edtName:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(name_add);
                else
                    setTickMark(name_add);
                break;
            case R.id.edtEmail:
                Matcher matcher = pattern.matcher(s.toString());
                if (TextUtils.isEmpty(s.toString()) || !matcher.matches())
                    setPlusMark(email_add);
                else
                    setTickMark(email_add);
                break;
            case R.id.edtPhone:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(phone_add);
                else
                    setTickMark(phone_add);
                break;
        }
    }

    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    try {
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();
                        Bundle extras = data.getExtras();
                        selectedPicUri = Global.getImageUri(this.getApplicationContext(), (Bitmap) extras.getParcelable("data"));

                        Global.doCrop(CusUpdateActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK:
                    try {
                        selectedPicUri = data.getData();
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();
                        Global.doCrop(CusUpdateActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.PICTURE_CROP:
                    if (!Global.isUriEmpty(croppedPicUri)) {
                        imgProfile.setImageURI(croppedPicUri);
                    } else {
                        Bundle extras = data.getExtras();
                        Bitmap thePic = extras.getParcelable("data");
                        imgProfile.setImageBitmap(thePic);
                        Global.saveImage(CusUpdateActivity.this, thePic, croppedPicUri);
                    }
                    PROFILE = new File(FilePath.getPath(CusUpdateActivity.this, croppedPicUri)).getName();
                    break;
                case Global.ADDRESS:
                    ADDRESS = data.getStringExtra("address");
                    ZIP_CODE = data.getStringExtra("postalCode");
                    LAT = data.getStringExtra("lat");
                    LNG = data.getStringExtra("lng");
                    edtAddress.setText(ADDRESS);
                    setTickMark(address_add);
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    if (!Global.isUriEmpty(selectedPicUri)) {
                        //Global.deleteFile(getApplicationContext(), selectedPicUri);
                        selectedPicUri = null;
                    }
                    croppedPicUri = null;
                    break;
                case Global.PICTURE_CROP:
                    croppedPicUri = null;
                    selectedPicUri = null;
                    break;

            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            //handleCropError(data);
            Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
            croppedPicUri = null;
        }
    }

    private boolean isValidated() {
        if (TextUtils.isEmpty(edtName.getText().toString())) {
            Toast.makeText(CusUpdateActivity.this, "Enter name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            Toast.makeText(CusUpdateActivity.this, "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtEmail.getText().toString())) {
            Matcher matcher = pattern.matcher(edtEmail.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(CusUpdateActivity.this, "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (edtAddress.getText().toString().equalsIgnoreCase("Address")) {
            Toast.makeText(CusUpdateActivity.this, "Select Address", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            Toast.makeText(CusUpdateActivity.this, "Enter Phone Number", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            long phone = Long.parseLong(edtPhone.getText().toString().trim());
            if (phone == 0 || edtPhone.getText().toString().trim().length() != 10) {
                Toast.makeText(CusUpdateActivity.this, "Enter Valid Phone number", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        NAME = edtName.getText().toString();
        EMAIL = edtEmail.getText().toString();
        ADDRESS = edtAddress.getText().toString();
        PHONE = edtPhone.getText().toString();
        return true;
    }

    private void gotoHome() {
        Intent intent = new Intent(CusUpdateActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onImageSelected(Uri uri, boolean isLicense) {
        try {
            if (!isLicense) {
                selectedPicUri = uri;
                if (Global.isUriEmpty(selectedPicUri)) {
                    imgProfile.setImageDrawable(null);
                    PROFILE = "";
                    croppedPicUri = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveData(CustomerUpdateModel.CustomerDetails details) {
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.EMAIL, details.getEmail());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.PASSWORD, details.getPassword());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.USER_TYPE, details.getUserType());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.NAME, details.getName());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.ADDRESS, details.getAddress());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.PHONE, details.getPhone());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.PROFILE_PHOTO, details.getProfilePhoto());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.ZIPCODE, details.getZipcode());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.LAT, details.getLat());
        PrefConnect.writeString(CusUpdateActivity.this, PrefConnect.LNG, details.getLng());
    }

    private class UpdateTask extends AsyncTask<String, Void, String> {
        HttpEntity resEntity;
        String url = Global.BASE_URL + "/api/index.php/profileimg";
        ProgressDialog p;

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(CusUpdateActivity.this);
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.setMessage(getString(R.string.loading));
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (!Global.isUriEmpty(croppedPicUri)) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url);
                    MultipartEntity reqEntity = getMultiPart();
                    post.setEntity(reqEntity);
                    HttpResponse response = client.execute(post);
                    resEntity = response.getEntity();
                    final String response_str = EntityUtils.toString(resEntity);
                    if (resEntity != null) {
                        Log.i("RESPONSE", response_str);
                    }
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            updateProfile(p);
        }

        public MultipartEntity getMultiPart() {
            MultipartEntity reqEntity = new MultipartEntity();
            File file = new File(FilePath.getPath(CusUpdateActivity.this, croppedPicUri));
            FileBody fileBody = new FileBody(file);
            reqEntity.addPart("profile", fileBody);
            return reqEntity;
        }

        private void updateProfile(final ProgressDialog p) {
            String location = String.format("%s,%s", LAT, LNG);
            //Getting TimeZone
            Call<TimeZone> call = googleService.getTimeZone(location, System.currentTimeMillis() / 1000,"AIzaSyCp77NabT4Yj4SUCXT7aG8eRh5I4mtki9M");
            call.enqueue(new Callback<TimeZone>() {
                @Override
                public void onResponse(Call<TimeZone> call, Response<TimeZone> response) {
                    if (response.isSuccessful()) {
                        TimeZone timeZone = response.body();
                        PROFILE = TextUtils.isEmpty(PROFILE) ? "" : PROFILE;
                        CustomerUpdateModel.Request request = new CustomerUpdateModel.Request(USER_ID, NAME, ADDRESS, PHONE, PROFILE, ZIP_CODE,
                                LAT, LNG, EMAIL, PASSWORD, timeZone.getTimeZoneId());
                        Call<CustomerUpdateModel.Response> upCall = apiService.cusUpdate(request);
                        upCall.enqueue(new Callback<CustomerUpdateModel.Response>() {
                            @Override
                            public void onResponse(Call<CustomerUpdateModel.Response> call, Response<CustomerUpdateModel.Response> response) {
                                Global.dismissProgress(p);
                                if (response.isSuccessful()) {
                                    CustomerUpdateModel.Response response1 = response.body();
                                    if (response1.getResult().equalsIgnoreCase("Success")) {
                                        CustomerUpdateModel.CustomerDetails details = response1.getCustomerDetails().get(0);
                                        saveData(details);
                                        gotoHome();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CustomerUpdateModel.Response> call, Throwable t) {
                                Global.dismissProgress(p);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<TimeZone> call, Throwable t) {
                    Global.dismissProgress(p);
                }
            });

        }
    }

}
