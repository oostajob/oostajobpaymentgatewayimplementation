package com.oostajob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomCheckBox;
import com.Green.customfont.CustomEditText;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.dialog.PhotoDialog;
import com.oostajob.dialog.TermsDialogFragment;
import com.oostajob.global.Global;
import com.oostajob.model.CusRegModel;
import com.oostajob.model.CusRegModel.Request;
import com.oostajob.model.TimeZone;
import com.oostajob.utils.FilePath;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.yalantis.ucrop.UCrop;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CusRegActivity extends AppCompatActivity implements AutoTextWatcher.TextWatcher, ImageListener {

    CustomButton btnRegister;
    private Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
    private String GCM_TOKEN;
    private ProgressDialog p;
    private Toolbar toolbar;
    private ImageView imgProfile, name_add, email_add, password_add, address_add, phone_add;
    private CustomEditText edtName, edtEmail, edtPassword, edtPhone;
    private TextView edtAddress, txtTerms;
    private CustomCheckBox chkAgree;
    private String postalCode, lat = "0.0", lng = "0.0";
    private APIService apiService, googleService;
    //Picture uri
    private Uri selectedPicUri;
    private Uri croppedPicUri;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus_reg);

        apiService = RetrofitSingleton.createService(APIService.class);
        googleService = RetrofitSingleton.createGoogleService(APIService.class);
        GCM_TOKEN = PrefConnect.readString(CusRegActivity.this, PrefConnect.GCM_TOKEN, "");
        if (savedInstanceState != null) {
            selectedPicUri = Uri.parse(savedInstanceState.getString("selectedPicUri", ""));
            croppedPicUri = Uri.parse(savedInstanceState.getString("croppedPicUri", ""));
        }
        init();
        setUpToolBar();
        setListener();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edtName = (CustomEditText) findViewById(R.id.edtName);
        edtEmail = (CustomEditText) findViewById(R.id.edtEmail);
        edtPassword = (CustomEditText) findViewById(R.id.edtPassword);
        edtAddress = (TextView) findViewById(R.id.edtAddress);
        edtPhone = (CustomEditText) findViewById(R.id.edtPhone);
        name_add = (ImageView) findViewById(R.id.name_add);
        email_add = (ImageView) findViewById(R.id.email_add);
        password_add = (ImageView) findViewById(R.id.password_add);
        address_add = (ImageView) findViewById(R.id.address_add);
        phone_add = (ImageView) findViewById(R.id.phone_add);
        btnRegister = (CustomButton) findViewById(R.id.btnRegister);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        txtTerms = (TextView) findViewById(R.id.txt_cus_Terms);
        chkAgree = (CustomCheckBox) findViewById(R.id.chkAgree);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        p = new ProgressDialog(CusRegActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);

        if (!Global.isUriEmpty(croppedPicUri)) {
            imgProfile.setImageURI(croppedPicUri);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (!Global.isUriEmpty(selectedPicUri))
            outState.putString("selectedPicUri", selectedPicUri.toString());
        if (!Global.isUriEmpty(croppedPicUri))
            outState.putString("croppedPicUri", croppedPicUri.toString());
        super.onSaveInstanceState(outState);
    }


    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(CusRegActivity.this, SelectAddrActivity.class);
                mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mapIntent.putExtra("title", "Select Address");
                mapIntent.putExtra("goto", "");
                mapIntent.putExtra("lat", Double.parseDouble(lat));
                mapIntent.putExtra("lng", Double.parseDouble(lng));
                startActivityForResult(mapIntent, Global.ADDRESS);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    if (chkAgree.isChecked()) {
                        new RegisterTask().execute();
//                        register();
                    } else {
                        Toast.makeText(CusRegActivity.this, getString(R.string.accept_terms), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString("title", "Picture");
                if (!Global.isUriEmpty(croppedPicUri)) {
                    args.putBoolean("isRemove", true);
                }
                photoDialog.setArguments(args);
                photoDialog.setCancelable(false);
                photoDialog.setImageListener(CusRegActivity.this);
                photoDialog.show(getSupportFragmentManager(), "Photo Dialog");
            }
        });

        txtTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsDialogFragment fragment = new TermsDialogFragment();
                Bundle args = new Bundle();
                args.putInt("dialog_id", 1);
                fragment.setArguments(args);
                fragment.show(getSupportFragmentManager(), "Terms Dialog");

            }
        });
        edtName.addTextChangedListener(new AutoTextWatcher(R.id.edtName, this));
        edtEmail.addTextChangedListener(new AutoTextWatcher(R.id.edtEmail, this));
        edtPassword.addTextChangedListener(new AutoTextWatcher(R.id.edtPassword, this));
        edtPhone.addTextChangedListener(new AutoTextWatcher(R.id.edtPhone, this));
    }

    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }

    private boolean isValidated() {
        if (TextUtils.isEmpty(edtName.getText().toString())) {
            Toast.makeText(CusRegActivity.this, "Enter name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            Toast.makeText(CusRegActivity.this, "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtEmail.getText().toString().trim());
            if (!matcher.matches()) {
                Toast.makeText(CusRegActivity.this, "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (TextUtils.isEmpty(edtPassword.getText())) {
            Toast.makeText(CusRegActivity.this, "Enter password", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtPassword.getText().length() < 10) { //minimum 10 character needed
            Toast.makeText(CusRegActivity.this, R.string.password_not_valid, Toast.LENGTH_LONG).show();
            return false;
        }

        if (edtAddress.getText().toString().equalsIgnoreCase("Address")) {
            Toast.makeText(CusRegActivity.this, "Select Address", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            Toast.makeText(CusRegActivity.this, "Enter Phone Number", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            long phone = Long.parseLong(edtPhone.getText().toString().trim());
            if (phone == 0 || edtPhone.getText().toString().trim().length() != 10) {
                Toast.makeText(CusRegActivity.this, "Enter Valid Phone number", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    private void startVerificationActivity() {
        Intent intent = new Intent(CusRegActivity.this, VerificationActivity.class);
        if (PrefConnect.readString(CusRegActivity.this, PrefConnect.SKIP, "").equalsIgnoreCase("skip_login")) {
            /*intent.putExtra("subTitle", getIntent().getStringExtra("subTitle"));
            intent.putExtra("imgUrl", getIntent().getStringExtra("imgUrl"));
            intent.putExtra("jobTypeId", getIntent().getStringExtra("jobTypeId"));*/
            intent.putExtra("list", getIntent().getSerializableExtra("list"));
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void afterTextChanged(int view, Editable s) {
        switch (view) {
            case R.id.edtName:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(name_add);
                else
                    setTickMark(name_add);
                break;
            case R.id.edtEmail:
                Matcher matcher = pattern.matcher(s.toString());
                if (TextUtils.isEmpty(s.toString()) || !matcher.matches())
                    setPlusMark(email_add);
                else
                    setTickMark(email_add);
                break;
            case R.id.edtPassword:

                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    edtPassword.setText(result);
                    edtPassword.setSelection(result.length());
                    // alert the user
                }

                if (TextUtils.isEmpty(result) || result.length() < 10) // password must have at least 10 characters
                    setPlusMark(password_add);
                else
                    setTickMark(password_add);
                break;
            case R.id.edtPhone:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(phone_add);
                else
                    setTickMark(phone_add);
                break;
        }
    }

    @Override
    public void onImageSelected(Uri uri, boolean isLicense) {
        try {
            if (!isLicense) {
                selectedPicUri = uri;
                if (Global.isUriEmpty(selectedPicUri)) {
                    imgProfile.setImageDrawable(null);
                    croppedPicUri = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    try {
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();

                        Bundle extras = data.getExtras();
                        selectedPicUri = Global.getImageUri(this.getApplicationContext(), (Bitmap) extras.getParcelable("data"));

                        Global.doCrop(CusRegActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK:
                    try {
                        selectedPicUri = data.getData();
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();
                        Global.doCrop(CusRegActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.PICTURE_CROP:
                    croppedPicUri = UCrop.getOutput(data);
                    if (!Global.isUriEmpty(croppedPicUri)) {
                        imgProfile.setImageURI(croppedPicUri);
                    } else {
                        Bundle extras = data.getExtras();
                        Bitmap thePic = null;
                        if (extras != null) {
                            thePic = extras.getParcelable("data");
                            imgProfile.setImageBitmap(thePic);
                        }
                        Global.saveImage(CusRegActivity.this, thePic, croppedPicUri);
                    }
                    break;
                case Global.ADDRESS:
                    edtAddress.setText(data.getStringExtra("address"));
                    postalCode = data.getStringExtra("postalCode");
                    lat = data.getStringExtra("lat");
                    lng = data.getStringExtra("lng");
                    setTickMark(address_add);
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    if (!Global.isUriEmpty(selectedPicUri)) {
                        //Global.deleteFile(getApplicationContext(), selectedPicUri);
                        selectedPicUri = null;
                    }
                    croppedPicUri = null;
                    break;
                case Global.PICTURE_CROP:
                    croppedPicUri = null;
                    selectedPicUri = null;
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            //handleCropError(data);
            Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
            croppedPicUri = null;
        }
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        /*final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("TAG", "handleCropError: ", cropError);
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {*/
        Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
        //}
    }

    private void saveData(CusRegModel.CustomerDetails details) {
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.USER_ID, details.getUserID());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.EMAIL, details.getEmail());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.PASSWORD, details.getPassword());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.USER_TYPE, details.getUserType());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.NAME, details.getName());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.ADDRESS, details.getAddress());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.PHONE, details.getPhone());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.PROFILE_PHOTO, details.getProfilePhoto());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.ZIPCODE, details.getZipcode());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.LAT, details.getLat());
        PrefConnect.writeString(CusRegActivity.this, PrefConnect.LNG, details.getLng());
        PrefConnect.writeInteger(CusRegActivity.this, PrefConnect.PAGE, 1);
    }

    private class RegisterTask extends AsyncTask<String, Void, String> {
        HttpEntity resEntity;
        String url = Global.BASE_URL + "/api/index.php/profileimg";

        @Override
        protected void onPreExecute() {
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (!Global.isUriEmpty(croppedPicUri)) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url);
                    MultipartEntity reqEntity = getMultiPart();
                    post.setEntity(reqEntity);
                    HttpResponse response = client.execute(post);
                    resEntity = response.getEntity();
                    String response_str = EntityUtils.toString(resEntity);
                    if (resEntity != null) {
                        Log.i("RESPONSE", response_str);
                        return response_str;
                    }
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (new JSONObject(s).getString("Result").equalsIgnoreCase("Success")) {
                    registerProfile(p, !Global.isUriEmpty(croppedPicUri) ?
                            new File(FilePath.getPath(CusRegActivity.this, croppedPicUri)).getName() : "");
                } else {
                    Toast.makeText(CusRegActivity.this, "Photo not uploaded", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                registerProfile(p, s);
            }
        }

        public MultipartEntity getMultiPart() {
            MultipartEntity reqEntity = new MultipartEntity();
            File file = new File(FilePath.getPath(CusRegActivity.this, croppedPicUri));
            FileBody fileBody = new FileBody(file);
            reqEntity.addPart("profile", fileBody);
            return reqEntity;
        }

        private void registerProfile(final ProgressDialog p, final String fileName) {
            String location = String.format("%s,%s", lat, lng);
            //############ Get TimeZone form Google API (e.g, Asia/Calcutta)
            Call<TimeZone> call = googleService.getTimeZone(location, System.currentTimeMillis() / 1000,"AIzaSyCp77NabT4Yj4SUCXT7aG8eRh5I4mtki9M");
            call.enqueue(new Callback<TimeZone>() {
                @Override
                public void onResponse(Call<TimeZone> call, Response<TimeZone> response) {
                    if (response.isSuccessful()) {
                        TimeZone timeZone = response.body();
                        if (timeZone.getStatus().equalsIgnoreCase("ok")) {
                            Request request = new Request(edtName.getText().toString(), edtAddress.getText().toString(),
                                    edtPhone.getText().toString(), fileName, postalCode, lat, lng, GCM_TOKEN, "1", edtEmail.getText().toString(),
                                    edtPassword.getText().toString(), timeZone.getTimeZoneId());
                            //############ Registering user information
                            Call<CusRegModel.Response> regCall = apiService.cusRegister(request);
                            regCall.enqueue(new Callback<CusRegModel.Response>() {
                                @Override
                                public void onResponse(Call<CusRegModel.Response> call, Response<CusRegModel.Response> response) {
                                    Global.dismissProgress(p);
                                    croppedPicUri = null;
                                    if (response.isSuccessful()) {
                                        CusRegModel.Response response1 = response.body();
                                        if (response1.getResult().equalsIgnoreCase("Success")) {
                                            Toast.makeText(CusRegActivity.this, "Registered", Toast.LENGTH_LONG).show();
                                            CusRegModel.CustomerDetails details = response1.getCustomerDetails().get(0);
                                            saveData(details);
                                            startVerificationActivity();
                                        } else {
                                            Toast.makeText(CusRegActivity.this, response1.getStatus(), Toast.LENGTH_LONG).show();
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<CusRegModel.Response> call, Throwable t) {
                                    Log.e("LOGMSG", t.getMessage());
                                    croppedPicUri = null;
                                    Global.dismissProgress(p);
                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<TimeZone> call, Throwable t) {
                    Global.dismissProgress(p);
                }
            });
        }
    }
}
