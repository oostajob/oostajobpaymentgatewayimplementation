package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * @author Admin on 14-Mar-16.
 */
public class AddNewFile {

    @Expose
    public String userID;
    @Expose
    public String jobID;
    @Expose
    public ArrayList<Media> media;

    public AddNewFile(String userID, String jobID, ArrayList<Media> media) {
        this.userID = userID;
        this.jobID = jobID;
        this.media = media;
    }

    public static class Media {
        @Expose
        public String mediaType;
        @Expose
        public String mediaContent;

        public Media(String mediaType, String mediaContent) {
            this.mediaType = mediaType;
            this.mediaContent = mediaContent;
        }
    }

    public class Result {
        @Expose
        public String Result;

        public String getResult() {
            return Result;
        }
    }
}
