package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mathan on 28/11/2015.
 */
public class CustomerUpdateModel {

    public static class Request {
        @Expose
        public String userID;
        @Expose
        public String Name;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String profilePhoto;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String Email;
        @Expose
        public String Password;
        @Expose
        public String timezone;

        public Request(String userID, String Name, String Address, String Phone, String profilePhoto, String Zipcode, String Lat, String Lng,
                       String Email, String Password, String timezone) {
            this.userID = userID;
            this.Name = Name;
            this.Address = Address;
            this.Phone = Phone;
            this.profilePhoto = profilePhoto;
            this.Zipcode = Zipcode;
            this.Lat = Lat;
            this.Lng = Lng;

            this.Email = Email;
            this.Password = Password;
            this.timezone = timezone;

        }

    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<CustomerDetails> CustomerDetails;

        public ArrayList<CustomerUpdateModel.CustomerDetails> getCustomerDetails() {
            return CustomerDetails;
        }

        public void setCustomerDetails(ArrayList<CustomerUpdateModel.CustomerDetails> customerDetails) {
            CustomerDetails = customerDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }


    }

    public class CustomerDetails {
        @Expose
        public String userID;
        @Expose
        public String Email;
        @Expose
        public String Password;
        @Expose
        public String userType;
        @Expose
        public String verificationCode;
        @Expose
        public String verifycationStatus;
        @Expose
        public String profileStatus;
        @Expose
        public String createdTime;
        @Expose
        public String updatedTime;
        @Expose
        public String customerID;
        @Expose
        public String Name;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String profilePhoto;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String notificationID;
        @Expose
        public String deviceType;

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getVerificationCode() {
            return verificationCode;
        }

        public void setVerificationCode(String verificationCode) {
            this.verificationCode = verificationCode;
        }

        public String getVerifycationStatus() {
            return verifycationStatus;
        }

        public void setVerifycationStatus(String verifycationStatus) {
            this.verifycationStatus = verifycationStatus;
        }

        public String getProfileStatus() {
            return profileStatus;
        }

        public void setProfileStatus(String profileStatus) {
            this.profileStatus = profileStatus;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getUpdatedTime() {
            return updatedTime;
        }

        public void setUpdatedTime(String updatedTime) {
            this.updatedTime = updatedTime;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getNotificationID() {
            return notificationID;
        }

        public void setNotificationID(String notificationID) {
            this.notificationID = notificationID;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }
    }

}
