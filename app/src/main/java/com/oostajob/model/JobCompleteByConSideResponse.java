package com.oostajob.model;

import com.google.gson.annotations.Expose;

public class JobCompleteByConSideResponse {
    @Expose
    private String Result;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }
}
