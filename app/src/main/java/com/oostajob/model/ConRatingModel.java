package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mani on 11/28/2015.
 */
public class ConRatingModel {

    public static class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<RatingDetails> RatingDetails;

        public ArrayList<ConRatingModel.RatingDetails> getRatingDetails() {
            return RatingDetails;
        }

        public void setRatingDetails(ArrayList<ConRatingModel.RatingDetails> ratingDetails) {
            RatingDetails = ratingDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }
    }

    public static class RatingDetails {
        @Expose
        public String jobtypeName;
        @Expose
        public ArrayList<customerDetails> customerDetails;
        @Expose
        public String ratingTime;
        @Expose
        public String comment;
        @Expose
        public String ratingOverall;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public ArrayList<ConRatingModel.customerDetails> getCustomerDetails() {
            return customerDetails;
        }

        public void setCustomerDetails(ArrayList<ConRatingModel.customerDetails> customerDetails) {
            this.customerDetails = customerDetails;
        }

        public String getJobtypeName() {
            return jobtypeName;
        }

        public void setJobtypeName(String jobtypeName) {
            this.jobtypeName = jobtypeName;
        }

        public String getRatingOverall() {
            return ratingOverall;
        }

        public void setRatingOverall(String ratingOverall) {
            this.ratingOverall = ratingOverall;
        }

        public String getRatingTime() {
            return ratingTime;
        }

        public void setRatingTime(String ratingTime) {
            this.ratingTime = ratingTime;
        }
    }

    public static class customerDetails {
        @Expose
        public String profilePhoto;
        @Expose
        public String username;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String Zipcode;

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }
    }

}
