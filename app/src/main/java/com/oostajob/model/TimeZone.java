package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 26-11-2015.
 */

public class TimeZone {

    @Expose
    public int dstOffset;
    @Expose
    public int rawOffset;
    @Expose
    public String status;
    @Expose
    public String timeZoneId;
    @Expose
    public String timeZoneName;

    public int getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(int dstOffset) {
        this.dstOffset = dstOffset;
    }

    public int getRawOffset() {
        return rawOffset;
    }

    public void setRawOffset(int rawOffset) {
        this.rawOffset = rawOffset;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }
}
