package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class JobIcons {

    @SerializedName("messageCount")
    private String messageCount;

    @SerializedName("Address")
    private String address;

    @SerializedName("description")
    private String description;

    @SerializedName("jobtypeName")
    private String jobtypeName;

    @SerializedName("jobtype_id")
    private String jobtypeId;

    @SerializedName("job_icon")
    private String jobIcon;


    @SerializedName("closedTime")
    private String closedTime;

    @SerializedName("jobpostTime")
    private String jobpostTime;

    @SerializedName("extend_time")
    private String extendTime;

    @SerializedName("ratedTime")
    private String ratedTime;

    @SerializedName("jobsubtypeID")
    private String jobsubtypeID;

    @SerializedName("createdTime")
    private String createdTime;

    @SerializedName("customerJobStatus")
    private String customerJobStatus;

    @SerializedName("lat")
    private String lat;

    @SerializedName("hiredTimed")
    private String hiredTimed;

    @SerializedName("updatedTime")
    private String updatedTime;

    @SerializedName("lng")
    private String lng;

    @SerializedName("notificationCount")
    private String notificationCount;

    @SerializedName("job_image")
    private String jobImage;

    @SerializedName("zipcode")
    private String zipcode;

    @SerializedName("is_customer_completed")
    private String isCustomerCompleted;

    @SerializedName("jobpostID")
    private String jobpostID;

    @SerializedName("jobshortisedTime")
    private String jobshortisedTime;

    @SerializedName("job_name")
    private String jobName;

    @SerializedName("customerID")
    private String customerID;

    @SerializedName("jobtypeID")
    private String jobtypeID;

    @SerializedName("rate_staus")
    private String rateStaus;

    @SerializedName("is_contractor_completed")
    private String isContractorCompleted;

    @SerializedName("contractor_remarks")
    private String contractorRemarks;

    public void setMessageCount(String messageCount) {
        this.messageCount = messageCount;
    }

    public String getMessageCount() {
        return messageCount;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setJobtypeName(String jobtypeName) {
        this.jobtypeName = jobtypeName;
    }

    public String getJobtypeName() {
        return jobtypeName;
    }

    public void setJobtypeId(String jobtypeId) {
        this.jobtypeId = jobtypeId;
    }

    public String getJobtypeId() {
        return jobtypeId;
    }

    public void setJobIcon(String jobIcon) {
        this.jobIcon = jobIcon;
    }

    public String getJobIcon() {
        return jobIcon;
    }


    public void setClosedTime(String closedTime) {
        this.closedTime = closedTime;
    }

    public String getClosedTime() {
        return closedTime;
    }

    public void setJobpostTime(String jobpostTime) {
        this.jobpostTime = jobpostTime;
    }

    public String getJobpostTime() {
        return jobpostTime;
    }

    public void setExtendTime(String extendTime) {
        this.extendTime = extendTime;
    }

    public String getExtendTime() {
        return extendTime;
    }

    public void setRatedTime(String ratedTime) {
        this.ratedTime = ratedTime;
    }

    public String getRatedTime() {
        return ratedTime;
    }

    public void setJobsubtypeID(String jobsubtypeID) {
        this.jobsubtypeID = jobsubtypeID;
    }

    public String getJobsubtypeID() {
        return jobsubtypeID;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCustomerJobStatus(String customerJobStatus) {
        this.customerJobStatus = customerJobStatus;
    }

    public String getCustomerJobStatus() {
        return customerJobStatus;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    public void setHiredTimed(String hiredTimed) {
        this.hiredTimed = hiredTimed;
    }

    public String getHiredTimed() {
        return hiredTimed;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setNotificationCount(String notificationCount) {
        this.notificationCount = notificationCount;
    }

    public String getNotificationCount() {
        return notificationCount;
    }

    public void setJobImage(String jobImage) {
        this.jobImage = jobImage;
    }

    public String getJobImage() {
        return jobImage;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setIsCustomerCompleted(String isCustomerCompleted) {
        this.isCustomerCompleted = isCustomerCompleted;
    }

    public String getIsCustomerCompleted() {
        return isCustomerCompleted;
    }

    public void setJobpostID(String jobpostID) {
        this.jobpostID = jobpostID;
    }

    public String getJobpostID() {
        return jobpostID;
    }

    public void setJobshortisedTime(String jobshortisedTime) {
        this.jobshortisedTime = jobshortisedTime;
    }

    public String getJobshortisedTime() {
        return jobshortisedTime;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setJobtypeID(String jobtypeID) {
        this.jobtypeID = jobtypeID;
    }

    public String getJobtypeID() {
        return jobtypeID;
    }

    public void setRateStaus(String rateStaus) {
        this.rateStaus = rateStaus;
    }

    public String getRateStaus() {
        return rateStaus;
    }

    public void setIsContractorCompleted(String isContractorCompleted) {
        this.isContractorCompleted = isContractorCompleted;
    }

    public String getIsContractorCompleted() {
        return isContractorCompleted;
    }

    public void setContractorRemarks(String contractorRemarks) {
        this.contractorRemarks = contractorRemarks;
    }

    public String getContractorRemarks() {
        return contractorRemarks;
    }

    @Override
    public String toString() {
        return
                "JobIcons{" +
                        "messageCount = '" + messageCount + '\'' +
                        ",address = '" + address + '\'' +
                        ",description = '" + description + '\'' +
                        ",jobtypeName = '" + jobtypeName + '\'' +
                        ",jobtype_id = '" + jobtypeId + '\'' +
                        ",job_icon = '" + jobIcon + '\'' +
                        ",extendTime = '" + extendTime + '\'' +
                        ",closedTime = '" + closedTime + '\'' +
                        ",jobpostTime = '" + jobpostTime + '\'' +
                        ",extend_time = '" + extendTime + '\'' +
                        ",ratedTime = '" + ratedTime + '\'' +
                        ",jobsubtypeID = '" + jobsubtypeID + '\'' +
                        ",createdTime = '" + createdTime + '\'' +
                        ",customerJobStatus = '" + customerJobStatus + '\'' +
                        ",lat = '" + lat + '\'' +
                        ",hiredTimed = '" + hiredTimed + '\'' +
                        ",updatedTime = '" + updatedTime + '\'' +
                        ",lng = '" + lng + '\'' +
                        ",notificationCount = '" + notificationCount + '\'' +
                        ",job_image = '" + jobImage + '\'' +
                        ",zipcode = '" + zipcode + '\'' +
                        ",is_customer_completed = '" + isCustomerCompleted + '\'' +
                        ",jobpostID = '" + jobpostID + '\'' +
                        ",jobshortisedTime = '" + jobshortisedTime + '\'' +
                        ",job_name = '" + jobName + '\'' +
                        ",customerID = '" + customerID + '\'' +
                        ",jobtypeID = '" + jobtypeID + '\'' +
                        ",rate_staus = '" + rateStaus + '\'' +
                        ",is_contractor_completed = '" + isContractorCompleted + '\'' +
                        ",contractor_remarks = '" + contractorRemarks + '\'' +
                        "}";
    }
}