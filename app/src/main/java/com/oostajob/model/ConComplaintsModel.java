package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mani on 11/30/2015.
 */
public class ConComplaintsModel {

    public static class Response
    {
        @Expose
        public String Result;
        @Expose
        public ArrayList<complientsDetails> complientsDetails;

        public ArrayList<complientsDetails> getComplientsDetails() {
            return complientsDetails;
        }

        public void setComplientsDetails(ArrayList<complientsDetails> complientsDetails) {
            this.complientsDetails = complientsDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }
    }

    public static class complientsDetails
    {
        @Expose
        public ArrayList<customerDetails> customerDetails;
        @Expose
        public String complientsTime;
        @Expose
        public String comment;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getComplientsTime() {
            return complientsTime;
        }

        public void setComplientsTime(String complientsTime) {
            this.complientsTime = complientsTime;
        }

        public ArrayList<customerDetails> getCustomerDetails() {
            return customerDetails;
        }

        public void setCustomerDetails(ArrayList<customerDetails> customerDetails) {
            this.customerDetails = customerDetails;
        }
    }

    public static class customerDetails
    {
        @Expose
        public String profilePhoto;
        @Expose
        public String username;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String Zipcode;


        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }
    }
}
