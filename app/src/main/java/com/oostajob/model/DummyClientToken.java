package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by abhigitmusri on 30/01/18.
 */

public class DummyClientToken {
    /*
    * {
    "Result": "Success",
    "Detail": "Customer Id",
    "Response": "103174446"
}*/

    @Expose
    String Success;

    @Expose
    String Detail;

    @Expose
    String Response;


    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }
}
