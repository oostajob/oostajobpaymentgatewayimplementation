package com.oostajob.model;

import java.io.Serializable;

public class GetClientTokenWithCustomerID implements Serializable {

    private String braintreeID;

    public String getBraintreeID() {
        return braintreeID;
    }

    public void setBraintreeID(String braintreeID) {
        this.braintreeID = braintreeID;
    }
}
