package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class TotalEarnings {

    @SerializedName("total")
    private String total;

    @SerializedName("contractorID")
    private String contractorID;

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setContractorID(String contractorID) {
        this.contractorID = contractorID;
    }

    public String getContractorID() {
        return contractorID;
    }

    @Override
    public String toString() {
        return
                "TotalEarnings{" +
                        "total = '" + total + '\'' +
                        ",contractorID = '" + contractorID + '\'' +
                        "}";
    }
}