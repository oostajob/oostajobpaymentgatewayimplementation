package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class JobIconForPaymentHistory {

    @SerializedName("job_icon")
    private String jobicon;

    @SerializedName("Address")
    private String Address;

    public String getJobicon() {
        return jobicon;
    }

    public void setJobicon(String jobicon) {
        this.jobicon = jobicon;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
