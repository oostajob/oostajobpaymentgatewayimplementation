package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ConPaymentHistoryJobResponse {

    @SerializedName("response")
    private ArrayList<ResponseItem> response;

    @SerializedName("media")
    public ArrayList<GetCustDetailsModel.Jobmedialist> jobmedialist;

    @SerializedName("answer")
    public ArrayList<GetCustDetailsModel.JobAnwserlist> jobanwserlist;

    @SerializedName("customerdetails")
    public CustomerDetailsForPaymentHistory customerDetailsForPaymentHistory;

    @SerializedName("job_icons")
    public JobIconForPaymentHistory jobIconForPaymentHistory;


    public ArrayList<ResponseItem> getResponse() {
        return response;
    }

    public void setResponse(ArrayList<ResponseItem> response) {
        this.response = response;
    }

    public ArrayList<GetCustDetailsModel.Jobmedialist> getJobmedialist() {
        return jobmedialist;
    }

    public void setJobmedialist(ArrayList<GetCustDetailsModel.Jobmedialist> jobmedialist) {
        this.jobmedialist = jobmedialist;
    }

    public ArrayList<GetCustDetailsModel.JobAnwserlist> getJobanwserlist() {
        return jobanwserlist;
    }

    public void setJobanwserlist(ArrayList<GetCustDetailsModel.JobAnwserlist> jobanwserlist) {
        this.jobanwserlist = jobanwserlist;
    }

    public CustomerDetailsForPaymentHistory getCustomerDetailsForPaymentHistory() {
        return customerDetailsForPaymentHistory;
    }

    public void setCustomerDetailsForPaymentHistory(CustomerDetailsForPaymentHistory customerDetailsForPaymentHistory) {
        this.customerDetailsForPaymentHistory = customerDetailsForPaymentHistory;
    }

    public JobIconForPaymentHistory getJobIconForPaymentHistory() {
        return jobIconForPaymentHistory;
    }

    public void setJobIconForPaymentHistory(JobIconForPaymentHistory jobIconForPaymentHistory) {
        this.jobIconForPaymentHistory = jobIconForPaymentHistory;
    }
}
