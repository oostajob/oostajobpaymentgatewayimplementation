package com.oostajob.model;

public class JobCompletedByConInput {

    private String jobId;
    private String action_by;
    private String userID;
    private String loggedUserId;
    private String contractor_remarks;

    public String getContractor_remarks() {
        return contractor_remarks;
    }

    public void setContractor_remarks(String contractor_remarks) {
        this.contractor_remarks = contractor_remarks;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getAction_by() {
        return action_by;
    }

    public void setAction_by(String action_by) {
        this.action_by = action_by;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getLoggedUserId() {
        return loggedUserId;
    }

    public void setLoggedUserId(String loggedUserId) {
        this.loggedUserId = loggedUserId;
    }
}
