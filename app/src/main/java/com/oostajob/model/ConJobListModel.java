package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mathan on 28/11/2015.
 */
public class ConJobListModel {


    public static class Request {
        @Expose
        String zipcode;
        @Expose
        String expertise;

        public Request(String zipcode, String expertise) {
            this.zipcode = zipcode;
            this.expertise = expertise;
        }

    }

    public static class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<ContractorJoblist> Contractor_joblist;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<ContractorJoblist> getContractor_joblist() {
            return Contractor_joblist;
        }

        public void setContractor_joblist(ArrayList<ContractorJoblist> contractor_joblist) {
            Contractor_joblist = contractor_joblist;
        }
    }

    public class ContractorJoblist implements Serializable {
        @Expose
        public String jobpostID;
        @Expose
        public String jobtypeID;
        @Expose
        public String jobtypeName;
        @Expose
        public String description;
        @Expose
        public String jobsubtypeID;
        @Expose
        public String Address;
        @Expose
        public String zipcode;
        @Expose
        public String lat;
        @Expose
        public String lng;
        @Expose
        public String Miles;
        @Expose
        public String icon;
        @Expose
        public String job_image;
        @Expose
        public String Post_time;
        @Expose
        public String Remaining_time;
        @Expose
        public String minimumbidamount;
        @Expose
        public ArrayList<Media> jobmedialist;
        @Expose
        public ArrayList<Answer> jobanwserlist;

        public String getMinimumbidamount() {
            return minimumbidamount;
        }

        public String getJobpostID() {
            return jobpostID;
        }

        public void setJobpostID(String jobpostID) {
            this.jobpostID = jobpostID;
        }

        public String getJobtypeID() {
            return jobtypeID;
        }

        public void setJobtypeID(String jobtypeID) {
            this.jobtypeID = jobtypeID;
        }

        public String getJobtypeName() {
            return jobtypeName;
        }

        public void setJobtypeName(String jobtypeName) {
            this.jobtypeName = jobtypeName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getJobsubtypeID() {
            return jobsubtypeID;
        }

        public void setJobsubtypeID(String jobsubtypeID) {
            this.jobsubtypeID = jobsubtypeID;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getMiles() {
            return Miles;
        }

        public void setMiles(String miles) {
            Miles = miles;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getJob_image() {
            return job_image;
        }

        public void setJob_image(String job_image) {
            this.job_image = job_image;
        }

        public String getPost_time() {
            return Post_time;
        }

        public void setPost_time(String post_time) {
            Post_time = post_time;
        }

        public String getRemaining_time() {
            return Remaining_time;
        }

        public void setRemaining_time(String remaining_time) {
            Remaining_time = remaining_time;
        }

        public ArrayList<Media> getJobmedialist() {
            return jobmedialist;
        }

        public void setJobmedialist(ArrayList<Media> jobmedialist) {
            this.jobmedialist = jobmedialist;
        }

        public ArrayList<Answer> getJobanwserlist() {
            return jobanwserlist;
        }

        public void setJobanwserlist(ArrayList<Answer> jobanwserlist) {
            this.jobanwserlist = jobanwserlist;
        }
    }

    public class Media implements Serializable {
        @Expose
        public String mediaType;
        @Expose
        public String mediaContent;

        public String getMediaType() {
            return mediaType;
        }

        public void setMediaType(String mediaType) {
            this.mediaType = mediaType;
        }

        public String getMediaContent() {
            return mediaContent;
        }

        public void setMediaContent(String mediaContent) {
            this.mediaContent = mediaContent;
        }
    }

    public class Answer implements Serializable {
        @Expose
        public String answer_id;
        @Expose
        public String job_id;
        @Expose
        public String quest_name;
        @Expose
        public String answer;
        @Expose
        public String tag_key;
        @Expose
        public String tag_keyword;

        public String getTag_keyword() {
            return tag_keyword;
        }

        public String getAnswer_id() {
            return answer_id;
        }

        public void setAnswer_id(String answer_id) {
            this.answer_id = answer_id;
        }

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        public String getQuest_name() {
            return quest_name;
        }

        public void setQuest_name(String quest_name) {
            this.quest_name = quest_name;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getTag_key() {
            return tag_key;
        }

        public void setTag_key(String tag_key) {
            this.tag_key = tag_key;
        }
    }
}
