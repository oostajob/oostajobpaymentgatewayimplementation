package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mani on 11/28/2015.
 */
public class ComplaintsModel {

    public static class Request {
        @Expose
        public String userID;
        @Expose
        public String jobpostID;
        @Expose
        public String comment;

        public Request(String userID, String jobpostID, String comment) {
            this.userID = userID;
            this.jobpostID = jobpostID;
            this.comment = comment;
        }
    }

    public class Response {

        @Expose
        public String Result;

        @Expose
        public String ComplientsDetails;

        public String getComplientsDetails() {
            return ComplientsDetails;
        }

        public void setComplientsDetails(String complientsDetails) {
            ComplientsDetails = complientsDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

    }

}
