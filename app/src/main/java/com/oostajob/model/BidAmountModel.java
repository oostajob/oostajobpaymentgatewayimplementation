package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mathan on 27/11/2015.
 */
public class BidAmountModel {
    public static class Request{
        @Expose
        String userID;
        @Expose
        String  jobpostID;
        @Expose
        String  bidAmount;
        public Request(String userID, String jobpostID, String bidAmount) {
            this.userID = userID;
            this.jobpostID = jobpostID;
            this.bidAmount = bidAmount;
        }

    }
    public static class Response{
        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getJobDetails() {
            return JobDetails;
        }

        public void setJobDetails(String jobDetails) {
            JobDetails = jobDetails;
        }

        @Expose
      String  Result;
        @Expose
        String  JobDetails;
    }
}
