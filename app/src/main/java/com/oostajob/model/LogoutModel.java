package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 04-12-2015.
 */
public class LogoutModel {

    @Expose
    public String Result;
    @Expose
    public String LogoutStatus;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getLogoutStatus() {
        return LogoutStatus;
    }

    public void setLogoutStatus(String logoutStatus) {
        LogoutStatus = logoutStatus;
    }
}
