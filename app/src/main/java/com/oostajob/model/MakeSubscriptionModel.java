package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mani on 11/30/2015.
 */
public class MakeSubscriptionModel {
    public static class Request
    {
        @Expose
        public String userID;
        @Expose
        public String plan;

        public Request(String userID, String plan)
        {
            this.userID = userID;
            this.plan = plan;
        }
    }

    public static class Response
    {
        @Expose
        public String Result;
        @Expose
        public String PaymentDetails;

        public String getPaymentDetails() {
            return PaymentDetails;
        }

        public void setPaymentDetails(String paymentDetails) {
            PaymentDetails = paymentDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }
    }
}
