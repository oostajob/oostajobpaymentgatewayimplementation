package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ConBankDetails {

    @SerializedName("Result")
    private String Result;

    @SerializedName("Details")
    private String Details;

    @SerializedName("Response")
    private ArrayList<BankDetailsResponse> Response;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public ArrayList<BankDetailsResponse> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<BankDetailsResponse> response) {
        Response = response;
    }
}
