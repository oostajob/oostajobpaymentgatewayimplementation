package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class CustomerDetailsForPaymentHistory {

    @SerializedName("deviceType")
    private String deviceType;

    @SerializedName("Email")
    private String email;

    @SerializedName("Address")
    private String address;

    @SerializedName("Lng")
    private String lng;

    @SerializedName("userID")
    private String userID;

    @SerializedName("Name")
    private String name;

    @SerializedName("braintreeID")
    private String braintreeID;

    @SerializedName("profilePhoto")
    private String profilePhoto;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("customerID")
    private String customerID;

    @SerializedName("Zipcode")
    private String zipcode;

    @SerializedName("notificationID")
    private String notificationID;

    @SerializedName("Lat")
    private String lat;

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBraintreeID(String braintreeID) {
        this.braintreeID = braintreeID;
    }

    public String getBraintreeID() {
        return braintreeID;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    @Override
    public String toString() {
        return
                "CustomerDetailsForPaymentHistory{" +
                        "deviceType = '" + deviceType + '\'' +
                        ",email = '" + email + '\'' +
                        ",address = '" + address + '\'' +
                        ",lng = '" + lng + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",name = '" + name + '\'' +
                        ",braintreeID = '" + braintreeID + '\'' +
                        ",profilePhoto = '" + profilePhoto + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",customerID = '" + customerID + '\'' +
                        ",zipcode = '" + zipcode + '\'' +
                        ",notificationID = '" + notificationID + '\'' +
                        ",lat = '" + lat + '\'' +
                        "}";
    }
}