package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Admin on 12/10/2015.
 */
public class JobSubTypeCharModel {

    public static class Response {
        @Expose
        public String Result;
        @Expose
        public String Totalcharacter;
        @Expose
        public ArrayList<JobsubtypecharDetails> JobsubtypecharDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getTotalcharacter() {
            return Totalcharacter;
        }

        public void setTotalcharacter(String totalcharacter) {
            Totalcharacter = totalcharacter;
        }

        public ArrayList<JobSubTypeCharModel.JobsubtypecharDetails> getJobsubtypecharDetails() {
            return JobsubtypecharDetails;
        }

        public void setJobsubtypecharDetails(ArrayList<JobSubTypeCharModel.JobsubtypecharDetails> jobsubtypecharDetails) {
            JobsubtypecharDetails = jobsubtypecharDetails;
        }
    }

    public static class JobsubtypecharDetails implements Serializable {
        @Expose
        public String jobsubtypechar_id;
        @Expose
        public String isdependent;
        @Expose
        public String jobsubtype_id;
        @Expose
        public String question_type;
        @Expose
        public String tag_status;
        @Expose
        public String char_name;
        @Expose
        public String tag_keyword;
        @Expose
        public ArrayList<CharValue> char_value;
        @Expose
        public int imgDone;
        @Expose
        public ArrayList<FileModel> fileList;

        public JobsubtypecharDetails(String jobsubtypechar_id, String isdependent, String jobsubtype_id, String question_type,
                                     String tag_status, String char_name, ArrayList<CharValue> char_value, int imgDone
                , ArrayList<FileModel> fileList, String tag_keyword) {
            this.jobsubtypechar_id = jobsubtypechar_id;
            this.isdependent = isdependent;
            this.jobsubtype_id = jobsubtype_id;
            this.question_type = question_type;
            this.tag_status = tag_status;
            this.char_name = char_name;
            this.char_value = char_value;
            this.imgDone = imgDone;
            this.fileList = fileList;
        }

        public String getTag_keyword() {
            return tag_keyword;
        }

        public void setTag_keyword(String tag_keyword) {
            this.tag_keyword = tag_keyword;
        }

        public String getJobsubtypechar_id() {
            return jobsubtypechar_id;
        }

        public void setJobsubtypechar_id(String jobsubtypechar_id) {
            this.jobsubtypechar_id = jobsubtypechar_id;
        }

        public String getIsdependent() {
            return isdependent;
        }

        public void setIsdependent(String isdependent) {
            this.isdependent = isdependent;
        }

        public String getJobsubtype_id() {
            return jobsubtype_id;
        }

        public void setJobsubtype_id(String jobsubtype_id) {
            this.jobsubtype_id = jobsubtype_id;
        }

        public String getQuestion_type() {
            return question_type;
        }

        public void setQuestion_type(String question_type) {
            this.question_type = question_type;
        }

        public String getTag_status() {
            return tag_status;
        }

        public void setTag_status(String tag_status) {
            this.tag_status = tag_status;
        }

        public String getChar_name() {
            return char_name;
        }

        public void setChar_name(String char_name) {
            this.char_name = char_name;
        }

        public ArrayList<CharValue> getChar_value() {
            return char_value;
        }

        public void setChar_value(ArrayList<CharValue> char_value) {
            this.char_value = char_value;
        }

        public int getImgDone() {
            return imgDone;
        }

        public void setImgDone(int imgDone) {
            this.imgDone = imgDone;
        }

        public ArrayList<FileModel> getFileList() {
            return fileList;
        }

        public void setFileList(ArrayList<FileModel> fileList) {
            this.fileList = fileList;
        }
    }

    public class CharValue implements Serializable {
        @Expose
        public String option;

        public String getOption() {
            return option;
        }

        public void setOption(String option) {
            this.option = option;
        }
    }


}
