package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mathan on 28/11/2015.
 */
public class DiscussionlistModel {
    public class Response {
        @Expose
        String Result;
        @Expose
        ArrayList<DiscustionDetails> DiscustionDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<DiscussionlistModel.DiscustionDetails> getDiscustionDetails() {
            return DiscustionDetails;
        }

        public void setDiscustionDetails(ArrayList<DiscussionlistModel.DiscustionDetails> discustionDetails) {
            DiscustionDetails = discustionDetails;
        }
    }

    public class DiscustionDetails {

        @Expose
        public String discussionID;
        @Expose
        public String jobID;
        @Expose
        public String userID;
        @Expose
        public String questionName;
        @Expose
        public String anwser;
        @Expose
        public String questionTime;
        @Expose
        public String RespondTime;
        @Expose
        public String contractor_profile;
        @Expose
        public String contractor_name;
        @Expose
        public String customer_name;
        @Expose
        public String customer_profile;

        public String getDiscussionID() {
            return discussionID;
        }

        public void setDiscussionID(String discussionID) {
            this.discussionID = discussionID;
        }

        public String getJobID() {
            return jobID;
        }

        public void setJobID(String jobID) {
            this.jobID = jobID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getQuestionName() {
            return questionName;
        }

        public void setQuestionName(String questionName) {
            this.questionName = questionName;
        }

        public String getAnwser() {
            return anwser;
        }

        public void setAnwser(String anwser) {
            this.anwser = anwser;
        }

        public String getQuestionTime() {
            return questionTime;
        }

        public void setQuestionTime(String questionTime) {
            this.questionTime = questionTime;
        }

        public String getRespondTime() {
            return RespondTime;
        }

        public void setRespondTime(String respondTime) {
            RespondTime = respondTime;
        }

        public String getContractor_profile() {
            return contractor_profile;
        }

        public void setContractor_profile(String contractor_profile) {
            this.contractor_profile = contractor_profile;
        }

        public String getContractor_name() {
            return contractor_name;
        }

        public void setContractor_name(String contractor_name) {
            this.contractor_name = contractor_name;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getCustomer_profile() {
            return customer_profile;
        }

        public void setCustomer_profile(String customer_profile) {
            this.customer_profile = customer_profile;
        }
    }
}

