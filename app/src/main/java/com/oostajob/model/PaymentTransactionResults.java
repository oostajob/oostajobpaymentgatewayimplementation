package com.oostajob.model;

import com.google.gson.annotations.Expose;

public class PaymentTransactionResults {

    @Expose
    private String Result;

    @Expose
    private String Detail;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }
}
