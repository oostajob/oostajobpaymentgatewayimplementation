package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mathan on 27/11/2015.
 */
public class ResendEmailModel {
    public class Response{
        @Expose
        public String Result;
        @Expose
        public String Details;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getDetails() {
            return Details;
        }

        public void setDetails(String details) {
            Details = details;
        }


    }
}
