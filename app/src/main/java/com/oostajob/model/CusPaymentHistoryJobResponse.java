package com.oostajob.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CusPaymentHistoryJobResponse {


    @SerializedName("response")
    private ResponseItem response;

    @SerializedName("media")
    public ArrayList<GetCustDetailsModel.Jobmedialist> jobmedialist;

    @SerializedName("answer")
    public ArrayList<GetCustDetailsModel.JobAnwserlist> jobanwserlist;

    @SerializedName("contractordetails")
    public ContDetailsForPaymentHistory contDetailsForPaymentHistories;

    @SerializedName("job_icons")
    public JobIconForPaymentHistory jobIconForPaymentHistory;


    public ResponseItem getResponse() {
        return response;
    }

    public void setResponse(ResponseItem response) {
        this.response = response;
    }

    public ArrayList<GetCustDetailsModel.Jobmedialist> getJobmedialist() {
        return jobmedialist;
    }

    public void setJobmedialist(ArrayList<GetCustDetailsModel.Jobmedialist> jobmedialist) {
        this.jobmedialist = jobmedialist;
    }

    public ArrayList<GetCustDetailsModel.JobAnwserlist> getJobanwserlist() {
        return jobanwserlist;
    }

    public void setJobanwserlist(ArrayList<GetCustDetailsModel.JobAnwserlist> jobanwserlist) {
        this.jobanwserlist = jobanwserlist;
    }

    public ContDetailsForPaymentHistory getContDetailsForPaymentHistories() {
        return contDetailsForPaymentHistories;
    }

    public void setContDetailsForPaymentHistories(ContDetailsForPaymentHistory contDetailsForPaymentHistories) {
        this.contDetailsForPaymentHistories = contDetailsForPaymentHistories;
    }

    public JobIconForPaymentHistory getJobIconForPaymentHistory() {
        return jobIconForPaymentHistory;
    }

    public void setJobIconForPaymentHistory(JobIconForPaymentHistory jobIconForPaymentHistory) {
        this.jobIconForPaymentHistory = jobIconForPaymentHistory;
    }
}
