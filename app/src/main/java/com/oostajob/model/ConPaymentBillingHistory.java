package com.oostajob.model;

import java.util.List;


import com.google.gson.annotations.SerializedName;

public class ConPaymentBillingHistory {

    @SerializedName("response")
    private List<ResponseItem> response;

    @SerializedName("Result")
    private String result;

    public void setResponse(List<ResponseItem> response) {
        this.response = response;
    }

    public List<ResponseItem> getResponse() {
        return response;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return
                "ConPaymentBillingHistory{" +
                        "response = '" + response + '\'' +
                        ",result = '" + result + '\'' +
                        "}";
    }
}