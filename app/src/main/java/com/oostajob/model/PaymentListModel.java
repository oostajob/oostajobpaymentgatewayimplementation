package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mani on 11/30/2015.
 */
public class PaymentListModel {

    public static class Response
    {
        @Expose
        public String Result;
        @Expose
        public ArrayList<JobDetails> JobDetails;

        public ArrayList<JobDetails> getJobDetails() {
            return JobDetails;
        }

        public void setJobDetails(ArrayList<JobDetails> jobDetails) {
            JobDetails = jobDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }
    }
    public static class JobDetails
    {
        @Expose
        public String subcriptionID;
        @Expose
        public String contractorID;
        @Expose
        public String plan;
        @Expose
        public String startDate;
        @Expose
        public String endDate;
        @Expose
        public String createdDate;

        public String getContractorID() {
            return contractorID;
        }

        public void setContractorID(String contractorID) {
            this.contractorID = contractorID;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getPlan() {
            return plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getSubcriptionID() {
            return subcriptionID;
        }

        public void setSubcriptionID(String subcriptionID) {
            this.subcriptionID = subcriptionID;
        }
    }
}
