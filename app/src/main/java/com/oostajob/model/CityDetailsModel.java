package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class CityDetailsModel {

    public class Response {
        @Expose
        public String Result;

        @Expose
        public ArrayList<CityDetails> CityDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<CityDetailsModel.CityDetails> getCityDetails() {
            return CityDetails;
        }

        public void setCityDetails(ArrayList<CityDetailsModel.CityDetails> cityDetails) {
            CityDetails = cityDetails;
        }
    }

    public class CityDetails {
        @Expose
        String city_id;
        @Expose
        String zipcode;
        @Expose
        String city_name;
        @Expose
        String city_image;
        @Expose
        String service_status;

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getCity_image() {
            return city_image;
        }

        public void setCity_image(String city_image) {
            this.city_image = city_image;
        }

        public String getService_status() {
            return service_status;
        }

        public void setService_status(String service_status) {
            this.service_status = service_status;
        }

    }
}
