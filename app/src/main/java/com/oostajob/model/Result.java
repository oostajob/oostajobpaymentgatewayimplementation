package com.oostajob.model;

import com.google.gson.annotations.Expose;

public class Result {
    @Expose
    public String Result;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }
}
