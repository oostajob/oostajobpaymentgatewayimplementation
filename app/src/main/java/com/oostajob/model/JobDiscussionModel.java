package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mani on 11/28/2015.
 */
public class JobDiscussionModel {

    public static class Request
    {
        @Expose
        public String userID;

        @Expose
        public String jobpostID;

        @Expose
        public String questionName;

        public Request(String userID, String jobpostID,String questionName)
        {
            this.userID = userID;
            this.jobpostID = jobpostID;
            this.questionName = questionName;
        }
    }

    public static class Response
    {
        @Expose
        public String Result;

        @Expose
        public String DiscussionDetails;

        public String getDiscussionDetails() {
            return DiscussionDetails;
        }

        public void setDiscussionDetails(String discussionDetails) {
            DiscussionDetails = discussionDetails;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }
    }
}
