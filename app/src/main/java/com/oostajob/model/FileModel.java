package com.oostajob.model;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Admin on 12/12/2015.
 */
public class FileModel implements Serializable {

    public String mediaType; // 1 for Image, 2 for Video
    public File file;

    public FileModel(String mediaType, File file) {
        this.mediaType = mediaType;
        this.file = file;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
