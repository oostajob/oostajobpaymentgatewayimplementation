package com.oostajob.model;

import com.google.gson.annotations.Expose;


public class JobDiscussionReplyModel {

    public static class Request {
        @Expose
        public String discussionID;
        @Expose
        public String userID;
        @Expose
        public String jobpostID;
        @Expose
        public String anwser;

        public Request(String discussionID, String userID, String jobpostID, String anwser) {
            this.discussionID = discussionID;
            this.userID = userID;
            this.jobpostID = jobpostID;
            this.anwser = anwser;
        }


    }

    public static class Respose {
        @Expose
        public String Result;
        @Expose
        public String DiscussionDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getDiscussionDetails() {
            return DiscussionDetails;
        }

        public void setDiscussionDetails(String discussionDetails) {
            DiscussionDetails = discussionDetails;
        }
    }
}
