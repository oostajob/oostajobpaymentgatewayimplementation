package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mathan on 27/11/2015.
 */
public class LogInModel {
    public static class Request {
        @Expose
        public String Email;
        @Expose
        public String Password;
        @Expose
        public String DeviceID;
        @Expose
        public String DeviceType;

        public Request(String Email, String Password, String DeviceID, String DeviceType) {

            this.Email = Email;
            this.Password = Password;
            this.DeviceID = DeviceID;
            this.DeviceType = DeviceType;
        }
    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public String status;
        @Expose
        public UserDetails UserDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public LogInModel.UserDetails getUserDetails() {
            return UserDetails;
        }

        public void setUserDetails(LogInModel.UserDetails userDetails) {
            UserDetails = userDetails;
        }
    }

    public class UserDetails {
        @Expose
        public String userID;
        @Expose
        public String Email;
        @Expose
        public String userType;
        @Expose
        public String verifycationStatus;
        @Expose
        public String profileStatus;
        @Expose
        public String Name;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String profilePhoto;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String notificationID;
        @Expose
        public String deviceType;
        @Expose
        public String SSN;
        @Expose
        public String expertiseAT;
        @Expose
        public String Licence;
        @Expose
        public String Password;
        @Expose
        public String hourlyRate;

        @Expose
        public String bank_name;
        @Expose
        public String account_holder_name;

        @Expose
        public String routing;

        @Expose
        public String account_number;

        @Expose
        public String braintreeID;

        public String getBraintreeID() {
            return braintreeID;
        }

        public void setBraintreeID(String braintreeID) {
            this.braintreeID = braintreeID;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getAccount_holder_name() {
            return account_holder_name;
        }

        public void setAccount_holder_name(String account_holder_name) {
            this.account_holder_name = account_holder_name;
        }

        public String getRouting() {
            return routing;
        }

        public void setRouting(String routing) {
            this.routing = routing;
        }

        public String getAccount_number() {
            return account_number;
        }

        public void setAccount_number(String account_number) {
            this.account_number = account_number;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getVerifycationStatus() {
            return verifycationStatus;
        }

        public void setVerifycationStatus(String verifycationStatus) {
            this.verifycationStatus = verifycationStatus;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }

        public String getProfileStatus() {
            return profileStatus;
        }

        public void setProfileStatus(String profileStatus) {
            this.profileStatus = profileStatus;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getNotificationID() {
            return notificationID;
        }

        public void setNotificationID(String notificationID) {
            this.notificationID = notificationID;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getSSN() {
            return SSN;
        }

        public void setSSN(String SSN) {
            this.SSN = SSN;
        }

        public String getExpertiseAT() {
            return expertiseAT;
        }

        public void setExpertiseAT(String expertiseAT) {
            this.expertiseAT = expertiseAT;
        }

        public String getLicence() {
            return Licence;
        }

        public void setLicence(String licence) {
            Licence = licence;
        }

        public String getHourlyRate() {
            return hourlyRate;
        }

        public void setHourlyRate(String hourlyRate) {
            this.hourlyRate = hourlyRate;
        }
    }
}