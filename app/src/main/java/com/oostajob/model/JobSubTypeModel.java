package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mathan on 28/11/2015.
 */
public class JobSubTypeModel {
    public class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<JobsubtypeDetails> JobsubtypeDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<JobSubTypeModel.JobsubtypeDetails> getJobsubtypeDetails() {
            return JobsubtypeDetails;
        }

        public void setJobsubtypeDetails(ArrayList<JobSubTypeModel.JobsubtypeDetails> jobsubtypeDetails) {
            JobsubtypeDetails = jobsubtypeDetails;
        }
    }

    public class JobsubtypeDetails {
        @Expose
        public String jobsubtype_id;
        @Expose
        public String jobtype_id;
        @Expose
        public String subtype_name;

        public String getJobsubtype_id() {
            return jobsubtype_id;
        }

        public void setJobsubtype_id(String jobsubtype_id) {
            this.jobsubtype_id = jobsubtype_id;
        }

        public String getJobtype_id() {
            return jobtype_id;
        }

        public void setJobtype_id(String jobtype_id) {
            this.jobtype_id = jobtype_id;
        }

        public String getSubtype_name() {
            return subtype_name;
        }

        public void setSubtype_name(String subtype_name) {
            this.subtype_name = subtype_name;
        }
    }
}
