package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class PayableAmountResponse implements Serializable {

    @Expose
    public String Result;

    @Expose
    public String Detail;

    @Expose
    public Response Response;

    public class Response implements Serializable {


        @Expose
        public String jobbidID;

        @Expose
        public String jobID;


        @Expose
        public String bidAmount;

        @Expose
        public String payableAmount;


    }
}
