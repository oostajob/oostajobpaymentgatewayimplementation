package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class PostJobModel {

    public static class Request {
        @Expose
        public String userID;
        @Expose
        public String jobtypeID;
        @Expose
        public String jobsubtypeID;
        @Expose
        public String Address;
        @Expose
        public String zipcode;
        @Expose
        public String lat;
        @Expose
        public String lng;
        @Expose
        public String description;
        @Expose
        public ArrayList<Media> media;
        @Expose
        public ArrayList<AnswerKey> answerkey;


        public Request(String userID, String jobtypeID, String jobsubtypeID, String address, String zipcode, String lat, String lng, String description, ArrayList<Media> media, ArrayList<AnswerKey> answerkey) {
            this.userID = userID;
            this.jobtypeID = jobtypeID;
            this.jobsubtypeID = jobsubtypeID;
            Address = address;
            this.zipcode = zipcode;
            this.lat = lat;
            this.lng = lng;
            this.description = description;
            this.media = media;
            this.answerkey = answerkey;

        }
    }

    public static class Media {
        @Expose
        public String mediaType;
        @Expose
        public String mediaContent;

        public Media(String mediaType, String mediaContent) {
            this.mediaType = mediaType;
            this.mediaContent = mediaContent;
        }


    }

    public static class AnswerKey {
        @Expose
        public String quest_name;
        @Expose
        public String answer;
        @Expose
        public String tag_key;
        @Expose
        public String tag_keyword;

        public AnswerKey(String quest_name, String answer, String tag_key, String tag_keyword) {
            this.quest_name = quest_name;
            this.answer = answer;
            this.tag_key = tag_key;
            this.tag_keyword = tag_keyword;
        }
    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public String JobDetails;

        @Expose
        public String braintreeID;

        public String getBraintreeID() {
            return braintreeID;
        }

        public void setBraintreeID(String braintreeID) {
            this.braintreeID = braintreeID;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getJobDetails() {
            return JobDetails;
        }

        public void setJobDetails(String jobDetails) {
            JobDetails = jobDetails;
        }


    }

}
