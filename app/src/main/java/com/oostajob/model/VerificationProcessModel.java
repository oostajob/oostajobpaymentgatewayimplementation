package com.oostajob.model;

import com.google.gson.annotations.Expose;


public class VerificationProcessModel {
    public static class Response {
        @Expose
        String Result;
        @Expose
        String ProfileStatus;
        @Expose
        String EmailStatus;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getProfileStatus() {
            return ProfileStatus;
        }

        public void setProfileStatus(String profileStatus) {
            ProfileStatus = profileStatus;
        }

        public String getEmailStatus() {
            return EmailStatus;
        }

        public void setEmailStatus(String emailStatus) {
            EmailStatus = emailStatus;
        }
    }
}
