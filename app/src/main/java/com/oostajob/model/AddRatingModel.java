package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mathan on 28/11/2015.
 */
public class AddRatingModel {
    public static class Request {
        @Expose
        public String userID;
        @Expose
        public String jobpostID;
        @Expose
        public String ratingPrice;
        @Expose
        public String ratingTime;
        @Expose
        public String ratingQuality;
        @Expose
        public String ratingOverall;
        @Expose
        public String comment;

        public Request(String userID, String jobpostID, String ratingPrice, String ratingTime, String ratingQuality, String ratingOverall, String comment) {
            this.userID = userID;
            this.jobpostID = jobpostID;
            this.ratingPrice = ratingPrice;
            this.ratingTime = ratingTime;
            this.ratingQuality = ratingQuality;
            this.ratingOverall = ratingOverall;
            this.comment = comment;
        }


    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public String RatingDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getRatingDetails() {
            return RatingDetails;
        }

        public void setRatingDetails(String ratingDetails) {
            RatingDetails = ratingDetails;
        }


    }
}