package com.oostajob.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class CustomerPaymentJobHistory {

    @SerializedName("response")
    private ArrayList<CusPaymentHistoryJobResponse> response;

    @SerializedName("Result")
    private String result;

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return
                "CustomerPaymentJobHistory{" +
                        ",result = '" + result + '\'' +
                        "}";
    }

    public ArrayList<CusPaymentHistoryJobResponse> getResponse() {
        return response;
    }

    public void setResponse(ArrayList<CusPaymentHistoryJobResponse> response) {
        this.response = response;
    }
}