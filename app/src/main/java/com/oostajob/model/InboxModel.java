package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Admin on 30-Dec-15.
 */
public class InboxModel {
    @Expose
    public String Result;
    @Expose
    public ArrayList<Inbox> inboxlist;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public ArrayList<Inbox> getInboxlist() {
        return inboxlist;
    }

    public void setInboxlist(ArrayList<Inbox> inboxlist) {
        this.inboxlist = inboxlist;
    }

    public class Inbox {
        @Expose
        public String notifiyID;
        @Expose
        public String job_img;
        @Expose
        public String UserID;
        @Expose
        public String subject;
        @Expose
        public String message;
        @Expose
        public String createdTime;

        public String getNotifiyID() {
            return notifiyID;
        }

        public void setNotifiyID(String notifiyID) {
            this.notifiyID = notifiyID;
        }

        public String getJob_img() {
            return job_img;
        }

        public void setJob_img(String job_img) {
            this.job_img = job_img;
        }

        public String getUserID() {
            return UserID;
        }

        public void setUserID(String userID) {
            UserID = userID;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }
    }
}
