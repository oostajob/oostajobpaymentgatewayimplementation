package com.oostajob.model;

import java.io.Serializable;

public class PaymentTransactionInputResponse implements Serializable {
    private String jobID;
    private String userID;
    private String contractorID;
    private String amount;
    private String transaction_for;
    private String status;

    public PaymentTransactionInputResponse(String jobID, String userID, String contractorID, String amount, String transaction_for, String status) {
        this.jobID = jobID;
        this.userID = userID;
        this.contractorID = contractorID;
        this.amount = amount;
        this.transaction_for = transaction_for;
        this.status = status;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getContractorID() {
        return contractorID;
    }

    public void setContractorID(String contractorID) {
        this.contractorID = contractorID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransaction_for() {
        return transaction_for;
    }

    public void setTransaction_for(String transaction_for) {
        this.transaction_for = transaction_for;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
