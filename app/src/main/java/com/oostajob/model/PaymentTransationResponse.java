package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class PaymentTransationResponse {

    @SerializedName("jobID")
    private String jobID;

    @SerializedName("amount")
    private String amount;

    @SerializedName("transaction_for")
    private String transactionFor;

    @SerializedName("contractorID")
    private String contractorID;

    @SerializedName("userID")
    private String userID;

    @SerializedName("status")
    private String status;

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getJobID() {
        return jobID;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setTransactionFor(String transactionFor) {
        this.transactionFor = transactionFor;
    }

    public String getTransactionFor() {
        return transactionFor;
    }

    public void setContractorID(String contractorID) {
        this.contractorID = contractorID;
    }

    public String getContractorID() {
        return contractorID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "PaymentTransationResponse{" +
                        "jobID = '" + jobID + '\'' +
                        ",amount = '" + amount + '\'' +
                        ",transaction_for = '" + transactionFor + '\'' +
                        ",contractorID = '" + contractorID + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}