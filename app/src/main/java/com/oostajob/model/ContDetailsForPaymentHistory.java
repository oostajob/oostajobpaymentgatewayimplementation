package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class ContDetailsForPaymentHistory {

    @SerializedName("expertiseAT")
    private String expertiseAT;

    @SerializedName("deviceType")
    private String deviceType;

    @SerializedName("Email")
    private String email;

    @SerializedName("Address")
    private String address;

    @SerializedName("Lng")
    private String lng;

    @SerializedName("contractorID")
    private String contractorID;

    @SerializedName("totalRating")
    private String totalRating;

    @SerializedName("noComplients")
    private String noComplients;

    @SerializedName("totalEarning")
    private String totalEarning;

    @SerializedName("userID")
    private String userID;

    @SerializedName("Name")
    private String name;

    @SerializedName("SSN")
    private String sSN;

    @SerializedName("register_type")
    private String registerType;

    @SerializedName("profilePhoto")
    private String profilePhoto;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("Licence")
    private String licence;

    @SerializedName("Zipcode")
    private String zipcode;

    @SerializedName("notificationID")
    private String notificationID;

    @SerializedName("hourlyRate")
    private String hourlyRate;

    @SerializedName("Lat")
    private String lat;

    public void setExpertiseAT(String expertiseAT) {
        this.expertiseAT = expertiseAT;
    }

    public String getExpertiseAT() {
        return expertiseAT;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setContractorID(String contractorID) {
        this.contractorID = contractorID;
    }

    public String getContractorID() {
        return contractorID;
    }

    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    public String getTotalRating() {
        return totalRating;
    }

    public void setNoComplients(String noComplients) {
        this.noComplients = noComplients;
    }

    public String getNoComplients() {
        return noComplients;
    }

    public void setTotalEarning(String totalEarning) {
        this.totalEarning = totalEarning;
    }

    public String getTotalEarning() {
        return totalEarning;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSSN(String sSN) {
        this.sSN = sSN;
    }

    public String getSSN() {
        return sSN;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getLicence() {
        return licence;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setHourlyRate(String hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String getHourlyRate() {
        return hourlyRate;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    @Override
    public String toString() {
        return
                "ContDetailsForPaymentHistory{" +
                        "expertiseAT = '" + expertiseAT + '\'' +
                        ",deviceType = '" + deviceType + '\'' +
                        ",email = '" + email + '\'' +
                        ",address = '" + address + '\'' +
                        ",lng = '" + lng + '\'' +
                        ",contractorID = '" + contractorID + '\'' +
                        ",totalRating = '" + totalRating + '\'' +
                        ",noComplients = '" + noComplients + '\'' +
                        ",totalEarning = '" + totalEarning + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",name = '" + name + '\'' +
                        ",sSN = '" + sSN + '\'' +
                        ",register_type = '" + registerType + '\'' +
                        ",profilePhoto = '" + profilePhoto + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",licence = '" + licence + '\'' +
                        ",zipcode = '" + zipcode + '\'' +
                        ",notificationID = '" + notificationID + '\'' +
                        ",hourlyRate = '" + hourlyRate + '\'' +
                        ",lat = '" + lat + '\'' +
                        "}";
    }
}