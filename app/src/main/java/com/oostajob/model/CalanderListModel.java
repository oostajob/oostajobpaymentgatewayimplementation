package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mathan on 30/11/2015.
 */
public class CalanderListModel {
    public class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<CalenderDetails> CalenderDetails;
        @Expose
        public ArrayList<CalenderDetailsCustomer> CalenderDetailsCustomer;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<CalanderListModel.CalenderDetails> getCalenderDetails() {
            return CalenderDetails;
        }

        public void setCalenderDetails(ArrayList<CalanderListModel.CalenderDetails> calenderDetails) {
            CalenderDetails = calenderDetails;
        }

        public ArrayList<CalenderDetailsCustomer> getCalenderDetailsCustomer() {
            return CalenderDetailsCustomer;
        }

        public void setCalenderDetailsCustomer(ArrayList<CalenderDetailsCustomer> calenderDetailsCustomer) {
            CalenderDetailsCustomer = calenderDetailsCustomer;
        }
    }

    public class CalenderDetails {
        @Expose
        String notAvailabledate;
        @Expose
        String notAvailableTime;

        public String getNotAvailabledate() {
            return notAvailabledate;
        }

        public void setNotAvailabledate(String notAvailabledate) {
            this.notAvailabledate = notAvailabledate;
        }

        public String getNotAvailableTime() {
            return notAvailableTime;
        }

        public void setNotAvailableTime(String notAvailableTime) {
            this.notAvailableTime = notAvailableTime;
        }
    }

    public class CalenderDetailsCustomer {
        @Expose
        String notAvailabledate;
        @Expose
        String notAvailableTime;

        public String getNotAvailabledate() {
            return notAvailabledate;
        }

        public void setNotAvailabledate(String notAvailabledate) {
            this.notAvailabledate = notAvailabledate;
        }

        public String getNotAvailableTime() {
            return notAvailableTime;
        }

        public void setNotAvailableTime(String notAvailableTime) {
            this.notAvailableTime = notAvailableTime;
        }
    }
}
