package com.oostajob.model;


import com.google.gson.annotations.SerializedName;

public class ResponseItem {

    @SerializedName("reason")
    private String reason;

    @SerializedName("transaction_status")
    private String transactionStatus;

    @SerializedName("messageCount")
    private String messageCount;

    @SerializedName("Address")
    private String address;

    @SerializedName("created_at")
    private Object createdAt;

    @SerializedName("description")
    private String description;

    @SerializedName("jobtypeName")
    private String jobtypeName;

    @SerializedName("userID")
    private String userID;


    @SerializedName("closedTime")
    private String closedTime;

    @SerializedName("jobID")
    private String jobID;

    @SerializedName("jobpostTime")
    private String jobpostTime;

    @SerializedName("extend_time")
    private String extendTime;

    @SerializedName("ratedTime")
    private String ratedTime;

    @SerializedName("credit_token")
    private String creditToken;

    @SerializedName("updated_at")
    private Object updatedAt;

    @SerializedName("jobsubtypeID")
    private String jobsubtypeID;

    @SerializedName("createdTime")
    private String createdTime;

    @SerializedName("id")
    private Object id;

    @SerializedName("amount_after_deduction")
    private String amountAfterDeduction;

    @SerializedName("customerJobStatus")
    private String customerJobStatus;

    @SerializedName("lat")
    private String lat;

    @SerializedName("hiredTimed")
    private String hiredTimed;

    @SerializedName("updatedTime")
    private String updatedTime;

    @SerializedName("amount")
    private String amount;

    @SerializedName("transaction_for")
    private String transactionFor;

    @SerializedName("lng")
    private String lng;

    @SerializedName("notificationCount")
    private String notificationCount;

    @SerializedName("contractorID")
    private String contractorID;

    @SerializedName("payment_status")
    private String paymentStatus;

    @SerializedName("zipcode")
    private String zipcode;

    @SerializedName("is_customer_completed")
    private String isCustomerCompleted;

    @SerializedName("jobpostID")
    private String jobpostID;

    @SerializedName("jobshortisedTime")
    private String jobshortisedTime;

    @SerializedName("job_id")
    private Object jobId;

    @SerializedName("customer_token")
    private String customerToken;

    @SerializedName("customerID")
    private String customerID;

    @SerializedName("jobtypeID")
    private String jobtypeID;

    @SerializedName("rate_staus")
    private String rateStaus;

    @SerializedName("is_contractor_completed")
    private String isContractorCompleted;

    @SerializedName("contractor_remarks")
    private String contractorRemarks;

    @SerializedName("earning_available_date")
    private String earning_available_date;


    public String getEarning_available_date() {
        return earning_available_date;
    }

    public void setEarning_available_date(String earning_available_date) {
        this.earning_available_date = earning_available_date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setMessageCount(String messageCount) {
        this.messageCount = messageCount;
    }

    public String getMessageCount() {
        return messageCount;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setJobtypeName(String jobtypeName) {
        this.jobtypeName = jobtypeName;
    }

    public String getJobtypeName() {
        return jobtypeName;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }


    public void setClosedTime(String closedTime) {
        this.closedTime = closedTime;
    }

    public String getClosedTime() {
        return closedTime;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobpostTime(String jobpostTime) {
        this.jobpostTime = jobpostTime;
    }

    public String getJobpostTime() {
        return jobpostTime;
    }

    public void setExtendTime(String extendTime) {
        this.extendTime = extendTime;
    }

    public String getExtendTime() {
        return extendTime;
    }

    public void setRatedTime(String ratedTime) {
        this.ratedTime = ratedTime;
    }

    public String getRatedTime() {
        return ratedTime;
    }

    public void setCreditToken(String creditToken) {
        this.creditToken = creditToken;
    }

    public String getCreditToken() {
        return creditToken;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setJobsubtypeID(String jobsubtypeID) {
        this.jobsubtypeID = jobsubtypeID;
    }

    public String getJobsubtypeID() {
        return jobsubtypeID;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getId() {
        return id;
    }

    public String getAmountAfterDeduction() {
        return amountAfterDeduction;
    }

    public void setAmountAfterDeduction(String amountAfterDeduction) {
        this.amountAfterDeduction = amountAfterDeduction;
    }

    public void setCustomerJobStatus(String customerJobStatus) {
        this.customerJobStatus = customerJobStatus;
    }

    public String getCustomerJobStatus() {
        return customerJobStatus;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    public void setHiredTimed(String hiredTimed) {
        this.hiredTimed = hiredTimed;
    }

    public String getHiredTimed() {
        return hiredTimed;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setTransactionFor(String transactionFor) {
        this.transactionFor = transactionFor;
    }

    public String getTransactionFor() {
        return transactionFor;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setNotificationCount(String notificationCount) {
        this.notificationCount = notificationCount;
    }

    public String getNotificationCount() {
        return notificationCount;
    }

    public void setContractorID(String contractorID) {
        this.contractorID = contractorID;
    }

    public String getContractorID() {
        return contractorID;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setIsCustomerCompleted(String isCustomerCompleted) {
        this.isCustomerCompleted = isCustomerCompleted;
    }

    public String getIsCustomerCompleted() {
        return isCustomerCompleted;
    }

    public void setJobpostID(String jobpostID) {
        this.jobpostID = jobpostID;
    }

    public String getJobpostID() {
        return jobpostID;
    }

    public void setJobshortisedTime(String jobshortisedTime) {
        this.jobshortisedTime = jobshortisedTime;
    }

    public String getJobshortisedTime() {
        return jobshortisedTime;
    }

    public void setJobId(Object jobId) {
        this.jobId = jobId;
    }

    public Object getJobId() {
        return jobId;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setJobtypeID(String jobtypeID) {
        this.jobtypeID = jobtypeID;
    }

    public String getJobtypeID() {
        return jobtypeID;
    }

    public void setRateStaus(String rateStaus) {
        this.rateStaus = rateStaus;
    }

    public String getRateStaus() {
        return rateStaus;
    }

    public void setIsContractorCompleted(String isContractorCompleted) {
        this.isContractorCompleted = isContractorCompleted;
    }

    public String getIsContractorCompleted() {
        return isContractorCompleted;
    }

    public void setContractorRemarks(String contractorRemarks) {
        this.contractorRemarks = contractorRemarks;
    }

    public String getContractorRemarks() {
        return contractorRemarks;
    }

    @Override
    public String toString() {
        return
                "ResponseItem{" +
                        "reason = '" + reason + '\'' +
                        ",transaction_status = '" + transactionStatus + '\'' +
                        ",messageCount = '" + messageCount + '\'' +
                        ",address = '" + address + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",description = '" + description + '\'' +
                        ",jobtypeName = '" + jobtypeName + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",extendTime = '" + extendTime + '\'' +
                        ",closedTime = '" + closedTime + '\'' +
                        ",jobID = '" + jobID + '\'' +
                        ",jobpostTime = '" + jobpostTime + '\'' +
                        ",extend_time = '" + extendTime + '\'' +
                        ",ratedTime = '" + ratedTime + '\'' +
                        ",credit_token = '" + creditToken + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",jobsubtypeID = '" + jobsubtypeID + '\'' +
                        ",createdTime = '" + createdTime + '\'' +
                        ",id = '" + id + '\'' +
                        ",amount_after_deduction = '" + amountAfterDeduction + '\'' +
                        ",customerJobStatus = '" + customerJobStatus + '\'' +
                        ",lat = '" + lat + '\'' +
                        ",hiredTimed = '" + hiredTimed + '\'' +
                        ",updatedTime = '" + updatedTime + '\'' +
                        ",amount = '" + amount + '\'' +
                        ",transaction_for = '" + transactionFor + '\'' +
                        ",lng = '" + lng + '\'' +
                        ",notificationCount = '" + notificationCount + '\'' +
                        ",contractorID = '" + contractorID + '\'' +
                        ",payment_status = '" + paymentStatus + '\'' +
                        ",zipcode = '" + zipcode + '\'' +
                        ",is_customer_completed = '" + isCustomerCompleted + '\'' +
                        ",jobpostID = '" + jobpostID + '\'' +
                        ",jobshortisedTime = '" + jobshortisedTime + '\'' +
                        ",job_id = '" + jobId + '\'' +
                        ",customer_token = '" + customerToken + '\'' +
                        ",customerID = '" + customerID + '\'' +
                        ",jobtypeID = '" + jobtypeID + '\'' +
                        ",rate_staus = '" + rateStaus + '\'' +
                        ",is_contractor_completed = '" + isContractorCompleted + '\'' +
                        ",contractor_remarks = '" + contractorRemarks + '\'' +
                        "}";
    }
}