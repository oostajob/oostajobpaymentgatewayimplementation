package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 17-11-2015.
 */
public class Urls {
   /* "Result":"Success",
            "Aboutus":"http://52.11.208.116/public/assets/Dynamic/aboutus.html",
            "FAQ":"http://52.11.208.116/public/assets/Dynamic/faq.html",
            "CustomerTerms":"http://52.11.208.116/public/assets/Dynamic/customer-terms.html",
            "ContractorTerms":"http://52.11.208.116/public/assets/Dynamic/contractor-terms.html",
            "CustomerStories":"http://52.11.208.116/public/assets/Dynamic/customerstories.html"*/

    @Expose
    public String Result;
    @Expose
    public String Aboutus;
    @Expose
    public String FAQ;
    @Expose
    public String CustomerTerms;
    @Expose
    public String ContractorTerms;
    @Expose
    public String CustomerStories;

    @Expose
    public String Customerpaymentsterms;

    @Expose
    public String Contractorpayments;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getAboutus() {
        return Aboutus;
    }

    public void setAboutus(String aboutus) {
        Aboutus = aboutus;
    }

    public String getFAQ() {
        return FAQ;
    }

    public void setFAQ(String FAQ) {
        this.FAQ = FAQ;
    }

    public String getCustomerTerms() {
        return CustomerTerms;
    }

    public void setCustomerTerms(String customerTerms) {
        CustomerTerms = customerTerms;
    }

    public String getContractorTerms() {
        return ContractorTerms;
    }

    public void setContractorTerms(String contractorTerms) {
        ContractorTerms = contractorTerms;
    }

    public String getCustomerStories() {
        return CustomerStories;
    }

    public void setCustomerStories(String customerStories) {
        CustomerStories = customerStories;
    }

    public String getCustomerpaymentsterms() {
        return Customerpaymentsterms;
    }

    public void setCustomerpaymentsterms(String customerpaymentsterms) {
        Customerpaymentsterms = customerpaymentsterms;
    }

    public String getContractorpayments() {
        return Contractorpayments;
    }

    public void setContractorpayments(String contractorpayments) {
        Contractorpayments = contractorpayments;
    }
}
