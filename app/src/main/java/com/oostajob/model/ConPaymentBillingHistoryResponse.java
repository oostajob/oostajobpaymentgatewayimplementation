package com.oostajob.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ConPaymentBillingHistoryResponse{

	/*@SerializedName("response")
	private List<ResponseItem> response;

	@SerializedName("Result")
	private String result;

	public void setResponse(List<ResponseItem> response){
		this.response = response;
	}

	public List<ResponseItem> getResponse(){
		return response;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	@Override
 	public String toString(){
		return 
			"ConPaymentBillingHistoryResponse{" + 
			"response = '" + response + '\'' + 
			",result = '" + result + '\'' + 
			"}";
		}*/

	@SerializedName("response")
	private ArrayList<ConPaymentHistoryJobResponse> response;

	@SerializedName("Result")
	private String result;

	public void setResult(String result) {
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	@Override
	public String toString() {
		return
				"CustomerPaymentJobHistory{" +
						",result = '" + result + '\'' +
						"}";
	}

	public ArrayList<ConPaymentHistoryJobResponse> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<ConPaymentHistoryJobResponse> response) {
		this.response = response;
	}
}