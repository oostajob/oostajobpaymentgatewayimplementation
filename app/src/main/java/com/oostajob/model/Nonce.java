package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 11-Mar-16.
 */
public class Nonce {


    @Expose
    String braintreeID;


    @Expose
    public String nonce;

    @Expose
    public String amount;

    public Nonce(String braintreeID, String nonce, String amount) {
        this.braintreeID = braintreeID;
        this.nonce = nonce;
        this.amount = amount;
    }

    public String getBraintreeID() {
        return braintreeID;
    }

    public void setBraintreeID(String braintreeID) {
        this.braintreeID = braintreeID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }


    public String getNonce() {
        return nonce;
    }

    public class NonceResult {
        @Expose
        public Result result;

        public Result getResult() {
            return result;
        }
    }

    public class Result {
        @Expose
        public boolean success;

        /*@Expose
        public Attributes _attributes;
*/
        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

       /* public Attributes get_attributes() {
            return _attributes;
        }

        public void set_attributes(Attributes _attributes) {
            this._attributes = _attributes;
        }*/
    }

    public class Attributes {

        @Expose
        public String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
