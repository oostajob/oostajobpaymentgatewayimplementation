package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mani on 11/28/2015.
 */
public class ContractorJobListModel {

    public static class Request {
        @Expose
        public String zipcode;

        @Expose
        public String expertise;

        public Request(String zipcode, String expertise) {
            this.zipcode = zipcode;
            this.expertise = expertise;
        }
    }

    public class Response {
        @Expose
        public String Result;

        @Expose
        public Userststaus userststaus;

        public Userststaus getUserststaus() {
            return userststaus;
        }

        public void setUserststaus(Userststaus userststaus) {
            this.userststaus = userststaus;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }
    }

    public class Userststaus {
        @Expose
        public String jobpostID;

        @Expose
        public String customerID;

        @Expose
        public String jobtypeID;

        @Expose
        public String jobtypeName;

        @Expose
        public String jobsubtypeID;

        @Expose
        public String Address;

        @Expose
        public String zipcode;

        @Expose
        public String lat;

        @Expose
        public String lng;

        @Expose
        public String customerJobStatus;

        @Expose
        public String messageCount;

        @Expose
        public String notificationCount;

        @Expose
        public String jobpostTime;

        @Expose
        public String jobshortisedTime;

        @Expose
        public String createdTime;

        @Expose
        public String updatedTime;

        @Expose
        public String Miles;

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getCustomerJobStatus() {
            return customerJobStatus;
        }

        public void setCustomerJobStatus(String customerJobStatus) {
            this.customerJobStatus = customerJobStatus;
        }

        public String getJobpostID() {
            return jobpostID;
        }

        public void setJobpostID(String jobpostID) {
            this.jobpostID = jobpostID;
        }

        public String getJobpostTime() {
            return jobpostTime;
        }

        public void setJobpostTime(String jobpostTime) {
            this.jobpostTime = jobpostTime;
        }

        public String getJobshortisedTime() {
            return jobshortisedTime;
        }

        public void setJobshortisedTime(String jobshortisedTime) {
            this.jobshortisedTime = jobshortisedTime;
        }

        public String getJobsubtypeID() {
            return jobsubtypeID;
        }

        public void setJobsubtypeID(String jobsubtypeID) {
            this.jobsubtypeID = jobsubtypeID;
        }

        public String getJobtypeID() {
            return jobtypeID;
        }

        public void setJobtypeID(String jobtypeID) {
            this.jobtypeID = jobtypeID;
        }

        public String getJobtypeName() {
            return jobtypeName;
        }

        public void setJobtypeName(String jobtypeName) {
            this.jobtypeName = jobtypeName;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getMessageCount() {
            return messageCount;
        }

        public void setMessageCount(String messageCount) {
            this.messageCount = messageCount;
        }

        public String getMiles() {
            return Miles;
        }

        public void setMiles(String miles) {
            Miles = miles;
        }

        public String getNotificationCount() {
            return notificationCount;
        }

        public void setNotificationCount(String notificationCount) {
            this.notificationCount = notificationCount;
        }

        public String getUpdatedTime() {
            return updatedTime;
        }

        public void setUpdatedTime(String updatedTime) {
            this.updatedTime = updatedTime;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

    }
}
