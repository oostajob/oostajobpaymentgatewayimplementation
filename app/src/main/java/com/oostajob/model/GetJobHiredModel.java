package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class GetJobHiredModel implements Serializable {
    public static class Request implements Serializable {
        @Expose
        public String userID;
        @Expose
        public String jobid;
        @Expose
        public String meetingDate;
        @Expose
        public String meetingTime;


        public Request() {

        }

        public Request(String userID, String jobid, String meetingDate, String meetingTime) {
            this.userID = userID;
            this.jobid = jobid;
            this.meetingDate = meetingDate;
            this.meetingTime = meetingTime;
        }
    }

    public static class Response {
        @Expose
        public String Result;
        @Expose
        public String HiredDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getHiredDetails() {
            return HiredDetails;
        }

        public void setHiredDetails(String hiredDetails) {
            HiredDetails = hiredDetails;
        }
    }

}
