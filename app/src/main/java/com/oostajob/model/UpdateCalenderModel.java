package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mathan on 30/11/2015.
 */
public class UpdateCalenderModel {

    public static class Request {
        @Expose
        public ArrayList<Calander> Calander;

        public Request(ArrayList<UpdateCalenderModel.Calander> calander) {
            Calander = calander;
        }
    }

    public static class Calander {
        @Expose
        public String date;
        @Expose
        public String Times;

        public Calander(String date, String times) {
            this.date = date;
            Times = times;
        }
    }

    public class Response {
        @Expose
        public String Result;

        @Expose
        public String CalendarDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getCalendarDetails() {
            return CalendarDetails;
        }

        public void setCalendarDetails(String calendarDetails) {
            CalendarDetails = calendarDetails;
        }
    }

}
