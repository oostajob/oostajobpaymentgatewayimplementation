package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class BankDetailsResponse{

	@SerializedName("routing")
	private String routing;

	@SerializedName("account_number")
	private String accountNumber;

	@SerializedName("contractor_id")
	private String contractorId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("account_holder_name")
	private String accountHolderName;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private String id;

	public void setRouting(String routing){
		this.routing = routing;
	}

	public String getRouting(){
		return routing;
	}

	public void setAccountNumber(String accountNumber){
		this.accountNumber = accountNumber;
	}

	public String getAccountNumber(){
		return accountNumber;
	}

	public void setContractorId(String contractorId){
		this.contractorId = contractorId;
	}

	public String getContractorId(){
		return contractorId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setAccountHolderName(String accountHolderName){
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderName(){
		return accountHolderName;
	}

	public void setBankName(String bankName){
		this.bankName = bankName;
	}

	public String getBankName(){
		return bankName;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"BankDetailsResponse{" + 
			"routing = '" + routing + '\'' + 
			",account_number = '" + accountNumber + '\'' + 
			",contractor_id = '" + contractorId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",account_holder_name = '" + accountHolderName + '\'' + 
			",bank_name = '" + bankName + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}