package com.oostajob.model;

import com.google.gson.annotations.SerializedName;

public class ConEarningsResponse {

    @SerializedName("total_earnings")
    private String totalEarning;

    @SerializedName("Result")
    private String result;

    public String getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(String totalEarning) {
        this.totalEarning = totalEarning;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return
                "ConEarningsResponse{" +
                        "total_earning = '" + totalEarning + '\'' +
                        ",result = '" + result + '\'' +
                        "}";
    }
}