package com.oostajob.model;

/**
 * Created by codewave on 5/03/2018.
 */

public class EarningPojo {

    private String jobDate;
    private String jobCategory;
    private String jobCustomer;
    private String jobLocation;
    private String jobValue;
    private String jobEarning;
    private String availDate;
    private String paymentStatus;

    public EarningPojo(String jobDate,
                       String jobCategory,
                       String jobCustomer,
                       String jobLocation,
                       String jobValue,
                       String jobEarning,
                       String availDate,
                       String paymentStatus) {

        this.jobDate = jobDate;
        this.jobCategory = jobCategory;
        this.jobCustomer = jobCustomer;
        this.jobLocation = jobLocation;
        this.jobValue = jobValue;
        this.jobEarning = jobEarning;
        this.availDate = availDate;
        this.paymentStatus = paymentStatus;


    }


    public String getJobDate() {
        return jobDate;
    }

    public void setJobDate(String jobDate) {
        this.jobDate = jobDate;
    }

    public String getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(String jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getJobCustomer() {
        return jobCustomer;
    }

    public void setJobCustomer(String jobCustomer) {
        this.jobCustomer = jobCustomer;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getJobValue() {
        return jobValue;
    }

    public void setJobValue(String jobValue) {
        this.jobValue = jobValue;
    }

    public String getJobEarning() {
        return jobEarning;
    }

    public void setJobEarning(String jobEarning) {
        this.jobEarning = jobEarning;
    }

    public String getAvailDate() {
        return availDate;
    }

    public void setAvailDate(String availDate) {
        this.availDate = availDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }


}
