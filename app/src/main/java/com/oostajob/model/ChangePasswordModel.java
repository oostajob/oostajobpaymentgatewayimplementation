package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 28-Dec-15.
 */
public class ChangePasswordModel {
    public static class Request {
        @Expose
        public String userID;
        @Expose
        public String OldPassword;
        @Expose
        public String NewPassword;

        public Request(String userID, String oldPassword, String newPassword) {
            this.userID = userID;
            OldPassword = oldPassword;
            NewPassword = newPassword;
        }
    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public String UserDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getUserDetails() {
            return UserDetails;
        }

        public void setUserDetails(String userDetails) {
            UserDetails = userDetails;
        }
    }
}
