package com.oostajob.model;

/**
 * Created by Admin on 30-11-2015.
 */
public class MenuModel {
    public int icon;
    public String title;

    public MenuModel(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
