package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mathan on 27/11/2015.
 */
public class ForgotPasswordModel  {
    public static class Request {
        @Expose
        public String Email;

        public Request(String Email) {

            this.Email = Email;

        }
    }
    public class Response {
        @Expose
        public String Result;
        @Expose
        public String UserDetails;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getUserDetails() {
            return UserDetails;
        }

        public void setUserDetails(String userDetails) {
            UserDetails = userDetails;
        }


    }
}