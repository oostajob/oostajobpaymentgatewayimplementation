package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Admin on 23-Dec-15.
 */
public class ContractorJobList implements Serializable {
    @Expose
    public String Result;
    @Expose
    public ArrayList<List> Jobinvites;
    @Expose
    public ArrayList<List> Jobbidding;
    @Expose
    public ArrayList<List> Jobhired;
    @Expose
    public ArrayList<List> Jobclosed;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public ArrayList<List> getJobinvites() {
        return Jobinvites;
    }

    public void setJobinvites(ArrayList<List> jobinvites) {
        Jobinvites = jobinvites;
    }

    public ArrayList<List> getJobbidding() {
        return Jobbidding;
    }

    public void setJobbidding(ArrayList<List> jobbidding) {
        Jobbidding = jobbidding;
    }

    public ArrayList<List> getJobhired() {
        return Jobhired;
    }

    public void setJobhired(ArrayList<List> jobhired) {
        Jobhired = jobhired;
    }

    public ArrayList<List> getJobclosed() {
        return Jobclosed;
    }

    public void setJobclosed(ArrayList<List> jobclosed) {
        Jobclosed = jobclosed;
    }

    public class List implements Serializable {

        @Expose
        public String jobpostID;
        @Expose
        public String jobtypeName;
        @Expose
        public String contractorID;
        @Expose
        public String bidAmount;
        @Expose
        public String job_icon;
        @Expose
        public String job_image;
        @Expose
        public String messageCount;
        @Expose
        public String notificationCont;
        @Expose
        public String dateofbid;
        @Expose
        public String description;
        @Expose
        public String dateofAppoinment;
        @Expose
        public String jobstatus;
        @Expose
        public String shortlistedTime;
        @Expose
        public String hiredTimed;
        @Expose
        public String closedTime;
        @Expose
        public String InvitesTime;
        @Expose
        public String jobtypeID;
        @Expose
        public String jobsubtypeID;
        @Expose
        public String Address;
        @Expose
        public String zipcode;
        @Expose
        public String Remaining_time;
        @Expose
        public String Miles;
        @Expose
        public String lat;
        @Expose
        public String meetingDate;
        @Expose
        public String meetingTime;
        @Expose
        public String lng;
        @Expose
        public String minimumbidamount;
        @Expose
        public String rebidoptions;// 0-can bid, 1-cannot bid
        @Expose
        public ArrayList<Media> jobmedialist;
        @Expose
        public ArrayList<Answer> jobanwserlist;
        @Expose
        public Customer customerDetails;
        @Expose
        public String question_mesage;

        @Expose
        public String is_contractor_completed;
        @Expose
        public String is_customer_completed;


        @Expose
        public String reason;


        public String getIs_contractor_completed() {
            return is_contractor_completed;
        }

        public void setIs_contractor_completed(String is_contractor_completed) {
            this.is_contractor_completed = is_contractor_completed;
        }

        public String getIs_customer_completed() {
            return is_customer_completed;
        }

        public void setIs_customer_completed(String is_customer_completed) {
            this.is_customer_completed = is_customer_completed;
        }

        public String getQuestion_mesage() {
            return question_mesage;
        }

        public String getRebidoptions() {
            return rebidoptions;
        }

        public String getMinimumbidamount() {
            return minimumbidamount;
        }

        public String getJobpostID() {
            return jobpostID;
        }

        public void setJobpostID(String jobpostID) {
            this.jobpostID = jobpostID;
        }

        public String getJobtypeName() {
            return jobtypeName;
        }

        public void setJobtypeName(String jobtypeName) {
            this.jobtypeName = jobtypeName;
        }

        public String getContractorID() {
            return contractorID;
        }

        public void setContractorID(String contractorID) {
            this.contractorID = contractorID;
        }

        public String getBidAmount() {
            return bidAmount;
        }

        public void setBidAmount(String bidAmount) {
            this.bidAmount = bidAmount;
        }

        public String getJob_icon() {
            return job_icon;
        }

        public void setJob_icon(String job_icon) {
            this.job_icon = job_icon;
        }

        public String getJob_image() {
            return job_image;
        }

        public void setJob_image(String job_image) {
            this.job_image = job_image;
        }

        public String getNotificationCont() {
            return notificationCont;
        }

        public void setNotificationCont(String notificationCont) {
            this.notificationCont = notificationCont;
        }

        public String getDateofbid() {
            return dateofbid;
        }

        public void setDateofbid(String dateofbid) {
            this.dateofbid = dateofbid;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDateofAppoinment() {
            return dateofAppoinment;
        }

        public void setDateofAppoinment(String dateofAppoinment) {
            this.dateofAppoinment = dateofAppoinment;
        }

        public String getJobstatus() {
            return jobstatus;
        }

        public void setJobstatus(String jobstatus) {
            this.jobstatus = jobstatus;
        }

        public String getShortlistedTime() {
            return shortlistedTime;
        }

        public void setShortlistedTime(String shortlistedTime) {
            this.shortlistedTime = shortlistedTime;
        }

        public String getHiredTimed() {
            return hiredTimed;
        }

        public void setHiredTimed(String hiredTimed) {
            this.hiredTimed = hiredTimed;
        }

        public String getClosedTime() {
            return closedTime;
        }

        public void setClosedTime(String closedTime) {
            this.closedTime = closedTime;
        }

        public String getInvitesTime() {
            return InvitesTime;
        }

        public void setInvitesTime(String invitesTime) {
            InvitesTime = invitesTime;
        }

        public String getJobtypeID() {
            return jobtypeID;
        }

        public void setJobtypeID(String jobtypeID) {
            this.jobtypeID = jobtypeID;
        }

        public String getJobsubtypeID() {
            return jobsubtypeID;
        }

        public void setJobsubtypeID(String jobsubtypeID) {
            this.jobsubtypeID = jobsubtypeID;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getRemaining_time() {
            return Remaining_time;
        }

        public void setRemaining_time(String remaining_time) {
            Remaining_time = remaining_time;
        }

        public String getMiles() {
            return Miles;
        }

        public void setMiles(String miles) {
            Miles = miles;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getMeetingDate() {
            return meetingDate;
        }

        public void setMeetingDate(String meetingDate) {
            this.meetingDate = meetingDate;
        }

        public String getMeetingTime() {
            return meetingTime;
        }

        public void setMeetingTime(String meetingTime) {
            this.meetingTime = meetingTime;
        }

        public String getMessageCount() {
            return messageCount;
        }

        public void setMessageCount(String messageCount) {
            this.messageCount = messageCount;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public ArrayList<Media> getJobmedialist() {
            return jobmedialist;
        }

        public void setJobmedialist(ArrayList<Media> jobmedialist) {
            this.jobmedialist = jobmedialist;
        }

        public ArrayList<Answer> getJobanwserlist() {
            return jobanwserlist;
        }

        public void setJobanwserlist(ArrayList<Answer> jobanwserlist) {
            this.jobanwserlist = jobanwserlist;
        }

        public Customer getCustomerDetails() {
            return customerDetails;
        }

        public void setCustomerDetails(Customer customerDetails) {
            this.customerDetails = customerDetails;
        }
    }

    public class Media implements Serializable {
        @Expose
        public String mediaType;
        @Expose
        public String mediaContent;

        public String getMediaType() {
            return mediaType;
        }

        public void setMediaType(String mediaType) {
            this.mediaType = mediaType;
        }

        public String getMediaContent() {
            return mediaContent;
        }

        public void setMediaContent(String mediaContent) {
            this.mediaContent = mediaContent;
        }
    }

    public class Answer implements Serializable {
        @Expose
        public String answer_id;
        @Expose
        public String job_id;
        @Expose
        public String quest_name;
        @Expose
        public String answer;
        @Expose
        public String tag_key;
        @Expose
        public String tag_keyword;

        public String getTag_keyword() {
            return tag_keyword;
        }

        public String getAnswer_id() {
            return answer_id;
        }

        public void setAnswer_id(String answer_id) {
            this.answer_id = answer_id;
        }

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        public String getQuest_name() {
            return quest_name;
        }

        public void setQuest_name(String quest_name) {
            this.quest_name = quest_name;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getTag_key() {
            return tag_key;
        }

        public void setTag_key(String tag_key) {
            this.tag_key = tag_key;
        }
    }

    public class Customer implements Serializable {
        @Expose
        public String customerID;
        @Expose
        public String profilePhoto;
        @Expose
        public String username;
        @Expose
        public String Address;
        @Expose
        public String Email;
        @Expose
        public String Phone;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String totalRating;

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getTotalRating() {
            return totalRating;
        }

        public void setTotalRating(String totalRating) {
            this.totalRating = totalRating;
        }
    }
}
