package com.oostajob.model;

import com.google.gson.annotations.Expose;


public class SendUserID {
    @Expose
    String userID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
