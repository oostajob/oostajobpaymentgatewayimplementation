package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;


public class GetCustDetailsModel {

    public static class Jobmedialist implements Serializable {
        @Expose
        public String mediaType;
        @Expose
        public String mediaContent;
        @Expose
        public File file;

        public Jobmedialist(String mediaType, String mediaContent, File file) {
            this.mediaType = mediaType;
            this.mediaContent = mediaContent;
            this.file = file;
        }

        public String getMediaType() {
            return mediaType;
        }

        public void setMediaType(String mediaType) {
            this.mediaType = mediaType;
        }

        public String getMediaContent() {
            return mediaContent;
        }

        public void setMediaContent(String mediaContent) {
            this.mediaContent = mediaContent;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }
    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<Details> Jobbidding;
        @Expose
        public ArrayList<Details> Jobhired;
        @Expose
        public ArrayList<Details> Jobrated;
        @Expose
        public ArrayList<Details> Jobclosed;

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<Details> getJobbidding() {
            return Jobbidding;
        }

        public void setJobbidding(ArrayList<Details> jobbidding) {
            Jobbidding = jobbidding;
        }

        public ArrayList<Details> getJobhired() {
            return Jobhired;
        }

        public void setJobhired(ArrayList<Details> jobhired) {
            Jobhired = jobhired;
        }

        public ArrayList<Details> getJobrated() {
            return Jobrated;
        }

        public void setJobrated(ArrayList<Details> jobrated) {
            Jobrated = jobrated;
        }

        public ArrayList<Details> getJobclosed() {
            return Jobclosed;
        }

        public void setJobclosed(ArrayList<Details> jobclosed) {
            Jobclosed = jobclosed;
        }
    }

    public class Details implements Serializable {

        @Expose
        public String jobpostID;
        @Expose
        public String customerID;
        @Expose
        public String jobtypeID;
        @Expose
        public String jobtypeName;
        @Expose
        public String Address;
        @Expose
        public String jobicon;
        @Expose
        public String job_image;
        @Expose
        public String jobsubtypeID;
        @Expose
        public String description;
        @Expose
        public String zipcode;
        @Expose
        public String lat;
        @Expose
        public String lng;
        @Expose
        public String customerJobStatus;
        @Expose
        public String messageCount;
        @Expose
        public String notificationCount;
        @Expose
        public String replay_mesage;
        @Expose
        public String Remaining_time;
        @Expose
        public String jobpostTime;
        @Expose
        public String bidAmount;
        @Expose
        public String Contractorcnt;
        @Expose
        public ContractorDetails ContractorDetails;
        @Expose
        public ArrayList<Jobmedialist> jobmedialist;
        @Expose
        public ArrayList<JobAnwserlist> jobanwserlist;
        @Expose
        public ArrayList<Contractorlist> contractorlist;
        @Expose
        public String question_mesage;

        @Expose
        public String reason;

        @Expose
        public String is_contractor_completed;

        @Expose
        public String is_customer_completed;

        public String getIs_contractor_completed() {
            return is_contractor_completed;
        }

        public void setIs_contractor_completed(String is_contractor_completed) {
            this.is_contractor_completed = is_contractor_completed;
        }

        public String getIs_customer_completed() {
            return is_customer_completed;
        }

        public void setIs_customer_completed(String is_customer_completed) {
            this.is_customer_completed = is_customer_completed;
        }

        public ArrayList<Contractorlist> getContractorlist() {
            return contractorlist;
        }

        public void setContractorlist(ArrayList<Contractorlist> contractorlist) {
            this.contractorlist = contractorlist;
        }

        public String getBidAmount() {
            return bidAmount;
        }

        public void setBidAmount(String bidAmount) {
            this.bidAmount = bidAmount;
        }

        public String getJobpostID() {
            return jobpostID;
        }

        public void setJobpostID(String jobpostID) {
            this.jobpostID = jobpostID;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getJobtypeID() {
            return jobtypeID;
        }

        public void setJobtypeID(String jobtypeID) {
            this.jobtypeID = jobtypeID;
        }

        public String getJobtypeName() {
            return jobtypeName;
        }

        public void setJobtypeName(String jobtypeName) {
            this.jobtypeName = jobtypeName;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getJobicon() {
            return jobicon;
        }

        public void setJobicon(String jobicon) {
            this.jobicon = jobicon;
        }

        public String getJob_image() {
            return job_image;
        }

        public void setJob_image(String job_image) {
            this.job_image = job_image;
        }

        public String getJobsubtypeID() {
            return jobsubtypeID;
        }

        public void setJobsubtypeID(String jobsubtypeID) {
            this.jobsubtypeID = jobsubtypeID;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getCustomerJobStatus() {
            return customerJobStatus;
        }

        public void setCustomerJobStatus(String customerJobStatus) {
            this.customerJobStatus = customerJobStatus;
        }

        public String getMessageCount() {
            return messageCount;
        }

        public void setMessageCount(String messageCount) {
            this.messageCount = messageCount;
        }

        public String getNotificationCount() {
            return notificationCount;
        }

        public void setNotificationCount(String notificationCount) {
            this.notificationCount = notificationCount;
        }

        public String getJobpostTime() {
            return jobpostTime;
        }

        public void setJobpostTime(String jobpostTime) {
            this.jobpostTime = jobpostTime;
        }

        public String getRemaining_time() {
            return Remaining_time;
        }

        public void setRemaining_time(String remaining_time) {
            Remaining_time = remaining_time;
        }

        public ArrayList<Jobmedialist> getJobmedialist() {
            return jobmedialist;
        }

        public void setJobmedialist(ArrayList<Jobmedialist> jobmedialist) {
            this.jobmedialist = jobmedialist;
        }

        public ArrayList<JobAnwserlist> getJobanwserlist() {
            return jobanwserlist;
        }

        public void setJobanwserlist(ArrayList<JobAnwserlist> jobanwserlist) {
            this.jobanwserlist = jobanwserlist;
        }

        public String getQuestion_mesage() {
            return question_mesage;
        }

        public void setQuestion_mesage(String question_mesage) {
            this.question_mesage = question_mesage;
        }

        public String getReplay_mesage() {
            return replay_mesage;
        }

        public void setReplay_mesage(String replay_mesage) {
            this.replay_mesage = replay_mesage;
        }

        public String getContractorcnt() {
            return Contractorcnt;
        }

        public void setContractorcnt(String contractorcnt) {
            Contractorcnt = contractorcnt;
        }

        public GetCustDetailsModel.ContractorDetails getContractorDetails() {
            return ContractorDetails;
        }

        public void setContractorDetails(GetCustDetailsModel.ContractorDetails contractorDetails) {
            ContractorDetails = contractorDetails;
        }
    }

    public class ContractorDetails implements Serializable {
        @Expose
        public String contractorID;
        @Expose
        public String profilePhoto;
        @Expose
        public String username;
        @Expose
        public String Address;
        @Expose
        public String Email;
        @Expose
        public String Phone;
        @Expose
        public String expertiseAT;
        @Expose
        public String meetingDate;
        @Expose
        public String meetingTime;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String totalRating;

        public String getContractorID() {
            return contractorID;
        }

        public void setContractorID(String contractorID) {
            this.contractorID = contractorID;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getExpertiseAT() {
            return expertiseAT;
        }

        public void setExpertiseAT(String expertiseAT) {
            this.expertiseAT = expertiseAT;
        }

        public String getMeetingDate() {
            return meetingDate;
        }

        public void setMeetingDate(String meetingDate) {
            this.meetingDate = meetingDate;
        }

        public String getMeetingTime() {
            return meetingTime;
        }

        public void setMeetingTime(String meetingTime) {
            this.meetingTime = meetingTime;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getTotalRating() {
            return totalRating;
        }

        public void setTotalRating(String totalRating) {
            this.totalRating = totalRating;
        }
    }

    public class JobAnwserlist implements Serializable {
        @Expose
        public String answer_id;
        @Expose
        public String job_id;
        @Expose
        public String quest_name;
        @Expose
        public String answer;
        @Expose
        public String tag_key;
        @Expose
        public String tag_keyword;

        public String getTag_keyword() {
            return tag_keyword;
        }

        public String getAnswer_id() {
            return answer_id;
        }

        public void setAnswer_id(String answer_id) {
            this.answer_id = answer_id;
        }

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        public String getQuest_name() {
            return quest_name;
        }

        public void setQuest_name(String quest_name) {
            this.quest_name = quest_name;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getTag_key() {
            return tag_key;
        }

        public void setTag_key(String tag_key) {
            this.tag_key = tag_key;
        }
    }

    public class Contractorlist implements Serializable {
        @Expose
        public String userID;
        @Expose
        public String Name;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String profilePhoto;
        @Expose
        public String SSN;
        @Expose
        public String expertiseAT;
        @Expose
        public String Licence;
        @Expose
        public String bidAmount;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String notificationID;
        @Expose
        public String deviceType;
        @Expose
        public String totalEarning;
        @Expose
        public String totalRatingbycategory;
        @Expose
        public ArrayList<Reviewlist> reviewlist;
        @Expose
        public String totalRating;
        @Expose
        public String noComplients;
        @Expose
        public String hourlyRate;
        @Expose
        public String meetingDate;
        @Expose
        public String meetingTime;

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getSSN() {
            return SSN;
        }

        public void setSSN(String SSN) {
            this.SSN = SSN;
        }

        public String getExpertiseAT() {
            return expertiseAT;
        }

        public void setExpertiseAT(String expertiseAT) {
            this.expertiseAT = expertiseAT;
        }

        public String getLicence() {
            return Licence;
        }

        public void setLicence(String licence) {
            Licence = licence;
        }

        public String getBidAmount() {
            return bidAmount;
        }

        public void setBidAmount(String bidAmount) {
            this.bidAmount = bidAmount;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getNotificationID() {
            return notificationID;
        }

        public void setNotificationID(String notificationID) {
            this.notificationID = notificationID;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getTotalEarning() {
            return totalEarning;
        }

        public void setTotalEarning(String totalEarning) {
            this.totalEarning = totalEarning;
        }

        public String getTotalRatingbycategory() {
            return totalRatingbycategory;
        }

        public void setTotalRatingbycategory(String totalRatingbycategory) {
            this.totalRatingbycategory = totalRatingbycategory;
        }

        public ArrayList<Reviewlist> getReviewlist() {
            return reviewlist;
        }

        public void setReviewlist(ArrayList<Reviewlist> reviewlist) {
            this.reviewlist = reviewlist;
        }

        public String getTotalRating() {
            return totalRating;
        }

        public void setTotalRating(String totalRating) {
            this.totalRating = totalRating;
        }

        public String getNoComplients() {
            return noComplients;
        }

        public void setNoComplients(String noComplients) {
            this.noComplients = noComplients;
        }

        public String getHourlyRate() {
            return hourlyRate;
        }

        public void setHourlyRate(String hourlyRate) {
            this.hourlyRate = hourlyRate;
        }

        public String getMeetingDate() {
            return meetingDate;
        }

        public void setMeetingDate(String meetingDate) {
            this.meetingDate = meetingDate;
        }

        public String getMeetingTime() {
            return meetingTime;
        }

        public void setMeetingTime(String meetingTime) {
            this.meetingTime = meetingTime;
        }
    }

    public class Reviewlist implements Serializable {
        @Expose
        public String jobtypeName;
        @Expose
        public ArrayList<CustomerDetails> customerDetails;
        @Expose
        public String ratingTime;
        @Expose
        public String comment;
        @Expose
        public String ratingOverall;

        public String getJobtypeName() {
            return jobtypeName;
        }

        public void setJobtypeName(String jobtypeName) {
            this.jobtypeName = jobtypeName;
        }

        public ArrayList<CustomerDetails> getCustomerDetails() {
            return customerDetails;
        }

        public String getRatingTime() {
            return ratingTime;
        }

        public void setRatingTime(String ratingTime) {
            this.ratingTime = ratingTime;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getRatingOverall() {
            return ratingOverall;
        }

        public void setRatingOverall(String ratingOverall) {
            this.ratingOverall = ratingOverall;
        }
    }

    public class CustomerDetails implements Serializable {
        @Expose
        public String profilePhoto;
        @Expose
        public String username;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }
    }

}
