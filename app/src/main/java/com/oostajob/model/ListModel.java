package com.oostajob.model;

/**
 * Created by Admin on 12/11/2015.
 */
public class ListModel {
    public String id;
    public String name;

    public ListModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
