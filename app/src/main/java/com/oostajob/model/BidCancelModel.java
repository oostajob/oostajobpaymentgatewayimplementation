package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Mathan on 28/11/2015.
 */
public class BidCancelModel {
    @Expose
    String Result;
    @Expose
    String JobDetails;
    public class Response {
        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getJobDetails() {
            return JobDetails;
        }

        public void setJobDetails(String jobDetails) {
            JobDetails = jobDetails;
        }


    }

}
