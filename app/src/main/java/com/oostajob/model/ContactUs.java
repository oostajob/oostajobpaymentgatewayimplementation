package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 04-Jan-16.
 */
public class ContactUs {

    public static class Request {
        @Expose
        public String id;
        @Expose
        public String name;
        @Expose
        public String email_id;
        @Expose
        public String msg;
        @Expose
        public String userID;

        public Request(String id, String name, String email_id, String msg, String userID) {
            this.id = id;
            this.name = name;
            this.email_id = email_id;
            this.msg = msg;
            this.userID = userID;
        }
    }

    public class Response {
        @Expose
        public String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
    }
}
