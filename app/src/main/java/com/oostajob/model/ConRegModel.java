package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Mathan on 26/11/2015.
 */
public class ConRegModel {
    public static class Request {
        @Expose
        public String Name;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String profilePhoto;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String notificationID;
        @Expose
        public String deviceType;
        @Expose
        public String Email;
        @Expose
        public String Password;
        @Expose
        public String SSN;
        @Expose
        public String expertiseAT;
        @Expose
        public String Licence;
        @Expose
        public String hourlyRate;
        @Expose
        public String timezone;

        public Request(String Name, String Address, String Phone, String profilePhoto, String Zipcode, String Lat, String Lng, String notificationID,
                       String deviceType, String Email, String Password, String SSN, String expertiseAT, String Licence, /*String hourlyRate, */ String timezone) {
            this.Name = Name;
            this.Address = Address;
            this.Phone = Phone;
            this.profilePhoto = profilePhoto;
            this.Zipcode = Zipcode;
            this.Lat = Lat;
            this.Lng = Lng;
            this.notificationID = notificationID;
            this.deviceType = deviceType;
            this.Email = Email;
            this.Password = Password;
            this.SSN = SSN;
            this.expertiseAT = expertiseAT;
            this.Licence = Licence;
            //this.hourlyRate = hourlyRate;
            this.timezone = timezone;
        }
    }

    public class Response {
        @Expose
        public String Result;
        @Expose
        public ArrayList<ContractorDetails> ContractorDetails;
        @Expose
        public String Status;

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public ArrayList<ContractorDetails> getContractorDetails() {
            return ContractorDetails;
        }

        public void setContractorDetails(ArrayList<ContractorDetails> ContractorDetails) {
            this.ContractorDetails = ContractorDetails;
        }
    }


    public class ContractorBankDetailsResponse {

        @Expose
        public String Result;

        @Expose
        public String Details;

        @Expose
        public ResponseForBankDetails Response;


        public String getResult() {
            return Result;
        }

        public void setResult(String result) {
            Result = result;
        }

        public String getDetails() {
            return Details;
        }

        public void setDetails(String details) {
            Details = details;
        }

        public ResponseForBankDetails getResponse() {
            return Response;
        }

        public void setResponse(ResponseForBankDetails response) {
            Response = response;
        }
    }

    public class ResponseForBankDetails {

        @Expose
        public String account_holder_name;

        @Expose
        public String account_number;

        @Expose
        public String bank_name;

        @Expose
        public String routing;


        public String getAccount_holder_name() {
            return account_holder_name;
        }

        public void setAccount_holder_name(String account_holder_name) {
            this.account_holder_name = account_holder_name;
        }

        public String getAccount_number() {
            return account_number;
        }

        public void setAccount_number(String account_number) {
            this.account_number = account_number;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getRouting() {
            return routing;
        }

        public void setRouting(String routing) {
            this.routing = routing;
        }
    }

    public class ContractorDetails {
        @Expose
        public String userID;
        @Expose
        public String Email;
        @Expose
        public String Password;
        @Expose
        public String userType;
        @Expose
        public String verificationCode;
        @Expose
        public String verifycationStatus;
        @Expose
        public String profileStatus;
        @Expose
        public String createdTime;
        @Expose
        public String updatedTime;
        @Expose
        public String contractorID;
        @Expose
        public String Name;
        @Expose
        public String Address;
        @Expose
        public String Phone;
        @Expose
        public String profilePhoto;

        public String SSN;
        @Expose
        public String expertiseAT;
        @Expose
        public String Licence;
        @Expose
        public String Zipcode;
        @Expose
        public String Lat;
        @Expose
        public String Lng;
        @Expose
        public String notificationID;
        @Expose
        public String deviceType;
        @Expose
        public String totalEarning;
        @Expose
        public String totalRating;
        @Expose
        public String noComplients;
        @Expose
        public String hourlyRate;

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getVerificationCode() {
            return verificationCode;
        }

        public void setVerificationCode(String verificationCode) {
            this.verificationCode = verificationCode;
        }

        public String getVerifycationStatus() {
            return verifycationStatus;
        }

        public void setVerifycationStatus(String verifycationStatus) {
            this.verifycationStatus = verifycationStatus;
        }

        public String getProfileStatus() {
            return profileStatus;
        }

        public void setProfileStatus(String profileStatus) {
            this.profileStatus = profileStatus;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getUpdatedTime() {
            return updatedTime;
        }

        public void setUpdatedTime(String updatedTime) {
            this.updatedTime = updatedTime;
        }

        public String getContractorID() {
            return contractorID;
        }

        public void setContractorID(String contractorID) {
            this.contractorID = contractorID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

        public String getSSN() {
            return SSN;
        }

        public void setSSN(String SSN) {
            this.SSN = SSN;
        }

        public String getExpertiseAT() {
            return expertiseAT;
        }

        public void setExpertiseAT(String expertiseAT) {
            this.expertiseAT = expertiseAT;
        }

        public String getLicence() {
            return Licence;
        }

        public void setLicence(String licence) {
            Licence = licence;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String zipcode) {
            Zipcode = zipcode;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getNotificationID() {
            return notificationID;
        }

        public void setNotificationID(String notificationID) {
            this.notificationID = notificationID;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getTotalEarning() {
            return totalEarning;
        }

        public void setTotalEarning(String totalEarning) {
            this.totalEarning = totalEarning;
        }

        public String getTotalRating() {
            return totalRating;
        }

        public void setTotalRating(String totalRating) {
            this.totalRating = totalRating;
        }

        public String getNoComplients() {
            return noComplients;
        }

        public void setNoComplients(String noComplients) {
            this.noComplients = noComplients;
        }

        public String getHourlyRate() {
            return hourlyRate;
        }

        public void setHourlyRate(String hourlyRate) {
            this.hourlyRate = hourlyRate;
        }

    }
}