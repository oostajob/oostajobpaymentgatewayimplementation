package com.oostajob.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class JobTypeModel {

    @Expose
    public String Result;
    @Expose
    public ArrayList<JobtypeDetails> JobtypeDetails;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public ArrayList<JobTypeModel.JobtypeDetails> getJobtypeDetails() {
        return JobtypeDetails;
    }

    public void setJobtypeDetails(ArrayList<JobTypeModel.JobtypeDetails> jobtypeDetails) {
        JobtypeDetails = jobtypeDetails;
    }

    public static class JobtypeDetails {
        @Expose
        public String jobtype_id;
        @Expose
        public String job_name;
        @Expose
        public String job_icon;
        @Expose
        public String job_image;

        public JobtypeDetails(String jobtype_id, String job_name, String job_icon, String job_image) {
            this.jobtype_id = jobtype_id;
            this.job_name = job_name;
            this.job_icon = job_icon;
            this.job_image = job_image;
        }

        public String getJobtype_id() {
            return jobtype_id;
        }

        public void setJobtype_id(String jobtype_id) {
            this.jobtype_id = jobtype_id;
        }

        public String getJob_name() {
            return job_name;
        }

        public void setJob_name(String job_name) {
            this.job_name = job_name;
        }

        public String getJob_icon() {
            return job_icon;
        }

        public void setJob_icon(String job_icon) {
            this.job_icon = job_icon;
        }

        public String getJob_image() {
            return job_image;
        }

        public void setJob_image(String job_image) {
            this.job_image = job_image;
        }
    }

}
