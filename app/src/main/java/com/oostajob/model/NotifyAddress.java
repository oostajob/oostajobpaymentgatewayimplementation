package com.oostajob.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Admin on 13-Jan-16.
 */
public class NotifyAddress {

    @Expose
    public String id;
    @Expose
    public String zipcode;
    @Expose
    public String email;
    @Expose
    public String createdTime;

    public NotifyAddress(String id, String zipcode, String email, String createdTime) {
        this.id = id;
        this.zipcode = zipcode;
        this.email = email;
        this.createdTime = createdTime;
    }

    public class Result {
        @Expose
        public String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
    }

}
