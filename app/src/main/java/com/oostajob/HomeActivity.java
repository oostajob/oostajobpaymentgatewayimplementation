package com.oostajob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.oostajob.fragment.ContactFragment;
import com.oostajob.fragment.InboxFragment;
import com.oostajob.fragment.contractor.BillingFragment;
import com.oostajob.fragment.contractor.CalendarFragment;
import com.oostajob.fragment.contractor.ConBankDetailsFragment;
import com.oostajob.fragment.contractor.ConJobsFragment;
import com.oostajob.fragment.contractor.FindJobFragment;
import com.oostajob.fragment.customer.CusJobsFragment;
import com.oostajob.fragment.customer.CustomerPaymentHistoryFragment;
import com.oostajob.fragment.customer.JobTypesFragment;
import com.oostajob.fragment.customer.ConEarnings;
import com.oostajob.navigationdrawer.DrawerHomeFragment;
import com.oostajob.utils.PrefConnect;

public class HomeActivity extends AppCompatActivity implements DrawerHomeFragment.DrawerHomeFragmentCallbacks {

    private Toolbar toolbar;
    private DrawerHomeFragment drawerFragment;
    private String mTitle;
    private String USER_TYPE;
    private int check, PAGE;

    public static boolean fromConEarnings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        USER_TYPE = PrefConnect.readString(HomeActivity.this, PrefConnect.USER_TYPE, "");

        init();
        // setListener();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerFragment = (DrawerHomeFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        setUpToolBar();
    }

    private void setListener() {

    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        int fragPosition = getIntent().getIntExtra("fragPosition", 0);
        drawerFragment.initDrawer((DrawerLayout) findViewById(R.id.drawerLayout), toolbar, fragPosition);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (!drawerFragment.isDrawerOpen()) {
            onRestoreTitle();
        }

        return true;
    }

    private void onRestoreTitle() {
        toolbar.setTitle(mTitle);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!HomeActivity.fromConEarnings) {
            switch (position) {
                case 0:
                    if (USER_TYPE.equals("1") || USER_TYPE.equals("")) {
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new JobTypesFragment())
                                .commit();
                    } else {

                        check = getIntent().getIntExtra("check", 1);
                        Bundle bundle = new Bundle();
                        bundle.putInt("check", check);
                        bundle.putString("job_status", getIntent().getStringExtra("job_status"));

                        ConJobsFragment fragment = new ConJobsFragment();
                        /*CusJobsFragmentCheck fragment = new CusJobsFragmentCheck();*/
                        fragment.setArguments(bundle);
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment)
                                .commit();

                       /* fragmentManager.beginTransaction()
                                .replace(R.id.container, new FindJobFragment())
                                .commit();*/
                    }
                    break;
                case 1:

                    check = getIntent().getIntExtra("check", 1);
                    Bundle bundle = new Bundle();
                    bundle.putInt("check", check);
                    bundle.putString("job_status", getIntent().getStringExtra("job_status"));
                    if (USER_TYPE.equals("1")) {  // customer
                        CusJobsFragment fragment = new CusJobsFragment();
                        fragment.setArguments(bundle);
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment)
                                .commit();
                    } else {
                        ConJobsFragment fragment = new ConJobsFragment();
                        /*CusJobsFragmentCheck fragment = new CusJobsFragmentCheck();*/
                        fragment.setArguments(bundle);
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment)
                                .commit();
                    }
                    getIntent().removeExtra("check");
                    getIntent().removeExtra("job_status");
                    break;
                case 2:
                    if (USER_TYPE.equals("1")) {
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new CustomerPaymentHistoryFragment())
                                .commit();
                    } else {
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new CalendarFragment())
                                .commit();
                    }
                    break;
                /*case 3:

                    if (USER_TYPE.equals("1")) {
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new InboxFragment())
                                .commit();
                    } else {
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new InboxFragment())
                                .commit();
                    }
                    break;*/
                case 3:
                    //Old code with Bill
              /*  fragmentManager.beginTransaction()
                        .replace(R.id.container, new BillingFragment(), "Bill")
                        .commit();*/

               /* fragmentManager.beginTransaction()
                        .replace(R.id.container, new ConEarnings(), "Earnings")
                        .commit();*/

                    if (USER_TYPE.equals("1")) {
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new ContactFragment())
                                .commit();
                    } else {

                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new ConEarnings(), "Earnings")
                                .commit();

                    }

                    break;

                case 4:


                    fragmentManager.beginTransaction()
                            .replace(R.id.container, new ConBankDetailsFragment(), "Bank details")
                            .commit();

                    break;
                case 5:
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, new ContactFragment())
                            .commit();
                    break;
            }

        } else {
            Bundle bundle = new Bundle();
            ConJobsFragment fragment = new ConJobsFragment();
            HomeActivity.fromConEarnings = true;
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();

        }
    }

    public void onAttached(String title) {
        mTitle = title;
    }
}
