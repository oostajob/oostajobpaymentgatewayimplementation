package com.oostajob;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.oostajob.utils.PrefConnect;


public class SplashActivity extends AppCompatActivity {

    private String TAG = SplashActivity.this.getClass().getName();
    private int page;
    private String skip, user_type;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        page = PrefConnect.readInteger(SplashActivity.this, PrefConnect.PAGE, 0);
        skip = PrefConnect.readString(SplashActivity.this, PrefConnect.SKIP, "");
        user_type = PrefConnect.readString(SplashActivity.this, PrefConnect.USER_TYPE, "1");

        navigationPage();
    }

    private void navigationPage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (page) {
                    case -1:
                        if (skip.equalsIgnoreCase("skip_login") && user_type.equalsIgnoreCase("1")) {
                            intent = new Intent(SplashActivity.this, QuestionsActivity.class);
                            startActivity(intent);
                        } else {
                            intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                        }
                        finish();
                        break;
                    case 0:
                        intent = new Intent(SplashActivity.this, RegisterActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case 1:
                        intent = new Intent(SplashActivity.this, VerificationActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(SplashActivity.this, ProfileConStatActivity.class);
                        startActivity(intent);
                        finish();
                        break;

                    case 3:
                        intent = new Intent(SplashActivity.this, ConBankDetailActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                }

            }
        }, 3000);
    }


}
