package com.oostajob.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.Green.customfont.calendar.CalendarView;
import com.Green.customfont.calendar.CalendarView.DateEventHandler;
/*import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;*/
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.CalanderListModel;
import com.oostajob.model.GetClientTokenWithCustomerID;
import com.oostajob.model.GetJobHiredModel;
import com.oostajob.model.Nonce;
import com.oostajob.model.PayableAmountInput;
import com.oostajob.model.PayableAmountResponse;
import com.oostajob.model.PaymentTransactionInputResponse;
import com.oostajob.model.PaymentTransactionResults;
import com.oostajob.payment.internal.ApiClient;
import com.oostajob.payment.models.ClientToken;
import com.oostajob.utils.CalHolder;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CusCalendarDialog extends DialogFragment implements View.OnClickListener, DateEventHandler {


    private String TAG = CusCalendarDialog.class.getSimpleName();

    private String CONTRACTOR_USER_ID, POST_ID, USER_ID, BRAIN_TREE_CUSTOMER_ID;
    private CustomTextView txtDate;
    private CalendarView calendarView;
    private NumberPicker timePicker;
    private CustomTextView txtNoTime;
    private CustomButton btnMeet;
    private LinearLayout skipLay;
    private List<String> timeList = new ArrayList<>();
    private String[] arrTime = {"8am-10am", "10am-12pm", "12pm-2pm", "2pm-4pm", "4pm-6pm"};
    private ArrayList<String> arrTimeList = new ArrayList<>();
    private Date mDate;
    private Date tomorrow;

    private APIService apiService;
    private ProgressDialog p;
    private HashMap<String, String> calMap = new HashMap<>();
    private HashMap<String, String> calCus = new HashMap<>();
    private HashMap<Date, Boolean> calUpdate = new HashMap<>();
    private String CURRENT_DATE;


    String date;
    private int bidAmount = 0;


    GetJobHiredModel.Request request;


    boolean timingsAvaliable = true;
    PayableAmountResponse payableAmountResponse;


    private ApiClient apiClient;
    private String mClientToken;

    boolean gotResultForCardDetails;
    private static int REQUEST_CODE = 2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTime();
        timeList = Arrays.asList(arrTime);


        CONTRACTOR_USER_ID = getArguments().getString("userId");
        POST_ID = getArguments().getString("postId");
        bidAmount = getArguments().getInt("bidAmount");

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");


        p = new ProgressDialog(getActivity());
        p.setIndeterminate(false);
        p.setMessage(getString(R.string.loading));
        p.setCancelable(false);
        apiService = RetrofitSingleton.createService(APIService.class);
        apiClient = RetrofitSingleton.createService(ApiClient.class);
        BRAIN_TREE_CUSTOMER_ID = PrefConnect.readString(getActivity(), PrefConnect.BRAIN_TREE_CUSTOMER_ID, "");


    }

    private void setTime() {
        arrTimeList.add("8am-10am");
        arrTimeList.add("10am-12pm");
        arrTimeList.add("12pm-2pm");
        arrTimeList.add("2pm-4pm");
        arrTimeList.add("4pm-6pm");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_cus_calendar, null);
        init(view);

        setListener();
        getCalendar();
        builder.setView(view);
        calendarView.setDateEventHandler(this);
        return builder.create();
    }

    private void loadPicker(String[] arr, String[] arr1) {
        ArrayList<String> arrayList = (ArrayList<String>) arrTimeList.clone();
        for (String value : arr) {
            arrayList.remove(value.trim());
        }
        for (String value : arr1) {
            arrayList.remove(value.trim());
        }

        String[] temp = arrayList.toString().replace("[", "").replace("]", "").replace(" ", "").split(",");
        if (temp.length == 1 && TextUtils.isEmpty(temp[0])) {
            timePicker.setDisplayedValues(null);
            timePicker.setVisibility(View.GONE);
            timePicker.setValue(0);
            timingsAvaliable = false;
            txtNoTime.setVisibility(View.VISIBLE);
        } else {
            try {
                timingsAvaliable = true;
                txtNoTime.setVisibility(View.GONE);
                timePicker.setVisibility(View.VISIBLE);
                timePicker.setDisplayedValues(null);
                timePicker.setMaxValue(temp.length);
                timePicker.setMinValue(1);
                timePicker.setDisplayedValues(temp);
                setNumberPickerTextColor(timePicker, R.color.black);
                arrTime = temp;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void init(View view) {
        calendarView = (CalendarView) view.findViewById(R.id.calendarView);
        txtDate = (CustomTextView) view.findViewById(R.id.txtDate);
        timePicker = (NumberPicker) view.findViewById(R.id.timePicker);
        btnMeet = (CustomButton) view.findViewById(R.id.btnMeet);
        skipLay = (LinearLayout) view.findViewById(R.id.skipLay);
        txtNoTime = (CustomTextView) view.findViewById(R.id.txtNoTime);

        timePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        getTomorrow();
        setDate(tomorrow);
    }

    private void setListener() {


        btnMeet.setOnClickListener(CusCalendarDialog.this);
        skipLay.setOnClickListener(CusCalendarDialog.this);

    }

    @Override
    public void onClick(View v) {
        if (timingsAvaliable) {
            switch (v.getId()) {
                case R.id.btnMeet:

                    if (isValidated()) {
                        String date = Global.getDate(mDate, "yyyy-MM-dd");
                        request = new GetJobHiredModel.Request(CONTRACTOR_USER_ID, POST_ID, date, arrTime[timePicker.getValue() - 1]);
                        //hireContractor(request);

                        p.show();
                        getClientToken();
                        getPayableAmount();
                    }
                    break;
                case R.id.skipLay:
                    request = new GetJobHiredModel.Request(CONTRACTOR_USER_ID, POST_ID, "", "");
                    // hireContractor(request);
                    p.show();
                    getClientToken();
                    getPayableAmount();
                    break;
            }
        }


    }

    public void getPayableAmount() {
        PayableAmountInput payableAmountInput = new PayableAmountInput();

        payableAmountInput.setContractor_id(CONTRACTOR_USER_ID);
        payableAmountInput.setJobId(POST_ID);
        Call<PayableAmountResponse> call = apiService.payableAmount(payableAmountInput);
        call.enqueue(new Callback<PayableAmountResponse>() {
            @Override
            public void onResponse(Call<PayableAmountResponse> call, Response<PayableAmountResponse> response) {
                payableAmountResponse = response.body();
                if (payableAmountResponse != null) {


                    dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<PayableAmountResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "please try later", Toast.LENGTH_SHORT).show();
                p.dismiss();
            }

        });
    }

    private void dismissProgressDialog() {
        if (payableAmountResponse != null && gotResultForCardDetails)

        {
            p.dismiss();
        }

    }

    private void callPaymentAlertDialog(String alredyStored) {
        CusPaymentAlert dialogFragment = new CusPaymentAlert();
        Bundle args = new Bundle();
        args.putSerializable("request", request);
        args.putSerializable("payableAmountResponse", payableAmountResponse);
        if (alredyStored != null) {
            args.putBoolean("cardAlreadyStored", true);
        }

        dialogFragment.setArguments(args);
        dialogFragment.show(getActivity().getSupportFragmentManager(), "cus_payment_alert");

    }


    private boolean isValidated() {
        if (mDate == null) {
            timingsAvaliable = true;
            Toast.makeText(getActivity(), "Select Date", Toast.LENGTH_LONG).show();
            return false;
        }
        if (timePicker == null || timePicker.getValue() <= 0) {
            timingsAvaliable = false;
            Toast.makeText(getActivity(), "No Timing available for Selected date", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onDayPress(Date date) {
        setDate(date);
        CURRENT_DATE = Global.getDateNotUTC(date, "yyyy-MM-dd");
        int day = date.getDate();
        int month = date.getMonth();
        int year = date.getYear();
        btnMeet.setEnabled(true);
        if (calUpdate != null) {
            for (Map.Entry<Date, Boolean> entry : calUpdate.entrySet()) {
                Date eventDate = entry.getKey();
                if ((eventDate.getDate() == day &&
                        eventDate.getMonth() == month &&
                        eventDate.getYear() == year) && entry.getValue()) {
                    btnMeet.setEnabled(false);
                    break;
                }
            }
        }
        // 'calUpdate' contain whether the date is fully available or not value.
        if (calMap.containsKey(CURRENT_DATE) && calCus.containsKey(CURRENT_DATE)) {
            loadPicker(calMap.get(CURRENT_DATE).split(","), calCus.get(CURRENT_DATE).split(","));
        } else if (calCus.containsKey(CURRENT_DATE)) {
            loadPicker(new String[]{"Select Date"}, calCus.get(CURRENT_DATE).split(","));
        } else if (calMap.containsKey(CURRENT_DATE)) {
            loadPicker(calMap.get(CURRENT_DATE).split(","), new String[]{"Select Date"});
        } else {
            loadPicker(new String[]{"Select Date"}, new String[]{"Select Date"});
        }
    }

    @Override
    public void updateCalendar() {
        calendarView.updateCalendar(calUpdate);
    }

    private void setDate(Date date) {
        mDate = date;
        txtDate.setText(Global.getDate(date, "dd MMMM yyyy"));
    }

    public boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


    private void getCalendar() {
        p.show();
        Call<CalanderListModel.Response> call = apiService.getCalendar(CONTRACTOR_USER_ID);
        call.enqueue(new Callback<CalanderListModel.Response>() {
            @Override
            public void onResponse(Call<CalanderListModel.Response> call, Response<CalanderListModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    CalanderListModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        setHashMapValues(response1.getCalenderDetails(), response1.getCalenderDetailsCustomer());
                        getTomorrow();
                        CURRENT_DATE = Global.getDate(tomorrow, "yyyy-MM-dd");
                        if (TextUtils.isEmpty(calMap.get(CURRENT_DATE)) && TextUtils.isEmpty(calCus.get(CURRENT_DATE)))
                            loadPicker(new String[0], new String[0]);
                        else if (calMap.containsKey(CURRENT_DATE) && calCus.containsKey(CURRENT_DATE)) {
                            loadPicker(calMap.get(CURRENT_DATE).split(","), calCus.get(CURRENT_DATE).split(","));
                        } else if (calMap.containsKey(CURRENT_DATE)) {
                            loadPicker(calMap.get(CURRENT_DATE).split(","), new String[0]);
                        } else if (calCus.containsKey(CURRENT_DATE)) {
                            loadPicker(new String[0], calCus.get(CURRENT_DATE).split(","));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CalanderListModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void setHashMapValues(ArrayList<CalanderListModel.CalenderDetails> calenderDetails, ArrayList<CalanderListModel.CalenderDetailsCustomer> calenderDetailsCus) {

        for (CalanderListModel.CalenderDetails calDetail : calenderDetails) {
            CalHolder holder = getCalHolder(calDetail.getNotAvailableTime());
            calMap.put(calDetail.getNotAvailabledate(), calDetail.getNotAvailableTime());
            Date date = Global.getDate(calDetail.getNotAvailabledate(), "yyyy-MM-dd");
            calUpdate.put(date, holder.isFullNotAvailable());
        }
        for (CalanderListModel.CalenderDetailsCustomer calDetailCus : calenderDetailsCus) {
            calCus.put(calDetailCus.getNotAvailabledate(), calDetailCus.getNotAvailableTime());
        }
        calendarView.updateCalendar(calUpdate);
        getTomorrow();
        calendarView.setItemChecked(Global.getDate(tomorrow, "yyyy-MM-dd"));
        //calendarView.setItemChecked(Global.getDate(new Date(), "yyyy-MM-dd"));
    }

    private void getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        tomorrow = calendar.getTime();

    }

    private CalHolder getCalHolder(String time) {
        CalHolder holder = new CalHolder();
        String[] timeArr = time.split(",");
        boolean[] val = holder.getTime();
        try {
            val[timeList.indexOf(timeArr[0].trim())] = getCheckInfo(timeArr[0].trim());
            val[timeList.indexOf(timeArr[1].trim())] = getCheckInfo(timeArr[1].trim());
            val[timeList.indexOf(timeArr[2].trim())] = getCheckInfo(timeArr[2].trim());
            val[timeList.indexOf(timeArr[3].trim())] = getCheckInfo(timeArr[3].trim());
            val[timeList.indexOf(timeArr[4].trim())] = getCheckInfo(timeArr[4].trim());
        } catch (Exception ignored) {
        }
        holder.setTime(val);
        return holder;
    }

    private boolean getCheckInfo(String time) {
        switch (time) {
            case "8am-10am":
                return true;
            case "10am-12pm":
                return true;
            case "12pm-2pm":
                return true;
            case "2pm-4pm":
                return true;
            case "4pm-6pm":
                return true;
            default:
                return false;
        }
    }


    private void getClientToken() {
        GetClientTokenWithCustomerID getClientTokenWithCustomerID = new GetClientTokenWithCustomerID();
        getClientTokenWithCustomerID.setBraintreeID(BRAIN_TREE_CUSTOMER_ID);
        Call<ClientToken> call = apiClient.getClientToken(getClientTokenWithCustomerID);
        call.enqueue(new Callback<ClientToken>() {
            @Override
            public void onResponse(Call<ClientToken> call, Response<ClientToken> response) {
                if (response.isSuccessful()) {
                    ClientToken clientToken = response.body();
                    if (clientToken.getResult().equalsIgnoreCase("Success")) {
                        if (!TextUtils.isEmpty(clientToken.getClientToken())) {
                            mClientToken = clientToken.getClientToken();
                            //  imgPayPal.setEnabled(true);

                            checkForPreviousCallUser();
                            CusPaymentAlert.clientToken = true;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ClientToken> call, Throwable t) {

                Global.dismissProgress(p);
            }
        });

    }

    private void checkForPreviousCallUser() {

        gotResultForCardDetails = true;
        DropInResult.fetchDropInResult(getActivity(), mClientToken, new DropInResult.DropInResultListener() {
            @Override
            public void onError(Exception exception) {
                // an error occurred
                Log.d(TAG, "" + exception);
                dismissProgressDialog();
            }

            @Override
            public void onResult(DropInResult result) {
                if (result.getPaymentMethodType() != null) {
                    // use the icon and name to show in your UI
                    int icon = result.getPaymentMethodType().getDrawable();
                    int name = result.getPaymentMethodType().getLocalizedName();

                    dismissProgressDialog();
                    PaymentMethodType paymentMethodType = result.getPaymentMethodType();
                    if (paymentMethodType == PaymentMethodType.ANDROID_PAY || paymentMethodType == PaymentMethodType.GOOGLE_PAYMENT) {
                        // The last payment method the user used was Android Pay or Google Pay.
                        // The Android/Google Pay flow will need to be performed by the
                        // user again at the time of checkout.
                    } else {
                        // Use the payment method show in your UI and charge the user
                        // at the time of checkout.


                        //PaymentMethodNonce paymentMethod = result.getPaymentMethodNonce();
                        //  hireContractor(request);
                        callPaymentAlertDialog(getActivity().getResources().getString(R.string.card_alreday_stored));
                    }
                } else {
                    // there was no existing payment method
                    callPaymentAlertDialog(null);
                    dismissProgressDialog();
                }
            }

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                String nonce = result.getPaymentMethodNonce().getNonce();

                {
                    hireContractor(request);
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            }
        }


    }


    private void hireContractor(final GetJobHiredModel.Request request) {
        p.show();
        Call<GetJobHiredModel.Response> call = apiService.hireContractor(request);
        call.enqueue(new Callback<GetJobHiredModel.Response>() {
            @Override
            public void onResponse(Call<GetJobHiredModel.Response> call, Response<GetJobHiredModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    GetJobHiredModel.Response response1 = response.body();
                    /*hireSucessfull = true;
                    sucessfullypayment();*/

                    Toast.makeText(getActivity(), "Hired", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("fragPosition", 1);
                    intent.putExtra("job_status", "hired");
                    intent.putExtra("check", 3);
                    dismiss();
                    startActivity(intent);

                    dismiss();

                }
            }

            @Override
            public void onFailure(Call<GetJobHiredModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

}
