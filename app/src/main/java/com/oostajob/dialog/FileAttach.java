package com.oostajob.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;


public class FileAttach extends DialogFragment implements View.OnClickListener {


    private SuccessListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_file_attach, null);
        CustomTextView txtTitle = (CustomTextView) view.findViewById(R.id.txtTitle);
        CustomTextView btnYes = (CustomTextView) view.findViewById(R.id.btnYes);
        CustomTextView btnNo = (CustomTextView) view.findViewById(R.id.btnNo);

        txtTitle.setText(getArguments().getString("title"));
        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof SuccessListener)
            listener = (SuccessListener) activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnYes:
                if (listener != null) {
                    listener.attach();
                }
                dismiss();
                break;
            case R.id.btnNo:
                dismiss();
                break;
        }
    }


    public void setSuccessListener(SuccessListener listener) {
        this.listener = listener;
    }

    public interface SuccessListener {
        void attach();
    }
}
