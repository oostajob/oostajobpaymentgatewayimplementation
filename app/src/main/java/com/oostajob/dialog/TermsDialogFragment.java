package com.oostajob.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.oostajob.R;
import com.oostajob.utils.PrefConnect;


public class TermsDialogFragment extends AppCompatDialogFragment {
    ImageView btn_close;
    String customer_terms_webview, contractor_terms_webview, customer_payment_terms, contractor_payment_terms;
    int terms_id;
    private WebView mWebView;
    private ProgressBar progress;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        customer_terms_webview = PrefConnect.readString(getActivity(), PrefConnect.CUSTOMER_TERMS, "");
        contractor_terms_webview = PrefConnect.readString(getActivity(), PrefConnect.CONTRACTOR_TERMS, "");
        customer_payment_terms = PrefConnect.readString(getActivity(), PrefConnect.CUSTOMER_PAYMENT_TERMS, "");
        contractor_payment_terms = PrefConnect.readString(getActivity(), PrefConnect.CONTRACTOR_PAYMENT_TERMS, "");
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        terms_id = getArguments().getInt("dialog_id");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_terms_and_conditions, null);
        init(view);
        setListener();

        load_terms_and_cond_url();
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    private void setListener() {
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void load_terms_and_cond_url() {
        if (terms_id == 1) {
            mWebView.loadUrl(customer_terms_webview);
        } else if (terms_id == 3) {
            mWebView.loadUrl(customer_payment_terms);
        } else if (terms_id == 4) {
            mWebView.loadUrl(contractor_payment_terms);
        } else {
            mWebView.loadUrl(contractor_terms_webview);
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void init(View rootVIew) {
        btn_close = (ImageView) rootVIew.findViewById(R.id.close_icon);
        mWebView = (WebView) rootVIew.findViewById(R.id.web_terms_and_cond);
        progress = (ProgressBar) rootVIew.findViewById(R.id.progress);
        mWebView.clearCache(true);

        mWebView.setWebViewClient(new WebClient());
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    public class WebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progress.setVisibility(View.GONE);
        }
    }
}
