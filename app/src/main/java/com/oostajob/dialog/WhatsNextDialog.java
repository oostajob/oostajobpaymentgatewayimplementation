package com.oostajob.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.Green.customfont.CustomButton;
import com.oostajob.HomeActivity;
import com.oostajob.R;


public class WhatsNextDialog extends DialogFragment implements View.OnClickListener {


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_whats_next, null);
        CustomButton btnWhatsNext = (CustomButton) view.findViewById(R.id.btnWhatsNext);

        btnWhatsNext.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        //
        switch (v.getId()) {
            case R.id.btnWhatsNext:
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("fragPosition", 1);
                startActivity(intent);
                break;
        }
    }
}
