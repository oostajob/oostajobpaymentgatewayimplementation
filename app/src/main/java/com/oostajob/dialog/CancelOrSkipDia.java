package com.oostajob.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.Result;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CancelOrSkipDia extends DialogFragment implements View.OnClickListener {


    boolean isCustomer;
    private SuccessListener listener;
    private String title, jobPostID;
    private ProgressDialog p;
    private APIService apiService;
    private String USER_ID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");

        title = getArguments().getString("title");
        jobPostID = getArguments().getString("jobPostID");
        isCustomer = getArguments().getBoolean("isCustomer", false);

        p = new ProgressDialog(getActivity());
        p.setMessage(getActivity().getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_cancel_or_skip, null);
        CustomTextView txtSkipDesc = (CustomTextView) view.findViewById(R.id.txtSkipDesc);
        CustomTextView btnSkip = (CustomTextView) view.findViewById(R.id.btnSkip);
        CustomTextView btnDismiss = (CustomTextView) view.findViewById(R.id.btnDismiss);

        txtSkipDesc.setText(getString(R.string.skip_desc, title));

        if (isCustomer) {
            btnSkip.setText("Cancel Job");
        }
        btnSkip.setOnClickListener(this);
        btnDismiss.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSkip:
                if (isCustomer) {
                    cancelJob();
                } else {
                    skipJob();
                }
                break;
            case R.id.btnDismiss:
                dismiss();
                break;
        }
    }

    private void cancelJob() {
        p.show();
        Call<Result> call = apiService.cancelJob(USER_ID, jobPostID);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    Result result = response.body();
                    if (result.getResult().equalsIgnoreCase("Success")) {
                        Toast.makeText(getActivity(), "Job Cancelled", Toast.LENGTH_SHORT).show();
                        if (listener != null) {
                            listener.onSuccess();
                        }
                        dismiss();
                    } else {
                        Toast.makeText(getActivity(), "Job not Cancelled", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Global.dismissProgress(p);
                Toast.makeText(getActivity(), "Job not Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void skipJob() {
        p.show();
        Call<Result> call = apiService.skipJob(USER_ID, jobPostID);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    Result result = response.body();
                    if (result.getResult().equalsIgnoreCase("Success")) {
                        Toast.makeText(getActivity(), "Job Cancelled", Toast.LENGTH_SHORT).show();
                        if (listener != null) {
                            listener.onSuccess();
                        }
                        dismiss();
                    } else {
                        Toast.makeText(getActivity(), "Job not Cancelled", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Global.dismissProgress(p);
                Toast.makeText(getActivity(), "Job not Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setSuccessListener(SuccessListener listener) {
        this.listener = listener;
    }

    public interface SuccessListener {
        void onSuccess();
    }
}
