package com.oostajob.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.global.Global;


public class PhotoDialog extends DialogFragment implements View.OnClickListener {

    public ImageListener listener;
    private String title;
    private boolean isLicence, isRemove;

    public void setImageListener(ImageListener listener) {
        this.listener = listener;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof ImageListener)
            listener = (ImageListener) activity;
        title = getArguments().getString("title");
        isLicence = getArguments().getBoolean("isLicence", false);
        isRemove = getArguments().getBoolean("isRemove", false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_camera, null);
        CustomTextView txtHeading = (CustomTextView) view.findViewById(R.id.txtHeading);
        CustomTextView txtCancel = (CustomTextView) view.findViewById(R.id.txtCancel);
        CustomTextView txtCamera = (CustomTextView) view.findViewById(R.id.txtCamera);
        CustomTextView txtGallery = (CustomTextView) view.findViewById(R.id.txtGallery);
        CustomTextView txtRemove = (CustomTextView) view.findViewById(R.id.txtRemove);

        if (isRemove) {
            txtRemove.setVisibility(View.VISIBLE);
        }

        txtHeading.setText(title);
        txtCancel.setOnClickListener(this);
        txtCamera.setOnClickListener(this);
        txtGallery.setOnClickListener(this);
        txtRemove.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }


    @Override
    public void onClick(View v) {
        //
        switch (v.getId()) {
            case R.id.txtCamera:
                try {
                    Global.openCameraActivity(getActivity(), listener, isLicence);

                    dismiss();
                } catch (ActivityNotFoundException e) {
                    String errorMessage = "Your device doesn't support capturing images!";
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txtGallery:
                /*Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image*//*");
                if (isLicence) {
                    getActivity().startActivityForResult(Intent.createChooser(intent, getString(R.string.add_photo)), Global.GALLERY_PICK_lICENCE);
                } else {
                    getActivity().startActivityForResult(Intent.createChooser(intent, getString(R.string.add_photo)), Global.GALLERY_PICK);
                }*/
                Global.pickImageFromGallery(getActivity(), isLicence);
                dismiss();
                break;
            case R.id.txtCancel:
                dismiss();
                break;
            case R.id.txtRemove:
                if (listener != null) {
                    listener.onImageSelected(null, isLicence);
                }
                dismiss();
                break;
        }
    }


}
