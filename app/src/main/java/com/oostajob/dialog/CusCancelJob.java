package com.oostajob.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.oostajob.FeedBackActivity;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.CancelJobByCustomer;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobCompleteByConSideResponse;
import com.oostajob.model.PostJobModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CusCancelJob extends DialogFragment {


    CustomButton btnSubmit, btncancel;
    CheckBox checkOne, checkTwo, checkThree, checkFour;


    private String USER_ID, JOB_ID = "", JOB_SUB_ID = "", POSTAL_CODE = "", LAT = "0.0", LNG = "0.0", ADDRESS = "";
    ArrayList<PostJobModel.Media> mediaList = new ArrayList<>();
    ArrayList<PostJobModel.AnswerKey> answerList = new ArrayList<>();
    private APIService apiService;
    private ProgressDialog p;

    GetCustDetailsModel.Details details;
    Activity activity;

    //private GetCustDetailsModel.Details contDetails;

    private Toast mToastToShow;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dia_cancel_and_repost, null);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        details = (GetCustDetailsModel.Details) getArguments().getSerializable("GetCustDetailsModel");
        //  contDetails = (GetCustDetailsModel.Details) getArguments().getSerializable("contractor");
        init(view);
        builder.setView(view);
        //builder.setTitle(getResources().getString(R.string.job_cancel));
        return builder.create();
    }

    private void init(View view) {
        btnSubmit = view.findViewById(R.id.btnSubmit);
        btncancel = view.findViewById(R.id.btncancel);
        checkOne = view.findViewById(R.id.checkOne);
        checkTwo = view.findViewById(R.id.checkTwo);
        checkThree = view.findViewById(R.id.checkThree);
        checkFour = view.findViewById(R.id.checkFour);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (validate()) {
                    sendDataToServer();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.select_atleast_one_reason), Toast.LENGTH_SHORT).show();
                }


            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }

    private boolean validate() {
        boolean flage = true;
        if (((!checkOne.isChecked()) && (!checkTwo.isChecked()) && (!checkThree.isChecked()) && (!checkFour.isChecked()))) {
            flage = false;

        }
        return flage;
    }

    private void sendDataToServer() {

        Call<JobCompleteByConSideResponse> call = apiService.jobCancelByCustomer(populateData());

        call.enqueue(new Callback<JobCompleteByConSideResponse>() {
            @Override
            public void onResponse(Call<JobCompleteByConSideResponse> call, Response<JobCompleteByConSideResponse> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                  /*  PostJobModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("fragPosition", 1);
                        activity.startActivity(intent);
                    }*/

                    if (response.body().getResult().equalsIgnoreCase("success")) {
                        showRepostJob();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobCompleteByConSideResponse> call, Throwable t) {
                Global.dismissProgress(p);
                Toast.makeText(getActivity(), "Server Error. Try Again...", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showRepostJob() {
        /*this.dismiss();
        CusRepostJob dialog = new CusRepostJob();
        Bundle bundle = new Bundle();
        bundle.putSerializable("GetCustDetailsModel", details);
        bundle.putSerializable("jobPostID", details.getJobpostID());
        bundle.putString("title", "cancel");
        bundle.putBoolean("isCustomer", true);
        dialog.setArguments(bundle);
        dialog.setArguments(bundle);
        dialog.show(getActivity().getSupportFragmentManager(), "Hire Dialog");*/

      /*  if (getActivity() != null) {
            // Toast.makeText(getActivity(), getResources().getString(R.string.amount_refund), Toast.LENGTH_LONG).show();
            showToast(view);
        }
*/

        Bundle bundle = new Bundle();
        bundle.putSerializable("contractor", details);
        bundle.putBoolean("fromCancelJOb", true);

        Intent intent = new Intent(getActivity(), FeedBackActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    private CancelJobByCustomer populateData() {
        CancelJobByCustomer cancelJobByCustomer = new CancelJobByCustomer();
        String reasons = null;
        cancelJobByCustomer.setJobId(details.getJobpostID());
        if (checkOne.isChecked()) {
            reasons = checkOne.getText().toString();
        }
        if (checkTwo.isChecked()) {
            reasons = reasons + "," + checkTwo.getText().toString();
        }
        if (checkThree.isChecked()) {
            reasons = reasons + "," + checkThree.getText().toString();
        }
        if (checkFour.isChecked()) {
            reasons = reasons + "," + checkFour.getText().toString();
        }

        cancelJobByCustomer.setReason(reasons);
        return cancelJobByCustomer;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    public void showToast(View view) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 180000;
        mToastToShow = Toast.makeText(getActivity(), getResources().getString(R.string.amount_refund), Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }

            public void onFinish() {
                mToastToShow.cancel();
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }

}
