package com.oostajob.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.BidAmountModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RebidFragment extends AppCompatDialogFragment {
    ImageView btn_close;
    private CustomButton btnBidNow;
    private CustomEditText edtBidAmount;
    private APIService apiService;
    private ProgressDialog p;
    private String USER_ID, JOB_POST_ID;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);
        p = Global.initProgress(getActivity());

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        JOB_POST_ID = getArguments().getString("post_id");
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_rebid, null);
        init(view);
        setListener();
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    private void setListener() {
        btnBidNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtBidAmount.getText().toString().trim())) {
                    reBid();
                }
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        edtBidAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    btnBidNow.setEnabled(true);
                } else {
                    btnBidNow.setEnabled(false);
                }
            }
        });
    }

    private void init(View rootVIew) {
        btn_close = (ImageView) rootVIew.findViewById(R.id.close_icon);
        btnBidNow = (CustomButton) rootVIew.findViewById(R.id.btnBidNow);
        edtBidAmount = (CustomEditText) rootVIew.findViewById(R.id.edtBidAmount);
    }

    private void reBid() {
        p.show();
        BidAmountModel.Request request = new BidAmountModel.Request(USER_ID,
                JOB_POST_ID, edtBidAmount.getText().toString());
        Call<BidAmountModel.Response> call = apiService.bidJob(request);
        call.enqueue(new Callback<BidAmountModel.Response>() {
            @Override
            public void onResponse(Call<BidAmountModel.Response> call, Response<BidAmountModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    BidAmountModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        Intent homeIntent = new Intent(getActivity(), HomeActivity.class);
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        homeIntent.putExtra("fragPosition", 1);
                        homeIntent.putExtra("check", 2);
                        startActivity(homeIntent);
                    }
                }
            }

            @Override
            public void onFailure(Call<BidAmountModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


}
