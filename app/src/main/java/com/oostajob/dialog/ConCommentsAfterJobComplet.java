package com.oostajob.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.Green.customfont.CustomTextView;
import com.oostajob.FeedBackActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.JobCompleteByConSideResponse;
import com.oostajob.model.JobCompletedByConInput;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConCommentsAfterJobComplet extends DialogFragment implements View.OnClickListener {

    com.Green.customfont.CustomEditText edtComments;

    private APIService apiService;
    private ProgressDialog p;
    private String USER_ID;
    private String jobpostID;
    private String customerDetailsId;
    private com.Green.customfont.CustomButton btnSubmit, btncancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        jobpostID = getArguments().getString("jobpostID");
        customerDetailsId = getArguments().getString("customerDetailsId");
        p = new ProgressDialog(getActivity());
        p.setMessage(getActivity().getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_frg_commnet_con, null);
       /* CustomTextView txtSkipDesc = (CustomTextView) view.findViewById(R.id.txtSkipDesc);
        CustomTextView btnSkip = (CustomTextView) view.findViewById(R.id.btnSkip);
        CustomTextView btnDismiss = (CustomTextView) view.findViewById(R.id.btnDismiss);*/
        btnSubmit = view.findViewById(R.id.btnSubmit);
        btncancel = view.findViewById(R.id.btncancel);
        edtComments = view.findViewById(R.id.edtComments);

        //txtSkipDesc.setText(getString(R.string.skip_desc, title));


        btnSubmit.setOnClickListener(this);
        btncancel.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }


    @Override
    public void onClick(View v) {

        if (btnSubmit.getId() == v.getId()) {
            if (edtComments.getText().toString().trim().length() > 0) {
                JobCompletedByConInput jobCompletedByConInput = new JobCompletedByConInput();
                jobCompletedByConInput.setLoggedUserId(USER_ID);
                jobCompletedByConInput.setAction_by("contractor");
                jobCompletedByConInput.setJobId(jobpostID);
                jobCompletedByConInput.setUserID(customerDetailsId);
                jobCompletedByConInput.setContractor_remarks(edtComments.getText().toString());
                Call<JobCompleteByConSideResponse> call = apiService.jobcompletedByCon(jobCompletedByConInput);
                call.enqueue(new Callback<JobCompleteByConSideResponse>() {
                    @Override
                    public void onResponse(Call<JobCompleteByConSideResponse> call, Response<JobCompleteByConSideResponse> response) {
                        Global.dismissProgress(p);
                        if (response.isSuccessful()) {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), response.body().getResult(), Toast.LENGTH_SHORT).show();
                                dismiss();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<JobCompleteByConSideResponse> call, Throwable t) {
                        Global.dismissProgress(p);
                    }
                });
            } else {

                if (getActivity() != null) {
                    Toast.makeText(getActivity(), "Enter Feedback", Toast.LENGTH_LONG).show();
                }

            }
        } else if (btncancel.getId() == v.getId()) {
            dismiss();
        }
    }
}
