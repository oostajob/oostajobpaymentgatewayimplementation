package com.oostajob.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;


import com.Green.customfont.CustomButton;
import com.oostajob.HomeActivity;
import com.oostajob.global.Global;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.PostJobModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CusRepostJob extends android.support.v4.app.DialogFragment {

    CustomButton btnSubmit, btnRepost;

    private String USER_ID, JOB_ID = "", JOB_SUB_ID = "", POSTAL_CODE = "", LAT = "0.0", LNG = "0.0", ADDRESS = "";
    ArrayList<PostJobModel.Media> mediaList = new ArrayList<>();
    ArrayList<PostJobModel.AnswerKey> answerList = new ArrayList<>();
    private APIService apiService;
    private ProgressDialog p;
    GetCustDetailsModel.Details details;
    Activity activity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        details = (GetCustDetailsModel.Details) getArguments().getSerializable("GetCustDetailsModel");
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(getActivity());

        ADDRESS = details.getAddress();
        for (int i = 0; i < details.getJobmedialist().size(); i++) {
            GetCustDetailsModel.Jobmedialist jobmedialist = details.getJobmedialist().get(i);
            String[] mediaContent = jobmedialist.getMediaContent().split("/");
            String mediaContentName = null;
            if (mediaContent.length > 0) {
                mediaContentName = mediaContent[mediaContent.length - 1];
            }

            mediaList.add(new PostJobModel.Media(jobmedialist.getMediaType(), mediaContentName));
        }

        for (int i = 0; i < details.getJobanwserlist().size(); i++) {
            GetCustDetailsModel.JobAnwserlist jobAnwserlist = details.getJobanwserlist().get(i);
            //String quest_name, String answer, String tag_key, String tag_keyword
            PostJobModel.AnswerKey answerKey = new PostJobModel.AnswerKey(jobAnwserlist.getQuest_name(), jobAnwserlist.getAnswer(),
                    jobAnwserlist.getTag_key(), jobAnwserlist.getTag_keyword());
            answerList.add(answerKey);
        }
        details.getJobmedialist();
        LAT = details.getLat();
        LNG = details.getLng();
        JOB_ID = details.getJobtypeID();
        JOB_SUB_ID = details.getJobsubtypeID();
        POSTAL_CODE = details.getZipcode();

        return new AlertDialog.Builder(getActivity())
                // set dialog icon
                .setIcon(android.R.drawable.stat_notify_error)
                // set Dialog Title
                .setTitle("Job Repost")
                // Set Dialog Message
                .setMessage("Do you want to repost Job ?")

                // positive button
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        repost();
                    }
                })
                // negative button
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("fragPosition", 1);
                        activity.startActivity(intent);
                    }
                }).create();
    }

    /*@Override
    public void onAttach(Activity activity) {
        super
        this.activity = activity;
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    private void repost() {
        final PostJobModel.Request request = new PostJobModel.Request(USER_ID, JOB_ID, JOB_SUB_ID, ADDRESS,
                POSTAL_CODE, LAT, LNG, "", mediaList, answerList);
        p.show();
        Call<PostJobModel.Response> call = apiService.postJob(request);
        call.enqueue(new Callback<PostJobModel.Response>() {
            @Override
            public void onResponse(Call<PostJobModel.Response> call, Response<PostJobModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    PostJobModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("fragPosition", 1);
                        activity.startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<PostJobModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
                Toast.makeText(getActivity(), "Server Error. Try Again...", Toast.LENGTH_LONG).show();
            }
        });
    }

}
