package com.oostajob.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.VideoActivity;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.global.Global;

import java.io.File;


public class FileDialog extends DialogFragment implements View.OnClickListener {

    public ImageListener listener;

    public void setFileListener(ImageListener listener) {
        this.listener = listener;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_file, null);
        CustomTextView txtCancel = (CustomTextView) view.findViewById(R.id.txtCancel);
        CustomTextView txtPicture = (CustomTextView) view.findViewById(R.id.txtCamera);
        CustomTextView txtVideo = (CustomTextView) view.findViewById(R.id.txtVideo);
        CustomTextView txtGalleryPicture = (CustomTextView) view.findViewById(R.id.txtGalleryPicture);
        CustomTextView txtGalleryVideo = (CustomTextView) view.findViewById(R.id.txtGalleryVideo);

        txtCancel.setOnClickListener(this);
        txtPicture.setOnClickListener(this);
        txtVideo.setOnClickListener(this);
        txtGalleryPicture.setOnClickListener(this);
        txtGalleryVideo.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        //
        switch (v.getId()) {
            case R.id.txtCamera:
                try {
                    Global.openCameraActivity(getActivity(), listener, false);
                } catch (ActivityNotFoundException e) {
                    String errorMessage = "Your device doesn't support capturing images!";
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                }
                dismiss();
                break;
            case R.id.txtVideo:
                Intent vidIntent = new Intent(getActivity(), VideoActivity.class);
                getActivity().startActivityForResult(vidIntent, Global.VIDEO_CAPTURE);
                dismiss();
                break;
            case R.id.txtGalleryPicture:
                Global.pickImageFromGallery(getActivity(), false);
                dismiss();
                break;
            case R.id.txtGalleryVideo:
                Global.pickVideoFromGallery(getActivity());
                dismiss();
                break;
            case R.id.txtCancel:
                dismiss();
                break;
        }
    }

}
