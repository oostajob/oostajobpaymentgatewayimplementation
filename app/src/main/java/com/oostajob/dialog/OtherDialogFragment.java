package com.oostajob.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.oostajob.R;


public class OtherDialogFragment extends AppCompatDialogFragment {

    String[] arr = new String[2];
    private String SELECTED;
    private CustomEditText edtText;
    private CustomButton btnDone;
    private ImageView close_icon;
    private OtherListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SELECTED = getArguments().getString("selected");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_others, null);
        init(view);
        loadDefault();
        setListener();

        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    private void init(View rootVIew) {
        edtText = (CustomEditText) rootVIew.findViewById(R.id.edtText);
        btnDone = (CustomButton) rootVIew.findViewById(R.id.btnDone);
        close_icon = (ImageView) rootVIew.findViewById(R.id.close_icon);
    }

    private void loadDefault() {
        try {
            arr = SELECTED.split(":");
            edtText.setText(arr[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnValue();
            }
        });
        close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void returnValue() {
        if (listener != null) {
            if (TextUtils.isEmpty(edtText.getText().toString().trim())) {
                String selectedValue = arr[0];
                listener.onSelected(selectedValue);
            } else {
                String selectedValue = arr[0] + ":" + edtText.getText().toString();
                listener.onSelected(selectedValue);
            }

            dismiss();
        }
    }

    public void setListener(OtherListener listener) {
        this.listener = listener;
    }


    public interface OtherListener {
        void onSelected(String value);
    }
}
