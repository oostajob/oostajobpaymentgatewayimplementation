package com.oostajob.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.Green.customfont.CustomTextView;
/*import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;*/
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.oostajob.ConRegActivity;
import com.oostajob.FeedBackActivity;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.GetClientTokenWithCustomerID;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.GetJobHiredModel;
import com.oostajob.model.JobCompleteByConSideResponse;
import com.oostajob.model.JobCompletedByConInput;
import com.oostajob.model.Nonce;
import com.oostajob.model.PayableAmountInput;
import com.oostajob.model.PayableAmountResponse;
import com.oostajob.model.PaymentTransactionInputResponse;
import com.oostajob.model.PaymentTransactionResults;
import com.oostajob.payment.internal.ApiClient;
import com.oostajob.payment.models.ClientToken;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CusPaymentAlert extends DialogFragment implements View.OnClickListener {

    private String TAG = CusPaymentAlert.class.getSimpleName();

    private String USER_ID, BRAIN_TREE_CUSTOMER_ID;

    private APIService apiService;
    private ProgressDialog p;
    private String customerDetailsId;
    private ApiClient apiClient;
    private com.Green.customfont.CustomTextView btnSubmit, btncancel, txtPayment;

    static boolean clientToken;
    PayableAmountResponse payableAmountResponse;
    private String mClientToken;
    boolean paymentSucessfull, hireSucessfull;
    private static int REQUEST_CODE = 2;

    GetJobHiredModel.Request request;
    com.Green.customfont.CustomCheckBox chkAgree;
    private CustomTextView txt_terms;

    Boolean fromCustomerJobComplete, cardAlreadyStored;
    boolean paymentTransaction, jobCompleteByCustomer;
    private GetCustDetailsModel.Details contDetails;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        BRAIN_TREE_CUSTOMER_ID = PrefConnect.readString(getActivity(), PrefConnect.BRAIN_TREE_CUSTOMER_ID, "");


        fromCustomerJobComplete = getArguments().getBoolean("fromCustomerJobComplete", false);
        cardAlreadyStored = getArguments().getBoolean("cardAlreadyStored", false);
        //args.putSerializable("payableAmountResponse", payableAmountResponse);
        payableAmountResponse = (PayableAmountResponse) getArguments().getSerializable("payableAmountResponse");

        if (!fromCustomerJobComplete) {
            request = (GetJobHiredModel.Request) getArguments().getSerializable("request");
        } else {
            request = new GetJobHiredModel.Request();
            request.jobid = getArguments().getString("jobId");
            contDetails = (GetCustDetailsModel.Details) getArguments().getSerializable("contDetails");
            request.userID = contDetails.getContractorDetails().contractorID;
        }

        p = new ProgressDialog(getActivity());
        p.setMessage(getActivity().getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        apiClient = RetrofitSingleton.createService(ApiClient.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_alert_payment_show, null);

        btnSubmit = view.findViewById(R.id.btnSubmit);
        txtPayment = view.findViewById(R.id.txtPayment);
        btncancel = view.findViewById(R.id.btncancel);
        chkAgree = view.findViewById(R.id.chkAgree);
        txt_terms = (CustomTextView) view.findViewById(R.id.txt_con_terms);
        setPayableAmountText();
        getClientToken();


        btncancel.setOnClickListener(this);

        txt_terms.setOnClickListener(this);

        builder.setView(view);
        return builder.create();
    }


    @Override
    public void onClick(View v) {


        if (btnSubmit.getId() == v.getId()) {
            if (chkAgree.isChecked()) {
                sendToPaymentGateway();
            } else {
                Toast.makeText(getActivity(), getString(R.string.accept_terms), Toast.LENGTH_LONG).show();
            }


        } else if (btncancel.getId() == v.getId()) {
            dismiss();
        } else if (txt_terms.getId() == v.getId()) {
            TermsDialogFragment dialogFragment = new TermsDialogFragment();
            Bundle args = new Bundle();
            args.putInt("dialog_id", 3);
            dialogFragment.setArguments(args);
            dialogFragment.show(getActivity().getSupportFragmentManager(), "con_dialog");
        }


    }

    private void getClientToken() {
        GetClientTokenWithCustomerID getClientTokenWithCustomerID = new GetClientTokenWithCustomerID();
        getClientTokenWithCustomerID.setBraintreeID(BRAIN_TREE_CUSTOMER_ID);
        Call<ClientToken> call = apiClient.getClientToken(getClientTokenWithCustomerID);
        call.enqueue(new Callback<ClientToken>() {
            @Override
            public void onResponse(Call<ClientToken> call, Response<ClientToken> response) {
                if (response.isSuccessful()) {
                    ClientToken clientToken = response.body();
                    if (clientToken.getResult().equalsIgnoreCase("Success")) {
                        if (!TextUtils.isEmpty(clientToken.getClientToken())) {
                            mClientToken = clientToken.getClientToken();
                            //  imgPayPal.setEnabled(true);


                            CusPaymentAlert.clientToken = true;
                            setListener();

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ClientToken> call, Throwable t) {

                Global.dismissProgress(p);
            }
        });

    }


    private void setPayableAmountText() {
        if (payableAmountResponse != null && payableAmountResponse.Response != null && payableAmountResponse.Response.payableAmount != null) {

            if (!fromCustomerJobComplete) {
                // txtPayment.setText("Initial installment of amount $" + payableAmountResponse.Response.payableAmount + " will deducted from the account ");
                txtPayment.setText("We will hold your card in our vault, there will not be any charges on the card at this point, The charge will occur post the completion of the job.");
            } else {
                txtPayment.setText("Amount $" + payableAmountResponse.Response.bidAmount + "will deducted from  the account ");
            }

            if (cardAlreadyStored) {
                txtPayment.setText("Your card is already saved, you can update if neccessary");
            }

        }


    }

    private void setListener() {


        btnSubmit.setOnClickListener(CusPaymentAlert.this);
        //skipLay.setOnClickListener(CusCalendarDialog.this);

    }


    private void sendToPaymentGateway() {

        if ((payableAmountResponse.Response.payableAmount != null) && mClientToken != null) {

            DropInRequest dropInRequest = new DropInRequest();
            dropInRequest.disablePayPal()
                    .disableVenmo()
                    .disablePayPal()
                    .disableGooglePayment()
                    .disableAndroidPay()
                    .amount(payableAmountResponse.Response.payableAmount)
                    .clientToken(mClientToken);

            startActivityForResult(dropInRequest.getIntent(getActivity()), REQUEST_CODE);

/*

            PaymentRequest request = new PaymentRequest();
            request.primaryDescription("OOstaJob");
            //request.secondaryDescription(payableAmountResponse.Response.payableAmount);
            request.submitButtonText("Pay");
            request.amount(payableAmountResponse.Response.payableAmount);
            request.currencyCode("USD");
            request.clientToken(mClientToken);
            startActivityForResult(request.getIntent(getActivity()), REQUEST_CODE);
*/

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.please_try_later), Toast.LENGTH_SHORT).show();
        }
    }

    private void sendPaymentTransaction() {
        PaymentTransactionInputResponse paymentTransactionInputResponse = null;
        if (!fromCustomerJobComplete) {
            //paymentTransactionInputResponse = new PaymentTransactionInputResponse(request.jobid, USER_ID, request.userID, "0", "initial", "success");
        } else {
            paymentTransactionInputResponse = new PaymentTransactionInputResponse(request.jobid, USER_ID, request.userID, payableAmountResponse.Response.bidAmount, "final", "success");
        }

        p.show();
        Call<PaymentTransactionResults> call = apiService.transctionStatus(paymentTransactionInputResponse);
        call.enqueue(new Callback<PaymentTransactionResults>() {
            @Override
            public void onResponse(Call<PaymentTransactionResults> call, Response<PaymentTransactionResults> response) {

                if (response.isSuccessful()) {
                    if (!fromCustomerJobComplete) {
                        paymentSucessfull = true;
                        sucessfullypayment();
                    } else {
                        paymentTransaction = true;
                        callFeedBackActivity();
                    }
                }
            }

            @Override
            public void onFailure(Call<PaymentTransactionResults> call, Throwable t) {
                Global.dismissProgress(p);
                //Toast.makeText()
            }
        });

    }

    private void sucessfullypayment() {

        if (paymentSucessfull && hireSucessfull) {


            Global.dismissProgress(p);
            if (getActivity() != null) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("fragPosition", 1);
                intent.putExtra("job_status", "hired");
                intent.putExtra("check", 3);
                dismiss();
                startActivity(intent);
            }
        }

    }

    private void hireContractor(final GetJobHiredModel.Request request) {
        p.show();
        Call<GetJobHiredModel.Response> call = apiService.hireContractor(request);
        call.enqueue(new Callback<GetJobHiredModel.Response>() {
            @Override
            public void onResponse(Call<GetJobHiredModel.Response> call, Response<GetJobHiredModel.Response> response) {

                if (response.isSuccessful()) {
                    GetJobHiredModel.Response response1 = response.body();
                    hireSucessfull = true;
                    if (getActivity() != null) {
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("fragPosition", 1);
                        intent.putExtra("job_status", "hired");
                        intent.putExtra("check", 3);
                        dismiss();
                        startActivity(intent);
                    }


                }
            }

            @Override
            public void onFailure(Call<GetJobHiredModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    public void sendNonceToServer(String non) {
        p.show();
        //String braintreeID, String nonce, String amount
        Nonce nonce = new Nonce(BRAIN_TREE_CUSTOMER_ID, non, payableAmountResponse.Response.payableAmount);
        Call<Nonce.NonceResult> call = apiService.sendNonce(nonce);
        call.enqueue(new Callback<Nonce.NonceResult>() {
            @Override
            public void onResponse(Call<Nonce.NonceResult> call, Response<Nonce.NonceResult> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResult().isSuccess()) {
                        if (!fromCustomerJobComplete) {
                            hireContractor(request);
                            sendPaymentTransaction();
                        } else {
                            sendPaymentTransaction();
                            onJobCompletedByCustomer();
                        }


                    } else {
                        Global.dismissProgress(p);
                       /* if ((response.body().getResult() != null) && (response.body().getResult() == null) && (response.body().getResult().get_attributes().getMessage() != null)) {

                        }*/
                        p.dismiss();
                        Toast.makeText(getContext(), "payment unsucessfull", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<Nonce.NonceResult> call, Throwable t) {
                Global.dismissProgress(p);
                p.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                String nonce = result.getPaymentMethodNonce().getNonce();

                if (fromCustomerJobComplete) {
                    sendNonceToServer(nonce);

                } else {
                    hireContractor(request);
                    sendPaymentTransaction();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            }
        }


    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                *//*PaymentMethodNonce paymentMethodNonce = data.getParcelableExtra(
                        BraintreePaymentActivity.EXTRA_PAYMENT_METHOD_NONCE
                );
*//*
     *//*  String nonce = paymentMethodNonce.getNonce();
                if (!TextUtils.isEmpty(nonce)) {
                    // Send the nonce to your server.
                    sendNonceToServer(nonce);
                }*//*
            }
        }
    }*/

    public void onJobCompletedByCustomer() {

        JobCompletedByConInput jobCompletedByConInput = new JobCompletedByConInput();
        jobCompletedByConInput.setAction_by("customer");
        jobCompletedByConInput.setLoggedUserId(USER_ID);
        jobCompletedByConInput.setJobId(request.jobid);
        jobCompletedByConInput.setUserID(contDetails.getContractorDetails().contractorID);


        Call<JobCompleteByConSideResponse> call = apiService.jobcompletedByCon(jobCompletedByConInput);
        call.enqueue(new Callback<JobCompleteByConSideResponse>() {
            @Override
            public void onResponse(Call<JobCompleteByConSideResponse> call, Response<JobCompleteByConSideResponse> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {

                    jobCompleteByCustomer = true;
                    //Toast.makeText(this,response.body().)
                    callFeedBackActivity();
                }
            }

            @Override
            public void onFailure(Call<JobCompleteByConSideResponse> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });

    }

    public void callFeedBackActivity() {
        if (paymentTransaction && jobCompleteByCustomer) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("contractor", contDetails);

            Intent intent = new Intent(getActivity(), FeedBackActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


}