package com.oostajob.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.Green.customfont.CustomCheckedTextView;
import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.adapter.ListCommonAdapter;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.JobSubTypeModel;
import com.oostajob.model.ListModel;

import java.util.ArrayList;


public class ListDialogFragment extends AppCompatDialogFragment implements OtherDialogFragment.OtherListener {

    ImageView btn_close;
    private String TITLE, SELECTED, SELECTED_JOB_ID = "", OTHERS = "";
    private int POSITION, CHOICE_MODE;
    private CustomTextView txtHeading;
    private ListView listAnsView;
    private SelectedListener listener;
    private ArrayList<String> checkedList;
    private ArrayList<JobSubTypeModel.JobsubtypeDetails> jobSubList;
    private ArrayList<JobSubTypeCharModel.CharValue> choiceList;
    private ArrayList<ListModel> valueList;
    private ListCommonAdapter adapter;
    private boolean firstTime = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkedList = new ArrayList<>();
        jobSubList = new ArrayList<>();
        choiceList = new ArrayList<>();
        valueList = new ArrayList<>();

        TITLE = getArguments().getString("title");
        SELECTED = getArguments().getString("selected");
        CHOICE_MODE = getArguments().getInt("choiceMode");
        POSITION = getArguments().getInt("position");
        if (POSITION == 1) {
            jobSubList = (ArrayList<JobSubTypeModel.JobsubtypeDetails>) getArguments().getSerializable("list");
            for (JobSubTypeModel.JobsubtypeDetails details : jobSubList) {
                ListModel model = new ListModel(details.getJobsubtype_id(), details.getSubtype_name());
                valueList.add(model);
            }
        } else {
            choiceList = (ArrayList<JobSubTypeCharModel.CharValue>) getArguments().getSerializable("list");
            for (JobSubTypeCharModel.CharValue details : choiceList) {
                ListModel model = new ListModel("0", details.getOption());
                valueList.add(model);
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_list, null);
        init(view);
        loadDefault();
        setListener();
        setAdapter();

        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    private void init(View rootVIew) {
        btn_close = (ImageView) rootVIew.findViewById(R.id.close_icon);
        txtHeading = (CustomTextView) rootVIew.findViewById(R.id.txtHeading);
        listAnsView = (ListView) rootVIew.findViewById(R.id.listView);
    }

    private void loadDefault() {
        txtHeading.setText(TITLE);
    }

    private void setAdapter() {
        adapter = new ListCommonAdapter(getActivity(), valueList);
        listAnsView.setChoiceMode(CHOICE_MODE);
        listAnsView.setAdapter(adapter);

        setChecked();
    }

    private void setChecked() {
        if (!TextUtils.isEmpty(SELECTED)) {
            String[] arr = SELECTED.split(",");
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < adapter.getCount(); j++) {
                    if (adapter.getItem(j).getName().equalsIgnoreCase(arr[i].trim())) {
                        listAnsView.setItemChecked(j, true);
                    }
                }
            }
        }
    }

    private void setListener() {
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnValue();
            }
        });
        listAnsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CustomCheckedTextView checkBox = (CustomCheckedTextView) view;
                if (checkBox.isChecked())
                    if (adapter.getItem(position).getName().contains("Other")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("selected", adapter.getItem(position).getName());
                        OtherDialogFragment otherDialogFragment = new OtherDialogFragment();
                        otherDialogFragment.setListener(ListDialogFragment.this);
                        otherDialogFragment.setArguments(bundle);
                        otherDialogFragment.show(getChildFragmentManager(), "Others");
                    } else if (CHOICE_MODE == ListView.CHOICE_MODE_SINGLE) {
                        returnValue();
                    }
            }
        });
    }

    private void returnValue() {
        getCheckedList();
        if (listener != null) {
            String selectedList = checkedList.toString().replace("[", "").replace("]", "");
            listener.onSelected(POSITION, selectedList, SELECTED_JOB_ID);
        }
        dismiss();
    }

    private void getCheckedList() {
        try {
            checkedList = new ArrayList<>();
            SparseBooleanArray array = listAnsView.getCheckedItemPositions();
            for (int i = 0; i < array.size(); i++) {
                if (array.get(array.keyAt(i))) {
//                    if (adapter.getItem(array.keyAt(i)).getName().contains("Other")) {
//                        checkedList.add(OTHERS);
//                    } else {
                    checkedList.add(adapter.getItem(array.keyAt(i)).getName());
//                    }
                    SELECTED_JOB_ID = adapter.getItem(array.keyAt(i)).getId();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setListener(SelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public void onSelected(String value) {
        OTHERS = value;
        ListModel model = adapter.getItem(adapter.getCount() - 1);
        model.setName(value);
        valueList.set(adapter.getCount() - 1, model);
        adapter.notifyDataSetChanged();
        if (!value.contains(":")) {
            listAnsView.setItemChecked(adapter.getCount() - 1, false);
        }
    }


    public interface SelectedListener {
        public void onSelected(int position, String selectedValue, String id);
    }
}
