package com.oostajob.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ForgotPasswordModel;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgetDialogFragment extends AppCompatDialogFragment {

    private ImageView btn_close;
    private EditText edtEmail;
    private CustomButton btnSubmit;
    private APIService apiService;
    private ProgressDialog p;
    private InputMethodManager imm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(getActivity());
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_forgot_password, null);
        init(view);
        setListener();
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    private void setListener() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(edtEmail.getWindowToken(), 0);
                if (validate()) {
                    forgotPassword();
                }
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void init(View rootVIew) {
        btn_close = (ImageView) rootVIew.findViewById(R.id.close_icon);
        edtEmail = (EditText) rootVIew.findViewById(R.id.edtEmail);
        btnSubmit = (CustomButton) rootVIew.findViewById(R.id.btnSubmit);
    }

    private void forgotPassword() {
        p.show();
        ForgotPasswordModel.Request request = new ForgotPasswordModel.Request(edtEmail.getText().toString().trim());
        Call<ForgotPasswordModel.Response> call = apiService.forgotPassword(request);
        call.enqueue(new Callback<ForgotPasswordModel.Response>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel.Response> call, Response<ForgotPasswordModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ForgotPasswordModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("success")) {
                        Toast.makeText(getActivity(), "Password Recovery Email Sent", Toast.LENGTH_LONG).show();
                        dismiss();
                    } else if (response1.getResult().equalsIgnoreCase("Failed")) {
                        Toast.makeText(getActivity(), response1.getUserDetails(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


    private boolean validate() {
        if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtEmail.getText().toString().trim());
            if (!matcher.matches()) {
                Toast.makeText(getActivity(), "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }
}
