package com.oostajob.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ComplaintsModel;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ComplaintDialog extends DialogFragment implements View.OnClickListener {

    private APIService apiService;
    private ProgressDialog p;
    private String userId, jobPostId;
    private CustomEditText edtComments;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userId = getArguments().getString("userId");
        jobPostId = getArguments().getString("jobPostId");
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(getActivity());
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_complaint, null);
        edtComments = (CustomEditText) view.findViewById(R.id.edtComments);
        CustomTextView btnCancel = (CustomTextView) view.findViewById(R.id.btnCancel);
        CustomTextView btnPost = (CustomTextView) view.findViewById(R.id.btnPost);

        btnCancel.setOnClickListener(this);
        btnPost.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        //
        switch (v.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnPost:
                if (isValidated()) {
                    postComplaint();
                }
                break;
        }
    }

    private boolean isValidated() {
        if (TextUtils.isEmpty(edtComments.getText().toString())) {
            Toast.makeText(getActivity(), getString(R.string.feedback), Toast.LENGTH_LONG).show();
            return true;
        }
        return true;
    }

    private void postComplaint() {
        p.show();
        ComplaintsModel.Request request = new ComplaintsModel.Request(userId, jobPostId, edtComments.getText().toString());
        Call<ComplaintsModel.Response> call = apiService.postComplaints(request);
        call.enqueue(new Callback<ComplaintsModel.Response>() {
            @Override
            public void onResponse(Call<ComplaintsModel.Response> call, Response<ComplaintsModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ComplaintsModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        Toast.makeText(getActivity(), "Complaint Posted", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("fragPosition", 1);
                        intent.putExtra("check", 4);
                        startActivity(intent);
                        dismiss();
                    } else if (response1.getResult().equalsIgnoreCase("Failed")) {
                        Toast.makeText(getActivity(), response1.getComplientsDetails(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<ComplaintsModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
                Toast.makeText(getActivity(), "Server Error, Try again..", Toast.LENGTH_LONG).show();
            }
        });
    }
}
