package com.oostajob.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ChangePasswordModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PasswordChangeFragment extends AppCompatDialogFragment implements AutoTextWatcher.TextWatcher{

    ImageView btn_close;
    private String USER_ID;
    private CustomEditText edtOldPass, edtNewPass, edtConfirmPass;
    private CustomButton btnSubmit;
    private APIService apiService;
    private ProgressDialog p;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(getActivity());
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_change_password, null);
        init(view);
        setListener();
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    private void setListener() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    changePassword();
                }
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        edtNewPass.addTextChangedListener(new AutoTextWatcher(R.id.edtNewPass, this));
        edtConfirmPass.addTextChangedListener(new AutoTextWatcher(R.id.edtConfirmPass, this));
    }

    private void init(View rootVIew) {
        btn_close = (ImageView) rootVIew.findViewById(R.id.close_icon);
        edtOldPass = (CustomEditText) rootVIew.findViewById(R.id.edtOldPass);
        edtNewPass = (CustomEditText) rootVIew.findViewById(R.id.edtNewPass);
        edtConfirmPass = (CustomEditText) rootVIew.findViewById(R.id.edtConfirmPass);
        btnSubmit = (CustomButton) rootVIew.findViewById(R.id.btnSubmit);
    }

    private void changePassword() {
        p.show();
        ChangePasswordModel.Request request = new ChangePasswordModel.Request(USER_ID,
                edtOldPass.getText().toString(), edtNewPass.getText().toString());
        Call<ChangePasswordModel.Response> call = apiService.changePassword(request);
        call.enqueue(new Callback<ChangePasswordModel.Response>() {
            @Override
            public void onResponse(Call<ChangePasswordModel.Response> call, Response<ChangePasswordModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ChangePasswordModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("success")) {
                        Toast.makeText(getActivity(), "Password Changed", Toast.LENGTH_LONG).show();
                        dismiss();
                    } else if (response1.getResult().equalsIgnoreCase("failed")) {
                        Toast.makeText(getActivity(), "Old password is incorrect", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


    private boolean validate() {
        if (TextUtils.isEmpty(edtOldPass.getText().toString())) {
            Toast.makeText(getActivity(), "Enter Old Password", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtNewPass.getText().toString())) {
            Toast.makeText(getActivity(), "Enter New Password", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtConfirmPass.getText().toString())) {
            Toast.makeText(getActivity(), "Enter Confirm Password", Toast.LENGTH_LONG).show();
            return false;
        }
        if (edtNewPass.getText().length() < 10) { //password must have minimum 10 characters
            Toast.makeText(getActivity(), R.string.password_not_valid, Toast.LENGTH_LONG).show();
            return false;
        }
        if (!edtConfirmPass.getText().toString().equals(edtNewPass.getText().toString())) {
            Toast.makeText(getActivity(), "Password doesn't match", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void afterTextChanged(int view, Editable s) {
        String result;
        switch (view) {
            case R.id.edtNewPass:
                result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    edtNewPass.setText(result);
                    edtNewPass.setSelection(result.length());
                }
                break;
            case R.id.edtConfirmPass:
                result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    edtConfirmPass.setText(result);
                    edtConfirmPass.setSelection(result.length());
                }
                break;
            default:
                break;
        }
    }
}
