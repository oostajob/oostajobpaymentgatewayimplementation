package com.oostajob.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.adapter.contractor.ExpertiseAdapter;
import com.oostajob.model.JobTypeModel;
import com.oostajob.model.JobTypeModel.JobtypeDetails;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExpertiseDialog extends AppCompatDialogFragment {

    private APIService apiService;
    private CustomTextView txtDone;
    private ListView listExpert;
    private ArrayList<JobtypeDetails> list = new ArrayList<>();
    private ExpertiseAdapter adapter;
    private String select;
    private ArrayList<String> checkedList = new ArrayList<>();
    private ArrayList<String> checkedIdList = new ArrayList<>();
    private ProgressBar progress;

    private ExpertiseListener expertiseListener;

    public void setOnDone(ExpertiseListener expertiseListener) {
        this.expertiseListener = expertiseListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);
        select = getArguments().getString("select");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_expertise, null);
        init(view);
        setListener();
        setAdapter();

        builder.setView(view);
        return builder.create();
    }


    private void init(View view) {
        txtDone = (CustomTextView) view.findViewById(R.id.txtDone);
        listExpert = (ListView) view.findViewById(R.id.listExpert);
        progress = (ProgressBar) view.findViewById(R.id.progress);
    }

    private void setListener() {
        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCheckedList();
                getCheckedIdList();
                if (expertiseListener != null) {
                    if (checkedList.size() > 0 && checkedIdList.size() > 0) {
                        String selectedList = checkedList.toString().replace("[", "").replace("]", "");
                        String selectedIdList = checkedIdList.toString().replace("[", "").replace("]", "");
                        expertiseListener.onDone(selectedList, selectedIdList);
                        dismiss();
                    } else {
                        expertiseListener.onDone(getActivity().getString(R.string.expertise), "");
                        dismiss();
                    }
                }
            }
        });
    }

    private void getCheckedList() {
        try {
            checkedList = new ArrayList<>();
            SparseBooleanArray array = listExpert.getCheckedItemPositions();
            for (int i = 0; i < array.size(); i++)
                if (array.get(array.keyAt(i)))
                    checkedList.add(adapter.getItem(array.keyAt(i)).getJob_name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCheckedIdList() {
        try {
            checkedIdList = new ArrayList<>();
            SparseBooleanArray array = listExpert.getCheckedItemPositions();
            for (int i = 0; i < array.size(); i++)
                if (array.get(array.keyAt(i)))
                    checkedIdList.add(adapter.getItem(array.keyAt(i)).getJobtype_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                progress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    JobTypeModel  model = response.body();
                    if (model.getResult().equalsIgnoreCase("Success")) {
                        list = model.getJobtypeDetails();
                        for (JobtypeDetails det : list) {
                            if (det.getJob_name().equalsIgnoreCase("Others")) {
                                list.remove(det);
                                break;
                            }
                        }
                        Collections.sort(list, new Comparator<JobtypeDetails>() {
                            @Override
                            public int compare(JobtypeDetails lhs, JobtypeDetails rhs) {
                                return lhs.getJob_name().compareTo(rhs.getJob_name());
                            }
                        });
                        adapter = new ExpertiseAdapter(getActivity(), list);
                        listExpert.setAdapter(adapter);
                        setChecked();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void setChecked() {
        if (!TextUtils.isEmpty(select)) {
            String[] arr = select.split(",");
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < adapter.getCount(); j++) {
                    if (arr[i].trim().equalsIgnoreCase(adapter.getItem(j).getJob_name())) {
                        listExpert.setItemChecked(j, true);
                    }
                }
            }
        }
    }

    public interface ExpertiseListener {
        public void onDone(String value, String ids);
    }
}
