package com.oostajob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomCheckBox;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.dialog.ExpertiseDialog;
import com.oostajob.dialog.PhotoDialog;
import com.oostajob.dialog.TermsDialogFragment;
import com.oostajob.global.Global;
import com.oostajob.model.ConRegModel;
import com.oostajob.model.TimeZone;
import com.oostajob.utils.FilePath;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.yalantis.ucrop.UCrop;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConRegActivity extends AppCompatActivity implements AutoTextWatcher.TextWatcher, ExpertiseDialog.ExpertiseListener,
        ImageListener {

    private String GCM_TOKEN;
    private CustomButton btnRegister;
    private Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
    private Toolbar toolbar;
    private CustomTextView txt_terms, edtLicence, edtExpAt, edtAdd;
    private CustomEditText edtName, edtEmail, edtPassword, edtPhone, edtHourRate;
    private ImageView imgProfile, img_licence_gal, name_add, email_add, password_add, address_add, phone_add, img_hourrate, img_licence, img_ExpAt;
    private String postalCode, lat = "0.0", lng = "0.0";
    private CustomCheckBox chkAgree;
    private APIService apiService, googleService;
    private Uri selectedPicUri, selectedLicPicUri;
    private Uri croppedPicUri, croppedLicPicUri;
    private ProgressDialog p;
    private RelativeLayout licence_layout;
    private String expertiseIDs;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_reg);

        apiService = RetrofitSingleton.createService(APIService.class);
        googleService = RetrofitSingleton.createGoogleService(APIService.class);
        GCM_TOKEN = PrefConnect.readString(ConRegActivity.this, PrefConnect.GCM_TOKEN, "");
        if (savedInstanceState != null) {
            selectedPicUri = Uri.parse(savedInstanceState.getString("selectedPicUri", ""));
            selectedLicPicUri = Uri.parse(savedInstanceState.getString("selectedLicPicUri", ""));
            croppedPicUri = Uri.parse(savedInstanceState.getString("croppedPicUri", ""));
            croppedLicPicUri = Uri.parse(savedInstanceState.getString("croppedLicPicUri", ""));
        }
        init();
        setUpToolBar();
        setListener();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txt_terms = (CustomTextView) findViewById(R.id.txt_con_terms);
        edtName = (CustomEditText) findViewById(R.id.edt_contractor_name);
        edtAdd = (CustomTextView) findViewById(R.id.edt_contractor_address);
        //edtSsn = (CustomEditText) findViewById(R.id.edt_contractor_ssn);
        edtEmail = (CustomEditText) findViewById(R.id.edt_contractor_email);
        edtPassword = (CustomEditText) findViewById(R.id.edt_contractor_password);
        edtPhone = (CustomEditText) findViewById(R.id.edt_contractor_phone);
        edtExpAt = (CustomTextView) findViewById(R.id.edt_expertise);
        edtHourRate = (CustomEditText) findViewById(R.id.edt_contractor_hourlybase);
        edtLicence = (CustomTextView) findViewById(R.id.edt_contractor_license);
        name_add = (ImageView) findViewById(R.id.name_add);
        email_add = (ImageView) findViewById(R.id.email_add);
        password_add = (ImageView) findViewById(R.id.password_add);
        address_add = (ImageView) findViewById(R.id.address_add);
        phone_add = (ImageView) findViewById(R.id.phone_add);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        img_licence_gal = (ImageView) findViewById(R.id.img_licence_gallery);
        //img_ssn = (ImageView) findViewById(R.id.img_ssn);
        img_hourrate = (ImageView) findViewById(R.id.img_hourrate);
        img_licence = (ImageView) findViewById(R.id.img_licence);
        img_ExpAt = (ImageView) findViewById(R.id.img_ExpAt);
        chkAgree = (CustomCheckBox) findViewById(R.id.chkAgree);
        licence_layout = (RelativeLayout) findViewById(R.id.licence_layout);
        btnRegister = (CustomButton) findViewById(R.id.btnRegister);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        p = new ProgressDialog(ConRegActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);

        if (!Global.isUriEmpty(croppedPicUri)) {
            imgProfile.setImageURI(croppedPicUri);
        }
        if (!Global.isUriEmpty(croppedLicPicUri)) {
            img_licence_gal.setImageURI(croppedLicPicUri);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (!Global.isUriEmpty(selectedPicUri))
            outState.putString("selectedPicUri", selectedPicUri.toString());
        if (!Global.isUriEmpty(selectedLicPicUri))
            outState.putString("selectedLicPicUri", selectedLicPicUri.toString());
        if (!Global.isUriEmpty(croppedPicUri))
            outState.putString("croppedPicUri", croppedPicUri.toString());
        if (!Global.isUriEmpty(croppedLicPicUri))
            outState.putString("croppedLicPicUri", croppedLicPicUri.toString());
        super.onSaveInstanceState(outState);
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edtAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(ConRegActivity.this, SelectAddrActivity.class);
                mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mapIntent.putExtra("title", "Select Address");
                mapIntent.putExtra("goto", "");
                mapIntent.putExtra("lat", Double.parseDouble(lat));
                mapIntent.putExtra("lng", Double.parseDouble(lng));
                startActivityForResult(mapIntent, Global.ADDRESS);
            }
        });
        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsDialogFragment dialogFragment = new TermsDialogFragment();
                Bundle args = new Bundle();
                args.putInt("dialog_id", 2);
                dialogFragment.setArguments(args);
                dialogFragment.show(getSupportFragmentManager(), "con_dialog");
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    if (chkAgree.isChecked()) {
                        new RegisterTask().execute();
                    } else {
                        Toast.makeText(ConRegActivity.this, getString(R.string.accept_terms), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString("title", "Picture");
                args.putBoolean("isLicence", false);
                if (!Global.isUriEmpty(croppedPicUri)) {
                    args.putBoolean("isRemove", true);
                }
                photoDialog.setArguments(args);
                photoDialog.setCancelable(false);
                photoDialog.setImageListener(ConRegActivity.this);
                photoDialog.show(getSupportFragmentManager(), "Photo Dialog");
            }
        });
        licence_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString("title", "Picture");
                args.putBoolean("isLicence", true);
                if (!Global.isUriEmpty(croppedLicPicUri)) {
                    args.putBoolean("isRemove", true);
                }
                photoDialog.setArguments(args);
                photoDialog.setCancelable(false);
                photoDialog.setImageListener(ConRegActivity.this);
                photoDialog.show(getSupportFragmentManager(), "lic_Photo Dialog");
            }
        });
        edtExpAt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("select", edtExpAt.getText().toString());
                ExpertiseDialog expertiseDialog = new ExpertiseDialog();
                expertiseDialog.setCancelable(false);
                expertiseDialog.setOnDone(ConRegActivity.this);
                expertiseDialog.setArguments(bundle);
                expertiseDialog.show(getSupportFragmentManager(), "ExpertiseAT Dialog");
            }
        });
        edtName.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_name, this));
        edtEmail.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_email, this));
        edtPassword.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_password, this));
        edtPhone.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_phone, this));
        // edtSsn.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_ssn, this));
        edtHourRate.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_hourlybase, this));
    }


    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }


    private boolean isValidated() {
        if (TextUtils.isEmpty(edtName.getText().toString())) {
            Toast.makeText(ConRegActivity.this, "Enter name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            Toast.makeText(ConRegActivity.this, "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtEmail.getText().toString().trim());
            if (!matcher.matches()) {
                Toast.makeText(ConRegActivity.this, "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        /*if (TextUtils.isEmpty(edtSsn.getText().toString())) {
            Toast.makeText(ConRegActivity.this, "Enter SSN", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtSsn.getText().toString())) {
            Pattern pattern = Pattern.compile("^([0-9]{9})$", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtSsn.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(ConRegActivity.this, "Enter valid SSN", Toast.LENGTH_LONG).show();
                return false;
            }
        }*/
        if (TextUtils.isEmpty(edtPassword.getText())) {
            Toast.makeText(ConRegActivity.this, "Enter password", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtPassword.getText().length() < 10) { //minimum 10 character needed
            Toast.makeText(ConRegActivity.this, R.string.password_not_valid, Toast.LENGTH_LONG).show();
            return false;
        }
        if (edtAdd.getText().toString().equalsIgnoreCase("Address")) {
            Toast.makeText(ConRegActivity.this, "Select Address", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            Toast.makeText(ConRegActivity.this, "Enter Phone Number", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            long phone = Long.parseLong(edtPhone.getText().toString().trim());
            if (phone == 0 || edtPhone.getText().toString().trim().length() != 10) {
                Toast.makeText(ConRegActivity.this, "Enter Valid Phone number", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (edtExpAt.getText().toString().equalsIgnoreCase("Expertise At")) {
            Toast.makeText(ConRegActivity.this, "Select Expertise", Toast.LENGTH_LONG).show();
            return false;
        }
        /* Removed hourly rate, so the validation also commenting
        if (TextUtils.isEmpty(edtHourRate.getText().toString())) {
            Toast.makeText(ConRegActivity.this, "Enter Hourly Rates", Toast.LENGTH_LONG).show();
            return false;
        }*/
        if (Global.isUriEmpty(croppedPicUri)) {
            Toast.makeText(ConRegActivity.this, "Upload your picture", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Global.isUriEmpty(croppedLicPicUri)) {
            Toast.makeText(ConRegActivity.this, "Select License to Upload", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /* private void startVerificationActivity() {
         Intent intent = new Intent(ConRegActivity.this, VerificationActivity.class);
         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
         startActivity(intent);
     }
 */
    @Override
    public void afterTextChanged(int view, Editable s) {
        switch (view) {
            case R.id.edt_contractor_name:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(name_add);
                else
                    setTickMark(name_add);
                break;
            case R.id.edt_contractor_email:
                Matcher matcher = pattern.matcher(s.toString());
                if (TextUtils.isEmpty(s.toString()) || !matcher.matches())
                    setPlusMark(email_add);
                else
                    setTickMark(email_add);
                break;
            case R.id.edt_contractor_password:
                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    edtPassword.setText(result);
                    edtPassword.setSelection(result.length());
                    // alert the user
                }
                if (TextUtils.isEmpty(result) || result.length() < 10) // minimum 10 character must
                    setPlusMark(password_add);
                else
                    setTickMark(password_add);
                break;
            case R.id.edt_contractor_phone:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(phone_add);
                else
                    setTickMark(phone_add);
                break;
          /*  case R.id.edt_contractor_ssn:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(img_ssn);
                else
                    setTickMark(img_ssn);
                break;*/
            case R.id.edt_contractor_hourlybase:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(img_hourrate);
                else
                    setTickMark(img_hourrate);
                break;
        }
    }

    @Override
    public void onDone(String value, String ids) {
        edtExpAt.setText(value);
        expertiseIDs = ids;
        if (value.equalsIgnoreCase(getString(R.string.expertise))) {
            setPlusMark(img_ExpAt);
        } else {
            setTickMark(img_ExpAt);

        }
    }

    @Override
    public void onImageSelected(Uri uri, boolean isLicense) {
        try {
            if (isLicense) {
                selectedLicPicUri = uri;
                if (Global.isUriEmpty(selectedLicPicUri)) {
                    img_licence_gal.setImageResource(R.drawable.camera);
                    img_licence_gal.setVisibility(View.GONE);
                    edtLicence.setVisibility(View.VISIBLE);
                    croppedLicPicUri = null;
                    setPlusMark(img_licence);
                }
            } else {
                selectedPicUri = uri;
                if (Global.isUriEmpty(selectedPicUri)) {
                    imgProfile.setImageDrawable(null);
                    croppedPicUri = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    try {
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();

                        /*
                        *
                        * Bundle extras = data.getExtras();
                        Bitmap thePic = extras.getParcelable("data");
                        imgProfile.setImageBitmap(thePic);
                        * */
                        Bundle extras = data.getExtras();
                        selectedPicUri = Global.getImageUri(this.getApplicationContext(), (Bitmap) extras.getParcelable("data"));

                        Global.doCrop(ConRegActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK:
                    try {
                        selectedPicUri = data.getData();
                        croppedPicUri = Uri.fromFile(Global.getPhotoDirectory());
                        Global.doCrop(ConRegActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.PICTURE_CROP:
                    croppedPicUri = UCrop.getOutput(data);
                    if (!Global.isUriEmpty(croppedPicUri)) {
                        imgProfile.setImageURI(croppedPicUri);
                    } else {
                        Bundle extras = data.getExtras();
                        Bitmap thePic = extras.getParcelable("data");
                        imgProfile.setImageBitmap(thePic);
                        Global.saveImage(ConRegActivity.this, thePic, croppedPicUri);
                    }
                    break;
                // =====lic pic
                case Global.CAMERA_CAPTURE_lICENCE:
                    try {
                        croppedLicPicUri = Global.generateTimeStampPhotoFileUri();
                        Bundle extras = data.getExtras();
                        selectedLicPicUri = Global.getImageUri(this.getApplicationContext(), (Bitmap) extras.getParcelable("data"));

                        Global.doCrop(ConRegActivity.this, selectedLicPicUri, croppedLicPicUri, Global.PICTURE_CROP_lICENCE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK_lICENCE:
                    try {
                        selectedLicPicUri = data.getData();
                        croppedLicPicUri = Uri.fromFile(Global.getPhotoDirectory());
                        Global.doCrop(ConRegActivity.this, selectedLicPicUri, croppedLicPicUri, Global.PICTURE_CROP_lICENCE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.PICTURE_CROP_lICENCE:
                    croppedLicPicUri = UCrop.getOutput(data);
                    if (!Global.isUriEmpty(croppedLicPicUri)) {
                        img_licence_gal.setImageURI(croppedLicPicUri);
                    } else {
                        Bundle lic_extras = data.getExtras();
                        Bitmap lic_thePic = lic_extras.getParcelable("data");
                        img_licence_gal.setImageBitmap(lic_thePic);
                        Global.saveImage(ConRegActivity.this, lic_thePic, croppedLicPicUri);
                    }
                    img_licence_gal.setVisibility(View.VISIBLE);
                    edtLicence.setVisibility(View.GONE);
                    setTickMark(img_licence);
                    break;

                case Global.ADDRESS:
                    edtAdd.setText(data.getStringExtra("address"));
                    postalCode = data.getStringExtra("postalCode");
                    lat = data.getStringExtra("lat");
                    lng = data.getStringExtra("lng");
                    setTickMark(address_add);
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    if (!Global.isUriEmpty(selectedPicUri)) {
                        //Global.deleteFile(getApplicationContext(), selectedPicUri);
                        selectedPicUri = null;
                    }
                    croppedPicUri = null;
                    break;
                case Global.PICTURE_CROP:
                    croppedPicUri = null;
                    selectedPicUri = null;
                    break;
                case Global.PICTURE_CROP_lICENCE:
                    croppedLicPicUri = null;
                    selectedLicPicUri = null;
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            //handleCropError(data);
            Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
            if (requestCode == Global.PICTURE_CROP) {
                croppedPicUri = null;
            } else if (requestCode == Global.PICTURE_CROP_lICENCE) {
                croppedLicPicUri = null;
            }
        }
    }

    private void saveData(ConRegModel.ContractorDetails details) {
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.USER_ID, details.getUserID());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.EMAIL, details.getEmail());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.PASSWORD, details.getPassword());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.USER_TYPE, details.getUserType());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.NAME, details.getName());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.ADDRESS, details.getAddress());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.PHONE, details.getPhone());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.PROFILE_PHOTO, details.getProfilePhoto());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.ZIPCODE, details.getZipcode());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.LAT, details.getLat());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.LNG, details.getLng());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.SSN, details.getSSN());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.EXPERTISE_AT, details.getExpertiseAT());
        //   PrefConnect.writeString(ConRegActivity.this, PrefConnect.HOURLY_RATE, details.getHourlyRate());
        PrefConnect.writeString(ConRegActivity.this, PrefConnect.LICENSE, details.getLicence());
        PrefConnect.writeInteger(ConRegActivity.this, PrefConnect.PAGE, 1);
    }

    private class RegisterTask extends AsyncTask<String, Void, String> {
        HttpEntity resEntity;
        String url = Global.BASE_URL + "/api/index.php/license";

        @Override
        protected void onPreExecute() {
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                MultipartEntity reqEntity = getMultiPart();
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                final String response_str = EntityUtils.toString(resEntity);
                if (resEntity != null) {
                    Log.i("RESPONSE", response_str);
                    return response_str;
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {
                try {
                    if (new JSONObject(s).getString("Result").equalsIgnoreCase("Success")) {
                        registerProfile(p);
                    } else {
                        Toast.makeText(ConRegActivity.this, "Photo and License not uploaded", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(ConRegActivity.this, "Photo and License not uploaded", Toast.LENGTH_LONG).show();
            }
        }

        public MultipartEntity getMultiPart() {
            MultipartEntity reqEntity = new MultipartEntity();
            if (!Global.isUriEmpty(croppedPicUri)) {
                File pFile = new File(FilePath.getPath(ConRegActivity.this, croppedPicUri));
                FileBody fileBody = new FileBody(pFile);
                reqEntity.addPart("profile", fileBody);
            }

            if (!Global.isUriEmpty(croppedLicPicUri)) {
                File lFile = new File(FilePath.getPath(ConRegActivity.this, croppedLicPicUri));
                FileBody fileBody1 = new FileBody(lFile);
                reqEntity.addPart("license", fileBody1);
            }
            return reqEntity;
        }

        private void registerProfile(final ProgressDialog p) {
            final File pFile = new File(FilePath.getPath(ConRegActivity.this, croppedPicUri));
            String location = String.format("%s,%s", lat, lng);
            //############ Get TimeZone form Google API (e.g, Asia/Calcutta)
            Call<TimeZone> call = googleService.getTimeZone(location, System.currentTimeMillis() / 1000,"AIzaSyCp77NabT4Yj4SUCXT7aG8eRh5I4mtki9M");
            call.enqueue(new Callback<TimeZone>() {
                @Override
                public void onResponse(Call<TimeZone> call, Response<TimeZone> response) {
                    if (response.isSuccessful()) {
                        TimeZone timeZone = response.body();
                        if (timeZone.getStatus().equalsIgnoreCase("ok")) {
                            ConRegModel.Request request;
                            final File lFile = new File(FilePath.getPath(ConRegActivity.this, croppedLicPicUri));
                            request = new ConRegModel.Request(edtName.getText().toString(), edtAdd.getText().toString(), edtPhone.getText().toString(),
                                    !Global.isUriEmpty(croppedPicUri) ? pFile.getName() : "",
                                    postalCode, lat, lng, GCM_TOKEN, "1", edtEmail.getText().toString().trim(), edtPassword.getText().toString(), "",
                                    expertiseIDs, !Global.isUriEmpty(croppedLicPicUri) ? lFile.getName() : "", /*edtHourRate.getText().toString(),*/ timeZone.getTimeZoneId());

                            request.hourlyRate = "";
                            //############ Register Contractor Information
                            Call<ConRegModel.Response> regCall = apiService.conRegister(request);
                            regCall.enqueue(new Callback<ConRegModel.Response>() {
                                @Override
                                public void onResponse(Call<ConRegModel.Response> call, Response<ConRegModel.Response> response) {
                                    Global.dismissProgress(p);
                                    if (response.isSuccessful()) {
                                        ConRegModel.Response response1 = response.body();
                                        if (response1.getResult().equalsIgnoreCase("Success")) {
                                            Toast.makeText(ConRegActivity.this, "Registered", Toast.LENGTH_LONG).show();
                                            ConRegModel.ContractorDetails details = response1.getContractorDetails().get(0);
                                            saveData(details);

                                            // startContractorBankDetailsActivity();
                                            startVerificationActivity();
                                        } else {
                                            Toast.makeText(ConRegActivity.this, response1.getStatus(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ConRegModel.Response> call, Throwable t) {
                                    Global.dismissProgress(p);
//                                    if (t.getCause()..getKind().name().equalsIgnoreCase("conversion")) {
//                                        Toast.makeText(ConRegActivity.this,  "Email Already Exists", Toast.LENGTH_LONG).show();
//                                    }
                                }
                            });
                        }
                    }

                }

                @Override
                public void onFailure(Call<TimeZone> call, Throwable t) {
                    Global.dismissProgress(p);
                }
            });
        }
    }

    private void startVerificationActivity() {
        Intent intent = new Intent(ConRegActivity.this, VerificationActivity.class);
        startActivity(intent);
    }


    private void startContractorBankDetailsActivity() {
        Intent intent = new Intent(ConRegActivity.this, ConBankDetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
