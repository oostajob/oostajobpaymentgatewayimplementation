package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.dialog.ComplaintDialog;
import com.oostajob.dialog.CusRepostJob;
import com.oostajob.global.Global;
import com.oostajob.model.AddRatingModel;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobTypeModel;
import com.oostajob.utils.CircleTransform;
import com.oostajob.utils.FetchAddress;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FeedBackActivity extends AppCompatActivity {

    private GetCustDetailsModel.Details details;
    private GetCustDetailsModel.ContractorDetails contractor;
    private ProgressDialog p;
    private Toolbar toolbar;
    private APIService apiService;
    private CustomTextView txtName, txtExpertise, txtAddress, txtComplaint;
    private RatingBar rateTime, ratePrice, rateQuality, rateSatis;
    private ImageView imgProfile;
    private CustomEditText edtComments;
    private CustomButton btnPost;

    private boolean fromCancelJobBYCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        init();
        setUpToolbar();
        setListener();
        loadValues();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        txtName = (CustomTextView) findViewById(R.id.txtName);
        txtExpertise = (CustomTextView) findViewById(R.id.txtExpertise);
        txtAddress = (CustomTextView) findViewById(R.id.txtAddress);
        txtComplaint = (CustomTextView) findViewById(R.id.txtComplaint);
        edtComments = (CustomEditText) findViewById(R.id.edtComments);
        btnPost = (CustomButton) findViewById(R.id.btnPost);
        rateTime = (RatingBar) findViewById(R.id.rateTime);
        ratePrice = (RatingBar) findViewById(R.id.ratePrice);
        rateQuality = (RatingBar) findViewById(R.id.rateQuality);
        rateSatis = (RatingBar) findViewById(R.id.rateSatis);

        p = new ProgressDialog(FeedBackActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        apiService = RetrofitSingleton.createService(APIService.class);
    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fromCancelJobBYCustomer) {
                    Intent homeIntent = new Intent(FeedBackActivity.this, HomeActivity.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    homeIntent.putExtra("fragPosition", 1);
                    homeIntent.putExtra("check", 4);
                    homeIntent.putExtra("job_status", "rated");
                    startActivity(homeIntent);
                } else {
                    CusRepostJob dialog = new CusRepostJob();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("GetCustDetailsModel", details);
                    bundle.putSerializable("jobPostID", details.getJobpostID());
                    bundle.putString("title", "cancel");
                    bundle.putBoolean("isCustomer", true);
                    dialog.setArguments(bundle);
                    dialog.setArguments(bundle);
                    dialog.show(getSupportFragmentManager(), "Hire Dialog");

                }
            }
        });
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    sendFeedBack();
                }
            }
        });

        txtComplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("userId", contractor.getContractorID());
                bundle.putString("jobPostId", details.getJobpostID());
                ComplaintDialog dialog = new ComplaintDialog();
                dialog.setArguments(bundle);
                dialog.show(getSupportFragmentManager(), "Complaints");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(FeedBackActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void loadValues() {
        details = (GetCustDetailsModel.Details) getIntent().getExtras().getSerializable("contractor");
        assert details != null;
        contractor = details.getContractorDetails();

        // Load Profile Image
        String IMG_URL = String.format("%s/%s", Global.BASE_URL, contractor.getProfilePhoto());
        Picasso.with(this).load(IMG_URL).transform(new CircleTransform()).fit().into(imgProfile);

        //Name
        txtName.setText(contractor.getUsername());
        //Address
        LatLng latLng = new LatLng(Double.parseDouble(contractor.getLat()), Double.parseDouble(contractor.getLng()));
        FetchAddress fetchAddress = new FetchAddress(this, latLng);
        fetchAddress.getAddress();
        txtAddress.setText(String.format("%s - %s", fetchAddress.getLocality(), fetchAddress.getPostalCode()));

        fromCancelJobBYCustomer = getIntent().getExtras().getBoolean("fromCancelJOb");

        //Expertise At
        setExpertise();
    }

    private void setExpertise() {
        p.show();
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    JobTypeModel jobTypeModel = response.body();
                    ArrayList<String> expertiseList = new ArrayList<>();
                    if (jobTypeModel.getResult().equalsIgnoreCase("success")) {
                        ArrayList<JobTypeModel.JobtypeDetails> jobTypesList = jobTypeModel.getJobtypeDetails();
                        String[] arr = contractor.getExpertiseAT().split(",");
                        for (String anArr : arr) {
                            for (int j = 0; j < jobTypesList.size(); j++) {
                                if (jobTypesList.get(j).getJobtype_id().equals(anArr.trim())) {
                                    expertiseList.add(jobTypesList.get(j).getJob_name());
                                }
                            }
                        }
                        String tempVar = expertiseList.toString().replace("[", "").replace("]", "");
                        txtExpertise.setText(tempVar);
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private boolean isValidated() {
        if (TextUtils.isEmpty(edtComments.getText().toString())) {
            Toast.makeText(FeedBackActivity.this, "Enter Feedback", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void sendFeedBack() {
        p.show();
        final AddRatingModel.Request request = new AddRatingModel.Request(contractor.getContractorID(), details.getJobpostID(), Float.toString(ratePrice.getRating())
                , Float.toString(rateTime.getRating()), Float.toString(rateQuality.getRating()), Float.toString(rateSatis.getRating())
                , edtComments.getText().toString());
        Call<AddRatingModel.Response> call = apiService.addRating(request);
        call.enqueue(new Callback<AddRatingModel.Response>() {
            @Override
            public void onResponse(Call<AddRatingModel.Response> call, Response<AddRatingModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    AddRatingModel.Response response1 = response.body();

                    if (!fromCancelJobBYCustomer) {

                        if (response1.getResult().equalsIgnoreCase("Success")) {
                            Intent homeIntent = new Intent(FeedBackActivity.this, HomeActivity.class);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            homeIntent.putExtra("fragPosition", 1);
                            homeIntent.putExtra("check", 4);
                            homeIntent.putExtra("job_status", "rated");
                            startActivity(homeIntent);
                        }
                    } else {
                        CusRepostJob dialog = new CusRepostJob();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("GetCustDetailsModel", details);
                        bundle.putSerializable("jobPostID", details.getJobpostID());
                        bundle.putString("title", "cancel");
                        bundle.putBoolean("isCustomer", true);
                        dialog.setArguments(bundle);
                        dialog.setArguments(bundle);
                        dialog.show(getSupportFragmentManager(), "Hire Dialog");
                    }
                }

            }

            @Override
            public void onFailure(Call<AddRatingModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }
}
