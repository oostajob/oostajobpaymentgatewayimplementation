package com.oostajob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomEditText;
import com.oostajob.adapter.InboxAdapter;
import com.oostajob.fragment.InboxFragment;
import com.oostajob.global.Global;
import com.oostajob.model.ConBankInputModel;
import com.oostajob.model.ConRegModel;
import com.oostajob.model.InboxModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConBankDetailActivity extends AppCompatActivity implements View.OnClickListener, AutoTextWatcher.TextWatcher {

    ImageView skipBankDetails;
    Button btnSubmit;
    private APIService apiService;
    private Toolbar toolbar;
    private ProgressDialog p;

    ImageView imgRounting, imgBankAccountNumber, imgBankAccountName, imgBankName;

    com.Green.customfont.CustomEditText edtBankName, edtBankAccountNumber, edtUserAccountName, edtRounting;
    private String USER_ID;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contr_bank_details);
        init();
        setUpToolBar();
        setListener();
        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
    }

    private void setListener() {
        skipBankDetails.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        edtBankName.addTextChangedListener(new AutoTextWatcher(R.id.edtBankName, this));
        edtBankAccountNumber.addTextChangedListener(new AutoTextWatcher(R.id.edtBankAccountNumber, this));
        edtUserAccountName.addTextChangedListener(new AutoTextWatcher(R.id.edtUserAccountName, this));
        edtRounting.addTextChangedListener(new AutoTextWatcher(R.id.edtRounting, this));
    }

    private void init() {
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(this);
        skipBankDetails = (ImageView) findViewById(R.id.skip);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edtBankName = (CustomEditText) findViewById(R.id.edtBankName);
        edtBankAccountNumber = (CustomEditText) findViewById(R.id.edtBankAccountNumber);
        edtUserAccountName = (CustomEditText) findViewById(R.id.edtUserAccountName);
        edtRounting = (CustomEditText) findViewById(R.id.edtRounting);
        imgRounting = (ImageView) findViewById(R.id.imgRounting);
        imgBankAccountNumber = (ImageView) findViewById(R.id.imgBankAccountNumber);
        imgBankAccountName = (ImageView) findViewById(R.id.imgBankAccountName);
        imgBankName = (ImageView) findViewById(R.id.imgBankName);


        // p = Global.initProgress(this);
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.background_back);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skip:

                startHomeActivity();
                break;

            case R.id.btnSubmit:
                if (isValidated()) {

                    p.show();
                    Call<ConRegModel.ContractorBankDetailsResponse> call = apiService.changeBankDetails(populateData());

                    call.enqueue(new Callback<ConRegModel.ContractorBankDetailsResponse>() {
                        @Override
                        public void onResponse(Call<ConRegModel.ContractorBankDetailsResponse> call, Response<ConRegModel.ContractorBankDetailsResponse> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                //if(response.body().getResult().equalsIgnoreCase("Success"))
                                Toast.makeText(ConBankDetailActivity.this, response.body().getDetails(), Toast.LENGTH_SHORT).show();
                                saveBankDetails(response.body());
                                startHomeActivity();

                            }
                        }

                        @Override
                        public void onFailure(Call<ConRegModel.ContractorBankDetailsResponse> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });
                }

                break;
        }
    }

    private void saveBankDetails(ConRegModel.ContractorBankDetailsResponse contractorBankDetailsResponse) {
        /*
         public static final String BANK_NAME = "BANK_NAME";
    public static final String ACCOUNT_HOLDER_NAME = "ACCOUNT_HOLDER_NAME";
    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String ROUTING = "ROUTING";
         */
        if (contractorBankDetailsResponse.getResponse() != null) {
            PrefConnect.writeString(ConBankDetailActivity.this, PrefConnect.BANK_NAME, edtBankName.getText().toString());
            PrefConnect.writeString(ConBankDetailActivity.this, PrefConnect.ACCOUNT_HOLDER_NAME, edtUserAccountName.getText().toString());
            PrefConnect.writeString(ConBankDetailActivity.this, PrefConnect.ACCOUNT_NUMBER, edtBankAccountNumber.getText().toString());
            PrefConnect.writeString(ConBankDetailActivity.this, PrefConnect.ROUTING, edtRounting.getText().toString());

        }
    }


    private ConBankInputModel populateData() {
        ConBankInputModel contractorBankDetailsInput = new ConBankInputModel();
        contractorBankDetailsInput.setContractor_id(USER_ID);
        contractorBankDetailsInput.setBank_name(edtBankName.getText().toString());
        contractorBankDetailsInput.setAccount_holder_name(edtUserAccountName.getText().toString());
        contractorBankDetailsInput.setAccount_number(edtBankAccountNumber.getText().toString());
        contractorBankDetailsInput.setRouting(edtRounting.getText().toString());


        return contractorBankDetailsInput;

    }

    private boolean isValidated() {

        boolean falge = true;
        if (TextUtils.isEmpty(edtBankName.getText().toString().trim())) {
            Toast.makeText(ConBankDetailActivity.this, "Enter Bank name", Toast.LENGTH_LONG).show();
            falge = false;
        } else if (TextUtils.isEmpty(edtUserAccountName.getText().toString().trim())) {
            Toast.makeText(ConBankDetailActivity.this, "Enter user account name", Toast.LENGTH_LONG).show();
            falge = false;
        } else if (TextUtils.isEmpty(edtBankAccountNumber.getText().toString().trim())) {
            Toast.makeText(ConBankDetailActivity.this, "Enter bank account number", Toast.LENGTH_LONG).show();
            falge = false;
        } else if (TextUtils.isEmpty(edtRounting.getText().toString().trim())) {
            Toast.makeText(ConBankDetailActivity.this, "Enter routing number", Toast.LENGTH_LONG).show();
            falge = false;
        }

        return falge;
    }


    @Override
    public void afterTextChanged(int view, Editable s) {

        //edtBankName, edtBankAccountNumber, edtBankAccountName, edtRounting;``
        // ImageView imgRounting, imgBankAccountNumber, imgBankAccountName, imgBankName;
        switch (view) {
            case R.id.edtBankName:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgBankName);
                else
                    setTickMark(imgBankName);
                break;

            case R.id.edtBankAccountNumber:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgBankAccountNumber);
                else
                    setTickMark(imgBankAccountNumber);
                break;

            case R.id.edtUserAccountName:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgBankAccountName);
                else
                    setTickMark(imgBankAccountName);
                break;

            case R.id.edtRounting:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgRounting);
                else
                    setTickMark(imgRounting);
                break;
        }
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }

    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private void startHomeActivity() {
        PrefConnect.writeInteger(getApplicationContext(), PrefConnect.PAGE, -1);
        Intent intent;
        intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


}

