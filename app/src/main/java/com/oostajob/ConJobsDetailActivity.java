package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.adapter.contractor.ContractorJobsAdapter;
import com.oostajob.adapter.contractor.DiscussionAdapter;
import com.oostajob.dialog.RebidFragment;
import com.oostajob.global.Global;
import com.oostajob.model.ContractorJobList;
import com.oostajob.model.DiscussionlistModel;
import com.oostajob.model.JobDiscussionModel;
import com.oostajob.utils.BlurTransformation;
import com.oostajob.utils.CircleTransform;
import com.oostajob.utils.FetchAddress;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.google.android.gms.maps.model.LatLng;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConJobsDetailActivity extends AppCompatActivity {

    private int CURRENT_ITEM;
    private String USER_ID;
    private ProgressDialog p;
    private Toolbar toolbar;
    private ImageView imageView, imgPlay, imgClose, imgMenuArrow, imgIcon, imgBack, imgProfile;
    private FrameLayout videoLay;
    private VideoView videoView;
    private HorizontalScrollView scrollView;
    private LinearLayout scrollContent, menuLay, bottomLay, timeLay, sendMsgLay;
    private CustomTextView txtMenuItem, txtTitle, txtCloses, txtZipCode, txtDescription, txtDiscussion, txtBidAmount,
            txtName, txtAddress, txtDateTime, txtLowestBid, txtBottomHead;
    private ChipView chipView;
    private RatingBar ratingBar;
    private SlidingUpPanelLayout panelLayout;
    private ListView discussionView;
    private APIService apiService;
    private ContractorJobList.List details;
    private MediaController mediaController;
    private DiscussionAdapter discussionAdapter;
    private CustomEditText edtMessage;
    private CustomButton btnSend, btnEmail, btnCall, btnRebid;
    private InputMethodManager imm;
    private boolean showFirst = true;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_bid_detail);

        init();
        setUpToolbar();
        setListener();
        loadValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.imageView);
        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        imgMenuArrow = (ImageView) findViewById(R.id.imgMenuArrow);
        imgIcon = (ImageView) findViewById(R.id.imgIcon);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        videoLay = (FrameLayout) findViewById(R.id.videoLay);
        videoView = (VideoView) findViewById(R.id.videoView);
        scrollView = (HorizontalScrollView) findViewById(R.id.scrollView);
        scrollContent = (LinearLayout) findViewById(R.id.scrollContent);
        menuLay = (LinearLayout) findViewById(R.id.menuLay);
        bottomLay = (LinearLayout) findViewById(R.id.bottomLay);
        timeLay = (LinearLayout) findViewById(R.id.timeLay);
        sendMsgLay = (LinearLayout) findViewById(R.id.sendMsgLay);
        txtMenuItem = (CustomTextView) findViewById(R.id.txtMenuItem);
        txtTitle = (CustomTextView) findViewById(R.id.txtTitle);
        txtCloses = (CustomTextView) findViewById(R.id.txtCloses);
        txtZipCode = (CustomTextView) findViewById(R.id.txtZipCode);
        txtDescription = (CustomTextView) findViewById(R.id.txtDescription);
        txtDiscussion = (CustomTextView) findViewById(R.id.txtDiscussion);
        txtLowestBid = (CustomTextView) findViewById(R.id.txtLowestBid);
        discussionView = (ListView) findViewById(R.id.discussionView);
        chipView = (ChipView) findViewById(R.id.chipView);
        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.panelLayout);
        txtBidAmount = (CustomTextView) findViewById(R.id.txtBidAmount);
        txtBottomHead = (CustomTextView) findViewById(R.id.txtBottomHead);
        edtMessage = (CustomEditText) findViewById(R.id.edtMessage);
        btnSend = (CustomButton) findViewById(R.id.btnSend);
        btnRebid = (CustomButton) findViewById(R.id.btnRebid);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        txtName = (CustomTextView) findViewById(R.id.txtName);
        txtAddress = (CustomTextView) findViewById(R.id.txtAddress);
        txtDateTime = (CustomTextView) findViewById(R.id.txtDateTime);
        btnEmail = (CustomButton) findViewById(R.id.btnEmail);
        btnCall = (CustomButton) findViewById(R.id.btnCall);


        panelLayout.setScrollableView(discussionView);

        p = new ProgressDialog(ConJobsDetailActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        imgMenuArrow.setVisibility(View.GONE);

        mediaController = new MediaController(ConJobsDetailActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPlay.setVisibility(View.GONE);
                videoView.start();
                scrollView.setVisibility(View.GONE);
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ConJobsDetailActivity.this, "Cannot load video", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                setMediaVisibility(View.GONE, View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtMessage.getText().toString())) {
                    p.show();
                    JobDiscussionModel.Request request = new JobDiscussionModel.Request(USER_ID,
                            details.getJobpostID(), edtMessage.getText().toString());
                    Call<JobDiscussionModel.Response> call = apiService.postDiscussion(request);
                    call.enqueue(new Callback<JobDiscussionModel.Response>() {
                        @Override
                        public void onResponse(Call<JobDiscussionModel.Response> call, Response<JobDiscussionModel.Response> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                JobDiscussionModel.Response response1 = response.body();
                                if (response1.getResult().equalsIgnoreCase("Success")) {
                                    edtMessage.setText("");
                                    imm.hideSoftInputFromWindow(edtMessage.getWindowToken(), 0);
                                    loadDiscussion(details.getJobpostID());
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<JobDiscussionModel.Response> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });
                } else {
                    Toast.makeText(ConJobsDetailActivity.this, "Enter Message", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.sendEmail(ConJobsDetailActivity.this, details.getCustomerDetails().getEmail());
            }
        });
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.call(ConJobsDetailActivity.this, details.getCustomerDetails().getPhone());
            }
        });
        btnRebid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("post_id", details.getJobpostID());
                RebidFragment fragment = new RebidFragment();
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "Rebid");
            }
        });
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(ConJobsDetailActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void loadValues() {
        try {
            details = (ContractorJobList.List) getIntent().getExtras().getSerializable("details");
            CURRENT_ITEM = getIntent().getExtras().getInt("currentItem", 1);
            assert details != null;
            // Load Background Image
            String IMG_URL = String.format("%s/%s", Global.BASE_URL, details.getJob_image());
            Picasso.with(this).load(IMG_URL).transform(new BlurTransformation(this, 25)).fit().into(imgBack);
            //Menu
            txtMenuItem.setText(getMenuItemText());

            //load Files
            loadFiles(details.getJobmedialist());

            toolbar.setTitle(details.getJobtypeName());
            //title
            String ICON_URL = String.format("%s/%s", Global.BASE_URL, details.getJob_icon());
            Picasso.with(this).load(ICON_URL).fit().into(imgIcon);
            txtTitle.setText(details.getJobtypeName());

            //Bid Time
            Date date = getDate(details);

            long difference = System.currentTimeMillis() - date.getTime();

            long hours = TimeUnit.MILLISECONDS.toHours(difference);

            if (hours == 0) {
                long minu = TimeUnit.MILLISECONDS.toMinutes(difference);
                if (minu > 1)
                    txtCloses.setText(String.format("%d minutes ago", minu));
                else txtCloses.setText(String.format("%d minute ago", minu));
            } else if (hours >= 24) {
                long days = TimeUnit.MILLISECONDS.toDays(difference);
                if (days > 1)
                    txtCloses.setText(String.format("%d days ago", days));
                else txtCloses.setText(String.format("%d day ago", days));
            } else if (hours < 24) {
                if (hours > 1)
                    txtCloses.setText(String.format("%d hours ago", hours));
                else txtCloses.setText(String.format("%d hour ago", hours));
            }

            //Zipcode
            txtZipCode.setText(details.getZipcode());

            //Remaining Hours

            // Bid Amount
            txtBidAmount.setText(String.format("$%s", details.getBidAmount()));


            //Tags
            setTags(details);

            //Lowest Bit
            txtLowestBid.setText(String.format("Lowest bid as of now $%s", details.getMinimumbidamount()));

            //Description
            StringBuilder builder = new StringBuilder();
            builder.append(details.getDescription()).append("\n").append("\n");
            for (ContractorJobList.Answer answer : details.getJobanwserlist()) {
                if (answer.getTag_key().equalsIgnoreCase("2")) {
                    builder.append(answer.getQuest_name()).append("\n").append(answer.getAnswer()).append("\n");
                }
            }
            txtDescription.setText(builder.toString());

            //Set Visibility for rebid
            if (details.getRebidoptions() != null && details.getRebidoptions().equals("1")) {
                btnRebid.setVisibility(View.GONE);
            }
            loadDiscussion(details.getJobpostID());

            //Bottom Visibility
            setBottomVisibility();

            //Set Discussion send msg Visibility
            if (CURRENT_ITEM == ContractorJobsAdapter.HIRE || CURRENT_ITEM == ContractorJobsAdapter.CLOSE) {
                sendMsgLay.setVisibility(View.GONE);
                txtLowestBid.setVisibility(View.GONE);

            }

            //Bottom Content
            setBottomContent();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getMenuItemText() {
        switch (CURRENT_ITEM) {
            case ContractorJobsAdapter.BID:
                return "Bids";
            case ContractorJobsAdapter.HIRE:
                return "Hired";
            case ContractorJobsAdapter.CLOSE:
                return "Closed";
            default:
                return "";
        }
    }
    private Date getDate(ContractorJobList.List item) {
        switch (CURRENT_ITEM) {
            case ContractorJobsAdapter.BID:
                return Global.getDate(item.getDateofbid());
            case ContractorJobsAdapter.HIRE:
                return Global.getDate(item.getHiredTimed());
            case ContractorJobsAdapter.CLOSE:
                return Global.getDate(item.getClosedTime());
            default:
                return new Date();
        }
    }

    private void setTags(ContractorJobList.List details) {
        List<Chip> chipList = new ArrayList<>();
        for (ContractorJobList.Answer answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        chipView.setLineSpacing(10);
        chipView.setChipSpacing(10);
        chipView.setChipLayoutRes(R.layout.chip_lay);
        chipView.setChipBackgroundRes(R.drawable.tag);
        chipView.setChipList(chipList);
    }

    private void loadFiles(ArrayList<ContractorJobList.Media> fileList) {

        scrollContent.removeAllViews();
        final LayoutInflater inflater = LayoutInflater.from(this);

        for (final ContractorJobList.Media model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.image_view, scrollContent, false);
            ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            if (model.getMediaType().equals("1")) {
                Picasso.with(this).load(url).fit().into(imgChosen);
            } else {
                Picasso.with(this).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (model.getMediaType().equals("1")) {
                        setMediaVisibility(View.VISIBLE, View.GONE);
                        Picasso.with(ConJobsDetailActivity.this).load(url).into(imageView);
                    } else {
                        setMediaVisibility(View.GONE, View.VISIBLE);
                        videoView.setVideoPath(url);
                        videoView.requestFocus();
                    }

                }
            });
            if (showFirst) {
                imgChosen.performClick();
                showFirst = false;
            }
            scrollContent.addView(view);
        }
    }

    private void setMediaVisibility(int arg0, int arg1) {
        imageView.setVisibility(arg0);
        videoLay.setVisibility(arg1);
    }

    private void setBottomVisibility() {
        switch (CURRENT_ITEM) {
            case ContractorJobsAdapter.BID:
                bottomLay.setVisibility(View.GONE);
                break;
            case ContractorJobsAdapter.HIRE:
                ratingBar.setVisibility(View.GONE);
                btnRebid.setVisibility(View.GONE);
                break;
            case ContractorJobsAdapter.CLOSE:
                timeLay.setVisibility(View.VISIBLE);
                btnRebid.setVisibility(View.GONE);
                break;
        }
    }

    private void setBottomContent() {
        ContractorJobList.Customer customer = details.getCustomerDetails();
        String PROF_URL = String.format("%s/%s", Global.BASE_URL, customer.getProfilePhoto());
        Picasso.with(this).load(PROF_URL).transform(new CircleTransform()).placeholder(R.drawable.customer).into(imgProfile);
        txtName.setText(customer.getUsername());

        btnEmail.setText(customer.getEmail());
        btnCall.setText(customer.getPhone());

        LatLng latLng = new LatLng(Double.parseDouble(customer.getLat()), Double.parseDouble(customer.getLng()));
        FetchAddress fetchAddress = new FetchAddress(this, latLng);
        fetchAddress.getAddress();
        txtAddress.setText(String.format("%s - %s", fetchAddress.getLocality(), fetchAddress.getPostalCode()));
        switch (CURRENT_ITEM) {
            case ContractorJobsAdapter.HIRE:
                ratingBar.setVisibility(View.GONE);
                if ((details.getMeetingDate() == null && details.getMeetingTime() == null) || details.getMeetingDate().startsWith("0000")) {
                    txtDateTime.setText("Meeting not scheduled");
                } else {
                    String dateTime = String.format("%s, %s", Global.getDate(details.getMeetingDate(), "yyyy-MM-dd", "MM/dd/yyyy"), details.getMeetingTime());
                    txtDateTime.setText(dateTime);
                }
                break;
            case ContractorJobsAdapter.CLOSE:
                txtBottomHead.setText("Congratulations you have been rated");
                if ((details.getMeetingDate() == null && details.getMeetingTime() == null) || details.getMeetingDate().startsWith("0000")) {
                    txtDateTime.setText("Meeting not scheduled");
                } else {
                    String dateTime = String.format("%s, %s", Global.getDate(details.getMeetingDate(), "yyyy-MM-dd", "MM/dd/yyyy"), details.getMeetingTime());
                    txtDateTime.setText(dateTime);
                }
                ratingBar.setRating(Float.parseFloat(customer.getTotalRating()));
                break;
        }

    }

    private void loadDiscussion(String jobPostId) {
        p.show();
        Call<DiscussionlistModel.Response> call = apiService.discussionList(jobPostId);
        call.enqueue(new Callback<DiscussionlistModel.Response>() {
            @Override
            public void onResponse(Call<DiscussionlistModel.Response> call, Response<DiscussionlistModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    DiscussionlistModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        discussionAdapter = new DiscussionAdapter(ConJobsDetailActivity.this, response1.getDiscustionDetails());
                        discussionView.setAdapter(discussionAdapter);
                        txtDiscussion.setText(String.format("Discussion(%s)", discussionAdapter.getCount()));
                    }
                }

            }

            @Override
            public void onFailure(Call<DiscussionlistModel.Response> call, Throwable t) {
//                Log.e("Discussion Error", error.getMessage());
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }
        super.onBackPressed();
    }


}
