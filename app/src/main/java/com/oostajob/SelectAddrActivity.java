package com.oostajob;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.Green.customfont.CustomAutoComplete;
import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oostajob.adapter.PlaceAutocompleteAdapter;
import com.oostajob.callbacks.PlacesResultCallback;
import com.oostajob.callbacks.PlacesResultCallback.ResultCallback;
import com.oostajob.callbacks.PlacesTextWatcher;
import com.oostajob.callbacks.PlacesTextWatcher.TextWatcher;
import com.oostajob.global.Global;
import com.oostajob.model.CityDetailsModel;
import com.oostajob.utils.FetchAddress;
import com.oostajob.utils.Tracker;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SelectAddrActivity extends AppCompatActivity implements OnMapReadyCallback, OnConnectionFailedListener,
        ConnectionCallbacks, ResultCallback, TextWatcher, OnMarkerDragListener, Tracker.LocationCallback {

    private static final String TAG = "Places Api";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    LatLng currentLL;
    private SupportMapFragment mapFragment;
    private GoogleMap googleMap;
    private Toolbar toolbar;
    private CustomTextView txtClear;
    private InputMethodManager imm;
    private CustomAutoComplete txtAutoCompletePlace;
    private GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter pAdapter;
    private LatLngBounds.Builder bounds = new LatLngBounds.Builder();
    private CustomButton btnDone;
    private FetchAddress fetchAddress;
    private double LAT = 0, LNG = 0;
    private APIService apiService;
    private ArrayList<CityDetailsModel.CityDetails> cityList = new ArrayList<>();
    private ProgressDialog p;

    private Tracker tracker;
    private boolean requireLocationOnce = true;
    private String GOTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_addr);

        int GRANTED = PackageManager.PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == GRANTED || checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == GRANTED) {
                setUp();
            } else {
                Toast.makeText(this, "Enable Location Permission in Settings -> Apps -> OOstaJob -> Permissions.", Toast.LENGTH_LONG).show();
                finish();
            }
        } else {
            setUp();
        }


    }

    private void setUp() {
        tracker = new Tracker(this, this);
        buildApiClient();
        init();
        setUpToolBar();
        setListener();
        loadCity();
    }


    private void buildApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(SelectAddrActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();
    }

    private void init() {
        GOTO = getIntent().getStringExtra("goto");
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtAutoCompletePlace = (CustomAutoComplete) findViewById(R.id.txtAutoCompletePlace);
        txtClear = (CustomTextView) findViewById(R.id.txtClear);
        btnDone = (CustomButton) findViewById(R.id.btnDone);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_map);

        txtAutoCompletePlace.setThreshold(3);
        txtAutoCompletePlace.addTextChangedListener(new PlacesTextWatcher(SelectAddrActivity.this));

        apiService = RetrofitSingleton.createService(APIService.class);
        p = Global.initProgress(this);
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {

        txtAutoCompletePlace.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    imm.hideSoftInputFromWindow(txtAutoCompletePlace.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
        txtAutoCompletePlace.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                imm.hideSoftInputFromWindow(txtAutoCompletePlace.getWindowToken(), 0);
                final AutocompletePrediction item = pAdapter.getItem(position);
                final String placeId = item.getPlaceId();
                Log.i(TAG, "Autocomplete item selected: " + item.getFullText(null));

                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new PlacesResultCallback(SelectAddrActivity.this));
                Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkZipCode()) {
                    if (fetchAddress != null) {
                        Intent intent = new Intent(SelectAddrActivity.this, ZipCodeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("zipCode", fetchAddress.getPostalCode());
                        intent.putExtra("goto", GOTO);
                        startActivity(intent);
                        return;
                    }
                }
                if (!TextUtils.isEmpty(txtAutoCompletePlace.getText().toString().trim())) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("address", txtAutoCompletePlace.getText().toString());
                    resultIntent.putExtra("postalCode", fetchAddress.getPostalCode());
                    resultIntent.putExtra("lat", fetchAddress.getLatitude());
                    resultIntent.putExtra("lng", fetchAddress.getLongitude());
                    setResult(RESULT_OK, resultIntent);
                    finish();
                } else {
                    Toast.makeText(SelectAddrActivity.this, "Enter Address", Toast.LENGTH_LONG).show();
                }
            }
        });
        txtClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAutoCompletePlace.setText("");
            }
        });
    }

    private boolean checkZipCode() {
        for (CityDetailsModel.CityDetails details : cityList) {
            if (fetchAddress != null) {
                if (details.getZipcode().equalsIgnoreCase(fetchAddress.getPostalCode())) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public void onLocationReceived(Location location) {
        if (location != null && requireLocationOnce) {
            btnDone.setEnabled(true);
            getFromIntent(location);
            requireLocationOnce = false;
        }
    }

    @Override
    public void loadDefaultLocation() {
        btnDone.setEnabled(true);
       /* Intent intent = getIntent();
        LAT = intent.getDoubleExtra("lat", Global.DEFAULT_LAT);
        LNG = intent.getDoubleExtra("lng", Global.DEFAULT_LNG);
        if (LAT == Global.DEFAULT_LAT && LNG == Global.DEFAULT_LNG
                || LAT == 0 && LNG == 0) {
            currentLL = new LatLng(Global.DEFAULT_LAT, Global.DEFAULT_LNG);
        } else {
            currentLL = new LatLng(LAT, LNG);
        }
        //setAddress(currentLL);
        // txtAutoCompletePlace.setText(Global.DEFAULT_ADDRESS);*/
        mapFragment.getMapAsync(this);
    }

    private void getFromIntent(Location location) {
        Intent intent = getIntent();
        LAT = intent.getDoubleExtra("lat", 0);
        LNG = intent.getDoubleExtra("lng", 0);
        if (LAT == Global.DEFAULT_LAT && LNG == Global.DEFAULT_LNG
                || LAT == 0 && LNG == 0) {
            currentLL = new LatLng(location.getLatitude(), location.getLongitude());
        } else {
            currentLL = new LatLng(LAT, LNG);
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnMarkerDragListener(this);


        /*this.googleMap.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragStart..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragEnd..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);


                googleMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
                LatLng latLng = new LatLng(arg0.getPosition().latitude, arg0.getPosition().longitude);
                setAddress(latLng);
            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
                Log.i("System out", "onMarkerDrag...");
            }
        });*/


        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // Creating a marker


                // Clears the previously touched position
                googleMap.clear();

                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));


                googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .draggable(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker)))
                        .showInfoWindow();
                //  googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
                setAddress(latLng);

            }
        });

        if (currentLL != null) {
            refreshMap(currentLL);
            setAddress(currentLL);
            setUpAdapter();
        }
    }

    private void setUpAdapter() {
        LatLng ll = currentLL;
        bounds.include(ll);
        pAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, bounds.build(),
                null);
        txtAutoCompletePlace.setAdapter(pAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Tracker.REQUEST_CHECK_SETTINGS:
                    tracker = new Tracker(this, this);
                    tracker.disConnectClient();
                    tracker.connectClient();
                    break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        tracker.disConnectClient();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResult(PlaceBuffer places) {
        if (!places.getStatus().isSuccess()) {
            Log.e(TAG, "Place query did not complete. Error: " +
                    places.getStatus().toString());
            return;
        }
        // Selecting the first object buffer.
        final Place place = places.get(0);
        txtAutoCompletePlace.setAdapter(null);
        refreshMap(place.getLatLng());
        setAddress(place.getLatLng());
        //Release buffer to prevent leak
        places.release();
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.toString().length() > 0) {
            txtAutoCompletePlace.setAdapter(pAdapter);
        }
    }

    private void refreshMap(LatLng latLng) {
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker)))
                .showInfoWindow();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));


    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        setAddress(marker.getPosition());
    }

    private void setAddress(LatLng latLng) {
        fetchAddress = new FetchAddress(SelectAddrActivity.this, latLng);
        String address = fetchAddress.getAddress();
        txtAutoCompletePlace.setText(address);
    }

    private void loadCity() {
        cityList = new ArrayList<>();
        p.show();
        Call<CityDetailsModel.Response> call = apiService.getCityDetails();
        call.enqueue(new Callback<CityDetailsModel.Response>() {
            @Override
            public void onResponse(Call<CityDetailsModel.Response> call, Response<CityDetailsModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    CityDetailsModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        cityList = response1.getCityDetails();
                    }
                    tracker.connectClient();
                }
            }

            @Override
            public void onFailure(Call<CityDetailsModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

}
