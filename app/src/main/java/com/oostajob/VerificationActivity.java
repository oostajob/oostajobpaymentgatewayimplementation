package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomTextView;
import com.oostajob.global.Global;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.LogoutModel;
import com.oostajob.model.ResendEmailModel;
import com.oostajob.model.VerificationProcessModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VerificationActivity extends AppCompatActivity {

    APIService apiService;
    String userId, userType;
    String subTitle, imgUrl, jobTypeId;
    ArrayList<JobSubTypeCharModel.JobsubtypecharDetails> list = new ArrayList<>();
    private CustomTextView resend_mail;
    private ProgressDialog p;
    private ImageView backNav;

    private Handler mHandler = null;
    private Call<VerificationProcessModel.Response> mVerificationCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        apiService = RetrofitSingleton.createService(APIService.class);
        userId = PrefConnect.readString(getApplicationContext(), PrefConnect.USER_ID, "");
        userType = PrefConnect.readString(getApplicationContext(), PrefConnect.USER_TYPE, "");
        init();
        setListener();
    }

    private void setListener() {
        resend_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });
        backNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.equalsIgnoreCase("")) {
                    PrefConnect.writeString(VerificationActivity.this, PrefConnect.USER_ID, "");
                    PrefConnect.writeString(VerificationActivity.this, PrefConnect.USER_TYPE, "");
                    PrefConnect.writeInteger(VerificationActivity.this, PrefConnect.PAGE, 0);
                    Intent intent = new Intent(VerificationActivity.this, RegisterActivity.class);
                    intent.putExtra("fragPosition", 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {

                    p.show();
                    Call<LogoutModel> call = apiService.logout(userId);
                    call.enqueue(new Callback<LogoutModel>() {
                        @Override
                        public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                LogoutModel model = response.body();
                                if (model.getResult().equalsIgnoreCase("Success")) {
                                    PrefConnect.clearAllPrefs(VerificationActivity.this);
                                    Intent intent = new Intent(VerificationActivity.this, RegisterActivity.class);
                                    intent.putExtra("fragPosition", 0);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<LogoutModel> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });
                }
            }
        });
    }

    private void sendEmail() {
        p.show();
        Call<ResendEmailModel.Response> call = apiService.resendMail(userId);
        call.enqueue(new Callback<ResendEmailModel.Response>() {
            @Override
            public void onResponse(Call<ResendEmailModel.Response> call, Response<ResendEmailModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    if (response.body().getResult().equalsIgnoreCase("Success")) {
                        Toast.makeText(VerificationActivity.this, "Check Your Mail", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResendEmailModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void init() {
        resend_mail = (CustomTextView) findViewById(R.id.txt_click_here_to_resend);
        ImageView imgBack = (ImageView) findViewById(R.id.imgBack);
        backNav = (ImageView) findViewById(R.id.backnav);

        subTitle = PrefConnect.readString(VerificationActivity.this, PrefConnect.SUB_TITLE, "");
        imgUrl = PrefConnect.readString(VerificationActivity.this, PrefConnect.IMG_URL, "");
        jobTypeId = PrefConnect.readString(VerificationActivity.this, PrefConnect.JOB_ID, "");
        list = (ArrayList<JobSubTypeCharModel.JobsubtypecharDetails>) getIntent().getSerializableExtra("list");

        p = new ProgressDialog(VerificationActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startHandlerThread();
    }

    public void startHandlerThread() {
        HandlerThread mHandlerThread = new HandlerThread("verifyAccountActivationHandlerThread");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
        mHandler.postDelayed(mRunnable, Global.INTERVAL);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mVerificationCall != null)
                mVerificationCall.cancel();
            verifyActivation();
            mHandler.postDelayed(this, Global.INTERVAL); // repeating
        }
    };

    private void verifyActivation() {
        mVerificationCall = apiService.verifyMail(userId);
        mVerificationCall.enqueue(new Callback<VerificationProcessModel.Response>() {
            @Override
            public void onResponse(Call<VerificationProcessModel.Response> call, Response<VerificationProcessModel.Response> response) {
                if (response.isSuccessful()) {
                    VerificationProcessModel.Response response1 = response.body();
                    if (response1 != null && response1.getResult().equalsIgnoreCase("Success")) {
                        if (response1.getEmailStatus().equalsIgnoreCase("Active")) {
                            if (userType.equals("1")) {
                                PrefConnect.writeInteger(getApplicationContext(), PrefConnect.PAGE, -1);
                                activatedCustomer();
                            } else {
                                PrefConnect.writeInteger(getApplicationContext(), PrefConnect.PAGE, 2);
                                activatedContractor();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<VerificationProcessModel.Response> call, Throwable t) {

            }
        });
    }

    private void activatedContractor() {
        mHandler.removeCallbacks(mRunnable);
        Intent intent = new Intent(getApplicationContext(), ProfileConStatActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void activatedCustomer() {
        mHandler.removeCallbacks(mRunnable);
        String page = PrefConnect.readString(VerificationActivity.this, PrefConnect.SKIP, "");
        Intent intent;
        if (page.equalsIgnoreCase("skip_login")) {
            intent = new Intent(getApplicationContext(), QuestionsActivity.class);
            intent.putExtra("list", list);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }
}