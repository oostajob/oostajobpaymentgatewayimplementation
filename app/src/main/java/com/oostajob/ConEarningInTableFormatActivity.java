package com.oostajob;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.Green.customfont.CustomTextView;
import com.evrencoskun.tableview.TableView;
import com.oostajob.model.ConPaymentBillingHistoryResponse;
import com.oostajob.model.ConPaymentHistoryJobResponse;
import com.oostajob.model.CustomerDetailsForPaymentHistory;
import com.oostajob.model.EarningPojo;
import com.oostajob.model.ResponseItem;
import com.oostajob.tabelviewexample.CellModel;
import com.oostajob.tabelviewexample.ColumnHeaderModel;
import com.oostajob.tabelviewexample.MyTableAdapter;
import com.oostajob.tabelviewexample.RowHeaderModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConEarningInTableFormatActivity extends AppCompatActivity {


    // For TableView

    private String TAG = ConEarningInTableFormatActivity.class.getSimpleName();

    private TableView mTableView;
    private MyTableAdapter mTableAdapter;

    private List<List<CellModel>> mCellList;
    private List<ColumnHeaderModel> mColumnHeaderList;
    private List<RowHeaderModel> mRowHeaderList;

    List<EarningPojo> earningPojoList;


    ListView listView;
    private ProgressDialog p;
    private String USER_ID;
    private APIService apiService;

    CustomTextView txtEmptyView;
    private Toolbar toolbar;
    private ActionBar.LayoutParams lp;


    String amount = "0";
    private CustomTextView custClaimAmount;

    private RelativeLayout relPayment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_table);


        mTableView = (TableView) findViewById(R.id.my_TableView);

        custClaimAmount = findViewById(R.id.custClaimAmount);
        Bundle bundle = getIntent().getExtras();
        amount = bundle.getString("amount");

        relPayment = findViewById(R.id.relPayment);
        relPayment.setVisibility(View.GONE);
        // Create TableView Adapter
        mTableAdapter = new MyTableAdapter(getApplicationContext());
        mTableView.setAdapter(mTableAdapter);

        earningPojoList = new ArrayList<>();
        // Create listener
//        mTableView.setTableViewListener(new MyTableViewListener(mTableView));

        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
        listView = findViewById(R.id.listView);
        txtEmptyView = findViewById(R.id.txtEmptyView);
        p = new ProgressDialog(this);
        p.setMessage(this.getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        setUpCustomToolbar();


        getJobHistoryForContractor();


    }

    private void setUpCustomToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Payment History");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_home);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void populatedTableView() {
        // create Models
        mColumnHeaderList = createColumnHeaderModelList();
        mRowHeaderList = createRowHeaderList();

        // Set all items to the TableView
        mTableAdapter.setAllItems(mColumnHeaderList, mRowHeaderList, mCellList);
        custClaimAmount.setText(getResources().getString(R.string.amount_to_be_paid) + ":    " + amount);
    }


    private List<ColumnHeaderModel> createColumnHeaderModelList() {
        List<ColumnHeaderModel> list = new ArrayList<>();

        // Create Column Headers
        list.add(new ColumnHeaderModel("Job Date"));
        list.add(new ColumnHeaderModel("Job Category"));
        list.add(new ColumnHeaderModel("Job Customer"));
        list.add(new ColumnHeaderModel("Job Location"));
        list.add(new ColumnHeaderModel("Job Value"));
        list.add(new ColumnHeaderModel("Job Earning"));
        list.add(new ColumnHeaderModel("Earning Availability Date"));
        list.add(new ColumnHeaderModel("Payment Status"));


        return list;
    }

    private List<List<CellModel>> loadCellModelList(ArrayList<ConPaymentHistoryJobResponse> conPaymentHistoryJobResponses) {
        List<List<CellModel>> lists = new ArrayList<>();


        for (int i = 0; i < conPaymentHistoryJobResponses.size(); i++) {


            ConPaymentHistoryJobResponse details = conPaymentHistoryJobResponses.get(i);
            ResponseItem responseItem = details.getResponse().get(0);

            CustomerDetailsForPaymentHistory contDetailsForPaymentHistory = details.getCustomerDetailsForPaymentHistory();


            ConPaymentHistoryJobResponse conPaymentHistoryJobResponse = conPaymentHistoryJobResponses.get(i);

            List<CellModel> list = new ArrayList<>();

            // The order should be same with column header list;


            list.add(new CellModel("" + i, (responseItem.getJobpostTime())));      //earningPojo.getJobDate()
            list.add(new CellModel("" + i, responseItem.getJobtypeName()));  //earningPojo.getJobCategory()
            list.add(new CellModel("" + i, contDetailsForPaymentHistory.getName()));   //earningPojo.getJobCustomer()
            list.add(new CellModel("" + i, details.getJobIconForPaymentHistory().getAddress()));   //earningPojo.getJobLocation()
            list.add(new CellModel("" + i, "$" + responseItem.getAmount())); //earningPojo.getJobValue()
            list.add(new CellModel("" + i, "$" + responseItem.getAmountAfterDeduction()));   //earningPojo.getJobEarning()
            list.add(new CellModel("" + i, responseItem.getEarning_available_date()));   //earningPojo.getAvailDate()
            list.add(new CellModel("" + i, responseItem.getPaymentStatus()));      //earningPojo.getPaymentStatus()

            // Add
            lists.add(list);
        }

        return lists;
    }

    private List<RowHeaderModel> createRowHeaderList() {
        List<RowHeaderModel> list = new ArrayList<>();
        for (int i = 0; i < mCellList.size(); i++) {
            // In this example, Row headers just shows the index of the TableView List.
            list.add(new RowHeaderModel(String.valueOf(i + 1)));
        }
        return list;
    }


    private void getJobHistoryForContractor() {
        Call<ConPaymentBillingHistoryResponse> call = apiService.getConJobHistory(USER_ID);
        p.show();
        call.enqueue(new Callback<ConPaymentBillingHistoryResponse>() {
            @Override
            public void onResponse(Call<ConPaymentBillingHistoryResponse> call, Response<ConPaymentBillingHistoryResponse> response) {
                Log.d(TAG, "the response is " + response.body());
                // new CusJobBillingAdapter(this,response.body().getResponse());


                // ConJobBillingAdapter conJobBillingAdapter = new ConJobBillingAdapter(ConEarningInTableFormatActivity.this, response.body().getResponse());
                if (response.body().getResponse().size() > 0) {
                    // listView.setAdapter(conJobBillingAdapter);

                    mCellList = loadCellModelList(response.body().getResponse());
                    populatedTableView();
                } else {
                    txtEmptyView.setVisibility(View.VISIBLE);
                }

                p.dismiss();
            }

            @Override
            public void onFailure(Call<ConPaymentBillingHistoryResponse> call, Throwable t) {
                Log.d(TAG, "the response is " + t);
                p.dismiss();

            }
        });
    }

    private String changeDateFromat(String date) {
        /*
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
String formatted = dateFormat.format(date);

         */
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat spf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        date = spf.format(newDate);
        //System.out.println(date);

        return date;
    }


}