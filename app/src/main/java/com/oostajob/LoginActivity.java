package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.dialog.ForgetDialogFragment;
import com.oostajob.global.Global;
import com.oostajob.model.ConBankDetails;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.LogInModel;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements AutoTextWatcher.TextWatcher {

    private Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
    private APIService apiService;
    private String GCMTOKEN, skip_login, SUB_TITLE, IMG_URL, JOB_ID;
    private ArrayList<JobSubTypeCharModel.JobsubtypecharDetails> list = new ArrayList();
    private CustomEditText edt_email, edt_password;
    private ImageView email_add, pass_add;
    private Toolbar toolbar;
    private CustomButton btn_login;
    private ProgressDialog p;
    private CustomTextView txt_forgot_password;
    private RelativeLayout rootLay;
    private ImageView imgBack;


    private ActionBar.LayoutParams lp;
    private String USER_TYPE = "";
    private String USER_ID;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GCMTOKEN = PrefConnect.readString(LoginActivity.this, PrefConnect.GCM_TOKEN, "");
        apiService = RetrofitSingleton.createService(APIService.class);
        init();
        setUpToolBar();
        setListener();

    }


    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edt_email = (CustomEditText) findViewById(R.id.email_login);
        edt_password = (CustomEditText) findViewById(R.id.password_login);
        email_add = (ImageView) findViewById(R.id.email_add);
        pass_add = (ImageView) findViewById(R.id.pass_add);
        btn_login = (CustomButton) findViewById(R.id.login_button);
        txt_forgot_password = (CustomTextView) findViewById(R.id.forgot_password);
        rootLay = (RelativeLayout) findViewById(R.id.rootLay);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        p = new ProgressDialog(LoginActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
        //Params
        lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        skip_login = PrefConnect.readString(LoginActivity.this, PrefConnect.SKIP, "");
        list = (ArrayList<JobSubTypeCharModel.JobsubtypecharDetails>) getIntent().getSerializableExtra("list");
        SUB_TITLE = PrefConnect.readString(LoginActivity.this, PrefConnect.SUB_TITLE, "");
        IMG_URL = PrefConnect.readString(LoginActivity.this, PrefConnect.IMG_URL, "");
        JOB_ID = PrefConnect.readString(LoginActivity.this, PrefConnect.JOB_ID, "");
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    logIn();
                }
            }
        });
        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgetDialogFragment dialogFragment = new ForgetDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "Forget Password");
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edt_email.addTextChangedListener(new AutoTextWatcher(R.id.email_login, this));
        edt_password.addTextChangedListener(new AutoTextWatcher(R.id.password_login, this));
    }

    private void logIn() {
        p.show();
        final LogInModel.Request request = new LogInModel.Request(edt_email.getText().toString(),
                edt_password.getText().toString(), GCMTOKEN, "1");
        Call<LogInModel.Response> call = apiService.logIn(request);
        call.enqueue(new Callback<LogInModel.Response>() {
            @Override
            public void onResponse(Call<LogInModel.Response> call, Response<LogInModel.Response> response) {

                if (response.isSuccessful()) {

                    LogInModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("success")) {

                        LogInModel.UserDetails details = response1.getUserDetails();
                        USER_ID = details.getUserID();
                        USER_TYPE = details.getUserType();
                        savePrefs(details);
                        PrefConnect.writeInteger(LoginActivity.this, PrefConnect.PAGE, -1);
                        if (details.getUserType().equalsIgnoreCase("1") && skip_login.equalsIgnoreCase("skip_login")) {
                            Global.dismissProgress(p);
                            Intent intent = new Intent(LoginActivity.this, QuestionsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("list", list);
                            intent.putExtra("subTitle", SUB_TITLE);
                            intent.putExtra("imgUrl", IMG_URL);
                            intent.putExtra("jobTypeId", JOB_ID);
                            startActivity(intent);
                            finish();
                        } else {
                            if (!(USER_TYPE.equals("1") || USER_TYPE.equals(""))) {
                                getBankdDetailsForContractor();
                            } else {
                                Global.dismissProgress(p);
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        }
                    } else if (response1.getResult().equalsIgnoreCase("Failed")) {
                        Global.dismissProgress(p);
                        LogInModel.UserDetails details = response1.getUserDetails();
                        savePrefs(details);
                        startActivity(response1.getStatus());
                    }
                } else {
                    // Toast.makeText(LoginActivity.this,getResources().getString(R.string.internal_server_error),)
                    Global.dismissProgress(p);
                }
            }

            @Override
            public void onFailure(Call<LogInModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void getBankdDetailsForContractor() {

     //   p.show();
        Call<ConBankDetails> call = apiService.getBankDetailsForContractor(USER_ID);

        call.enqueue(new Callback<ConBankDetails>() {
            @Override
            public void onResponse(Call<ConBankDetails> call, Response<ConBankDetails> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    //if(response.body().getResult().equalsIgnoreCase("Success"))
                    if (response.body().getResult().equalsIgnoreCase("success")) {
                        if (response.body().getResponse() != null && response.body().getResponse().size() > 0) {
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(getApplicationContext(), ConBankDetailActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ConBankDetails> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });

    }

    private void savePrefs(LogInModel.UserDetails details) {
        try {

            PrefConnect.writeString(LoginActivity.this, PrefConnect.USER_ID, details.getUserID());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.EMAIL, details.getEmail());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.PASSWORD, details.getPassword());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.USER_TYPE, details.getUserType());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.NAME, details.getName());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.ADDRESS, details.getAddress());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.PHONE, details.getPhone());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.PROFILE_PHOTO, details.getProfilePhoto());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.ZIPCODE, details.getZipcode());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.LAT, details.getLat());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.LNG, details.getLng());
            PrefConnect.writeString(LoginActivity.this, PrefConnect.GCM_TOKEN, details.getNotificationID());
            if (details != null) {
                if (details.getUserType().equals("2")) {
                    PrefConnect.writeString(LoginActivity.this, PrefConnect.EXPERTISE_AT, details.getExpertiseAT());
                    PrefConnect.writeString(LoginActivity.this, PrefConnect.SSN, details.getSSN());
                    PrefConnect.writeString(LoginActivity.this, PrefConnect.LICENSE, details.getLicence());
                    PrefConnect.writeString(LoginActivity.this, PrefConnect.HOURLY_RATE, details.getHourlyRate());
                } else {
                    PrefConnect.writeString(LoginActivity.this, PrefConnect.BRAIN_TREE_CUSTOMER_ID, details.getBraintreeID());
                }
            }

            if (details.getBank_name() != null) {
                PrefConnect.writeString(LoginActivity.this, PrefConnect.BANK_NAME, details.getBank_name());
                PrefConnect.writeString(LoginActivity.this, PrefConnect.ACCOUNT_HOLDER_NAME, details.getAccount_holder_name());
                PrefConnect.writeString(LoginActivity.this, PrefConnect.ACCOUNT_NUMBER, details.getAccount_number());
                PrefConnect.writeString(LoginActivity.this, PrefConnect.ROUTING, details.getRouting());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startActivity(String status) {
        //"Customer has been blocked by admin"
        //"Customer email not yet activated"
        //"Contractor has been blocked by admin
        //"Contractor profile not yet activated"
        //"Contractor email not yet activated"
        //"Login Failed"
        Intent intent;
        switch (status) {
            case "Customer email not yet activated":
            case "Contractor email not yet activated":
                PrefConnect.writeInteger(LoginActivity.this, PrefConnect.PAGE, 1);
                intent = new Intent(LoginActivity.this, VerificationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case "Contractor profile not yet activated":
                PrefConnect.writeInteger(LoginActivity.this, PrefConnect.PAGE, 2);
                intent = new Intent(LoginActivity.this, ProfileConStatActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case "Login Failed":
                Toast.makeText(LoginActivity.this, "Invalid Email/Password", Toast.LENGTH_LONG).show();
                break;
            case "Contractor has been blocked by admin":
            case "Customer has been blocked by admin":
                Toast.makeText(LoginActivity.this, "Account Blocked by admin", Toast.LENGTH_LONG).show();
                break;

        }
    }

    private boolean validate() {
        if (TextUtils.isEmpty(edt_email.getText().toString())) {
            Toast.makeText(LoginActivity.this, "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edt_email.getText().toString())) {
            Matcher matcher = pattern.matcher(edt_email.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(LoginActivity.this, "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (TextUtils.isEmpty(edt_password.getText().toString())) {
            Toast.makeText(LoginActivity.this, "Enter password", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void afterTextChanged(int view, Editable s) {
        switch (view) {
            case R.id.email_login:
                Matcher matcher = pattern.matcher(s.toString());
                if (TextUtils.isEmpty(s.toString()) || !matcher.matches())
                    setPlusMark(email_add);
                else
                    setTickMark(email_add);
                break;
            case R.id.password_login:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(pass_add);
                else
                    setTickMark(pass_add);
                break;
        }
    }

    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }


}
