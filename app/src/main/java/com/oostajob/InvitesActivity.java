package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.VideoView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomCheckBox;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.adapter.contractor.DiscussionAdapter;
import com.oostajob.dialog.TermsDialogFragment;
import com.oostajob.global.Global;
import com.oostajob.model.BankDetailsResponse;
import com.oostajob.model.BidAmountModel;
import com.oostajob.model.ConBankDetails;
import com.oostajob.model.ContractorJobList;
import com.oostajob.model.DiscussionlistModel;
import com.oostajob.model.JobDiscussionModel;
import com.oostajob.utils.BlurTransformation;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InvitesActivity extends AppCompatActivity implements OnPreparedListener {

    private ListView discussionView;
    private String USER_ID;
    private ProgressDialog p;
    private FrameLayout videoLay;
    private VideoView videoView;
    private LinearLayout scrollContent;
    private CustomTextView txt_terms, txtTitle, txtCloses, txtZipCode, txtDistance, txtDescription, txtDiscussion, txtLowestBid;
    private ChipView chipView;
    private CustomEditText edtBidAmount, edtMessage;
    private CustomButton btnBidNow, btnSend;
    private SlidingUpPanelLayout panelLayout;
    private ContractorJobList.List details;
    private Toolbar toolbar;
    private HorizontalScrollView scrollView;
    private ImageView imgBack, imageView, imgPlay, imgClose, imgIcon;
    private ScrollView vScrollView;
    private MediaController mediaController;
    private boolean showFirst = true;
    private APIService apiService;
    private DiscussionAdapter discussionAdapter;
    private InputMethodManager imm;
    private ProgressBar progressBar;
    private CustomCheckBox chkAgree;
    private RelativeLayout relInvitesConTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid_now);

        init();
        setUpToolbar();
        setListener();

        loadValues();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txt_terms = (CustomTextView) findViewById(R.id.txt_con_terms);
        relInvitesConTerms = (RelativeLayout) findViewById(R.id.relInvitesConTerms);
        relInvitesConTerms.setVisibility(View.VISIBLE);
        chkAgree = (CustomCheckBox) findViewById(R.id.chkAgree);
        videoLay = (FrameLayout) findViewById(R.id.videoLay);
        videoView = (VideoView) findViewById(R.id.videoView);
        scrollContent = (LinearLayout) findViewById(R.id.scrollContent);
        txtTitle = (CustomTextView) findViewById(R.id.txtTitle);
        txtCloses = (CustomTextView) findViewById(R.id.txtCloses);
        txtZipCode = (CustomTextView) findViewById(R.id.txtZipCode);
        txtDistance = (CustomTextView) findViewById(R.id.txtDistance);
        txtDescription = (CustomTextView) findViewById(R.id.txtDescription);
        txtDiscussion = (CustomTextView) findViewById(R.id.txtDiscussion);
        chipView = (ChipView) findViewById(R.id.chipView);
        edtBidAmount = (CustomEditText) findViewById(R.id.edtBidAmount);
        btnBidNow = (CustomButton) findViewById(R.id.btnBidNow);
        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.panelLayout);
        imageView = (ImageView) findViewById(R.id.imageView);
        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        imgIcon = (ImageView) findViewById(R.id.imgIcon);
        scrollView = (HorizontalScrollView) findViewById(R.id.scrollView);
        discussionView = (ListView) findViewById(R.id.discussionView);
        edtMessage = (CustomEditText) findViewById(R.id.edtMessage);
        btnSend = (CustomButton) findViewById(R.id.btnSend);
        vScrollView = (ScrollView) findViewById(R.id.vScrollView);
        txtLowestBid = (CustomTextView) findViewById(R.id.txtLowestBid);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        panelLayout.setScrollableView(discussionView);

        p = new ProgressDialog(InvitesActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        mediaController = new MediaController(InvitesActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(InvitesActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPlay.setVisibility(View.GONE);
                videoView.start();
                scrollView.setVisibility(View.GONE);
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(InvitesActivity.this, "Cannot load video", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                setMediaVisibility(View.GONE, View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                imgPlay.setVisibility(View.VISIBLE);
            }
        });
        edtBidAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    if (chkAgree.isChecked()) {
                        btnBidNow.setEnabled(true);
                    }
                } else {
                    btnBidNow.setEnabled(false);
                }
            }
        });
        chkAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (chkAgree.isChecked() && (edtBidAmount.getText().toString().trim().length() > 0)) {
                    btnBidNow.setEnabled(true);
                } else {
                    btnBidNow.setEnabled(false);
                }

            }
        });

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsDialogFragment dialogFragment = new TermsDialogFragment();
                Bundle args = new Bundle();
                args.putInt("dialog_id", 4);
                dialogFragment.setArguments(args);
                dialogFragment.show(getSupportFragmentManager(), "con_dialog");
            }
        });

        btnBidNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validaForBankDetails()) {

                    p.show();
                    final BidAmountModel.Request request = new BidAmountModel.Request(USER_ID,
                            details.getJobpostID(), edtBidAmount.getText().toString());
                    Call<BidAmountModel.Response> call = apiService.bidJob(request);
                    call.enqueue(new Callback<BidAmountModel.Response>() {
                        @Override
                        public void onResponse(Call<BidAmountModel.Response> call, Response<BidAmountModel.Response> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                BidAmountModel.Response response1 = response.body();
                                if (response1.getResult().equalsIgnoreCase("Success")) {
                                    Intent homeIntent = new Intent(InvitesActivity.this, HomeActivity.class);
                                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    homeIntent.putExtra("fragPosition", 1);
                                    homeIntent.putExtra("check", 2);
                                    startActivity(homeIntent);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<BidAmountModel.Response> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });
                } else {
                    // Toast.makeText(InvitesActivity.this, getResources().getString(R.string.add_bank_details), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtMessage.getText().toString())) {
                    p.show();
                    JobDiscussionModel.Request request = new JobDiscussionModel.Request(USER_ID,
                            details.getJobpostID(), edtMessage.getText().toString());
                    Call<JobDiscussionModel.Response> call = apiService.postDiscussion(request);
                    call.enqueue(new Callback<JobDiscussionModel.Response>() {
                        @Override
                        public void onResponse(Call<JobDiscussionModel.Response> call, Response<JobDiscussionModel.Response> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                JobDiscussionModel.Response response1 = response.body();
                                if (response1.getResult().equalsIgnoreCase("Success")) {
                                    edtMessage.setText("");
                                    imm.hideSoftInputFromWindow(edtMessage.getWindowToken(), 0);
                                    loadDiscussion(details.getJobpostID());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<JobDiscussionModel.Response> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });

                } else {
                    Toast.makeText(InvitesActivity.this, "Enter Message", Toast.LENGTH_LONG).show();
                }
            }
        });
        panelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelCollapsed(View panel) {
                resizeScrollView(0.0f);
            }

            @Override
            public void onPanelExpanded(View panel) {
                resizeScrollView(1.0f);
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });

    }

    private boolean validaForBankDetails() {
        boolean flage = false;
        if (PrefConnect.readString(this, PrefConnect.BANK_NAME, "").trim().length() > 0) {
            flage = true;
        } else {
            getBankdDetailsForContractor();

        }
        return flage;
    }

    private void getBankdDetailsForContractor() {
        p.show();
        Call<ConBankDetails> call = apiService.getBankDetailsForContractor(USER_ID);

        call.enqueue(new Callback<ConBankDetails>() {
            @Override
            public void onResponse(Call<ConBankDetails> call, Response<ConBankDetails> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    //if(response.body().getResult().equalsIgnoreCase("Success"))
                    if (response.body().getResult().equalsIgnoreCase("success")) {
                        if (response.body().getResponse() != null && response.body().getResponse().size() > 0) {
                            setBankDeails(response.body().getResponse().get(0));
                            btnBidNow.performClick();
                            btnBidNow.setPressed(true);
                        } else {
                            Toast.makeText(InvitesActivity.this, getResources().getString(R.string.add_bank_details), Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ConBankDetails> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });

    }

    private void setBankDeails(BankDetailsResponse bankDetailsResponse) {
        if (bankDetailsResponse.getBankName() != null) {
            PrefConnect.writeString(InvitesActivity.this, PrefConnect.BANK_NAME, bankDetailsResponse.getBankName());
            PrefConnect.writeString(InvitesActivity.this, PrefConnect.ACCOUNT_HOLDER_NAME, bankDetailsResponse.getAccountHolderName());
            PrefConnect.writeString(InvitesActivity.this, PrefConnect.ACCOUNT_NUMBER, bankDetailsResponse.getAccountNumber());
            PrefConnect.writeString(InvitesActivity.this, PrefConnect.ROUTING, bankDetailsResponse.getRouting());
        }
    }

    private void resizeScrollView(float slideOffset) {
        // The scrollViewHeight calculation would need to change based on what views are
        // in the sliding panel. The calculation below works because the layout has
        // 2 views. 1) The row with the drag view which is layout.getPanelHeight() high.
        // 2) The ScrollView.
        final int scrollViewHeight = (int) ((panelLayout.getHeight() - panelLayout.getPanelHeight()) * (1.0f - slideOffset));
        final ViewGroup.LayoutParams currentLayoutParams = vScrollView.getLayoutParams();
        currentLayoutParams.height = scrollViewHeight;
        vScrollView.setLayoutParams(currentLayoutParams);
    }

    private void loadValues() {
        try {
            details = (ContractorJobList.List) getIntent().getExtras().getSerializable("details");
            assert details != null;
            // Load Background Image
            String IMG_URL = String.format("%s/%s", Global.BASE_URL, details.getJob_image());
            Picasso.with(this).load(IMG_URL).transform(new BlurTransformation(this, 25)).fit().into(imgBack);

            //load Files
            loadFiles(details.getJobmedialist());

            toolbar.setTitle(details.getJobtypeName());
            //title
            String ICON_URL = String.format("%s/%s", Global.BASE_URL, details.getJob_icon());
            Picasso.with(this).load(ICON_URL).fit().into(imgIcon);
            txtTitle.setText(details.getJobtypeName());

            //JOb CLoses
            txtCloses.setText(String.format("Job Closes in %s", details.getRemaining_time()));

            //Zipcode
            txtZipCode.setText(details.getZipcode());

            //Distance
            if (Integer.parseInt(details.getMiles()) > 1) {
                txtDistance.setText(String.format("%s miles from here", details.getMiles()));
            } else {
                txtDistance.setText(String.format("%s mile from here", details.getMiles()));
            }

            //Lowest Bid
            if (details.getMinimumbidamount() == null) {
                txtLowestBid.setVisibility(View.GONE);
            } else {
                txtLowestBid.setText(String.format("Lowest bid as of now $%s", details.getMinimumbidamount()));
            }


            //Tags
            setTags(details);

            //Description
            StringBuilder builder = new StringBuilder();
            builder.append(details.getDescription()).append("\n").append("\n");
            for (ContractorJobList.Answer answer : details.getJobanwserlist()) {
                if (answer.getTag_key().equalsIgnoreCase("2")) {
                    builder.append(answer.getQuest_name()).append("\n").append(answer.getAnswer()).append("\n");
                }
            }
            txtDescription.setText(builder.toString());

            loadDiscussion(details.getJobpostID());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTags(ContractorJobList.List details) {
        List<Chip> chipList = new ArrayList<>();
        for (ContractorJobList.Answer answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        chipView.setLineSpacing(10);
        chipView.setChipSpacing(10);
        chipView.setChipLayoutRes(R.layout.chip_lay);
        chipView.setChipBackgroundRes(R.drawable.tag);
        chipView.setChipList(chipList);
    }

    private void loadFiles(ArrayList<ContractorJobList.Media> fileList) {

        scrollContent.removeAllViews();
        final LayoutInflater inflater = LayoutInflater.from(this);

        for (final ContractorJobList.Media model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.image_view, scrollContent, false);
            ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            if (model.getMediaType().equals("1")) {
                Picasso.with(this).load(url).into(imgChosen);
            } else {
                Picasso.with(this).load(R.drawable.play).into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (model.getMediaType().equals("1")) {
                        setMediaVisibility(View.VISIBLE, View.GONE);
                        Picasso.with(InvitesActivity.this).load(url).fit().into(imageView);
                    } else {
                        setMediaVisibility(View.GONE, View.VISIBLE);
//                        p.show();
                        videoView.setVideoPath(url);
                        videoView.requestFocus();
//                        videoView.setOnPreparedListener(BidNowActivity.this);
                    }

                }
            });
            if (showFirst) {
                imgChosen.performClick();
                showFirst = false;
            }
            scrollContent.addView(view);
        }
    }

    private void setMediaVisibility(int arg0, int arg1) {
        imageView.setVisibility(arg0);
        videoLay.setVisibility(arg1);
    }

    private void loadDiscussion(String jobPostId) {
        p.show();
        Call<DiscussionlistModel.Response> call = apiService.discussionList(jobPostId);
        call.enqueue(new Callback<DiscussionlistModel.Response>() {
            @Override
            public void onResponse(Call<DiscussionlistModel.Response> call, Response<DiscussionlistModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    DiscussionlistModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        discussionAdapter = new DiscussionAdapter(InvitesActivity.this, response1.getDiscustionDetails());
                        discussionView.setAdapter(discussionAdapter);
                        txtDiscussion.setText(String.format("Discussion(%s)", discussionAdapter.getCount()));
                    }
                }
            }

            @Override
            public void onFailure(Call<DiscussionlistModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
//        Global.dismissProgress(p);
    }

    @Override
    public void onBackPressed() {
        if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }
        super.onBackPressed();
    }
}
