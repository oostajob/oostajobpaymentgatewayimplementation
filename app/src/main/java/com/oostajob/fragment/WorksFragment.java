package com.oostajob.fragment;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.Green.customfont.CustomButton;
import com.oostajob.LoginActivity;
import com.oostajob.R;
import com.oostajob.RegisterActivity;
import com.oostajob.SelectionActivity;
import com.oostajob.navigationdrawer.DrawRegisterFrag;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorksFragment extends Fragment implements ViewPager.OnPageChangeListener, OnClickListener {

    ViewPager pager;
    LinearLayout registerLay, indicatorLay;
    ArrayList<Integer> viewList = new ArrayList<>();
    WorksAdapter adapter;
    CustomButton btnLogin, btnRegister;
    private Display display;
    private Point size;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadView();
        size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        display = getActivity().getWindowManager().getDefaultDisplay();
    }

    private void loadView() {
        viewList.add(R.layout.welcome1);
        viewList.add(R.layout.welcome2);
        viewList.add(R.layout.welcome3);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_works, container, false);
        init(rootView);
        setAdapter();
        setListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        try {
            Bundle bundle = getArguments();
            int position = (bundle != null) ? bundle.getInt("position", 0) : 0;
            pager.setCurrentItem(position);
            setIndicator(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init(View rootView) {
        pager = (ViewPager) rootView.findViewById(R.id.pager);
        registerLay = (LinearLayout) rootView.findViewById(R.id.registerLay);
        indicatorLay = (LinearLayout) rootView.findViewById(R.id.indicatorLay);
        btnLogin = (CustomButton) rootView.findViewById(R.id.btnLogin);
        btnRegister = (CustomButton) rootView.findViewById(R.id.btnRegister);
    }

    private void setAdapter() {
        adapter = new WorksAdapter();
        pager.setAdapter(adapter);
    }

    private void setListener() {
        pager.setOnPageChangeListener(this);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((RegisterActivity) activity).onAttached(DrawRegisterFrag.GET_STARTED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setIndicator(position);
        if (position == 0)
            registerLay.setBackgroundColor(getResources().getColor(R.color.transparent));
        else
            registerLay.setBackgroundColor(getResources().getColor(R.color.darkgrey));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setIndicator(int position) {
        for (int i = 0; i < indicatorLay.getChildCount(); i++) {
            if (i == position)
                ((ImageView) indicatorLay.getChildAt(i)).setImageResource(R.drawable.circle_rose);
            else
                ((ImageView) indicatorLay.getChildAt(i)).setImageResource(R.drawable.circle_white);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnLogin:
                intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.btnRegister:
                intent = new Intent(getActivity(), SelectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

    public class WorksAdapter extends PagerAdapter {
        LayoutInflater inflater;

        public WorksAdapter() {
            inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public int getCount() {
            return viewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return object == view;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(viewList.get(position), container, false);
            if (position == 0) {
                ImageView img = (ImageView) view.findViewById(R.id.img);
                Picasso.with(getActivity()).load(R.drawable.welcome_bg)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .fit()
                        .centerCrop()
                        .into(img);
            }
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
