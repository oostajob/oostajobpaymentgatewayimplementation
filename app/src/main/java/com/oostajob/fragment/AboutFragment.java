package com.oostajob.fragment;


import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.oostajob.R;
import com.oostajob.RegisterActivity;
import com.oostajob.global.Global;
import com.oostajob.navigationdrawer.DrawRegisterFrag;
import com.oostajob.utils.PrefConnect;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    private WebView webAbout;
    private String WEB_URL;
    private ProgressBar progress;
    private ImageView imgBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WEB_URL = PrefConnect.readString(getActivity(), PrefConnect.ABOUT_US, "");
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        init(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadUrl();
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
    }

    private void init(View rootView) {
        webAbout = (WebView) rootView.findViewById(R.id.webAbout);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        webAbout.setWebViewClient(new WebClient());
        imgBack = (ImageView) rootView.findViewById(R.id.imgBack);
        WebSettings webSettings = webAbout.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    private void loadUrl() {
        webAbout.loadUrl(WEB_URL);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((RegisterActivity) activity).onAttached(DrawRegisterFrag.ABOUT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class WebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progress.setVisibility(View.GONE);
        }
    }
}
