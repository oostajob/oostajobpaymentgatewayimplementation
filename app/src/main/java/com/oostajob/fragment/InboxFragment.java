package com.oostajob.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.Green.customfont.CustomTextView;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.adapter.InboxAdapter;
import com.oostajob.adapter.InboxAdapter.RemoveListener;
import com.oostajob.global.Global;
import com.oostajob.model.InboxModel;
import com.oostajob.model.Result;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InboxFragment extends Fragment implements RemoveListener {

    private String USER_ID;
    private APIService apiService;
    private ListView inboxView;
    private InboxAdapter inboxAdapter;
    private ProgressDialog p;
    private ImageView imgBack;
    private CustomTextView txtEmptyView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);

        p = new ProgressDialog(getActivity());
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.setMessage(getString(R.string.loading));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        init(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
        loadInbox();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home_blue, menu);
    }

    private void init(View rootView) {
        inboxView = (ListView) rootView.findViewById(R.id.inboxView);
        txtEmptyView = (CustomTextView) rootView.findViewById(R.id.txtEmptyView);
        imgBack = (ImageView) rootView.findViewById(R.id.imgBack);

        inboxView.setEmptyView(txtEmptyView);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Inbox");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadInbox() {
        p.show();
        Call<InboxModel> call = apiService.getInbox(USER_ID);
        call.enqueue(new Callback<InboxModel>() {
            @Override
            public void onResponse(Call<InboxModel> call, Response<InboxModel> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    InboxModel inboxModel = response.body();
                    if (inboxModel.getResult().equalsIgnoreCase("Success")) {
                        inboxAdapter = new InboxAdapter(getActivity(), inboxModel.getInboxlist());
                        inboxAdapter.setRemoveListener(InboxFragment.this);
                        inboxView.setAdapter(inboxAdapter);
                    } else if (inboxModel.getResult().equalsIgnoreCase("Failed")) {
                        inboxAdapter = new InboxAdapter(getActivity(), new ArrayList<InboxModel.Inbox>());
                        inboxView.setAdapter(inboxAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<InboxModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onRemove(String id) {
        p.show();
        Call<Result> call = apiService.removeInbox(id);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    Result result = response.body();
                    if (result.getResult().equalsIgnoreCase("Success")) {
                        loadInbox();
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }
}
