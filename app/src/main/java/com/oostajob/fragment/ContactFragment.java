package com.oostajob.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.RegisterActivity;
import com.oostajob.global.Global;
import com.oostajob.model.ContactUs;
import com.oostajob.navigationdrawer.DrawRegisterFrag;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactFragment extends Fragment {

    private CustomEditText edtContactName, edtContactEmail, edtContactMsg;
    private String USER_ID, EMAIL;
    private APIService apiService;
    private CustomButton btnContactSend;
    private ProgressDialog p;
    private ImageView imgBack;
    private int MENU_RES;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        EMAIL = PrefConnect.readString(getActivity(), PrefConnect.EMAIL, "");
        apiService = RetrofitSingleton.createService(APIService.class);

        p = new ProgressDialog(getActivity());
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.setMessage(getString(R.string.loading));

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragmnet_contact_us, container, false);

        init(rootView);
        setListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(MENU_RES, menu);
    }

    private void init(View rootView) {
        edtContactName = (CustomEditText) rootView.findViewById(R.id.edtContactName);
        edtContactEmail = (CustomEditText) rootView.findViewById(R.id.edtContactEmail);
        edtContactMsg = (CustomEditText) rootView.findViewById(R.id.edtContactMsg);
        btnContactSend = (CustomButton) rootView.findViewById(R.id.btnContactSend);
        imgBack = (ImageView) rootView.findViewById(R.id.imgBack);

//        edtContactEmail.setText(EMAIL);
    }

    private void setListener() {
        btnContactSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    sendMail();
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((RegisterActivity) activity).onAttached(DrawRegisterFrag.CONTACT);
            MENU_RES = R.menu.menu_home;
        } catch (Exception e) {
            ((HomeActivity) activity).onAttached("Contact Us");
            MENU_RES = R.menu.menu_home_blue;
            e.printStackTrace();
        }
    }

    private void sendMail() {
        p.show();
        ContactUs.Request request = new ContactUs.Request("", edtContactName.getText().toString(),
                edtContactEmail.getText().toString(), edtContactMsg.getText().toString(), USER_ID);
        Call<ContactUs.Response> call = apiService.contactus(request);
        call.enqueue(new Callback<ContactUs.Response>() {
            @Override
            public void onResponse(Call<ContactUs.Response> call, Response<ContactUs.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ContactUs.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        Toast.makeText(getActivity(), "Message sent successfully", Toast.LENGTH_LONG).show();
                        edtContactName.setText("");
                        edtContactEmail.setText("");
                        edtContactMsg.setText("");
                    } else if (response1.getResult().equalsIgnoreCase("Failed")) {
                        Toast.makeText(getActivity(), "Message not sent", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactUs.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    public boolean isValidated() {
        if (TextUtils.isEmpty(edtContactName.getText().toString())) {
            Toast.makeText(getActivity(), "Enter name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtContactEmail.getText().toString())) {
            Toast.makeText(getActivity(), "Enter Email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtContactEmail.getText().toString())) {
            Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtContactEmail.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(getActivity(), "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (TextUtils.isEmpty(edtContactMsg.getText().toString())) {
            Toast.makeText(getActivity(), "Enter Message", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
