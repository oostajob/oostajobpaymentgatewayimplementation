package com.oostajob.fragment.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.oostajob.ConEarningInTableFormatActivity;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ConEarningsResponse;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abhigitmusri on 16/01/18.
 */

public class ConEarnings extends Fragment {
    private String USER_ID;
    private APIService apiService;
    private ProgressDialog p;
    private CustomButton btnRequest;

    private CustomTextView custClaimAmount, custTotalEarnings, custAmountDueBy;
    String totalEarnings;
    int v;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);
        // apiClient = RetrofitSingleton.createService(ApiClient.class);

        v++;

        p = Global.initProgress(getActivity());
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_your_earnings, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        btnRequest = rootView.findViewById(R.id.btnRequest);
        custClaimAmount = rootView.findViewById(R.id.custClaimAmount);
        custTotalEarnings = rootView.findViewById(R.id.custTotalEarnings);
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //HomeActivity.fromConEarnings = true;
                Intent intent = new Intent(getActivity(), ConEarningInTableFormatActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("amount", totalEarnings);
                intent.putExtras(bundle);
                ConEarnings.this.getActivity().startActivity(intent);
            }
        });
        sendDataToServer();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Earnings");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDataToServer() {
        p.show();
        Call<ConEarningsResponse> call = apiService.getContractorEarnings(USER_ID);

        call.enqueue(new Callback<ConEarningsResponse>() {
            @Override
            public void onResponse(Call<ConEarningsResponse> call, Response<ConEarningsResponse> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    setvaluesFromServer(response.body());
                }
            }

            @Override
            public void onFailure(Call<ConEarningsResponse> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void setvaluesFromServer(ConEarningsResponse response) {
        // custClaimAmount.setText("$" + response.getTotalEarning());
        totalEarnings = response.getTotalEarning();
        custClaimAmount.setText("$" + response.getTotalEarning());
        // custAmountDueBy.setText("Amount Claimable  on " + response.getNextMonthDate() + "");*/
    }

    @Override
    public void onStart() {
        super.onStart();


    }

   /* private String getClamiableMonth() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date nextMonthFirstDay = calendar.getTime();
        String date = sdfDate.format(nextMonthFirstDay);
        Log.d("getClamiableMonth", "" + date);
        *//*calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date nextMonthLastDay = calendar.getTime();*//*


        return date;
    }*/
}
