package com.oostajob.fragment.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomCheckBox;
import com.Green.customfont.CustomTextView;
import com.oostajob.FeedBackActivity;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.adapter.contractor.ContractorJobsAdapter;
import com.oostajob.adapter.contractor.InvitesAdapter;
import com.oostajob.adapter.customer.CustomerJobsAdapter;
import com.oostajob.adapter.customer.CustomerJobsAdapter.CancelJobListener;
import com.oostajob.dialog.CancelOrSkipDia;
import com.oostajob.dialog.CancelOrSkipDia.SuccessListener;
import com.oostajob.dialog.CusCancelJob;
import com.oostajob.dialog.CusPaymentAlert;
import com.oostajob.global.Global;
import com.oostajob.model.GetClientTokenWithCustomerID;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobCompleteByConSideResponse;
import com.oostajob.model.JobCompletedByConInput;
import com.oostajob.model.PayableAmountInput;
import com.oostajob.model.PayableAmountResponse;
import com.oostajob.model.PaymentTransactionInputResponse;
import com.oostajob.model.PaymentTransactionResults;
import com.oostajob.payment.internal.ApiClient;
import com.oostajob.payment.models.ClientToken;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CusJobsFragment extends Fragment implements CancelJobListener, SuccessListener, CustomerJobsAdapter.JobCompeletedOrCancledOrRateNowListner {

    private int CHECK;
    private ProgressDialog p;
    private String USER_ID;
    private CustomButton btnRequest;
    private CustomCheckBox check1, check2, check3, check4;
    private APIService apiService;
    private CustomerJobsAdapter adapter;
    private ViewPager viewPager;
    private ArrayList<GetCustDetailsModel.Details> jobBiddingArrayList = new ArrayList<>();
    private ArrayList<GetCustDetailsModel.Details> jobHireArrayList = new ArrayList<>();
    private ArrayList<GetCustDetailsModel.Details> jobRateNowArrayList = new ArrayList<>();
    private ArrayList<GetCustDetailsModel.Details> jobClosedArrayList = new ArrayList<>();

    private int check;

    private String mClientToken;
    private ApiClient apiClient;
    private static final int REQUEST_CODE = 1111;

    PayableAmountResponse payableAmountResponse;

    private String CONTRACTOR_USER_ID;
    private String jobId;

    private GetCustDetailsModel.Details contDetails;

    boolean paymentTransaction, jobCompleteByCustomer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        check = getArguments().getInt("check", 1);

        p = new ProgressDialog(getActivity());
        p.setMessage(getActivity().getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        apiClient = RetrofitSingleton.createService(ApiClient.class);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cus_jobs, container, false);
        init(rootView);
        setListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadValues();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void init(View rootView) {
        btnRequest = (CustomButton) rootView.findViewById(R.id.btnRequest);
        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        check1 = (CustomCheckBox) rootView.findViewById(R.id.check1);
        check2 = (CustomCheckBox) rootView.findViewById(R.id.check2);
        check3 = (CustomCheckBox) rootView.findViewById(R.id.check3);
        check4 = (CustomCheckBox) rootView.findViewById(R.id.check4);
        check1.setChecked(true);
    }

    private void setStatus() {
        if (check == 1) {
            check1.setChecked(true);
            check2.setChecked(false);
            check3.setChecked(false);
            check4.setChecked(false);
            viewPager.setCurrentItem(0);
        } else if (check == 2) {
            check1.setChecked(false);
            check2.setChecked(true);
            check3.setChecked(false);
            check4.setChecked(false);
            viewPager.setCurrentItem(1);
        } else if (check == 3) {
            check1.setChecked(false);
            check2.setChecked(false);
            check3.setChecked(true);
            check4.setChecked(false);
            viewPager.setCurrentItem(2);
        } else if (check == 4) {
            check1.setChecked(false);
            check2.setChecked(false);
            check3.setChecked(false);
            check4.setChecked(true);
            viewPager.setCurrentItem(3);
        }
    }

    private void setListener() {
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        check1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                viewPager.setCurrentItem(0);
                check2.setChecked(false);
                check3.setChecked(false);
                check4.setChecked(false);
            }
        });
        check2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPager.setCurrentItem(1);
                check1.setChecked(false);
                check3.setChecked(false);
                check4.setChecked(false);

            }
        });
        check3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPager.setCurrentItem(2);
                check1.setChecked(false);
                check2.setChecked(false);
                check4.setChecked(false);


            }
        });
        check4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                viewPager.setCurrentItem(3);
                check1.setChecked(false);
                check2.setChecked(false);
                check3.setChecked(false);
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Bidding");
                        getActivity().supportInvalidateOptionsMenu();
                        check1.setChecked(true);
                        check2.setChecked(false);
                        check3.setChecked(false);
                        check4.setChecked(false);
                        break;
                    case 1:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Hire");
                        getActivity().supportInvalidateOptionsMenu();
                        check2.setChecked(true);
                        check1.setChecked(false);
                        check3.setChecked(false);
                        check4.setChecked(false);
                        break;
                    case 2:
                        ((HomeActivity) getActivity()).onAttached("Jobs - In Progress");
                        getActivity().supportInvalidateOptionsMenu();

                        check3.setChecked(true);
                        check2.setChecked(false);
                        check1.setChecked(false);
                        check4.setChecked(false);
                        break;
                    case 3:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Closed");
                        getActivity().supportInvalidateOptionsMenu();
                        check4.setChecked(true);
                        check2.setChecked(false);
                        check3.setChecked(false);
                        check1.setChecked(false);
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Jobs - Bidding");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadValues() {
        jobBiddingArrayList = new ArrayList<>();
        jobHireArrayList = new ArrayList<>();
        jobRateNowArrayList = new ArrayList<>();
        jobClosedArrayList = new ArrayList<>();

        p.show();
        Call<GetCustDetailsModel.Response> call = apiService.getCustomerJobDetails(USER_ID);
        call.enqueue(new Callback<GetCustDetailsModel.Response>() {
            @Override
            public void onResponse(Call<GetCustDetailsModel.Response> call, Response<GetCustDetailsModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    GetCustDetailsModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        jobBiddingArrayList = response1.getJobbidding();
                        jobHireArrayList = response1.getJobhired();
                        jobRateNowArrayList = response1.getJobrated();
                        jobClosedArrayList = response1.getJobclosed();
                        if (isAdded()) {
                            CusPagerAdapter cusPagerAdapter = new CusPagerAdapter();
                            viewPager.setAdapter(cusPagerAdapter);
                        }
                        String jobStatus = getArguments().getString("job_status");
                        if (jobStatus != null) {
                            if (jobStatus.equalsIgnoreCase("hired")) {
                                viewPager.setCurrentItem(2);
                            }
                            if (jobStatus.equalsIgnoreCase("rated")) {
                                viewPager.setCurrentItem(3);
                            }
                        }
                    } else if (response1.getResult().equalsIgnoreCase("Failed")) {
                        if (isAdded()) {
                            CusPagerAdapter cusPagerAdapter = new CusPagerAdapter();
                            viewPager.setAdapter(cusPagerAdapter);
                        }
                    }
                    setStatus();
                }

            }

            @Override
            public void onFailure(Call<GetCustDetailsModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onJobCompleted(GetCustDetailsModel.Details details) {
        jobId = details.jobpostID;
        contDetails = details;
        CONTRACTOR_USER_ID = details.getContractorDetails().contractorID;


        getPayableAmount();
    }


    @Override
    public void onJobCancled(GetCustDetailsModel.Details details) {
        CusCancelJob dialog = new CusCancelJob();
        Bundle bundle = new Bundle();
        bundle.putSerializable("GetCustDetailsModel", details);
        bundle.putSerializable("jobPostID", details.getJobpostID());
        bundle.putString("title", "cancel");
        bundle.putBoolean("isCustomer", true);
        dialog.setArguments(bundle);
        dialog.setArguments(bundle);
        dialog.show(getActivity().getSupportFragmentManager(), "Hire Dialog");
    }

    @Override
    public void onRateNow(GetCustDetailsModel.Details details) {
        contDetails = details;
        callFeedBackActivity();
    }

    public class CusPagerAdapter extends PagerAdapter {
        private LayoutInflater inflater;
        private InvitesAdapter invitesAdapter;
        private ContractorJobsAdapter jobsAdapter;
        private ListView listView;
        private CustomTextView txtEmptyView;

        public CusPagerAdapter() {
            this.inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.item_job_list, container, false);
            init(view);
            setListAdapter(position);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private void init(View view) {
            listView = (ListView) view.findViewById(R.id.listView);
            txtEmptyView = (CustomTextView) view.findViewById(R.id.txtEmptyView);
            listView.setEmptyView(txtEmptyView);
        }

        private void setListAdapter(int position) {
            switch (position) {
                case 0:
                    adapter = new CustomerJobsAdapter(getActivity(), jobBiddingArrayList, CustomerJobsAdapter.BIDDING);
                    adapter.setCancelJobListener(CusJobsFragment.this);
                    listView.setAdapter(adapter);
                    txtEmptyView.setText("No Jobs in Bidding");
                    CHECK = 1;
                    break;
                case 1:
                    adapter = new CustomerJobsAdapter(getActivity(), jobHireArrayList, CustomerJobsAdapter.HIRE);
                    adapter.setCancelJobListener(CusJobsFragment.this);
                    listView.setAdapter(adapter);
                    txtEmptyView.setText("No Contractors to Hire");
                    CHECK = 2;
                    break;
                case 2:
                    adapter = new CustomerJobsAdapter(getActivity(), jobRateNowArrayList, CustomerJobsAdapter.RATE);
                    adapter.setCancelJobListener(null);
                    adapter.setJobCompeletedOrCancledListner(CusJobsFragment.this);
                    listView.setAdapter(adapter);
                    txtEmptyView.setText("No Contractors to Rate");
                    CHECK = 3;
                    break;
                case 3:
                    adapter = new CustomerJobsAdapter(getActivity(), jobClosedArrayList, CustomerJobsAdapter.CLOSE);
                    adapter.setCancelJobListener(null);
                    adapter.setJobCompeletedOrCancledListner(CusJobsFragment.this);
                    listView.setAdapter(adapter);
                    txtEmptyView.setText("No Jobs in Closed");
                    CHECK = 4;
                    break;
            }
        }

    }


    @Override
    public void onCancelJob(GetCustDetailsModel.Details details) {
        CancelOrSkipDia dialog = new CancelOrSkipDia();
        Bundle bundle = new Bundle();
        bundle.putSerializable("GetCustDetailsModel", details);
        bundle.putSerializable("jobPostID", details.getJobpostID());
        bundle.putString("title", "cancel");
        bundle.putBoolean("isCustomer", true);
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "Cancel Dialog");
    }


    @Override
    public void onSuccess() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("fragPosition", 1);
        intent.putExtra("check", CHECK);
        startActivity(intent);
    }


    public void getPayableAmount() {
        PayableAmountInput payableAmountInput = new PayableAmountInput();
        payableAmountInput.setContractor_id(CONTRACTOR_USER_ID);
        payableAmountInput.setJobId(jobId);
        p.show();
        Call<PayableAmountResponse> call = apiService.payableAmount(payableAmountInput);
        call.enqueue(new Callback<PayableAmountResponse>() {
            @Override
            public void onResponse(Call<PayableAmountResponse> call, Response<PayableAmountResponse> response) {
                payableAmountResponse = response.body();
                //  sendToPaymentGateway();
                CusPaymentAlert dialogFragment = new CusPaymentAlert();
                Bundle args = new Bundle();
                args.putBoolean("fromCustomerJobComplete", true);
                args.putString("jobId", jobId);
                args.putSerializable("payableAmountResponse", payableAmountResponse);
                args.putSerializable("contDetails", contDetails);
                dialogFragment.setArguments(args);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "cus_payment_alert");
                p.dismiss();
            }

            @Override
            public void onFailure(Call<PayableAmountResponse> call, Throwable t) {

                Toast.makeText(getActivity(), "please try later", Toast.LENGTH_SHORT).show();
                p.dismiss();

            }
        });
        // return payableAmount;
    }

    public void callFeedBackActivity() {

        Bundle bundle = new Bundle();
        bundle.putSerializable("contractor", contDetails);

        Intent intent = new Intent(getActivity(), FeedBackActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

