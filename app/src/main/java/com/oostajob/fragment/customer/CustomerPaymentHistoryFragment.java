package com.oostajob.fragment.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.adapter.customer.CusJobBillingAdapter;
import com.oostajob.model.CustomerPaymentJobHistory;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerPaymentHistoryFragment extends Fragment {

    private String TAG = CustomerPaymentHistoryFragment.class.getSimpleName();

    ListView listView;
    private ProgressDialog p;
    private String USER_ID;
    private APIService apiService;

    CusJobBillingAdapter conJobBillingAdapter;

    com.Green.customfont.CustomTextView txtEmptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        p = new ProgressDialog(getActivity());
        p.setMessage(getActivity().getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);


        getJobHistoryForCustomer();

    }

    private void getJobHistoryForCustomer() {
        Call<CustomerPaymentJobHistory> call = apiService.getCustomerJobHistory(USER_ID);
        p.show();
        call.enqueue(new Callback<CustomerPaymentJobHistory>() {
            @Override
            public void onResponse(Call<CustomerPaymentJobHistory> call, Response<CustomerPaymentJobHistory> response) {
                Log.d(TAG, "the response is " + response.body());
                CusJobBillingAdapter conJobBillingAdapter = new CusJobBillingAdapter(getActivity(), response.body().getResponse());
                if (response.body().getResponse().size() > 0) {
                    listView.setAdapter(conJobBillingAdapter);
                } else {
                    txtEmptyView.setVisibility(View.VISIBLE);
                }

                p.dismiss();
            }

            @Override
            public void onFailure(Call<CustomerPaymentJobHistory> call, Throwable t) {
                Log.d(TAG, "the response is " + t);
                p.dismiss();

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_customer_payment_history, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        listView = rootView.findViewById(R.id.listView);
        txtEmptyView = rootView.findViewById(R.id.txtEmptyView);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Billings");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
