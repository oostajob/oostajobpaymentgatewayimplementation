package com.oostajob.fragment.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.oostajob.HomeActivity;
import com.oostajob.QuestionsActivity;
import com.oostajob.R;
import com.oostajob.adapter.customer.JobTypeAdapter;
import com.oostajob.global.Global;
import com.oostajob.model.JobTypeModel;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
/*import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;*/

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JobTypesFragment extends Fragment {
    private ListView jobcatogry;
    private APIService apiService;
    private ProgressDialog p;
    private JobTypeAdapter adapter;
    //private AdView adView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(getActivity());
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_types, container, false);

        init(view);
        setListener();
        setAdapter();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //loadAd();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home_blue, menu);
    }

    private void setAdapter() {
        p.show();
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    JobTypeModel model = response.body();
                    if (model.getResult().equalsIgnoreCase("Success")) {
                        if(getActivity()!=null)
                        {
                            adapter = new JobTypeAdapter(getActivity(), model.getJobtypeDetails());
                            jobcatogry.setAdapter(adapter);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


    private void setListener() {
        jobcatogry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JobTypeModel.JobtypeDetails details = adapter.getItem(position);
                String imgUrl = String.format("%s/%s", Global.BASE_URL, details.getJob_image());
                Intent qusIntent = new Intent(getActivity(), QuestionsActivity.class);
                qusIntent.putExtra("subTitle", details.getJob_name());
                qusIntent.putExtra("jobTypeId", details.getJobtype_id());
                qusIntent.putExtra("imgUrl", imgUrl);
                startActivity(qusIntent);
            }
        });
    }

    private void init(View view) {

        jobcatogry = (ListView) view.findViewById(R.id.item_work_selection);
        /*adView = (AdView) view.findViewById(R.id.adView);*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Select a service that you want");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
