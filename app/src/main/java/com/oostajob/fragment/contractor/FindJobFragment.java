package com.oostajob.fragment.contractor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oostajob.BidNowActivity;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.adapter.contractor.FindJobAdapter;
import com.oostajob.adapter.contractor.SpinnerAdapter;
import com.oostajob.dialog.CancelOrSkipDia;
import com.oostajob.global.Global;
import com.oostajob.model.ConJobListModel;
import com.oostajob.model.JobTypeModel;
import com.oostajob.utils.CircleTransform;
import com.oostajob.utils.FetchAddress;
import com.oostajob.utils.PrefConnect;
import com.oostajob.utils.Tracker;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FindJobFragment extends Fragment implements OnMapReadyCallback, CancelOrSkipDia.SuccessListener,
        FindJobAdapter.ItemClickListener, FindJobAdapter.SkipJobListener, Tracker.LocationCallback {

    public static final int STATUS = 150;
    private ProgressDialog p;
    private String ZipCode = "", USER_ID;
    private ArrayList<JobTypeModel.JobtypeDetails> expertiseList = new ArrayList<>();
    private String expertiseId;
    private MenuItem mapItem, listItem;
    private ListView listView;
    private Spinner spinFilter;
    private SupportMapFragment mapFragment;
    private APIService apiService;
    private SpinnerAdapter spinnerAdapter;
    private CustomEditText edtZipCode;
    private ImageView imgsearch;
    private CustomTextView txtTotalJobs;
    private GoogleMap googleMap;
    private FetchAddress address;
    private InputMethodManager imm;
    private MotionEvent events;
    private LinearLayout infoLayout;
    private CustomTextView txtMarkerTitle, txtMarkerZip, txtBidNow, txtMarkerDesc, txtMarkerDistance, txtMarkerCloses;
    private ImageView imgMarkerPhoto, imgMarkerIcon;

    private Tracker tracker;
    private boolean requireLocationOnce = true;

    private ArrayList<ConJobListModel.ContractorJoblist> contractorJobLists;
    private FindJobAdapter findJobAdapter;
    private Map<String, ConJobListModel.ContractorJoblist> markers;
    private Marker mMarker;
    private Bitmap bitmap;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        apiService = RetrofitSingleton.createService(APIService.class);
        expertiseId = PrefConnect.readString(getActivity(), PrefConnect.EXPERTISE_AT, "");
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        p = new ProgressDialog(getActivity());
        p.setIndeterminate(false);
        p.setMessage(getString(R.string.loading));
        p.setCancelable(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_job, container, false);
        init(rootView);
        p.show();
        setListener();
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(View.GONE);
            }
        });
        this.googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(View.GONE);
            }
        });
        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMarker = marker;
                ConJobListModel.ContractorJoblist details = null;
                if (!TextUtils.isEmpty(marker.getId()) && markers != null && markers.size() > 0) {
                    if (markers.get(marker.getId()) != null) {
                        details = markers.get(marker.getId());
                    }
                }
                if (details != null) {
                    infoLayout.setVisibility(View.VISIBLE);
                    setValues(details, marker);
                }
                return true;
            }
        });
//        this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//            @Override
//            public void onInfoWindowClick(Marker marker) {
//                ConJobListModel.ContractorJoblist details = markers.get(marker.getId());
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("details", details);
//                Intent intent = new Intent(getContext(), BidNowActivity.class);
//                intent.putExtras(bundle);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//            }
//        });
        //this.googleMap.setInfoWindowAdapter(new CustomInfoWindow());

        setFilter();

    }

    private void init(View rootView) {
        listView = (ListView) rootView.findViewById(R.id.listView);
        spinFilter = (Spinner) rootView.findViewById(R.id.spinFilter);
        edtZipCode = (CustomEditText) rootView.findViewById(R.id.edtZipCode);
        txtTotalJobs = (CustomTextView) rootView.findViewById(R.id.txtTotalJobs);
        imgsearch = (ImageView) rootView.findViewById(R.id.imgsearch);
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        //info window
        infoLayout = (LinearLayout) rootView.findViewById(R.id.infoLayout);
        txtMarkerTitle = (CustomTextView) rootView.findViewById(R.id.txtMarkerTitle);
        txtMarkerZip = (CustomTextView) rootView.findViewById(R.id.txtMarkerZip);
        txtMarkerDesc = (CustomTextView) rootView.findViewById(R.id.txtMarkerDesc);
        txtMarkerDistance = (CustomTextView) rootView.findViewById(R.id.txtMarkerDistance);
        txtMarkerCloses = (CustomTextView) rootView.findViewById(R.id.txtMarkerCloses);
        txtBidNow = (CustomTextView) rootView.findViewById(R.id.txtBidNow);
        imgMarkerPhoto = (ImageView) rootView.findViewById(R.id.imgMarkerPhoto);
        imgMarkerIcon = (ImageView) rootView.findViewById(R.id.imgMarkerIcon);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_con_find_job, menu);
        mapItem = menu.findItem(R.id.action_map_filter);
        listItem = menu.findItem(R.id.action_list_filter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map_filter:
                if (infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(View.GONE);
                setMenuItemVisible(false);
                break;
            case R.id.action_list_filter:
                if (infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(View.GONE);
                setMenuItemVisible(true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        tracker.connectClient();
    }

    @Override
    public void onPause() {
        super.onPause();
        tracker.disConnectClient();
    }

    private void setMenuItemVisible(boolean b) {
        mapItem.setVisible(b);
        listItem.setVisible(!b);
        try {
            if (b) {
                mapFragment.getView().setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            } else {
                mapFragment.getView().setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Select a job to bid");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        infoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMarker != null) {
                    ConJobListModel.ContractorJoblist details = markers.get(mMarker.getId());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("details", details);
                    Intent intent = new Intent(getContext(), BidNowActivity.class);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
        imgsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(edtZipCode.getWindowToken(), 0);
                if (TextUtils.isEmpty(edtZipCode.getText().toString())) {
                    ZipCode = PrefConnect.readString(getActivity(), PrefConnect.ZIPCODE, address.getPostalCode());
                    edtZipCode.setText(ZipCode);
                } else {
                    ZipCode = edtZipCode.getText().toString().trim();
                    edtZipCode.setText(ZipCode);
                }

                JobTypeModel.JobtypeDetails details = spinnerAdapter.getItem(spinFilter.getSelectedItemPosition());
                loadValues(ZipCode, details.getJobtype_id());
            }
        });
        edtZipCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(View.GONE);
            }
        });
        edtZipCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    imm.hideSoftInputFromWindow(edtZipCode.getWindowToken(), 0);
                    if (TextUtils.isEmpty(edtZipCode.getText().toString())) {
                        ZipCode = PrefConnect.readString(getActivity(), PrefConnect.ZIPCODE, address.getPostalCode());
                        edtZipCode.setText(ZipCode);
                    } else {
                        ZipCode = edtZipCode.getText().toString().trim();
                        edtZipCode.setText(ZipCode);
                    }

                    JobTypeModel.JobtypeDetails details = spinnerAdapter.getItem(spinFilter.getSelectedItemPosition());
                    loadValues(ZipCode, details.getJobtype_id());
                    return true;
                }
                return false;
            }
        });

        spinFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(View.GONE);
                imm.hideSoftInputFromWindow(edtZipCode.getWindowToken(), 0);
                if (TextUtils.isEmpty(edtZipCode.getText().toString()))
                    ZipCode = address.getPostalCode();
                else
                    ZipCode = edtZipCode.getText().toString().trim();
                JobTypeModel.JobtypeDetails details = spinnerAdapter.getItem(position);
                loadValues(ZipCode, details.getJobtype_id());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tracker = new Tracker(getActivity(), FindJobFragment.this);
    }

    private void setFilter() {
        if (!p.isShowing())
        p.show();
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                if (response.isSuccessful()) {
                    JobTypeModel jobTypeModel = response.body();
                    if (jobTypeModel.getResult().equalsIgnoreCase("success")) {
                        ArrayList<JobTypeModel.JobtypeDetails> jobTypesList = jobTypeModel.getJobtypeDetails();
                        String[] arr = expertiseId.split(",");
                        for (String anArr : arr) {
                            for (int j = 0; j < jobTypesList.size(); j++) {
                                if (jobTypesList.get(j).getJobtype_id().equals(anArr.trim())) {
                                    expertiseList.add(jobTypesList.get(j));
                                }
                            }
                        }
                        if (expertiseList.size() > 1) {
                            JobTypeModel.JobtypeDetails details = new JobTypeModel.JobtypeDetails("", "All Jobs", "", "");
                            expertiseList.add(0, details);
                        }
                        spinnerAdapter = new SpinnerAdapter(getActivity(), expertiseList);
                        spinFilter.setAdapter(spinnerAdapter);
                        fetchZipCode();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {

            }
        });
    }

    private void fetchZipCode() {
        if (TextUtils.isEmpty(edtZipCode.getText().toString().trim())) {
            Location location = tracker.getLocation();
            address = new FetchAddress(getActivity(), new LatLng(location.getLatitude(), location.getLongitude()));
            address.getAddress();
            ZipCode = address.getPostalCode();
            edtZipCode.setText(ZipCode);
        } else {
            ZipCode = edtZipCode.getText().toString().trim();
        }
        JobTypeModel.JobtypeDetails details = spinnerAdapter.getItem(spinFilter.getSelectedItemPosition());
        loadValues(ZipCode, details.getJobtype_id());

    }

    private void loadValues(String zipCode, String jobId) {
        if (!p.isShowing())
            p.show();
        final ConJobListModel.Request request = new ConJobListModel.Request(zipCode, jobId);
        Call<ConJobListModel.Response> call = apiService.conJobList(USER_ID, request);
        call.enqueue(new Callback<ConJobListModel.Response>() {
            @Override
            public void onResponse(Call<ConJobListModel.Response> call, Response<ConJobListModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ConJobListModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("success")) {
                        new RefreshTask(address.getLatLng(), response1.getContractor_joblist()).execute();
                        findJobAdapter = new FindJobAdapter(getActivity(), response1.getContractor_joblist());
                        findJobAdapter.setViewListener(FindJobFragment.this);
                        findJobAdapter.setSkipJobListener(FindJobFragment.this);
                        listView.setAdapter(findJobAdapter);
                        txtTotalJobs.setText(String.format("%d job(s) around you Bid now!", findJobAdapter.getCount()));
                    } else {
                        new RefreshTask(address.getLatLng(), response1.getContractor_joblist()).execute();
                        listView.setAdapter(null);
                        txtTotalJobs.setText(R.string.no_matching_job);
                    }
                }
            }

            @Override
            public void onFailure(Call<ConJobListModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case STATUS:
                    JobTypeModel.JobtypeDetails details = spinnerAdapter.getItem(spinFilter.getSelectedItemPosition());
                    loadValues(ZipCode, details.getJobtype_id());
                    break;
                case Tracker.REQUEST_CHECK_SETTINGS:
                    tracker = new Tracker(getActivity(), FindJobFragment.this);
                    tracker.disConnectClient();
                    tracker.connectClient();
                    break;
            }
        }
    }

    @Override
    public void onItemClick(ConJobListModel.ContractorJoblist details) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("details", details);
        Intent intent = new Intent(getContext(), BidNowActivity.class);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, STATUS);
    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null && requireLocationOnce) {
            mapFragment.getMapAsync(FindJobFragment.this);
            requireLocationOnce = false;
        }
    }

    @Override
    public void loadDefaultLocation() {

    }

    private void setValues(ConJobListModel.ContractorJoblist details, final Marker marker) {
        //set Photo
        ConJobListModel.Media media = details.getJobmedialist().get(0);
        if (media.getMediaType().equals("1")) {
            String mediaContent = media.getMediaContent();
            mediaContent = mediaContent.replaceAll(" ", "%20");
            String url = String.format("%s/%s", Global.BASE_URL, mediaContent);
            // new ImageUpload(imgMarkerPhoto, url).execute();
            Picasso.with(getActivity()).load(url).fit().centerCrop().into(imgMarkerPhoto);
        } else {
            Picasso.with(getActivity()).load(R.drawable.play).into(imgMarkerPhoto);
        }

        //set Icon
        String icon = details.getIcon();
        icon = icon.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, icon);
        //new ImageUpload(imgMarkerIcon, url).execute();
        Picasso.with(getActivity()).load(url).fit().centerCrop().into(imgMarkerIcon);
        //set Values
        txtMarkerTitle.setText(details.getJobtypeName());
        txtMarkerZip.setText(details.getZipcode());
        txtMarkerDesc.setText(details.getDescription());
        if (Integer.parseInt(details.getMiles()) > 1) {
            txtMarkerDistance.setText(String.format("%s miles from here", details.getMiles()));
        } else {
            txtMarkerDistance.setText(String.format("%s mile from here", details.getMiles()));
        }
        txtMarkerCloses.setText(String.format("Job closes in %s", details.getRemaining_time()));
    }

    @Override
    public void onSkipJob(ConJobListModel.ContractorJoblist details) {

        if(isAdded()) {
            CancelOrSkipDia dialog = new CancelOrSkipDia();
            dialog.setSuccessListener(this);

            Bundle bundle = new Bundle();
            bundle.putSerializable("jobPostID", details.getJobpostID());
            bundle.putString("title", "skip");
            bundle.putBoolean("isCustomer", false);
            dialog.setArguments(bundle);
            dialog.show(getChildFragmentManager(), "Cancel Dialog");
        }
    }

    @Override
    public void onSuccess() {
        if(isAdded()) {
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("fragPosition", 1);
            startActivity(intent);
        }
    }

    private class RefreshTask extends AsyncTask<Void, Object, Void> {
        LatLng latLng;
        ArrayList<ConJobListModel.ContractorJoblist> contractor_joblist;

        public RefreshTask(LatLng latLng, ArrayList<ConJobListModel.ContractorJoblist> contractor_joblist) {
            this.latLng = latLng;
            this.contractor_joblist = contractor_joblist;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            googleMap.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            markers = new HashMap<>();
            for (ConJobListModel.ContractorJoblist jobs : contractor_joblist) {
                latLng = new LatLng(Double.parseDouble(jobs.getLat()), Double.parseDouble(jobs.getLng()));
                String icon = jobs.getIcon();
                icon = icon.replaceAll(" ", "%20");
                String url = String.format("%s/%s", Global.BASE_URL, icon);
                try {
                    Bitmap bitmap = Picasso.with(getActivity()).load(url).transform(new CircleTransform()).resize(50, 50).get();
                    publishProgress(bitmap, jobs);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Bitmap bitmap = (Bitmap) values[0];
            Bitmap bitmap_size = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
            drawMarker(bitmap_size);
            ConJobListModel.ContractorJoblist jobs = (ConJobListModel.ContractorJoblist) values[1];
            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .title(jobs.getJobtypeName())
                    .position(latLng)
                    .icon(BitmapDescriptorFactory
                            .fromBitmap(bitmap_size)));
            markers.put(marker.getId(), jobs);
        }

        private void drawMarker(Bitmap bitmap) {
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            paint.setColor(Color.BLUE);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(7.0f);
            paint.setAntiAlias(true);
            canvas.drawRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), paint);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
        }
    }

}
