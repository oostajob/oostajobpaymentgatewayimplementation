package com.oostajob.fragment.contractor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.Green.customfont.CustomTextView;
import com.oostajob.HomeActivity;
import com.oostajob.QuestionsActivity;
import com.oostajob.R;
import com.oostajob.adapter.contractor.ConJobBillingAdapter;
import com.oostajob.adapter.customer.CusJobBillingAdapter;
import com.oostajob.fragment.customer.CustomerPaymentHistoryFragment;
import com.oostajob.model.ConPaymentBillingHistoryResponse;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConPaymentHistoryActivity extends AppCompatActivity {

    private String TAG = CustomerPaymentHistoryFragment.class.getSimpleName();

    ListView listView;
    private ProgressDialog p;
    private String USER_ID;
    private APIService apiService;

    CustomTextView txtEmptyView;
    private Toolbar toolbar;
    private ActionBar.LayoutParams lp;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_payment_status);
        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(this, PrefConnect.USER_ID, "");
        listView = findViewById(R.id.listView);

        txtEmptyView = findViewById(R.id.txtEmptyView);
        p = new ProgressDialog(this);

        p.setMessage(this.getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);
        setUpCustomToolbar();


        getJobHistoryForContractor();

    }

    private void setUpCustomToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Payment History");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_home);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getJobHistoryForContractor() {
        Call<ConPaymentBillingHistoryResponse> call = apiService.getConJobHistory(USER_ID);
        p.show();
        call.enqueue(new Callback<ConPaymentBillingHistoryResponse>() {
            @Override
            public void onResponse(Call<ConPaymentBillingHistoryResponse> call, Response<ConPaymentBillingHistoryResponse> response) {
                Log.d(TAG, "the response is " + response.body());
                // new CusJobBillingAdapter(this,response.body().getResponse());
                ConJobBillingAdapter conJobBillingAdapter = new ConJobBillingAdapter(ConPaymentHistoryActivity.this, response.body().getResponse());
                if (response.body().getResponse().size() > 0) {
                    listView.setAdapter(conJobBillingAdapter);
                } else {
                    txtEmptyView.setVisibility(View.VISIBLE);
                }

                p.dismiss();
            }

            @Override
            public void onFailure(Call<ConPaymentBillingHistoryResponse> call, Throwable t) {
                Log.d(TAG, "the response is " + t);
                p.dismiss();

            }
        });
    }


}