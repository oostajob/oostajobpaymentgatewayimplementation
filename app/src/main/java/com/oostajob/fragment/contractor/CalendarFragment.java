package com.oostajob.fragment.contractor;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.Green.customfont.calendar.CalendarView;
import com.Green.customfont.calendar.CalendarView.DateEventHandler;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.CalanderListModel;
import com.oostajob.model.UpdateCalenderModel;
import com.oostajob.utils.CalHolder;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.oostajob.model.UpdateCalenderModel.Request;


public class CalendarFragment extends Fragment implements DateEventHandler, OnCheckedChangeListener {

    private Pattern pattern = Pattern.compile("[0-9][0-9][0-9][0-9]\\-[0-9][0-9]\\-[0-9][0-9]", Pattern.CASE_INSENSITIVE);
    private ProgressDialog p;
    private String USER_ID;
    private CalendarView calendarView;
    private CheckBox checkbox_8to10, checkbox_10to12, checkbox_12to2, checkbox_2to4, checkbox_4to6;
    private ImageView checkbox_8to10_img, checkbox_10to12_img, checkbox_12to2_img, checkbox_2to4_img, checkbox_4to6_img;
    private CustomTextView txt_8to10, txt_10to12, txt_12to2, txt_2to4, txt_4to6;
    private CustomButton btnNotAvailable, btnReset;
    private String CURRENT_DATE = "";
    private APIService apiService;
    private HashMap<String, CalHolder> calMap = new HashMap<>();
    private HashMap<String, String> calCus = new HashMap<>();
    private HashMap<Date, Boolean> calUpdate = new HashMap<>();
    private List<String> timeList = new ArrayList<>();
    private String[] arrTime = {"8am-10am", "10am-12pm", "12pm-2pm", "2pm-4pm", "4pm-6pm"};
    private ArrayList<CalanderListModel.CalenderDetails> calDetails;
    private ArrayList<CalanderListModel.CalenderDetailsCustomer> calDetailsCus;
    private Date tomorrow, seleceted_date;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        timeList = Arrays.asList(arrTime);

        p = new ProgressDialog(getActivity());
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.setMessage(getString(R.string.loading));

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        init(rootView);
        setListener();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getCalendar();
        getTomorrow();
    }

    private void loadCurrentAvailable(Date date) {
        String[] timeCus = {};
        CURRENT_DATE = Global.getDate(date, "yyyy-MM-dd");
        if (calMap.containsKey(CURRENT_DATE) && calCus.containsKey(CURRENT_DATE)) {
            timeCus = calCus.get(CURRENT_DATE).split(",");
            display(calMap.get(CURRENT_DATE), timeCus);
        } else if (calCus.containsKey(CURRENT_DATE)) {
            CalHolder calHolder = new CalHolder();
            calMap.put(CURRENT_DATE, calHolder);
            timeCus = calCus.get(CURRENT_DATE).split(",");
            display(calHolder, timeCus);
        } else if (calMap.containsKey(CURRENT_DATE)) {
            display(calMap.get(CURRENT_DATE), timeCus);
        } else {
            CalHolder calHolder = new CalHolder();
            calMap.put(CURRENT_DATE, calHolder);
            display(calHolder, timeCus);
        }
        //calendarView.updateCalendar(calUpdate);
        calendarView.setItemChecked(CURRENT_DATE);
    }

    private void getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        tomorrow = calendar.getTime();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_done, menu);
        CustomTextView textView = (CustomTextView) menu.findItem(R.id.action_done).getActionView();
        textView.setText(R.string.done);
        textView.setTextColor(getResources().getColor(R.color.black));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCalendarValue();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                updateCalendarValue();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init(View rootView) {
        calendarView = (CalendarView) rootView.findViewById(R.id.calendarView);
        checkbox_8to10 = (CheckBox) rootView.findViewById(R.id.checkbox_8to10);
        checkbox_10to12 = (CheckBox) rootView.findViewById(R.id.checkbox_10to12);
        checkbox_12to2 = (CheckBox) rootView.findViewById(R.id.checkbox_12to2);
        checkbox_2to4 = (CheckBox) rootView.findViewById(R.id.checkbox_2to4);
        checkbox_4to6 = (CheckBox) rootView.findViewById(R.id.checkbox_4to6);
        checkbox_8to10_img = (ImageView) rootView.findViewById(R.id.checkbox_8to10_img);
        checkbox_10to12_img = (ImageView) rootView.findViewById(R.id.checkbox_10to12_img);
        checkbox_12to2_img = (ImageView) rootView.findViewById(R.id.checkbox_12to2_img);
        checkbox_2to4_img = (ImageView) rootView.findViewById(R.id.checkbox_2to4_img);
        checkbox_4to6_img = (ImageView) rootView.findViewById(R.id.checkbox_4to6_img);
        txt_8to10 = (CustomTextView) rootView.findViewById(R.id.txt_8to10);
        txt_10to12 = (CustomTextView) rootView.findViewById(R.id.txt_10to12);
        txt_12to2 = (CustomTextView) rootView.findViewById(R.id.txt_12to2);
        txt_2to4 = (CustomTextView) rootView.findViewById(R.id.txt_2to4);
        txt_4to6 = (CustomTextView) rootView.findViewById(R.id.txt_4to6);
        btnNotAvailable = (CustomButton) rootView.findViewById(R.id.btnNotAvailable);
        btnReset = (CustomButton) rootView.findViewById(R.id.btnReset);
    }

    private void setListener() {
        calendarView.setDateEventHandler(this);
        btnNotAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Matcher matcher = pattern.matcher(CURRENT_DATE);
                if (matcher.matches()) {
                    checkbox_8to10.setChecked(true);
                    checkbox_10to12.setChecked(true);
                    checkbox_12to2.setChecked(true);
                    checkbox_2to4.setChecked(true);
                    checkbox_4to6.setChecked(true);

                    Date date = Global.getDate(CURRENT_DATE, "yyyy-MM-dd");
                    calUpdate.put(date, true);
                    calendarView.updateCalendar(calUpdate);
                } else {
                    Toast.makeText(getActivity(), "Select Date", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (calCus.containsKey(CURRENT_DATE)) {
                    String timeCus = calCus.get(CURRENT_DATE);

                    switch (timeCus) {
                        case "8am-10am":
                            checkbox_8to10.setChecked(true);
                            checkbox_8to10.setVisibility(View.GONE);
                            checkbox_10to12.setChecked(false);
                            checkbox_12to2.setChecked(false);
                            checkbox_2to4.setChecked(false);
                            checkbox_4to6.setChecked(false);
                            break;
                        case "10am-12pm":
                            checkbox_8to10.setChecked(false);
                            checkbox_10to12.setChecked(true);
                            checkbox_10to12.setVisibility(View.GONE);
                            checkbox_12to2.setChecked(false);
                            checkbox_2to4.setChecked(false);
                            checkbox_4to6.setChecked(false);
                            break;
                        case "12pm-2pm":
                            checkbox_8to10.setChecked(false);
                            checkbox_10to12.setChecked(false);
                            checkbox_12to2.setChecked(true);
                            checkbox_12to2.setVisibility(View.GONE);
                            checkbox_2to4.setChecked(false);
                            checkbox_4to6.setChecked(false);
                            break;
                        case "2pm-4pm":
                            checkbox_8to10.setChecked(false);
                            checkbox_10to12.setChecked(false);
                            checkbox_12to2.setChecked(false);
                            checkbox_2to4.setChecked(true);
                            checkbox_2to4.setVisibility(View.GONE);
                            checkbox_4to6.setChecked(false);
                            break;
                        case "4pm-6pm":
                            checkbox_8to10.setChecked(false);
                            checkbox_10to12.setChecked(false);
                            checkbox_12to2.setChecked(false);
                            checkbox_2to4.setChecked(false);
                            checkbox_4to6.setChecked(true);
                            checkbox_4to6.setVisibility(View.GONE);
                            break;
                    }
                } else {
                    checkbox_8to10.setVisibility(View.VISIBLE);
                    checkbox_10to12.setVisibility(View.VISIBLE);
                    checkbox_12to2.setVisibility(View.VISIBLE);
                    checkbox_2to4.setVisibility(View.VISIBLE);
                    checkbox_4to6.setVisibility(View.VISIBLE);*/
                checkbox_8to10.setChecked(false);
                checkbox_10to12.setChecked(false);
                checkbox_12to2.setChecked(false);
                checkbox_2to4.setChecked(false);
                checkbox_4to6.setChecked(false);
                // }

                calMap = new HashMap<>();
                calCus = new HashMap<>();
                calUpdate = new HashMap<>();
                setHashMapValues(calDetails, calDetailsCus);

            }
        });
        checkbox_8to10.setOnCheckedChangeListener(this);
        checkbox_10to12.setOnCheckedChangeListener(this);
        checkbox_12to2.setOnCheckedChangeListener(this);
        checkbox_2to4.setOnCheckedChangeListener(this);
        checkbox_4to6.setOnCheckedChangeListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Calendar");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCalendar() {
        p.show();
        Call<CalanderListModel.Response> call = apiService.getCalendar(USER_ID);
        call.enqueue(new Callback<CalanderListModel.Response>() {
            @Override
            public void onResponse(Call<CalanderListModel.Response> call, Response<CalanderListModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    CalanderListModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        calDetails = response1.getCalenderDetails();
                        //
                        calDetailsCus = response1.getCalenderDetailsCustomer();
                        //
                        setHashMapValues(calDetails, calDetailsCus);
                    }
                }

            }

            @Override
            public void onFailure(Call<CalanderListModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }


    private void updateCalendarValue() {
        p.show();
        removeEmptyHash();
        final Request request = getRequest();
        Call<UpdateCalenderModel.Response> call = apiService.updateCalender(USER_ID, request);
        call.enqueue(new Callback<UpdateCalenderModel.Response>() {
            @Override
            public void onResponse(Call<UpdateCalenderModel.Response> call, Response<UpdateCalenderModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    UpdateCalenderModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        getCalendar();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateCalenderModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void removeEmptyHash() {
        for (Iterator<Map.Entry<String, CalHolder>> it = calMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, CalHolder> entry = it.next();
            boolean[] val = entry.getValue().getTime();
            int a = 0;
            for (boolean aVal : val) {
                if (!aVal) {
                    a = a + 1;
                }
            }
            if (a == 5) {
                it.remove();
            }
        }
    }

    private void setHashMapValues(ArrayList<CalanderListModel.CalenderDetails> calenderDetails, ArrayList<CalanderListModel.CalenderDetailsCustomer> calenderDetailsCus) {
        for (CalanderListModel.CalenderDetails calDetail : calenderDetails) {
            CalHolder holder = getCalHolder(calDetail.getNotAvailableTime());
            calMap.put(calDetail.getNotAvailabledate(), holder);
            Date date = Global.getDate(calDetail.getNotAvailabledate(), "yyyy-MM-dd");
            calUpdate.put(date, holder.isFullNotAvailable());
        }
        //
        for (CalanderListModel.CalenderDetailsCustomer calDetailCus : calenderDetailsCus) {
            //Date dateCus = Global.getDate(calDetailCus.getNotAvailabledate(), "yyyy-MM-dd");
            calCus.put(calDetailCus.getNotAvailabledate(), calDetailCus.getNotAvailableTime());
        }
        calendarView.updateCalendar(calUpdate);
        if (seleceted_date == null)
            loadCurrentAvailable(tomorrow);
        else
            loadCurrentAvailable(seleceted_date);
    }

    private CalHolder getCalHolder(String time) {
        CalHolder holder = new CalHolder();
        String[] timeArr = time.split(",");
        boolean[] val = holder.getTime();
        try {
            val[timeList.indexOf(timeArr[0].trim())] = getCheckInfo(timeArr[0].trim());
            val[timeList.indexOf(timeArr[1].trim())] = getCheckInfo(timeArr[1].trim());
            val[timeList.indexOf(timeArr[2].trim())] = getCheckInfo(timeArr[2].trim());
            val[timeList.indexOf(timeArr[3].trim())] = getCheckInfo(timeArr[3].trim());
            val[timeList.indexOf(timeArr[4].trim())] = getCheckInfo(timeArr[4].trim());
        } catch (Exception ignored) {
        }
        holder.setTime(val);
        return holder;
    }

    private boolean getCheckInfo(String time) {
        switch (time) {
            case "8am-10am":
                return true;
            case "10am-12pm":
                return true;
            case "12pm-2pm":
                return true;
            case "2pm-4pm":
                return true;
            case "4pm-6pm":
                return true;
            default:
                return false;
        }
    }


    @Override
    public void onDayPress(Date date) {
        String[] timeCus = {};
        seleceted_date = date;
        CURRENT_DATE = Global.getDate(date, "yyyy-MM-dd");
        if (calMap.containsKey(CURRENT_DATE) && calCus.containsKey(CURRENT_DATE)) {
            timeCus = calCus.get(CURRENT_DATE).split(",");
            display(calMap.get(CURRENT_DATE), timeCus);
        } else if (calCus.containsKey(CURRENT_DATE)) {
            CalHolder calHolder = new CalHolder();
            calMap.put(CURRENT_DATE, calHolder);
            timeCus = calCus.get(CURRENT_DATE).split(",");
            display(calHolder, timeCus);
        } else if (calMap.containsKey(CURRENT_DATE)) {
            display(calMap.get(CURRENT_DATE), timeCus);
        } else {
            CalHolder calHolder = new CalHolder();
            calMap.put(CURRENT_DATE, calHolder);
            display(calHolder, timeCus);
        }

    }

    @Override
    public void updateCalendar() {
        calendarView.updateCalendar(calUpdate);
    }

    public void display(CalHolder calHolder, String[] timeCus) {
        boolean[] check = calHolder.getTime();
        /*if (calCus.containsKey(CURRENT_DATE)) {
            String timeCus = calCus.get(CURRENT_DATE);*/
        checkbox_8to10.setVisibility(View.VISIBLE);
        checkbox_10to12.setVisibility(View.VISIBLE);
        checkbox_12to2.setVisibility(View.VISIBLE);
        checkbox_2to4.setVisibility(View.VISIBLE);
        checkbox_4to6.setVisibility(View.VISIBLE);
        if (timeCus.length != 0) {
            for (String timesingle : timeCus) {
                switch (timesingle) {
                    case "8am-10am":
                        //checkbox_8to10.setChecked(true);
                        checkbox_8to10.setVisibility(View.GONE);
                        checkbox_10to12.setChecked(check[1]);
                        checkbox_12to2.setChecked(check[2]);
                        checkbox_2to4.setChecked(check[3]);
                        checkbox_4to6.setChecked(check[4]);
                        /*checkbox_10to12.setVisibility(View.VISIBLE);
                        checkbox_12to2.setVisibility(View.VISIBLE);
                        checkbox_2to4.setVisibility(View.VISIBLE);
                        checkbox_4to6.setVisibility(View.VISIBLE);*/
                        break;
                    case "10am-12pm":
                        //checkbox_10to12.setChecked(true);
                        checkbox_10to12.setVisibility(View.GONE);
                        checkbox_8to10.setChecked(check[0]);
                        checkbox_12to2.setChecked(check[2]);
                        checkbox_2to4.setChecked(check[3]);
                        checkbox_4to6.setChecked(check[4]);
                        /*checkbox_8to10.setVisibility(View.VISIBLE);
                        checkbox_12to2.setVisibility(View.VISIBLE);
                        checkbox_2to4.setVisibility(View.VISIBLE);
                        checkbox_4to6.setVisibility(View.VISIBLE);*/
                        break;
                    case "12pm-2pm":
                        //checkbox_12to2.setChecked(true);
                        checkbox_12to2.setVisibility(View.GONE);
                        checkbox_8to10.setChecked(check[0]);
                        checkbox_10to12.setChecked(check[1]);
                        checkbox_2to4.setChecked(check[3]);
                        checkbox_4to6.setChecked(check[4]);
                        /*checkbox_8to10.setVisibility(View.VISIBLE);
                        checkbox_10to12.setVisibility(View.VISIBLE);
                        checkbox_2to4.setVisibility(View.VISIBLE);
                        checkbox_4to6.setVisibility(View.VISIBLE);*/
                        break;
                    case "2pm-4pm":
                        //checkbox_2to4.setChecked(true);
                        checkbox_2to4.setVisibility(View.GONE);
                        checkbox_8to10.setChecked(check[0]);
                        checkbox_10to12.setChecked(check[1]);
                        checkbox_12to2.setChecked(check[2]);
                        checkbox_4to6.setChecked(check[4]);
                        /*checkbox_8to10.setVisibility(View.VISIBLE);
                        checkbox_10to12.setVisibility(View.VISIBLE);
                        checkbox_12to2.setVisibility(View.VISIBLE);
                        checkbox_4to6.setVisibility(View.VISIBLE);*/
                        break;
                    case "4pm-6pm":
                        //checkbox_4to6.setChecked(true);
                        checkbox_4to6.setVisibility(View.GONE);
                        checkbox_8to10.setChecked(check[0]);
                        checkbox_10to12.setChecked(check[1]);
                        checkbox_12to2.setChecked(check[2]);
                        checkbox_2to4.setChecked(check[3]);
                       /* checkbox_8to10.setVisibility(View.VISIBLE);
                        checkbox_10to12.setVisibility(View.VISIBLE);
                        checkbox_12to2.setVisibility(View.VISIBLE);
                        checkbox_2to4.setVisibility(View.VISIBLE);*/
                        break;
                }
            }
        } else

        {
            checkbox_8to10.setVisibility(View.VISIBLE);
            checkbox_10to12.setVisibility(View.VISIBLE);
            checkbox_12to2.setVisibility(View.VISIBLE);
            checkbox_2to4.setVisibility(View.VISIBLE);
            checkbox_4to6.setVisibility(View.VISIBLE);
            checkbox_8to10.setChecked(check[0]);
            checkbox_10to12.setChecked(check[1]);
            checkbox_12to2.setChecked(check[2]);
            checkbox_2to4.setChecked(check[3]);
            checkbox_4to6.setChecked(check[4]);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try {
            Matcher matcher = pattern.matcher(CURRENT_DATE);
            if (matcher.matches()) {
                CalHolder holder = calMap.get(CURRENT_DATE);
                boolean[] check = holder.getTime();
                switch (buttonView.getId()) {
                    case R.id.checkbox_8to10:
                        check[0] = isChecked;
                        break;
                    case R.id.checkbox_10to12:
                        check[1] = isChecked;
                        break;
                    case R.id.checkbox_12to2:
                        check[2] = isChecked;
                        break;
                    case R.id.checkbox_2to4:
                        check[3] = isChecked;
                        break;
                    case R.id.checkbox_4to6:
                        check[4] = isChecked;
                        break;
                }

                holder.setTime(check);
                calMap.put(CURRENT_DATE, holder);
//                boolean shouldRefreshCal = getRefreshStatus(check);
//                if (shouldRefreshCal) {
                Date date = Global.getDate(CURRENT_DATE, "yyyy-MM-dd");
                calUpdate.put(date, holder.isFullNotAvailable());
                calendarView.updateCalendar(calUpdate);
                calendarView.setItemChecked(CURRENT_DATE);
//                }
            } else {
                Toast.makeText(getActivity(), "Select Date", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* private boolean getRefreshStatus(boolean[] check) {
        int count = 0;
        for (int i = 0; i < check.length; i++) {
            if (check[i]) {
                count = count + 1;
            }
        }
        if (count == check.length) {
            return true;
        }
        return false;
    }*/

    public Request getRequest() {
        Request request = null;
        ArrayList<UpdateCalenderModel.Calander> calendarList = new ArrayList<>();
        for (Map.Entry<String, CalHolder> entry : calMap.entrySet()) {
            String times = getTimes(entry.getValue());
            UpdateCalenderModel.Calander calander = new UpdateCalenderModel.Calander(entry.getKey(), times);
            calendarList.add(calander);
        }
        request = new Request(calendarList);
        return request;
    }

    private String getTimes(CalHolder holder) {
        StringBuilder builder = new StringBuilder();
        boolean[] arr = holder.getTime();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]) {
                builder.append(timeList.get(i)).append(",");
            }
        }
        String str = builder.toString().substring(0, builder.toString().length() - 1);
        return str;
    }

    private void calTimeCus() {

    }

}
