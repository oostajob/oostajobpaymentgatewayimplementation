package com.oostajob.fragment.contractor;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.GetClientTokenWithCustomerID;
import com.oostajob.model.MakeSubscriptionModel;
import com.oostajob.payment.internal.ApiClient;
import com.oostajob.payment.models.ClientToken;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class BillingFragment extends Fragment {

    private String BRAIN_TREE_CUSTOMER_ID;
    private static final String TAG = "PayPal";
    private static final String BILL_AMOUNT = "30";
    private static final int REQUEST_CODE = 1111;
    private String USER_ID;
    private String mClientToken;

    private ProgressDialog p;
    private CustomTextView txtAmount;
    private CustomButton btnUnSubscribe;
    private ImageView imgPayPal;
    private APIService apiService;
    private ApiClient apiClient;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        BRAIN_TREE_CUSTOMER_ID = PrefConnect.readString(getActivity(), PrefConnect.BRAIN_TREE_CUSTOMER_ID, "");
        apiService = RetrofitSingleton.createService(APIService.class);
        apiClient = RetrofitSingleton.createService(ApiClient.class);


        getClientToken();

        p = Global.initProgress(getActivity());
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_billing, container, false);
        init(rootView);
        setListener();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        load();
        boolean billStatus = PrefConnect.readBoolean(getActivity(), PrefConnect.BILL_STATUS, false);
        if (billStatus)
            btnUnSubscribe.setVisibility(View.VISIBLE);
        else btnUnSubscribe.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home_blue, menu);
    }

    private void load() {
        txtAmount.setText(BILL_AMOUNT);
    }

    private void init(View rootView) {
        txtAmount = (CustomTextView) rootView.findViewById(R.id.txtAmount);
        btnUnSubscribe = (CustomButton) rootView.findViewById(R.id.btnUnSubscribe);
        imgPayPal = (ImageView) rootView.findViewById(R.id.imgPayPal);

        imgPayPal.setEnabled(false);
    }

    private void setListener() {
        imgPayPal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  PaymentRequest request = new PaymentRequest();
                request.primaryDescription("OOstaJob Premium");
                request.secondaryDescription("1 Month Subscription");
                request.submitButtonText("Subscribe Now");
                request.amount("$30");
                request.currencyCode("USD");
                request.clientToken(mClientToken);
                startActivityForResult(request.getIntent(getActivity()), REQUEST_CODE);*/

            }
        });
        btnUnSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    private void getClientToken() {

        GetClientTokenWithCustomerID getClientTokenWithCustomerID = new GetClientTokenWithCustomerID();
        getClientTokenWithCustomerID.setBraintreeID(BRAIN_TREE_CUSTOMER_ID);

        Call<ClientToken> call = apiClient.getClientToken(getClientTokenWithCustomerID);
        call.enqueue(new Callback<ClientToken>() {
            @Override
            public void onResponse(Call<ClientToken> call, Response<ClientToken> response) {
                if (response.isSuccessful()) {
                    ClientToken clientToken = response.body();
                    if (clientToken.getResult().equalsIgnoreCase("Success")) {
                        if (!TextUtils.isEmpty(clientToken.getClientToken())) {
                            mClientToken = clientToken.getClientToken();
                            imgPayPal.setEnabled(true);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ClientToken> call, Throwable t) {
                /*showDialog("Unable to get a client token. Response Code
                : " + error.getResponse().getStatus() + " Response body: " + error.getResponse().getBody());*/
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Billing");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentMethodNonce paymentMethodNonce = data.getParcelableExtra(
                        BraintreePaymentActivity.EXTRA_PAYMENT_METHOD_NONCE
                );
                String nonce = paymentMethodNonce.getNonce();
                if (!TextUtils.isEmpty(nonce)) {
                    // Send the nonce to your server.
                    sendNonceToServer(nonce);
                }
            }
        }
*/
       /* if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            }
        }*/

    }

    public void sendNonceToServer(String non) {
        p.show();
      /*  Nonce nonce = new Nonce(non);
        Call<Nonce.NonceResult> call = apiService.sendNonce(nonce);
        call.enqueue(new Callback<Nonce.NonceResult>() {
            @Override
            public void onResponse(Call<Nonce.NonceResult> call, Response<Nonce.NonceResult> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResult().isSuccess()) {
                        makeSubscription();
                    }
                }
            }

            @Override
            public void onFailure(Call<Nonce.NonceResult> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });*/
    }

    public void makeSubscription() {
        p.show();
        final MakeSubscriptionModel.Request request = new MakeSubscriptionModel.Request(USER_ID, "1 month");
        Call<MakeSubscriptionModel.Response> call = apiService.makeSubscription(request);
        call.enqueue(new Callback<MakeSubscriptionModel.Response>() {
            @Override
            public void onResponse(Call<MakeSubscriptionModel.Response> call, Response<MakeSubscriptionModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    MakeSubscriptionModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        PrefConnect.writeBoolean(getActivity(), PrefConnect.BILL_STATUS, true);
                        btnUnSubscribe.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<MakeSubscriptionModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }
}
