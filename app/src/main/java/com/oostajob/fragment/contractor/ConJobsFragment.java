package com.oostajob.fragment.contractor;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomCheckBox;
import com.Green.customfont.CustomTextView;
import com.oostajob.HomeActivity;
import com.oostajob.R;
import com.oostajob.adapter.contractor.ContractorJobsAdapter;
import com.oostajob.adapter.contractor.InvitesAdapter;
import com.oostajob.dialog.CancelOrSkipDia;
import com.oostajob.dialog.ConCommentsAfterJobComplet;
import com.oostajob.global.Global;
import com.oostajob.model.ContractorJobList;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobCompleteByConSideResponse;
import com.oostajob.model.JobCompletedByConInput;
import com.oostajob.utils.PrefConnect;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.oostajob.adapter.contractor.InvitesAdapter.SkipJobListener;
import static com.oostajob.dialog.CancelOrSkipDia.SuccessListener;

public class ConJobsFragment extends Fragment implements SkipJobListener, SuccessListener, ContractorJobsAdapter.JobCompeletedOrCancledListner {

    private ProgressDialog p;
    private String USER_ID;
    private APIService apiService;
    private CustomCheckBox check1, check2, check3, check4;
    private CustomButton btnFindJob;
    private ViewPager viewPager;
    private ArrayList<ContractorJobList.List> jobInviteArrayList = new ArrayList<>();
    private ArrayList<ContractorJobList.List> jobBidArrayList = new ArrayList<>();
    private ArrayList<ContractorJobList.List> jobHiredArrayList = new ArrayList<>();
    private ArrayList<ContractorJobList.List> jobClosedArrayList = new ArrayList<>();

    private int check;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = RetrofitSingleton.createService(APIService.class);
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        check = getArguments().getInt("check", 1);

        p = new ProgressDialog(getActivity());
        p.setMessage(getActivity().getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_con_jobs, container, false);
        loadValues();
        init(rootView);
        setListener();
        return rootView;
    }

    private void init(View rootView) {
        btnFindJob = (CustomButton) rootView.findViewById(R.id.btnFindJob);
        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        check1 = (CustomCheckBox) rootView.findViewById(R.id.check1);
        check2 = (CustomCheckBox) rootView.findViewById(R.id.check2);
        check3 = (CustomCheckBox) rootView.findViewById(R.id.check3);
        check4 = (CustomCheckBox) rootView.findViewById(R.id.check4);
    }

    private void setStatus() {
        if (check == 1) {
            check1.setChecked(true);
            check2.setChecked(false);
            check3.setChecked(false);
            check4.setChecked(false);
            viewPager.setCurrentItem(0);
        } else if (check == 2) {
            check1.setChecked(false);
            check2.setChecked(true);
            check3.setChecked(false);
            check4.setChecked(false);
            viewPager.setCurrentItem(1);
        } else if (check == 3) {
            check1.setChecked(false);
            check2.setChecked(false);
            check3.setChecked(true);
            check4.setChecked(false);
            viewPager.setCurrentItem(2);
        } else if (check == 4) {
            check1.setChecked(false);
            check2.setChecked(false);
            check3.setChecked(false);
            check4.setChecked(true);
            viewPager.setCurrentItem(3);
        } /*else if (check == 5) {
            check1.setChecked(false);
            check2.setChecked(false);
            check3.setChecked(false);
            check4.setChecked(true);
            viewPager.setCurrentItem(1);
        }*/

        if (HomeActivity.fromConEarnings) {
            viewPager.setCurrentItem(3);
            HomeActivity.fromConEarnings = false;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((HomeActivity) activity).onAttached("Jobs - Invites");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setListener() {
        btnFindJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        check1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                viewPager.setCurrentItem(0);
                check2.setChecked(false);
                check3.setChecked(false);
                check4.setChecked(false);
            }
        });
        check2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPager.setCurrentItem(1);
                check1.setChecked(false);
                check3.setChecked(false);
                check4.setChecked(false);

            }
        });
        check3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPager.setCurrentItem(2);
                check1.setChecked(false);
                check2.setChecked(false);
                check4.setChecked(false);


            }
        });
        check4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                viewPager.setCurrentItem(3);
                check1.setChecked(false);
                check2.setChecked(false);
                check3.setChecked(false);
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Invites");
                        getActivity().supportInvalidateOptionsMenu();
                        check1.setChecked(true);
                        check2.setChecked(false);
                        check3.setChecked(false);
                        check4.setChecked(false);
                        break;
                    case 1:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Bids");
                        getActivity().supportInvalidateOptionsMenu();
                        check2.setChecked(true);
                        check1.setChecked(false);
                        check3.setChecked(false);
                        check4.setChecked(false);
                        break;
                    case 2:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Hired");
                        getActivity().supportInvalidateOptionsMenu();

                        check3.setChecked(true);
                        check2.setChecked(false);
                        check1.setChecked(false);
                        check4.setChecked(false);
                        break;
                    case 3:
                        ((HomeActivity) getActivity()).onAttached("Jobs - Closed");
                        getActivity().supportInvalidateOptionsMenu();
                        check4.setChecked(true);
                        check2.setChecked(false);
                        check3.setChecked(false);
                        check1.setChecked(false);
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void loadValues() {
        jobInviteArrayList = new ArrayList<>();
        jobBidArrayList = new ArrayList<>();
        jobHiredArrayList = new ArrayList<>();
        jobClosedArrayList = new ArrayList<>();
        p.show();
        Call<ContractorJobList> call = apiService.getContractorJobList(USER_ID);
        call.enqueue(new Callback<ContractorJobList>() {
            @Override
            public void onResponse(Call<ContractorJobList> call, Response<ContractorJobList> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    ContractorJobList model = response.body();
                    if (model.getResult().equalsIgnoreCase("Success")) {
                        jobInviteArrayList = model.getJobinvites();
                        jobBidArrayList = model.getJobbidding();
                        jobHiredArrayList = model.getJobhired();
                        jobClosedArrayList = model.getJobclosed();
                        if (isAdded()) {
                            ConPagerAdapter conPagerAdapter = new ConPagerAdapter();
                            viewPager.setAdapter(conPagerAdapter);


                        }
                        setStatus();
                    }
                }
            }

            @Override
            public void onFailure(Call<ContractorJobList> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    @Override
    public void onSkipJob(ContractorJobList.List details) {
        CancelOrSkipDia dialog = new CancelOrSkipDia();
        dialog.setSuccessListener(this);

        Bundle bundle = new Bundle();
        bundle.putSerializable("jobPostID", details.getJobpostID());
        bundle.putString("title", "skip");
        bundle.putBoolean("isCustomer", false);
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "Cancel Dialog");
    }

    @Override
    public void onSuccess() {
        loadValues();
    }

    @Override
    public void onJobCompleted(ContractorJobList.List list) {
        /*
         TermsDialogFragment dialogFragment = new TermsDialogFragment();
                Bundle args = new Bundle();
                args.putInt("dialog_id", 2);
                dialogFragment.setArguments(args);
                dialogFragment.show(getSupportFragmentManager(), "con_dialog");
         */

        ConCommentsAfterJobComplet dialogFragment = new ConCommentsAfterJobComplet();
        Bundle args = new Bundle();
        args.putString("jobpostID", list.jobpostID);
        args.putString("customerDetailsId", list.customerDetails.getCustomerID());
        dialogFragment.setArguments(args);
        dialogFragment.show(getActivity().getSupportFragmentManager(), "con_dialog");



        /*p.show();
        */


    }


    public class ConPagerAdapter extends PagerAdapter {
        private LayoutInflater inflater;
        private InvitesAdapter invitesAdapter;
        private ContractorJobsAdapter jobsAdapter;
        private ListView listView;
        private CustomTextView txtEmptyView;

        public ConPagerAdapter() {
            this.inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.item_job_list, container, false);
            init(view);
            setListAdapter(position);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private void init(View view) {
            listView = (ListView) view.findViewById(R.id.listView);
            txtEmptyView = (CustomTextView) view.findViewById(R.id.txtEmptyView);

            listView.setEmptyView(txtEmptyView);
        }

        private void setListAdapter(int position) {
            switch (position) {
                case 0:
                    invitesAdapter = new InvitesAdapter(getActivity(), jobInviteArrayList);
                    invitesAdapter.setSkipJobListener(ConJobsFragment.this);
                    listView.setAdapter(invitesAdapter);
                    txtEmptyView.setText("No Jobs in Invites");
                    break;
                case 1:
                    jobsAdapter = new ContractorJobsAdapter(getActivity(), jobBidArrayList, ContractorJobsAdapter.BID);
                    listView.setAdapter(jobsAdapter);
                    txtEmptyView.setText("No Jobs in Bids");
                    break;
                case 2:
                    jobsAdapter = new ContractorJobsAdapter(getActivity(), jobHiredArrayList, ContractorJobsAdapter.HIRE);
                    listView.setAdapter(jobsAdapter);
                    jobsAdapter.setJobCompeletedOrCancledListner(ConJobsFragment.this);
                    txtEmptyView.setText("No Jobs in Hired");
                    break;
                case 3:
                    jobsAdapter = new ContractorJobsAdapter(getActivity(), jobClosedArrayList, ContractorJobsAdapter.CLOSE);
                    listView.setAdapter(jobsAdapter);
                    txtEmptyView.setText("No Jobs in Closed");
                    break;
            }
        }

    }

}
