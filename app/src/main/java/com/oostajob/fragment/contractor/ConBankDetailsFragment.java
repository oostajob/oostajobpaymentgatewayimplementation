package com.oostajob.fragment.contractor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomEditText;
import com.oostajob.ConBankDetailActivity;
import com.oostajob.HomeActivity;
import com.oostajob.LoginActivity;
import com.oostajob.R;
import com.oostajob.RegisterActivity;
import com.oostajob.global.Global;
import com.oostajob.model.BankDetailsResponse;
import com.oostajob.model.ConBankDetails;
import com.oostajob.model.ConBankInputModel;
import com.oostajob.model.ConRegModel;
import com.oostajob.navigationdrawer.DrawRegisterFrag;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConBankDetailsFragment extends Fragment implements View.OnClickListener, AutoTextWatcher.TextWatcher {

    ImageView skip;
    Button btnSubmit;
    private APIService apiService;
    private Toolbar toolbar;
    private ProgressDialog p;

    ImageView imgRounting, imgBankAccountNumber, imgBankAccountName, imgBankName;

    com.Green.customfont.CustomEditText edtBankName, edtBankAccountNumber, edtUserAccountName, edtRounting;
    private String USER_ID;

    private int MENU_RES;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_contr_bank_details, container, false);
        init(rootView);
        //setUpToolBar();
        setListener();
        populatFromSharePreference();
        USER_ID = PrefConnect.readString(getActivity(), PrefConnect.USER_ID, "");
        getBankdDetailsForContractor();


        return rootView;
    }

    private void getBankdDetailsForContractor() {

        p.show();
        Call<ConBankDetails> call = apiService.getBankDetailsForContractor(USER_ID);

        call.enqueue(new Callback<ConBankDetails>() {
            @Override
            public void onResponse(Call<ConBankDetails> call, Response<ConBankDetails> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    //if(response.body().getResult().equalsIgnoreCase("Success"))
                    if (response.body().getResult().equalsIgnoreCase("success")) {
                        if (response.body().getResponse() != null && response.body().getResponse().size() > 0) {
                            setBankDeails(response.body().getResponse().get(0));
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ConBankDetails> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });

    }

    private void setBankDeails(BankDetailsResponse bankDetailsResponse) {

        edtBankName.setText(bankDetailsResponse.getBankName());
        edtBankAccountNumber.setText(bankDetailsResponse.getAccountNumber());
        edtUserAccountName.setText(bankDetailsResponse.getAccountHolderName());
        edtRounting.setText(bankDetailsResponse.getRouting());

        if (bankDetailsResponse.getBankName() != null) {
            PrefConnect.writeString(getActivity(), PrefConnect.BANK_NAME, bankDetailsResponse.getBankName());
            PrefConnect.writeString(getActivity(), PrefConnect.ACCOUNT_HOLDER_NAME, bankDetailsResponse.getAccountHolderName());
            PrefConnect.writeString(getActivity(), PrefConnect.ACCOUNT_NUMBER, bankDetailsResponse.getAccountNumber());
            PrefConnect.writeString(getActivity(), PrefConnect.ROUTING, bankDetailsResponse.getRouting());
        }
    }

    private void populatFromSharePreference() {
        edtBankName.setText(PrefConnect.readString(getActivity(), PrefConnect.BANK_NAME, ""));
        edtBankAccountNumber.setText(PrefConnect.readString(getActivity(), PrefConnect.ACCOUNT_NUMBER, ""));
        edtUserAccountName.setText(PrefConnect.readString(getActivity(), PrefConnect.ACCOUNT_HOLDER_NAME, ""));
        edtRounting.setText(PrefConnect.readString(getActivity(), PrefConnect.ROUTING, ""));

    }

    private void init(View rootView) {
        apiService = RetrofitSingleton.createService(APIService.class);
        p = new ProgressDialog(getActivity());
        skip = (ImageView) rootView.findViewById(R.id.skip);

        skip.setVisibility(View.GONE);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);
        edtBankName = (CustomEditText) rootView.findViewById(R.id.edtBankName);
        edtBankAccountNumber = (CustomEditText) rootView.findViewById(R.id.edtBankAccountNumber);
        edtUserAccountName = (CustomEditText) rootView.findViewById(R.id.edtUserAccountName);
        edtRounting = (CustomEditText) rootView.findViewById(R.id.edtRounting);
        imgRounting = (ImageView) rootView.findViewById(R.id.imgRounting);
        imgBankAccountNumber = (ImageView) rootView.findViewById(R.id.imgBankAccountNumber);
        imgBankAccountName = (ImageView) rootView.findViewById(R.id.imgBankAccountName);
        imgBankName = (ImageView) rootView.findViewById(R.id.imgBankName);


        // p = Global.initProgress(this);
    }

    private void setListener() {
        // skipBankDetails.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        edtBankName.addTextChangedListener(new AutoTextWatcher(R.id.edtBankName, this));
        edtBankAccountNumber.addTextChangedListener(new AutoTextWatcher(R.id.edtBankAccountNumber, this));
        edtUserAccountName.addTextChangedListener(new AutoTextWatcher(R.id.edtUserAccountName, this));
        edtRounting.addTextChangedListener(new AutoTextWatcher(R.id.edtRounting, this));
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
           /* case R.id.skipBankDetails:
                break;*/

            case R.id.btnSubmit:
                if (isValidated()) {

                    p.show();
                    Call<ConRegModel.ContractorBankDetailsResponse> call = apiService.changeBankDetails(populateData());

                    call.enqueue(new Callback<ConRegModel.ContractorBankDetailsResponse>() {
                        @Override
                        public void onResponse(Call<ConRegModel.ContractorBankDetailsResponse> call, Response<ConRegModel.ContractorBankDetailsResponse> response) {
                            Global.dismissProgress(p);
                            if (response.isSuccessful()) {
                                //if(response.body().getResult().equalsIgnoreCase("Success"))
                                Toast.makeText(getActivity(), response.body().getDetails(), Toast.LENGTH_SHORT).show();
                                if (response.body().getResult().equalsIgnoreCase("success")) {
                                    saveBankDetails();
                                    Toast.makeText(getActivity(), getResources().getString(R.string.bank_details_added_go_for_bidding), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<ConRegModel.ContractorBankDetailsResponse> call, Throwable t) {
                            Global.dismissProgress(p);
                        }
                    });
                }

                break;
        }

    }

    private void saveBankDetails() {
        /*
         public static final String BANK_NAME = "BANK_NAME";
    public static final String ACCOUNT_HOLDER_NAME = "ACCOUNT_HOLDER_NAME";
    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String ROUTING = "ROUTING";
         */
        // if (contractorBankDetailsResponse.getResponse() != null)
        {
            PrefConnect.writeString(getActivity(), PrefConnect.BANK_NAME, edtBankName.getText().toString());
            PrefConnect.writeString(getActivity(), PrefConnect.ACCOUNT_HOLDER_NAME, edtUserAccountName.getText().toString());
            PrefConnect.writeString(getActivity(), PrefConnect.ACCOUNT_NUMBER, edtBankAccountNumber.getText().toString());
            PrefConnect.writeString(getActivity(), PrefConnect.ROUTING, edtRounting.getText().toString());
        }
    }

    private boolean isValidated() {

        boolean falge = true;
        if (TextUtils.isEmpty(edtBankName.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Enter Bank name", Toast.LENGTH_LONG).show();
            falge = false;
        } else if (TextUtils.isEmpty(edtUserAccountName.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Enter user account name", Toast.LENGTH_LONG).show();
            falge = false;
        } else if (TextUtils.isEmpty(edtBankAccountNumber.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Enter bank account number", Toast.LENGTH_LONG).show();
            falge = false;
        } else if (TextUtils.isEmpty(edtRounting.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Enter routing number", Toast.LENGTH_LONG).show();
            falge = false;
        }

        return falge;
    }

    @Override
    public void afterTextChanged(int view, Editable s) {
//edtBankName, edtBankAccountNumber, edtBankAccountName, edtRounting;
        // ImageView imgRounting, imgBankAccountNumber, imgBankAccountName, imgBankName;
        switch (view) {
            case R.id.edtBankName:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgBankName);
                else
                    setTickMark(imgBankName);
                break;

            case R.id.edtBankAccountNumber:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgBankAccountNumber);
                else
                    setTickMark(imgBankAccountNumber);
                break;

            case R.id.edtUserAccountName:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgBankAccountName);
                else
                    setTickMark(imgBankAccountName);
                break;

            case R.id.edtRounting:
                if (TextUtils.isEmpty(s.toString().trim()))
                    setPlusMark(imgRounting);
                else
                    setTickMark(imgRounting);
                break;
        }
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }

    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private ConBankInputModel populateData() {
        ConBankInputModel contractorBankDetailsInput = new ConBankInputModel();
        contractorBankDetailsInput.setContractor_id(USER_ID);
        contractorBankDetailsInput.setAccount_holder_name(edtUserAccountName.getText().toString());
        contractorBankDetailsInput.setBank_name(edtBankName.getText().toString());
        contractorBankDetailsInput.setAccount_number(edtBankAccountNumber.getText().toString());
        contractorBankDetailsInput.setRouting(edtRounting.getText().toString());


        return contractorBankDetailsInput;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((RegisterActivity) activity).onAttached(DrawRegisterFrag.CONTACT);
            MENU_RES = R.menu.menu_home;
        } catch (Exception e) {
            ((HomeActivity) activity).onAttached("Banking Details");
            MENU_RES = R.menu.menu_home_blue;
            e.printStackTrace();
        }
    }
}
