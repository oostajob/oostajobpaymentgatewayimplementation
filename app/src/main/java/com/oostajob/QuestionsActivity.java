package com.oostajob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oostajob.adapter.customer.QuestionsAdapter;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.dialog.FileAttach;
import com.oostajob.dialog.FileDialog;
import com.oostajob.dialog.ListDialogFragment;
import com.oostajob.dialog.WhatsNextDialog;
import com.oostajob.global.Global;
import com.oostajob.model.FileModel;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.JobSubTypeCharModel.CharValue;
import com.oostajob.model.JobSubTypeCharModel.JobsubtypecharDetails;
import com.oostajob.model.JobSubTypeModel;
import com.oostajob.model.PostJobModel;
import com.oostajob.utils.BlurTransformation;
import com.oostajob.utils.FilePath;
import com.oostajob.utils.PrefConnect;
import com.oostajob.video.TrimVideo;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class QuestionsActivity extends AppCompatActivity implements ListDialogFragment.SelectedListener, ImageListener,
        QuestionsAdapter.DialogOpenListener, FileAttach.SuccessListener {

    ListDialogFragment listFragment;
    FileAttach attach;
    private ProgressDialog p;
    private Toolbar toolbar;
    private ImageView imgBack;
    private LinearLayout notLay;
    private CustomButton btnPostJob;
    private String SUB_TITLE = "", IMG_URL = "", JOB_ID = "", JOB_SUB_ID = "", POSTAL_CODE = "", LAT = "0.0", LNG = "0.0", ADDRESS = "", USER_ID, USER_TYPE;
    private ActionBar.LayoutParams lp;
    private APIService apiService;
    private ListView listQuesView;
    private QuestionsAdapter adapter;
    private int[] imgDone = {R.drawable.add, R.drawable.tick};
    private ArrayList<JobsubtypecharDetails> list, stack_list, templist;
    private ArrayList<String> listTitle;
    private ArrayList<JobSubTypeModel.JobsubtypeDetails> jobSubList;
    private Uri fileUri;
    private CustomButton btnLogin, btnRegister;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        if (savedInstanceState != null) {
            fileUri = Uri.parse(savedInstanceState.getString("fileUri", ""));
        }

        getDefaultValues();
        init();
        setUpCustomToolbar();
        setListener();
        loadValues();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!Global.isUriEmpty(fileUri))
            outState.putString("fileUri", fileUri.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent homeIntent = new Intent(QuestionsActivity.this, HomeActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getDefaultValues() {
        SUB_TITLE = getIntent().getStringExtra("subTitle");
        IMG_URL = getIntent().getStringExtra("imgUrl");
        JOB_ID = getIntent().getStringExtra("jobTypeId");
        USER_ID = PrefConnect.readString(QuestionsActivity.this, PrefConnect.USER_ID, "");
        USER_TYPE = PrefConnect.readString(QuestionsActivity.this, PrefConnect.USER_TYPE, "");
        LAT = PrefConnect.readString(QuestionsActivity.this, PrefConnect.LAT, "0.0");
        LNG = PrefConnect.readString(QuestionsActivity.this, PrefConnect.LNG, "0.0");
        if (JOB_ID == null) {
            readPreferrence();
        }
    }

    private void init() {
        apiService = RetrofitSingleton.createService(APIService.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        listQuesView = (ListView) findViewById(R.id.listQuesView);
        btnPostJob = (CustomButton) findViewById(R.id.btnPostJob);
        notLay = (LinearLayout) findViewById(R.id.notLay);
        btnLogin = (CustomButton) findViewById(R.id.btnLogin);
        btnRegister = (CustomButton) findViewById(R.id.btnRegister);

        // Load Background Image
        Picasso.with(this).load(IMG_URL).transform(new BlurTransformation(this, 25)).fit().centerCrop().into(imgBack);

        //Params
        lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        p = new ProgressDialog(QuestionsActivity.this);
        p.setMessage(getString(R.string.loading));
        p.setIndeterminate(false);
        p.setCancelable(false);

        listFragment = new ListDialogFragment();
        listFragment.setListener(QuestionsActivity.this);

        if (TextUtils.isEmpty(USER_TYPE)) {
            notLay.setVisibility(View.VISIBLE);
            btnPostJob.setVisibility(View.GONE);
        } else {
            notLay.setVisibility(View.GONE);
            btnPostJob.setVisibility(View.VISIBLE);
        }

        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.do_you_want_to_include_this_pic));
        attach = new FileAttach();
        attach.setArguments(bundle);
        attach.setSuccessListener(QuestionsActivity.this);
    }

    private void setUpCustomToolbar() {
        if (toolbar != null) {
            toolbar.removeAllViews();
            View toolView = LayoutInflater.from(this).inflate(R.layout.toolbar_cus, toolbar, false);
            CustomTextView txtJobTitle = (CustomTextView) toolView.findViewById(R.id.txtJobTitle);
            txtJobTitle.setText(SUB_TITLE);
            txtJobTitle.setFontFace(getString(R.string.proxima_nova_semibold));
            toolbar.addView(toolView, lp);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black_trans1));
        }
    }

    private void setListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearPreferrence();
                Intent intent = new Intent(QuestionsActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("fragPosition", 0);
                startActivity(intent);
                finish();
            }
        });
        listQuesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Get Values
                JobsubtypecharDetails details = adapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString("title", listTitle.get(position));
                bundle.putString("selected", details.getChar_name());
                bundle.putInt("position", position);

                if (details.getQuestion_type().equalsIgnoreCase("Single option") && position == 1) {
                    bundle.putSerializable("list", jobSubList);
                    bundle.putInt("choiceMode", ListView.CHOICE_MODE_SINGLE);

                    listFragment.setArguments(bundle);
                    listFragment.show(getSupportFragmentManager(), "List Fragment");
                } else if (details.getQuestion_type().equalsIgnoreCase("Single option")) {

                    bundle.putSerializable("list", details.getChar_value());
                    bundle.putInt("choiceMode", ListView.CHOICE_MODE_SINGLE);

                    listFragment.setArguments(bundle);
                    listFragment.show(getSupportFragmentManager(), "List Fragment");

                } else if (details.getQuestion_type().equalsIgnoreCase("Multiple option")) {

                    bundle.putSerializable("list", details.getChar_value());
                    bundle.putInt("choiceMode", ListView.CHOICE_MODE_MULTIPLE);

                    listFragment.setArguments(bundle);
                    listFragment.show(getSupportFragmentManager(), "List Fragment");

                } else if (details.getQuestion_type().equalsIgnoreCase("Address")) {
                    Intent mapIntent = new Intent(QuestionsActivity.this, SelectAddrActivity.class);
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mapIntent.putExtra("title", "Select Job Address");
                    mapIntent.putExtra("goto", "home");
                    //mapIntent.putExtra("lat", Double.parseDouble(LAT));
                    //  mapIntent.putExtra("lng", Double.parseDouble(LNG));
                    startActivityForResult(mapIntent, Global.ADDRESS);
                } else if (details.getQuestion_type().equalsIgnoreCase("Text")) {
                    Intent txtIntent = new Intent(QuestionsActivity.this, TextActivity.class);
                    txtIntent.putExtra("title", listTitle.get(position));
                    txtIntent.putExtra("selected", details.getChar_name());
                    txtIntent.putExtra("position", position);
                    txtIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivityForResult(txtIntent, Global.TEXT);
                } else if (details.getQuestion_type().equalsIgnoreCase("Files")) {
                    openFileDialogFragement();
                }
            }
        });
        btnPostJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated())
                    new PostTask().execute();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writePreferrence();
                Intent intent = new Intent(QuestionsActivity.this, LoginActivity.class);
                intent.putExtra("list", list);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writePreferrence();
                Intent intent = new Intent(QuestionsActivity.this, SelectionActivity.class);
                intent.putExtra("list", list);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void writePreferrence() {
        gson = new Gson();
        String json = gson.toJson(list);
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.LIST, json);
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.SKIP, "skip_login");
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.SUB_TITLE, SUB_TITLE);
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.IMG_URL, IMG_URL);
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.JOB_ID, JOB_ID);
    }

    private void readPreferrence() {
        gson = new Gson();
        String json = PrefConnect.readString(QuestionsActivity.this, PrefConnect.LIST, "");
        Type type = new TypeToken<ArrayList<JobsubtypecharDetails>>() {
        }.getType();
        templist = gson.fromJson(json, type);
        SUB_TITLE = PrefConnect.readString(QuestionsActivity.this, PrefConnect.SUB_TITLE, "");
        IMG_URL = PrefConnect.readString(QuestionsActivity.this, PrefConnect.IMG_URL, "");
        JOB_ID = PrefConnect.readString(QuestionsActivity.this, PrefConnect.JOB_ID, "");
    }

    private void clearPreferrence() {
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.SKIP, "");
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.SUB_TITLE, "");
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.IMG_URL, "");
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.JOB_ID, "");
        PrefConnect.writeString(QuestionsActivity.this, PrefConnect.LIST, "");
    }

    private boolean isValidated() {
        for (JobsubtypecharDetails details : list) {
            if (details.getImgDone() == imgDone[0]) {
                Toast.makeText(QuestionsActivity.this, details.getChar_name(), Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    private void openFileDialogFragement() {
        FileDialog dialog = new FileDialog();
        dialog.setFileListener(QuestionsActivity.this);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                dialog.show(getSupportFragmentManager(), "File");
            } else {
                Toast.makeText(this, "Enable Camera Permission in Settings -> Apps -> OOstaJob -> Permissions.", Toast.LENGTH_LONG).show();
            }
        } else {
            dialog.show(getSupportFragmentManager(), "File");
        }
    }

    private void loadValues() {
        jobSubList = new ArrayList<>();
        p.show();
        Call<JobSubTypeModel.Response> call = apiService.getJobSubType(JOB_ID);
        call.enqueue(new Callback<JobSubTypeModel.Response>() {
            @Override
            public void onResponse(Call<JobSubTypeModel.Response> call, Response<JobSubTypeModel.Response> response) {
                if (response.isSuccessful()) {
                    JobSubTypeModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        jobSubList = response1.getJobsubtypeDetails();
                        JOB_SUB_ID = jobSubList.get(0).getJobsubtype_id();
                        getQuestionsList(JOB_SUB_ID, jobSubList.get(0).getSubtype_name());
                    }
                }
            }

            @Override
            public void onFailure(Call<JobSubTypeModel.Response> call, Throwable t) {

            }
        });
    }

    private void getQuestionsList(String subTypeId, final String subTypeName) {
        final JobsubtypecharDetails fileDet;
        if (list != null && !list.isEmpty()) {
            fileDet = list.get(0);
        } else {
            fileDet = new JobsubtypecharDetails("0", "0", "0", "Files", "0", "Include some pictures and videos of the job",
                    new ArrayList<CharValue>(), imgDone[0], new ArrayList<FileModel>(), "");
        }

        list = new ArrayList<>();
        listTitle = new ArrayList<>();
        if (!p.isShowing()) p.show();
        Call<JobSubTypeCharModel.Response> call = apiService.getJobSubTypeChar(subTypeId);
        call.enqueue(new Callback<JobSubTypeCharModel.Response>() {
            @Override
            public void onResponse(Call<JobSubTypeCharModel.Response> call, Response<JobSubTypeCharModel.Response> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    JobSubTypeCharModel.Response response1 = response.body();
                    if (response1.getResult().equalsIgnoreCase("Success")) {
                        list = response1.getJobsubtypecharDetails();
                        adapter = new QuestionsAdapter(QuestionsActivity.this, list);
                        adapter.setDialogOpenListener(QuestionsActivity.this);
                        listQuesView.setAdapter(adapter);
                        editList(subTypeName, fileDet);
                    }
                }

            }

            @Override
            public void onFailure(Call<JobSubTypeCharModel.Response> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void editList(String subTypeName, JobsubtypecharDetails fileDet) {
        JobsubtypecharDetails detail = fileDet;
        list.add(0, detail);
        detail = new JobsubtypecharDetails("0", "0", "0", "Single Option", "0", "Select a best match for your needs",
                new ArrayList<CharValue>(), imgDone[1], new ArrayList<FileModel>(), "");
        list.add(1, detail);
        detail = new JobsubtypecharDetails("0", "0", "0", "Address", "0", "Select Job Address",
                new ArrayList<CharValue>(), imgDone[0], new ArrayList<FileModel>(), "");
        list.add(2, detail);
    /*    detail = new JobsubtypecharDetails("0", "0", "0", "text", "0", "Write down some notes about the job description, specifiation, size, etc.",
                new ArrayList<CharValue>(), imgDone[0], new ArrayList<FileModel>(), "");
        list.add(list.size(), detail);*/

        for (int i = 0; i < list.size(); i++) {
            JobsubtypecharDetails details = list.get(i);
            if (details.getImgDone() == 0) {
                details.setImgDone(imgDone[0]);
            }
            list.set(i, details);
        }
        for (JobSubTypeCharModel.JobsubtypecharDetails details : list) {
            listTitle.add(details.getChar_name());
        }

        detail = new JobsubtypecharDetails("0", "0", "0", "Single Option", "0", subTypeName,
                new ArrayList<CharValue>(), imgDone[1], new ArrayList<FileModel>(), "");
        list.set(1, detail);

        adapter.notifyDataSetChanged();
        if (PrefConnect.readString(QuestionsActivity.this, PrefConnect.SKIP, "").equalsIgnoreCase("skip_login")) {
            stack_list = new ArrayList<>();
            stack_list = (ArrayList<JobsubtypecharDetails>) getIntent().getSerializableExtra("list");
            if (stack_list != null) {
                loadingDataSet(stack_list);
            } else {
                stack_list = templist;
                loadingDataSet(stack_list);
            }

            PrefConnect.writeString(QuestionsActivity.this, PrefConnect.SKIP, "");
        }
    }

    private void loadingDataSet(ArrayList<JobsubtypecharDetails> stack_list) {
        if (stack_list == null || stack_list.size() <= 0) return;
        list.get(0).setFileList(stack_list.get(0).fileList);
        list.get(0).setImgDone(stack_list.get(0).imgDone);
        list.get(2).setChar_name(stack_list.get(2).char_name);
        list.get(2).setImgDone(stack_list.get(2).imgDone);
        list.get(list.size() - 1).setChar_name(stack_list.get(stack_list.size() - 1).char_name);
        list.get(list.size() - 1).setImgDone(stack_list.get(stack_list.size() - 1).imgDone);
        for (int i = 3; i < stack_list.size(); i++) {
            list.get(i).setChar_name(stack_list.get(i).char_name);
            list.get(i).setImgDone(stack_list.get(i).imgDone);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inDither = true;
        JobsubtypecharDetails details;
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Global.ADDRESS:
                    ADDRESS = data.getStringExtra("address");
                    POSTAL_CODE = data.getStringExtra("postalCode");
                    LAT = data.getStringExtra("lat");
                    LNG = data.getStringExtra("lng");
                    PrefConnect.writeString(QuestionsActivity.this, PrefConnect.JOB_POSTAL, POSTAL_CODE);

                    details = adapter.getItem(2);
                    details.setChar_name(ADDRESS);
                    details.setImgDone(imgDone[1]);
                    list.set(2, details);
                    adapter.notifyDataSetChanged();
                    break;
                case Global.TEXT:
                    if (!TextUtils.isEmpty(data.getStringExtra("selected"))) {
                        int position = data.getIntExtra("position", -1);
                        details = adapter.getItem(position);
                        details.setChar_name(data.getStringExtra("selected"));
                        details.setImgDone(imgDone[1]);
                        list.set(position, details);
                        adapter.notifyDataSetChanged();
                    } else {
                        int position = data.getIntExtra("position", -1);
                        details = adapter.getItem(position);
                        details.setChar_name(listTitle.get(position));
                        details.setImgDone(imgDone[0]);
                        list.set(position, details);
                        adapter.notifyDataSetChanged();
                    }
                    break;
                case Global.CAMERA_CAPTURE:
                    try {
                        Bundle extras = data.getExtras();
                        fileUri = Global.getImageUri(this.getApplicationContext(), (Bitmap) extras.getParcelable("data"));
                        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                        Global.saveImage(QuestionsActivity.this, bitmap, fileUri);
//                        addFiles("1", FilePath.getPath(getApplicationContext(), fileUri));
                        attach.show(getSupportFragmentManager(), "File Attach");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK:
                    try {
                        Uri selectedImageUri = data.getData();
//                        String type = Global.getMimeType(FilePath.getPath(getApplicationContext(), selectedImageUri));
                        if (!Global.isUriEmpty(selectedImageUri)) {
                            String type = getContentResolver().getType(selectedImageUri);
                            if (!TextUtils.isEmpty(type) && type.contains("image")) {
                                fileUri = Uri.fromFile(Global.getPhotoDirectory());
                                Bitmap bitmap = BitmapFactory.decodeFile(FilePath.getPath(getApplicationContext(), selectedImageUri)
                                        , options);
                                Global.saveImage(QuestionsActivity.this, bitmap, fileUri);
                                addFiles("1", FilePath.getPath(getApplicationContext(), fileUri));
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK_VIDEO:
                    try {
                        Uri selectedImageUri = data.getData();
//                        String type = Global.getMimeType(FilePath.getPath(getApplicationContext(), selectedImageUri));
                        if (!Global.isUriEmpty(selectedImageUri)) {
                            String type = getContentResolver().getType(selectedImageUri);
                            if (!TextUtils.isEmpty(type) && type.contains("video")) {

                                fileUri = Uri.fromFile(Global.getVideoDirectory());
                                new TrimTask().execute(selectedImageUri, fileUri);

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.VIDEO_CAPTURE:
                    fileUri = data.getData();
                    addFiles("2", FilePath.getPath(getApplicationContext(), fileUri));
                    break;
            }
        }
    }

    private void addFiles(String type, String filePath) {
        File file = new File(filePath);
        int position = 0;
        JobsubtypecharDetails details = adapter.getItem(position);
        ArrayList<FileModel> fileList = details.getFileList();
        if (fileList.size() < 25) {
            if (type.equals("1")) {
                if (getFileCount(type, fileList) < 20) {
                    FileModel model = new FileModel(type, file);
                    fileList.add(model);
                    details.setFileList(fileList);
                    details.setImgDone(imgDone[1]);
                    list.set(position, details);
                    adapter.notifyDataSetChanged();
                } else
                    Toast.makeText(QuestionsActivity.this, "The Photo count is limited to 20", Toast.LENGTH_LONG).show();
            } else if (type.equals("2")) {
                if (getFileCount(type, fileList) < 5) {
                    FileModel model = new FileModel(type, file);
                    fileList.add(model);
                    details.setFileList(fileList);
                    details.setImgDone(imgDone[1]);
                    list.set(position, details);
                    adapter.notifyDataSetChanged();
                } else
                    Toast.makeText(QuestionsActivity.this, "The Video count is limited to 5", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(QuestionsActivity.this, "Attachment limit Reached", Toast.LENGTH_LONG).show();
        }
    }

    private int getFileCount(String type, ArrayList<FileModel> fileList) {
        int i = 0;
        for (FileModel model : fileList) {
            if (model.getMediaType().equals(type)) {
                i = i + 1;
            }
        }
        return i;
    }

    @Override
    public void onSelected(int position, String selectedValue, String id) {
        if (position == 1) {
            JobsubtypecharDetails detail = adapter.getItem(position);
            if (!detail.getChar_name().equals(selectedValue)) {
                getQuestionsList(id, selectedValue);
            }
        } else {
            if (!TextUtils.isEmpty(selectedValue)) {
                JobsubtypecharDetails details = adapter.getItem(position);
                details.setChar_name(selectedValue);
                details.setImgDone(imgDone[1]);
                if (selectedValue.contains("Other:") || selectedValue.contains("Others:")) {
                    String[] arr = selectedValue.split(",");
                    ArrayList<CharValue> charValues = details.getChar_value();
                    CharValue cv = charValues.get(charValues.size() - 1);
                    cv.setOption(arr[arr.length - 1].trim());
                    charValues.set(charValues.size() - 1, cv);
                    details.setChar_value(charValues);
                    list.set(position, details);
                } else {
                    list.set(position, details);
                }
                adapter.notifyDataSetChanged();
            } else {
                JobsubtypecharDetails details = adapter.getItem(position);
                details.setChar_name(listTitle.get(position));
                details.setImgDone(imgDone[0]);
                list.set(position, details);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onImageSelected(Uri uri, boolean isLicense) {
        fileUri = uri;
    }

    @Override
    public void openFileDialog() {
        openFileDialogFragement();
    }

    @Override
    public void removeImage(FileModel model) {
        int pos = 0;
        JobsubtypecharDetails details = adapter.getItem(pos);
        ArrayList<FileModel> fileList = details.getFileList();
        fileList.remove(model);
        details.setFileList(fileList);
        if (fileList.size() > 0) {
            details.setImgDone(imgDone[1]);
        } else {
            details.setImgDone(imgDone[0]);
        }
        list.set(pos, details);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void attach() {
        addFiles("1", FilePath.getPath(getApplicationContext(), fileUri));
    }

    public class TrimTask extends AsyncTask<Uri, Void, String> {

        String fromPath, toPath;
        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            progressDialog = Global.initProgress(QuestionsActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Uri... params) {
            fromPath = FilePath.getPath(QuestionsActivity.this, params[0]);
            toPath = FilePath.getPath(QuestionsActivity.this, params[1]);
            try {
                TrimVideo.trimVideo(fromPath, toPath);
                return "1";
            } catch (IOException e) {
                e.printStackTrace();
            } catch (UnsupportedOperationException e) {
                e.printStackTrace();
                return "2";
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Global.dismissProgress(progressDialog);
            if (!TextUtils.isEmpty(s)) {
                if (s.equals("1")) {
                    addFiles("2", toPath);
                } else if (s.equals("2")) {
                    Toast.makeText(QuestionsActivity.this, "Select file size less than 100MB", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    public class PostTask extends AsyncTask<String, Void, String> {
        HttpEntity resEntity;
        String url = Global.BASE_URL + "/api/index.php/postjobimgupload";

        @Override
        protected void onPreExecute() {
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                MultipartEntity reqEntity = getMultiPart();
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                final String response_str = EntityUtils.toString(resEntity);
                if (resEntity != null) {
                    Log.i("RESPONSE", response_str);
                    return response_str;
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {
                try {
                    String result = new JSONObject(s).getString("Result");
                    if (result.equalsIgnoreCase("success")) {
                        postData(p);
                    } else {
                        Global.dismissProgress(p);
                        Toast.makeText(getApplicationContext(), "File Upload Failed, Try again", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Global.dismissProgress(p);
                Toast.makeText(getApplicationContext(), "File Upload Failed, Try again", Toast.LENGTH_LONG).show();
            }
        }

        public MultipartEntity getMultiPart() {
            MultipartEntity reqEntity = new MultipartEntity();
            JobsubtypecharDetails details = adapter.getItem(0);
            ArrayList<FileModel> fileList = details.getFileList();
            for (int i = 1; i <= fileList.size(); i++) {
                FileBody fileBody = new FileBody(fileList.get(i - 1).getFile());
                String key = String.format("postjob%s", i);
                reqEntity.addPart(key, fileBody);
            }
            return reqEntity;
        }

        private void postData(final ProgressDialog p) {
            if (ADDRESS.equals("") || POSTAL_CODE.equals("")) {
                ADDRESS = list.get(2).char_name;
                POSTAL_CODE = PrefConnect.readString(QuestionsActivity.this, PrefConnect.JOB_POSTAL, "");
            }
            // Prepare Media List
            ArrayList<PostJobModel.Media> mediaList = new ArrayList<>();
            ArrayList<FileModel> fileModels = adapter.getItem(0).getFileList();
            for (FileModel f : fileModels) {
                PostJobModel.Media media = new PostJobModel.Media(f.getMediaType(), f.getFile().getName());
                mediaList.add(media);
            }

            // Prepare Answer Key
            ArrayList<PostJobModel.AnswerKey> answerList = new ArrayList<>();
            for (int i = 3; i < list.size(); i++) {
                PostJobModel.AnswerKey answerKey = new PostJobModel.AnswerKey(listTitle.get(i), list.get(i).getChar_name(),
                        list.get(i).getTag_status(), list.get(i).getTag_keyword());
                answerList.add(answerKey);
            }

            // Prepare For Post
            //String description = adapter.getItem(adapter.getCount() - 1).getChar_name();
            final PostJobModel.Request request = new PostJobModel.Request(USER_ID, JOB_ID, JOB_SUB_ID, ADDRESS,
                    POSTAL_CODE, LAT, LNG, "", mediaList, answerList);
            Call<PostJobModel.Response> call = apiService.postJob(request);

            call.enqueue(new Callback<PostJobModel.Response>() {
                @Override
                public void onResponse(Call<PostJobModel.Response> call, Response<PostJobModel.Response> response) {
                    Global.dismissProgress(p);
                    if (response.isSuccessful()) {
                        PostJobModel.Response response1 = response.body();
                        if (response1.getResult().equalsIgnoreCase("Success")) {
                            PrefConnect.writeString(QuestionsActivity.this, PrefConnect.BRAIN_TREE_CUSTOMER_ID, response1.getBraintreeID());
                            clearPreferrence();
                            WhatsNextDialog dialog = new WhatsNextDialog();
                            dialog.setCancelable(false);
                            dialog.show(getSupportFragmentManager(), "Whats Next");
                        }
                    }
                }

                @Override
                public void onFailure(Call<PostJobModel.Response> call, Throwable t) {
                    Global.dismissProgress(p);
                    Toast.makeText(QuestionsActivity.this, "Server Error. Try Again...", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
