package com.oostajob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.callbacks.ImageListener;
import com.oostajob.dialog.ExpertiseDialog;
import com.oostajob.dialog.PasswordChangeFragment;
import com.oostajob.dialog.PhotoDialog;
import com.oostajob.global.Global;
import com.oostajob.model.ConUpdateModel;
import com.oostajob.model.JobTypeModel;
import com.oostajob.model.TimeZone;
import com.oostajob.utils.FilePath;
import com.oostajob.utils.PrefConnect;
import com.oostajob.watcher.AutoTextWatcher;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConUpdateActivity extends AppCompatActivity implements AutoTextWatcher.TextWatcher, ExpertiseDialog.ExpertiseListener,
        ImageListener {

    CustomButton btnUpdate;
    ProgressDialog p;
    private Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
    private Toolbar toolbar;
    private CustomTextView edtlicence, edtExpAt, edtAddress;
    private CustomEditText edtName, edtEmail, edtPhone, edtHourRate;
    private ImageView imgProfile, img_licence_gal, name_add, email_add, password_add, address_add, phone_add, img_hourrate, img_licence, img_ExpAt;
    private APIService apiService, googleService;
    private Uri selectedPicUri, selectedLicPicUri;
    private Uri croppedPicUri, croppedLicPicUri;
    private RelativeLayout licence_layout;
    private String expertiseIDs;
    private String USER_ID, NAME, EMAIL, PHONE, ADDRESS, PASSWORD, PROFILE, ZIP_CODE, LAT = "0.0", LNG = "0.0", EXPERTISE, /*HOURLY_RATE,*/
            LICENSE;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_update);

        apiService = RetrofitSingleton.createService(APIService.class);
        googleService = RetrofitSingleton.createGoogleService(APIService.class);
        if (savedInstanceState != null) {
            selectedPicUri = Uri.parse(savedInstanceState.getString("selectedPicUri", ""));
            selectedLicPicUri = Uri.parse(savedInstanceState.getString("selectedLicPicUri", ""));
            croppedPicUri = Uri.parse(savedInstanceState.getString("croppedPicUri", ""));
            croppedLicPicUri = Uri.parse(savedInstanceState.getString("croppedLicPicUri", ""));
        }
        init();
        setUpToolBar();
        setListener();

        loadPrefValues();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edtName = (CustomEditText) findViewById(R.id.edt_contractor_name);
        edtAddress = (CustomTextView) findViewById(R.id.edt_contractor_address);
        // edtSsn = (CustomEditText) findViewById(R.id.edt_contractor_ssn);
        edtEmail = (CustomEditText) findViewById(R.id.edt_contractor_email);
        edtPhone = (CustomEditText) findViewById(R.id.edt_contractor_phone);
        edtExpAt = (CustomTextView) findViewById(R.id.edt_expertise);
        edtHourRate = (CustomEditText) findViewById(R.id.edt_contractor_hourlybase);
        edtlicence = (CustomTextView) findViewById(R.id.edt_contractor_license);
        name_add = (ImageView) findViewById(R.id.name_add);
        email_add = (ImageView) findViewById(R.id.email_add);
        password_add = (ImageView) findViewById(R.id.password_add);
        address_add = (ImageView) findViewById(R.id.address_add);
        phone_add = (ImageView) findViewById(R.id.phone_add);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        img_licence_gal = (ImageView) findViewById(R.id.img_licence_gallery);
        // img_ssn = (ImageView) findViewById(R.id.img_ssn);
        img_hourrate = (ImageView) findViewById(R.id.img_hourrate);
        img_licence = (ImageView) findViewById(R.id.img_licence);
        img_ExpAt = (ImageView) findViewById(R.id.img_ExpAt);
        licence_layout = (RelativeLayout) findViewById(R.id.licence_layout);
        btnUpdate = (CustomButton) findViewById(R.id.btnUpdate);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        p = new ProgressDialog(ConUpdateActivity.this);
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.setMessage(getString(R.string.loading));

        if (!Global.isUriEmpty(croppedPicUri)) {
            imgProfile.setImageURI(croppedPicUri);
        }
        if (!Global.isUriEmpty(croppedLicPicUri)) {
            img_licence_gal.setImageURI(croppedLicPicUri);
        }

        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
    }

    private void loadPrefValues() {
        USER_ID = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.USER_ID, "");
        EMAIL = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.EMAIL, "");
        PASSWORD = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.EMAIL, "");
        NAME = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.NAME, "");
        ADDRESS = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.ADDRESS, "");
        PHONE = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.PHONE, "");
        ZIP_CODE = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.ZIPCODE, "");
        LAT = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.LAT, "");
        LNG = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.LNG, "");
        //SSN = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.SSN, "");
        EXPERTISE = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.EXPERTISE_AT, "");
        //HOURLY_RATE = PrefConnect.readString(ConUpdateActivity.this, PrefConnect.HOURLY_RATE, "");

        croppedPicUri = Uri.parse(PrefConnect.readString(ConUpdateActivity.this, PrefConnect.PROFILE_PHOTO, ""));
        PROFILE = croppedPicUri.getLastPathSegment();
        croppedLicPicUri = Uri.parse(PrefConnect.readString(ConUpdateActivity.this, PrefConnect.LICENSE, ""));
        LICENSE = croppedLicPicUri.getLastPathSegment();

        edtName.setText(NAME);
        edtEmail.setText(EMAIL);
        edtAddress.setText(ADDRESS);
        edtPhone.setText(PHONE);
        //edtSsn.setText(SSN);
        //edtHourRate.setText(HOURLY_RATE);

        if (!Global.isUriEmpty(croppedPicUri)) {
            String pUrl = String.format("%s/%s", Global.BASE_URL, croppedPicUri.toString());
            Picasso.with(this).load(pUrl)/*.placeholder(R.drawable.user)*/.fit().into(imgProfile);// removing placeholder , since there is already a placeholder image
        }
        if (!Global.isUriEmpty(croppedLicPicUri)) {
            String lUrl = String.format("%s/%s", Global.BASE_URL, croppedLicPicUri.toString());
            Picasso.with(this).load(lUrl).fit().into(img_licence_gal);
            img_licence_gal.setVisibility(View.VISIBLE);
            edtlicence.setVisibility(View.GONE);
            setTickMark(img_licence);
        }
        setTickMark(address_add);
        setExpertise();
    }

    private void setExpertise() {
        p.show();
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    JobTypeModel jobTypeModel = response.body();
                    ArrayList<String> expertiseList = new ArrayList<>();
                    if (jobTypeModel.getResult().equalsIgnoreCase("success")) {
                        ArrayList<JobTypeModel.JobtypeDetails> jobTypesList = jobTypeModel.getJobtypeDetails();
                        String[] arr = EXPERTISE.split(",");
                        for (String anArr : arr) {
                            for (int j = 0; j < jobTypesList.size(); j++) {
                                if (jobTypesList.get(j).getJobtype_id().equals(anArr.trim())) {
                                    expertiseList.add(jobTypesList.get(j).getJob_name());
                                }
                            }
                        }
                        String tempVar = expertiseList.toString().replace("[", "").replace("]", "");
                        edtExpAt.setText(tempVar);
                        setTickMark(img_ExpAt);
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    private void setUpToolBar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setLogo(R.drawable.ic_logo);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_password, menu);
        CustomTextView textView = (CustomTextView) menu.findItem(R.id.action_password).getActionView();
        textView.setText(R.string.change_password);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordChangeFragment fragment = new PasswordChangeFragment();
                fragment.show(getSupportFragmentManager(), "Change Password");
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_password:
                PasswordChangeFragment fragment = new PasswordChangeFragment();
                fragment.show(getSupportFragmentManager(), "Change Password");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(ConUpdateActivity.this, SelectAddrActivity.class);
                mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mapIntent.putExtra("title", "Select Address");
                mapIntent.putExtra("goto", "");
                mapIntent.putExtra("lat", Double.parseDouble(LAT));
                mapIntent.putExtra("lng", Double.parseDouble(LNG));
                startActivityForResult(mapIntent, Global.ADDRESS);
            }
        });


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidated()) {
                    new UpdateTask().execute();
                }
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString("title", "Picture");
                args.putBoolean("isLicence", false);
                // Since profile image is mandatory , commenting remove functionality
                /*if (!Global.isUriEmpty(croppedPicUri) || !TextUtils.isEmpty(PROFILE)) {
                    args.putBoolean("isRemove", true);
                }*/
                photoDialog.setArguments(args);
                photoDialog.setCancelable(false);
                photoDialog.setImageListener(ConUpdateActivity.this);
                photoDialog.show(getSupportFragmentManager(), "Photo Dialog");
            }
        });
        licence_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoDialog photoDialog = new PhotoDialog();
                Bundle args = new Bundle();
                args.putString("title", "Picture");
                args.putBoolean("isLicence", true);
                //  Since License image is mandatory , commenting remove functionality
                /*if (!Global.isUriEmpty(croppedLicPicUri) || !TextUtils.isEmpty(LICENSE)) {
                    args.putBoolean("isRemove", true);
                }*/
                photoDialog.setArguments(args);
                photoDialog.setCancelable(false);
                photoDialog.setImageListener(ConUpdateActivity.this);
                photoDialog.show(getSupportFragmentManager(), "lic_Photo Dialog");
            }
        });
        /*edtExpAt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("select", edtExpAt.getText().toString());
                ExpertiseDialog expertiseDialog = new ExpertiseDialog();
                expertiseDialog.setCancelable(false);
                expertiseDialog.setOnDone(ConUpdateActivity.this);
                expertiseDialog.setArguments(bundle);
                expertiseDialog.show(getSupportFragmentManager(), "ExpertiseAT Dialog");
            }
        });*/
        edtName.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_name, this));
        edtEmail.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_email, this));
        edtPhone.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_phone, this));
        //edtSsn.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_ssn, this));
        //edtHourRate.addTextChangedListener(new AutoTextWatcher(R.id.edt_contractor_hourlybase, this));
    }


    private void setTickMark(ImageView view) {
        view.setImageResource(R.drawable.tick);
    }

    private void setPlusMark(ImageView view) {
        view.setImageDrawable(null);
    }

    private boolean isValidated() {
        if (TextUtils.isEmpty(edtName.getText().toString())) {
            Toast.makeText(ConUpdateActivity.this, "Enter name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            Toast.makeText(ConUpdateActivity.this, "Enter email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtEmail.getText().toString())) {
            Pattern pattern = Pattern.compile(Global.EMAIL_EXPRESSION, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtEmail.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(ConUpdateActivity.this, "Enter valid Email", Toast.LENGTH_LONG).show();
                return false;
            }
        }
       /* if (TextUtils.isEmpty(edtSsn.getText().toString())) {
            Toast.makeText(ConUpdateActivity.this, "Enter SSN", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtSsn.getText().toString())) {
            Pattern pattern = Pattern.compile("^([0-9]{9})$", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(edtSsn.getText().toString());
            if (!matcher.matches()) {
                Toast.makeText(ConUpdateActivity.this, "Enter valid SSN", Toast.LENGTH_LONG).show();
                return false;
            }
        }*/
        if (edtAddress.getText().toString().equalsIgnoreCase("Address")) {
            Toast.makeText(ConUpdateActivity.this, "Select Address", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            Toast.makeText(ConUpdateActivity.this, "Enter Phone Number", Toast.LENGTH_LONG).show();
            return false;
        } else if (!TextUtils.isEmpty(edtPhone.getText().toString().trim())) {
            long phone = Long.parseLong(edtPhone.getText().toString().trim());
            if (phone == 0 || edtPhone.getText().toString().trim().length() != 10) {
                Toast.makeText(ConUpdateActivity.this, "Enter Valid Phone number", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (edtExpAt.getText().toString().equalsIgnoreCase("Expertise At")) {
            Toast.makeText(ConUpdateActivity.this, "Select Expertise", Toast.LENGTH_LONG).show();
            return false;
        }
        /*if (TextUtils.isEmpty(edtHourRate.getText().toString())) {
            Toast.makeText(ConUpdateActivity.this, "Enter Hourly Rates", Toast.LENGTH_LONG).show();
            return false;
        }*/

        NAME = edtName.getText().toString();
        EMAIL = edtEmail.getText().toString();
        ADDRESS = edtAddress.getText().toString();
        PHONE = edtPhone.getText().toString();
        // SSN = edtSsn.getText().toString();
        //HOURLY_RATE = edtHourRate.getText().toString();

        return true;
    }

    private void gotoHome() {
        Intent intent = new Intent(ConUpdateActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void afterTextChanged(int view, Editable s) {
        switch (view) {
            case R.id.edt_contractor_name:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(name_add);
                else
                    setTickMark(name_add);
                break;
            case R.id.edt_contractor_email:
                Matcher matcher = pattern.matcher(s.toString());
                if (TextUtils.isEmpty(s.toString()) || !matcher.matches())
                    setPlusMark(email_add);
                else
                    setTickMark(email_add);
                break;
            case R.id.edt_contractor_password:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(password_add);
                else
                    setTickMark(password_add);
                break;
            case R.id.edt_contractor_phone:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(phone_add);
                else
                    setTickMark(phone_add);
                break;
           /* case R.id.edt_contractor_ssn:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(img_ssn);
                else
                    setTickMark(img_ssn);
                break;*/
            case R.id.edt_contractor_hourlybase:
                if (TextUtils.isEmpty(s.toString()))
                    setPlusMark(img_hourrate);
                else
                    setTickMark(img_hourrate);
                break;
        }
    }

    @Override
    public void onDone(String value, String ids) {
        edtExpAt.setText(value);
        EXPERTISE = ids;
        expertiseIDs = ids;
        if (value.equalsIgnoreCase(getString(R.string.expertise))) {
            setPlusMark(img_ExpAt);
        } else {
            setTickMark(img_ExpAt);

        }
    }

    @Override
    public void onImageSelected(Uri uri, boolean isLicense) {
        try {
            if (isLicense) {
                selectedLicPicUri = uri;
                if (Global.isUriEmpty(selectedLicPicUri)) {
                    img_licence_gal.setVisibility(View.GONE);
                    edtlicence.setVisibility(View.VISIBLE);
                    setPlusMark(img_licence);
                    LICENSE = "";
                    croppedLicPicUri = null;
                }
            } else {
                selectedPicUri = uri;
                if (Global.isUriEmpty(selectedPicUri)) {
                    imgProfile.setImageDrawable(null);
                    PROFILE = "";
                    croppedPicUri = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    try {
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();
                        Bundle extras = data.getExtras();
                        selectedPicUri = Global.getImageUri(this.getApplicationContext(), (Bitmap) extras.getParcelable("data"));
                        Global.doCrop(ConUpdateActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK:
                    try {
                        selectedPicUri = data.getData();
                        croppedPicUri = Global.generateTimeStampPhotoFileUri();
                        Global.doCrop(ConUpdateActivity.this, selectedPicUri, croppedPicUri, Global.PICTURE_CROP);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.PICTURE_CROP:
                    if (!Global.isUriEmpty(croppedPicUri)) {
                        imgProfile.setImageURI(croppedPicUri);
                    } else {
                        Bundle extras = data.getExtras();
                        Bitmap thePic = extras.getParcelable("data");
                        imgProfile.setImageBitmap(thePic);
                        Global.saveImage(ConUpdateActivity.this, thePic, croppedPicUri);
                    }
                    PROFILE = new File(FilePath.getPath(ConUpdateActivity.this, croppedPicUri)).getName();

                    break;
                // =====lic pic
                case Global.CAMERA_CAPTURE_lICENCE:
                    try {
                        croppedLicPicUri = Global.generateTimeStampPhotoFileUri();
                        Global.doCrop(ConUpdateActivity.this, selectedLicPicUri, croppedLicPicUri, Global.PICTURE_CROP_lICENCE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.GALLERY_PICK_lICENCE:
                    try {
                        selectedLicPicUri = data.getData();
                        croppedLicPicUri = Global.generateTimeStampPhotoFileUri();
                        Global.doCrop(ConUpdateActivity.this, selectedLicPicUri, croppedLicPicUri, Global.PICTURE_CROP_lICENCE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Global.PICTURE_CROP_lICENCE:
                    if (!Global.isUriEmpty(croppedLicPicUri)) {
                        img_licence_gal.setImageURI(croppedLicPicUri);
                    } else {
                        Bundle lic_extras = data.getExtras();
                        Bitmap lic_thePic = lic_extras.getParcelable("data");
                        img_licence_gal.setImageBitmap(lic_thePic);
                        Global.saveImage(ConUpdateActivity.this, lic_thePic, croppedLicPicUri);
                    }
                    LICENSE = new File(FilePath.getPath(ConUpdateActivity.this, croppedLicPicUri)).getName();
                    img_licence_gal.setVisibility(View.VISIBLE);
                    edtlicence.setVisibility(View.GONE);
                    setTickMark(img_licence);
                    break;

                case Global.ADDRESS:
                    ADDRESS = data.getStringExtra("address");
                    ZIP_CODE = data.getStringExtra("postalCode");
                    LAT = data.getStringExtra("lat");
                    LNG = data.getStringExtra("lng");
                    edtAddress.setText(ADDRESS);
                    setTickMark(address_add);
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case Global.CAMERA_CAPTURE:
                    if (!Global.isUriEmpty(selectedPicUri)) {
                        //Global.deleteFile(getApplicationContext(), selectedPicUri);
                        selectedPicUri = null;
                    }
                    croppedPicUri = null;
                    break;
                case Global.PICTURE_CROP:
                    croppedPicUri = null;
                    selectedPicUri = null;
                    break;
                case Global.PICTURE_CROP_lICENCE:
                    croppedLicPicUri = null;
                    selectedLicPicUri = null;
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            //handleCropError(data);
            Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
            if (requestCode == Global.PICTURE_CROP) {
                croppedPicUri = null;
                PROFILE = "";
            } else if (requestCode == Global.PICTURE_CROP_lICENCE) {
                croppedLicPicUri = null;
                LICENSE = "";
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (!Global.isUriEmpty(selectedPicUri))
            outState.putString("selectedPicUri", selectedPicUri.toString());
        if (!Global.isUriEmpty(selectedLicPicUri))
            outState.putString("selectedLicPicUri", selectedLicPicUri.toString());
        if (!Global.isUriEmpty(croppedPicUri))
            outState.putString("croppedPicUri", croppedPicUri.toString());
        if (!Global.isUriEmpty(croppedLicPicUri))
            outState.putString("croppedLicPicUri", croppedLicPicUri.toString());
        super.onSaveInstanceState(outState);
    }


    private void saveData(ConUpdateModel.ContractorDetails details) {
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.EMAIL, details.getEmail());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.PASSWORD, details.getPassword());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.USER_TYPE, details.getUserType());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.NAME, details.getName());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.ADDRESS, details.getAddress());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.PHONE, details.getPhone());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.PROFILE_PHOTO, details.getProfilePhoto());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.ZIPCODE, details.getZipcode());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.LAT, details.getLat());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.LNG, details.getLng());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.SSN, details.getSSN());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.EXPERTISE_AT, details.getExpertiseAT());
        //PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.HOURLY_RATE, details.getHourlyRate());
        PrefConnect.writeString(ConUpdateActivity.this, PrefConnect.LICENSE, details.getLicence());
    }

    private class UpdateTask extends AsyncTask<String, Void, Boolean> {
        HttpEntity resEntity;
        String url = Global.BASE_URL + "api/index.php/license";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                MultipartEntity reqEntity = getMultiPart();
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                final String response_str = EntityUtils.toString(resEntity);
                if (resEntity != null) {
                    Log.i("RESPONSE", response_str);
                    return true;
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            if (s)
                updateProfile(p);
            else Global.dismissProgress(p);
        }

        public MultipartEntity getMultiPart() {
            MultipartEntity reqEntity = new MultipartEntity();
            if (!Global.isUriEmpty(croppedPicUri) && !croppedPicUri.getPathSegments().get(0).equalsIgnoreCase("public")) {
                File pFile = new File(FilePath.getPath(ConUpdateActivity.this, croppedPicUri));
                FileBody fileBody = new FileBody(pFile);
                reqEntity.addPart("profile", fileBody);
            }

            if (!Global.isUriEmpty(croppedLicPicUri) && !croppedLicPicUri.getPathSegments().get(0).equalsIgnoreCase("public")) {
                File lFile = new File(FilePath.getPath(ConUpdateActivity.this, croppedLicPicUri));
                FileBody fileBody = new FileBody(lFile);
                reqEntity.addPart("license", fileBody);
            }

            return reqEntity;
        }

        private void updateProfile(final ProgressDialog p) {
            String location = String.format("%s,%s", LAT, LNG);
            //Getting TimeZone
            Call<TimeZone> call = googleService.getTimeZone(location, System.currentTimeMillis() / 1000,"AIzaSyCp77NabT4Yj4SUCXT7aG8eRh5I4mtki9M");
            call.enqueue(new Callback<TimeZone>() {
                @Override
                public void onResponse(Call<TimeZone> call, Response<TimeZone> response) {
                    if (response.isSuccessful()) {
                        TimeZone timeZone = response.body();
                        PROFILE = TextUtils.isEmpty(PROFILE) ? "" : PROFILE;
                        LICENSE = TextUtils.isEmpty(LICENSE) ? "" : LICENSE;

                        final ConUpdateModel.Request request = new ConUpdateModel.Request(USER_ID, NAME, ADDRESS, PHONE, PROFILE, ZIP_CODE,
                                LAT, LNG, EMAIL, PASSWORD, "", EXPERTISE, LICENSE,/* HOURLY_RATE,*/ timeZone.getTimeZoneId());
                        Call<ConUpdateModel.Response> upCall = apiService.conUpdate(request);
                        upCall.enqueue(new Callback<ConUpdateModel.Response>() {
                            @Override
                            public void onResponse(Call<ConUpdateModel.Response> call, Response<ConUpdateModel.Response> response) {
                                Global.dismissProgress(p);
                                if (response.isSuccessful()) {
                                    ConUpdateModel.Response response1 = response.body();
                                    if (response1.getResult().equalsIgnoreCase("Success")) {
                                        ConUpdateModel.ContractorDetails details = response1.getContractorDetails().get(0);
                                        saveData(details);
                                        gotoHome();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ConUpdateModel.Response> call, Throwable t) {
                                Global.dismissProgress(p);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<TimeZone> call, Throwable t) {
                    Global.dismissProgress(p);
                }
            });
        }
    }
}
