package com.oostajob.adapter.customer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.Green.customfont.CustomTextView;
import com.Green.customfont.tag.TagView;
import com.oostajob.CusBidDetailsActivity;
import com.oostajob.CusHireActivity;
import com.oostajob.FileViewActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.GetCustDetailsModel.Details;
import com.oostajob.model.GetCustDetailsModel.JobAnwserlist;
import com.oostajob.model.GetCustDetailsModel.Jobmedialist;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class CustomerJobsAdapter extends ArrayAdapter<Details> {

    public static final int BIDDING = 0;
    public static final int HIRE = 1;
    public static final int RATE = 2;
    public static final int CLOSE = 3;
    private LayoutInflater inflater;
    private int CURRENT_CHECKED;
    private String[] menuItem = {"Bidding", "Hire", "In Progress", "Closed"};
    private CancelJobListener listener;
    private JobCompeletedOrCancledOrRateNowListner jobCompeletedOrCancledListner;
    private HashMap<Integer, TagView> tagMap = new HashMap<>();
    private HashMap<Integer, LinearLayout> imageMap = new HashMap<>();

    public CustomerJobsAdapter(Context context, List<Details> objects, int filter) {
        super(context, R.layout.item_cus_my_jobs, objects);
        inflater = LayoutInflater.from(context);
        CURRENT_CHECKED = filter;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Details details = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_cus_my_jobs, parent, false);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgMarkerIcon);
            holder.txtTitle = (CustomTextView) convertView.findViewById(R.id.txtTitle);
            holder.txtPostCode = (CustomTextView) convertView.findViewById(R.id.txtPostCode);
            holder.txtTime = (CustomTextView) convertView.findViewById(R.id.txtTime);
            holder.txtCloses = (CustomTextView) convertView.findViewById(R.id.txtCloses);
            holder.txtDescription = (CustomTextView) convertView.findViewById(R.id.txtDescription);
            holder.txtMenuItem = (CustomTextView) convertView.findViewById(R.id.txtMenuItem);
            holder.txtNewMsg = (CustomTextView) convertView.findViewById(R.id.txtNewMsg);
            holder.menuLay = (LinearLayout) convertView.findViewById(R.id.menuLay);
            holder.ratelayout = (RelativeLayout) convertView.findViewById(R.id.ratelayout);
            holder.timeLay = (LinearLayout) convertView.findViewById(R.id.timeLay);
//            holder.tagView = (TagView) convertView.findViewById(R.id.tagView);
            holder.filesLay = (LinearLayout) convertView.findViewById(R.id.filesLay);
            holder.imgMsgIcon = (ImageView) convertView.findViewById(R.id.imgMsgIcon);
            holder.imgDropDown = (ImageView) convertView.findViewById(R.id.imgDropDown);
            holder.imgNotifIcon = (ImageView) convertView.findViewById(R.id.imgNotifIcon);
            holder.txtMsgCount = (CustomTextView) convertView.findViewById(R.id.txtMsgCount);
            holder.txtNotifCount = (CustomTextView) convertView.findViewById(R.id.txtNotifCount);
            holder.chipView = (ChipView) convertView.findViewById(R.id.chipView);
            holder.txtAmount = (CustomTextView) convertView.findViewById(R.id.txtAmount);
            holder.txtDateTime = (CustomTextView) convertView.findViewById(R.id.txtDateTime);
            holder.btncancel = (CustomTextView) convertView.findViewById(R.id.btncancel);
            holder.fmJobCompletedStatus = (FrameLayout) convertView.findViewById(R.id.fmJobCompletedStatus);
            holder.btnCompleted = (CustomTextView) convertView.findViewById(R.id.btnCompleted);
            holder.btnCancelForJob = (CustomTextView) convertView.findViewById(R.id.btnCancelForJob);
            holder.linJObCancel = convertView.findViewById(R.id.linJObCancel);
            holder.chipViewCancel = convertView.findViewById(R.id.chipViewCancel);
            holder.btnRateNow = convertView.findViewById(R.id.btnRateNow);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String icon = details.getJobicon();
        icon = icon.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, icon);
        Picasso.with(getContext()).load(url).fit().into(holder.imgIcon);
        holder.txtTitle.setText(details.getJobtypeName());
        holder.txtPostCode.setText(details.getZipcode());
        holder.txtDescription.setText(details.getDescription());

        Date date = Global.getDate(details.getJobpostTime());

        long difference = System.currentTimeMillis() - date.getTime();

        long hours = TimeUnit.MILLISECONDS.toHours(difference);

        if (hours == 0) {
            long minu = TimeUnit.MILLISECONDS.toMinutes(difference);
            if (minu > 1)
                holder.txtTime.setText(String.format("%d minutes ago", minu));
            else holder.txtTime.setText(String.format("few seconds ago", minu));
        } else if (hours >= 24) {
            long days = TimeUnit.MILLISECONDS.toDays(difference);
            if (days > 1 && days < 365)
                holder.txtTime.setText(String.format("%d days ago", days));
            else if (days >= 365) {
                long year = days / 365;
                if (year > 1) {
                    holder.txtTime.setText(String.format("%d years ago", year));
                } else holder.txtTime.setText(String.format("%d year ago", year));
            } else holder.txtTime.setText(String.format("%d day ago", days));
        } else if (hours < 24) {
            if (hours > 1)
                holder.txtTime.setText(String.format("%d hours ago", hours));
            else holder.txtTime.setText(String.format("%d hour ago", hours));
        }

        if (!details.getMessageCount().equals("0")) {
            holder.txtMsgCount.setText(details.getMessageCount());
            holder.txtMsgCount.setTextColor(getContext().getResources().getColor(R.color.blue));
            Picasso.with(getContext()).load(R.drawable.mail_blue).fit().into(holder.imgMsgIcon);
        } else {
            holder.txtMsgCount.setText("0");
            Picasso.with(getContext()).load(R.drawable.mail).fit().into(holder.imgMsgIcon);
        }

        if (!details.getContractorcnt().equals("0")) {
            holder.txtNotifCount.setText(details.getContractorcnt());
            holder.txtNotifCount.setTextColor(getContext().getResources().getColor(R.color.blue));
            Picasso.with(getContext()).load(R.drawable.contractor_off_blue).fit().into(holder.imgNotifIcon);
        } else {
            Picasso.with(getContext()).load(R.drawable.contractor_off).fit().into(holder.imgNotifIcon);
        }

        if (CURRENT_CHECKED != BIDDING) {
            holder.txtNewMsg.setVisibility(View.GONE);
            holder.txtCloses.setVisibility(View.GONE);
            holder.btncancel.setVisibility(View.GONE);
        } else {
            holder.txtCloses.setText(String.format("Wait %s to Hire", details.getRemaining_time()));
            if (!details.getNotificationCount().equals("0")) {
                holder.txtNewMsg.setVisibility(View.VISIBLE);
                holder.txtNewMsg.setText(details.getNotificationCount());
            } else {
                holder.txtNewMsg.setVisibility(View.GONE);
            }
        }
        if (CURRENT_CHECKED != RATE) {
            holder.timeLay.setVisibility(View.GONE);
        }

        if (CURRENT_CHECKED == RATE) {
            holder.fmJobCompletedStatus.setVisibility(View.VISIBLE);
            holder.btnCompleted.setVisibility(View.VISIBLE);
            holder.btnCancelForJob.setVisibility(View.VISIBLE);
            holder.btnCancelForJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jobCompeletedOrCancledListner != null)
                        jobCompeletedOrCancledListner.onJobCancled(getItem(position));
                }

            });

            holder.btnCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jobCompeletedOrCancledListner != null)
                        jobCompeletedOrCancledListner.onJobCompleted(getItem(position));
                }
            });


        }

        if (CURRENT_CHECKED == CLOSE) {
            holder.btnCancelForJob.setVisibility(View.GONE);
            holder.btnCompleted.setVisibility(View.GONE);

            if (CLOSE == CURRENT_CHECKED) {
                List<Chip> arrayListCancelReasons = new ArrayList<>();

                if ((details.reason != null) && !(details.reason.equalsIgnoreCase("null"))) {
                    String[] chipListCancel = details.reason.split(",");

                    if (chipListCancel.length > 0) {
                        holder.linJObCancel.setVisibility(View.VISIBLE);
                        for (String s : chipListCancel) {

                            if (s.trim().length() > 0 && !(s.equalsIgnoreCase("null"))) {
                                arrayListCancelReasons.add(new com.oostajob.utils.Tag(s));
                            }

                        }

                    }


                } else {
                    holder.linJObCancel.setVisibility(View.GONE);
                }

                holder.chipViewCancel.setLineSpacing(10);
                holder.chipViewCancel.setChipSpacing(10);
                holder.chipViewCancel.setChipLayoutRes(R.layout.chip_lay_cancel);
                holder.chipViewCancel.setChipBackgroundRes(R.drawable.tag_cancel);
                holder.chipViewCancel.setChipList(arrayListCancelReasons);
            }


        }

        if (CURRENT_CHECKED == RATE || CURRENT_CHECKED == CLOSE) {
            holder.txtAmount.setText("$" + details.getBidAmount());
            if ((details.getContractorDetails().getMeetingDate() == null && details.getContractorDetails().getMeetingTime() == null) || details.getContractorDetails().getMeetingDate().startsWith("0000")) {
                holder.txtDateTime.setText("Meeting not scheduled");


            } else {
                String dateTime = String.format("%s, %s", Global.getDate(details.getContractorDetails().getMeetingDate(), "yyyy-MM-dd", "MM/dd/yyyy"), details.getContractorDetails().getMeetingTime());
                holder.txtDateTime.setText(dateTime);

            }

        } else {
            holder.ratelayout.setVisibility(View.GONE);
            holder.fmJobCompletedStatus.setVisibility(View.GONE);
        }
        if (CURRENT_CHECKED == CLOSE && details.getContractorDetails().getTotalRating() != null) {
            holder.ratingBar.setVisibility(View.VISIBLE);
            holder.ratingBar.setRating(Float.parseFloat(details.getContractorDetails().getTotalRating()));
        } else {
            holder.ratingBar.setVisibility(View.GONE);


        }



        if (CURRENT_CHECKED == CLOSE && details.getContractorDetails().getTotalRating() == null) {
            holder.btnRateNow.setVisibility(View.VISIBLE);

            holder.btnRateNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jobCompeletedOrCancledListner.onRateNow(details);
                }
            });

        } else {
            holder.btnRateNow.setVisibility(View.GONE);
        }

        refreshImageLay(holder.filesLay, details.getJobmedialist(), position);

//        setTags(holder.tagView, details, position);
        List<Chip> chipList = new ArrayList<>();
        for (JobAnwserlist answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        holder.chipView.setLineSpacing(10);
        holder.chipView.setChipSpacing(10);
        holder.chipView.setChipLayoutRes(R.layout.chip_lay);
        holder.chipView.setChipBackgroundRes(R.drawable.tag);
        holder.chipView.setChipList(chipList);

        setMenu(holder, position);

        setListener(convertView, holder, position);
        return convertView;
    }

    private void setMenu(ViewHolder holder, int position) {
        switch (CURRENT_CHECKED) {
            case BIDDING:


                holder.txtMenuItem.setText(menuItem[BIDDING]);
                holder.imgDropDown.setVisibility(View.GONE);
                holder.menuLay.setBackgroundResource(R.drawable.blue_rectangle_curve);
                break;
            case HIRE:


                holder.btncancel.setVisibility(View.GONE);
                holder.txtMenuItem.setText(menuItem[HIRE]);
                holder.menuLay.setBackgroundResource(R.drawable.rose_rectangle_curved);
                break;
            case RATE:
                holder.txtMenuItem.setText(menuItem[RATE]);
                holder.menuLay.setVisibility(View.VISIBLE);
                holder.imgDropDown.setVisibility(View.GONE);
                holder.menuLay.setBackgroundResource(R.drawable.rose_rectangle_curved);
                break;
            case CLOSE:

                holder.menuLay.setVisibility(View.VISIBLE);
                holder.txtMenuItem.setVisibility(View.VISIBLE);


                holder.txtMenuItem.setText(menuItem[CLOSE]);
                holder.imgDropDown.setVisibility(View.GONE);
                holder.menuLay.setBackgroundResource(R.drawable.blue_rectangle_curve);
                break;
        }
    }

//    private void setTags(TagView tagView, final Details details, int position) {
//        tagView.removeAllTags();
//        ArrayList<JobAnwserlist> answerList = details.getJobanwserlist();
//        for (JobAnwserlist answer : answerList) {
//            if (answer.getTag_key().equalsIgnoreCase("1")) {
//                String ans = "";
//                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
//                    ans = String.format("%s %s", answer.getAnswer(), answer.getTag_keyword());
//                } else {
//                    ans = answer.getAnswer();
//                }
//
//                Tag tag = new Tag(ans);
//                tag.tagTextColor = getContext().getResources().getColor(R.color.black);
//                tag.layoutColor = getContext().getResources().getColor(R.color.white);
//                tag.layoutColorPress = getContext().getResources().getColor(R.color.lightGrey);
//                tag.radius = 20f;
//                tag.tagTextSize = 14f;
//                tag.layoutBorderSize = 1f;
//                tag.layoutBorderColor = getContext().getResources().getColor(R.color.black);
//                tag.isDeletable = false;
//                tagView.addTag(tag);
//            }
//        }
//    }


    private void setListener(View convertView, ViewHolder holder, final int position) {
        holder.btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCancelJob(getItem(position));
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetCustDetailsModel.Details details = getItem(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("details", details);
                bundle.putInt("currentItem", CURRENT_CHECKED);
                startActivity(bundle);
            }
        });
        holder.menuLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CURRENT_CHECKED == HIRE) {
                    PopupMenu popup = new PopupMenu(getContext(), v);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                /*case R.id.action_cancel:
                                    if (listener != null)
                                        listener.onCancelJob(getItem(position));
                                    return true;*/
                                case R.id.action_hire:
                                    GetCustDetailsModel.Details details = getItem(position);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("details", details);
                                    bundle.putInt("currentItem", CURRENT_CHECKED);
                                    startActivity(bundle);
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.inflate(R.menu.actions);
                    /*if (CURRENT_CHECKED == HIRE)*/
                    popup.getMenu().findItem(R.id.action_hire).setVisible(true);
                    popup.show();
                }
                if (CURRENT_CHECKED == RATE) {
                    GetCustDetailsModel.Details details = getItem(position);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("details", details);
                    bundle.putInt("currentItem", CURRENT_CHECKED);
                    startActivity(bundle);
                }
            }
        });

    }

    private void startActivity(Bundle bundle) {
        Intent intent;
        switch (CURRENT_CHECKED) {
            case BIDDING:
            case RATE:
                intent = new Intent(getContext(), CusBidDetailsActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getContext().startActivity(intent);
                break;
            case CLOSE:
                intent = new Intent(getContext(), CusBidDetailsActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getContext().startActivity(intent);
                break;
            case HIRE:
                intent = new Intent(getContext(), CusHireActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getContext().startActivity(intent);
                break;
        }
    }

    private void refreshImageLay(LinearLayout scrollContent, final ArrayList<Jobmedialist> fileList, int position) {

//        scrollContent.removeAllViews();
       /* LinearLayout layout = imageMap.get(position);
        if (layout != null) {
            scrollContent = layout;
        } else {*/
        scrollContent.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());

        for (final Jobmedialist model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.item_sub_image, scrollContent, false);
            final ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            ImageView imgClose = (ImageView) view.findViewById(R.id.imgClose);
            imgClose.setVisibility(View.GONE);
            if (model.getMediaType().equals("1")) {
                Picasso.with(getContext()).load(url).fit().into(imgChosen);
            } else {
                Picasso.with(getContext()).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FileViewActivity.class);
                    intent.putExtra("fileType", model.getMediaType());
                    intent.putExtra("filePath", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getContext().startActivity(intent);
                }
            });
            scrollContent.addView(view);
        }
            /*imageMap.put(position, scrollContent);
        }*/
    }

    public void setCancelJobListener(CancelJobListener listener) {
        this.listener = listener;
    }

    public void setJobCompeletedOrCancledListner(JobCompeletedOrCancledOrRateNowListner jobCompeletedOrCancledListner) {
        this.jobCompeletedOrCancledListner = jobCompeletedOrCancledListner;
    }


    public interface CancelJobListener {
        void onCancelJob(GetCustDetailsModel.Details details);


    }

    public interface JobCompeletedOrCancledOrRateNowListner {

        void onJobCompleted(GetCustDetailsModel.Details details);

        void onJobCancled(GetCustDetailsModel.Details details);

        void onRateNow(GetCustDetailsModel.Details details);
    }

    public class ViewHolder {
        ImageView imgIcon, imgMsgIcon, imgNotifIcon, imgDropDown;
        CustomTextView txtTitle, txtPostCode, txtMenuItem, txtTime, txtDescription, txtMsgCount, txtNotifCount, txtNewMsg, txtAmount, txtDateTime, txtCloses;
        LinearLayout filesLay;
        LinearLayout menuLay;
        LinearLayout timeLay;
        RelativeLayout ratelayout;
        CustomTextView btncancel;
        RatingBar ratingBar;
        FrameLayout fmJobCompletedStatus;
        //        TagView tagView;
        ChipView chipView, chipViewCancel;
        LinearLayout linJObCancel;
        CustomTextView btnCompleted, btnCancelForJob, btnRateNow;
    }
}
