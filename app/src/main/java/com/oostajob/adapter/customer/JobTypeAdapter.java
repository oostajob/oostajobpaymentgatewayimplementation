package com.oostajob.adapter.customer;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.QuestionsActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.JobTypeModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mathan on 27/11/2015.
 */
public class JobTypeAdapter extends ArrayAdapter<JobTypeModel.JobtypeDetails> {

    LayoutInflater inflater;

    public JobTypeAdapter(Context context, List<JobTypeModel.JobtypeDetails> objects) {
        super(context, R.layout.item_work_selection_activity, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final JobTypeModel.JobtypeDetails jobtypeDetails = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_work_selection_activity, parent, false);
            holder.img_bg = (ImageView) convertView.findViewById(R.id.img_category);
            holder.txtItem = (CustomTextView) convertView.findViewById(R.id.txt_category);
            holder.txt_custom_category = (CustomEditText) convertView.findViewById(R.id.txt_custom_category);
            holder.imgNext = (ImageView) convertView.findViewById(R.id.imgNext);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String image_name = jobtypeDetails.getJob_image();
        image_name = image_name.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, image_name);
        Picasso.with(getContext()).load(url).fit().centerCrop().into(holder.img_bg);
        holder.txtItem.setText(jobtypeDetails.getJob_name());

        if (jobtypeDetails.getJob_name().equalsIgnoreCase("Others")) {
            holder.txt_custom_category.setVisibility(View.VISIBLE);
            holder.imgNext.setVisibility(View.VISIBLE);
//            holder.txt_custom_category.requestFocus();
        } else {
            holder.txt_custom_category.setVisibility(View.GONE);
            holder.imgNext.setVisibility(View.GONE);
        }

        holder.imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(holder.txt_custom_category.getText().toString())) {
                    String imgUrl = String.format("%s/%s", Global.BASE_URL, jobtypeDetails.getJob_image());
                    Intent qusIntent = new Intent(getContext(), QuestionsActivity.class);
                    qusIntent.putExtra("subTitle", String.format("Others: %s", holder.txt_custom_category.getText().toString()));
                    qusIntent.putExtra("jobTypeId", jobtypeDetails.getJobtype_id());
                    qusIntent.putExtra("imgUrl", imgUrl);
                    getContext().startActivity(qusIntent);
                } else {
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.enter_service), Toast.LENGTH_LONG).show();
                }
            }
        });
        return convertView;
    }

    public class ViewHolder {
        ImageView img_bg, imgNext;
        CustomTextView txtItem;
        CustomEditText txt_custom_category;
    }
}
