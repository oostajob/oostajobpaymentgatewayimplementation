package com.oostajob.adapter.customer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.Green.customfont.CustomTextView;
import com.oostajob.FileViewActivity;
import com.oostajob.R;
import com.oostajob.model.FileModel;
import com.oostajob.model.JobSubTypeCharModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class QuestionsAdapter extends ArrayAdapter<JobSubTypeCharModel.JobsubtypecharDetails> {

    LayoutInflater inflater;
    DialogOpenListener listener;

    public QuestionsAdapter(Context context, List<JobSubTypeCharModel.JobsubtypecharDetails> objects) {
        super(context, R.layout.item_questions, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        JobSubTypeCharModel.JobsubtypecharDetails model = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_questions, parent, false);
            holder.imgDone = (ImageView) convertView.findViewById(R.id.imgDone);
            holder.txtItemQues = (CustomTextView) convertView.findViewById(R.id.txtItemQues);
            holder.scrollLay = (HorizontalScrollView) convertView.findViewById(R.id.scrollLay);
            holder.scrollContent = (LinearLayout) convertView.findViewById(R.id.scrollContent);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (model.getQuestion_type().equalsIgnoreCase("Files") && model.getFileList().size() > 0) {
            holder.scrollLay.setVisibility(View.VISIBLE);
            holder.txtItemQues.setVisibility(View.GONE);
            refreshImageLay(holder.scrollLay, holder.scrollContent, model.getFileList());
        } else {
            holder.scrollLay.setVisibility(View.GONE);
            holder.txtItemQues.setVisibility(View.VISIBLE);
        }
        holder.txtItemQues.setText(model.getChar_name());
        holder.imgDone.setImageResource(model.getImgDone());

        return convertView;
    }

    private void refreshImageLay(final HorizontalScrollView scrollLay, final LinearLayout scrollContent, final ArrayList<FileModel> fileList) {
        scrollContent.removeAllViews();
        final LayoutInflater inflater = LayoutInflater.from(getContext());

        for (final FileModel model : fileList) {
            View view;
            view = inflater.inflate(R.layout.item_sub_image, scrollContent, false);
            ImageView imgClose = (ImageView) view.findViewById(R.id.imgClose);
            ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            if (model.getMediaType().equals("1")) {
//                imgChosen.setImageURI(Uri.fromFile(model.getFile()));
                Picasso.with(getContext()).load(model.getFile()).fit().into(imgChosen);
            } else {
              /*  Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(model.getFile().getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                imgChosen.setImageBitmap(bitmap);*/
                Picasso.with(getContext()).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FileViewActivity.class);
                    intent.putExtra("fileType", model.getMediaType());
                    intent.putExtra("filePath", model.getFile().getPath());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getContext().startActivity(intent);
                }
            });
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.removeImage(model);
                    }
                }
            });
            scrollContent.addView(view);
        }

        // To Add More Files
        View addView = inflater.inflate(R.layout.image_plus, scrollContent, false);
        ImageView imgAdd = (ImageView) addView.findViewById(R.id.imgAdd);
        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.openFileDialog();
                }
            }
        });

        scrollContent.addView(addView, scrollContent.getChildCount());
        scrollLay.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollLay.fullScroll(View.FOCUS_RIGHT);
                scrollLay.scrollBy(500, 0);
            }
        }, 500);
    }

    public void setDialogOpenListener(DialogOpenListener listener) {
        this.listener = listener;
    }

    public interface DialogOpenListener {
        void openFileDialog();

        void removeImage(FileModel model);
    }

    public class ViewHolder {
        CustomTextView txtItemQues;
        ImageView imgDone;
        HorizontalScrollView scrollLay;
        LinearLayout scrollContent;
    }
}
