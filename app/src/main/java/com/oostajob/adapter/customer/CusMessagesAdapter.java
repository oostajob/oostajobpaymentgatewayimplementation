package com.oostajob.adapter.customer;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.DiscussionlistModel.DiscustionDetails;
import com.oostajob.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CusMessagesAdapter extends ArrayAdapter<DiscustionDetails> {

    private LayoutInflater inflater;
    private ReplyListener listener;
    private boolean showReply;

    /**
     * Creates new Message Adapter
     *
     * @param context   Application Context
     * @param objects   ArrayList of Values
     * @param showReply boolean indicating whether to show reply button or not
     */
    public CusMessagesAdapter(Context context, List<DiscustionDetails> objects, boolean showReply) {
        super(context, R.layout.item_customer_messages, objects);
        inflater = LayoutInflater.from(context);
        this.showReply = showReply;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiscustionDetails details = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_customer_messages, parent, false);
            holder.imgConProfile = (ImageView) convertView.findViewById(R.id.imgConProfile);
            holder.replyLay = (LinearLayout) convertView.findViewById(R.id.replyLay);
            holder.replySendLay = (LinearLayout) convertView.findViewById(R.id.replySendLay);
            holder.txtConName = (CustomTextView) convertView.findViewById(R.id.txtConName);
            holder.txtConTime = (CustomTextView) convertView.findViewById(R.id.txtConTime);
            holder.txtConText = (CustomTextView) convertView.findViewById(R.id.txtConText);
            holder.txtReply = (CustomTextView) convertView.findViewById(R.id.txtReply);
            holder.txtReplyTime = (CustomTextView) convertView.findViewById(R.id.txtReplyTime);
            holder.btnReply = (CustomButton) convertView.findViewById(R.id.btnReply);
            holder.btnSend = (CustomButton) convertView.findViewById(R.id.btnSend);
            holder.edtReply = (CustomEditText) convertView.findViewById(R.id.edtReply);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String CON_URL = String.format("%s/%s", Global.BASE_URL, details.getContractor_profile());
        Picasso.with(getContext()).load(CON_URL).transform(new CircleTransform()).fit().into(holder.imgConProfile);
        holder.txtConName.setText(details.getContractor_name());
        holder.txtConTime.setText(Global.getDate(details.getQuestionTime(), Global.SERVER_DATE_FORMAT,
                "MM/dd/yyyy, hh:mm aa"));
        holder.txtConText.setText(details.getQuestionName());

        if (!TextUtils.isEmpty(details.getAnwser())) {
            holder.replyLay.setVisibility(View.VISIBLE);
            holder.txtReply.setText(details.getAnwser());
            holder.txtReplyTime.setText(Global.getDate(details.getRespondTime(), Global.SERVER_DATE_FORMAT,
                    "MM/dd/yyyy, hh:mm aa"));
        } else {
            holder.replyLay.setVisibility(View.GONE);
        }

        if (showReply) {
            holder.btnReply.setVisibility(View.VISIBLE);
        } else {
            holder.btnReply.setVisibility(View.GONE);
        }
        holder.btnReply.setEnabled(true);
        holder.replySendLay.setVisibility(View.GONE);
        setListener(holder, details);

        return convertView;
    }

    private void setListener(final ViewHolder holder, final DiscustionDetails details) {
        holder.btnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.replySendLay.setVisibility(View.VISIBLE);
                holder.btnReply.setEnabled(false);
            }
        });
        holder.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.sendReply(details, holder.edtReply.getText().toString());
                }
            }
        });
    }

    public void setOnReplyListener(ReplyListener listener) {
        this.listener = listener;
    }

    public interface ReplyListener {
        void sendReply(DiscustionDetails details, String answer);
    }

    public class ViewHolder {
        ImageView imgConProfile;
        LinearLayout replyLay, replySendLay;
        CustomTextView txtConName, txtConTime, txtConText, txtReply, txtReplyTime;
        CustomButton btnReply, btnSend;
        CustomEditText edtReply;
    }
}
