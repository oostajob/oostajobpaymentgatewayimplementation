package com.oostajob.adapter.customer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ReviewAdapter extends ArrayAdapter<GetCustDetailsModel.Reviewlist> {

    LayoutInflater inflater;

    public ReviewAdapter(Context context, List<GetCustDetailsModel.Reviewlist> objects) {
        super(context, R.layout.item_review, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final GetCustDetailsModel.Reviewlist item = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_review, parent, false);
            holder.imgProfile = (ImageView) convertView.findViewById(R.id.imgProfile);
            holder.txtName = (CustomTextView) convertView.findViewById(R.id.txtName);
            holder.txtTime = (CustomTextView) convertView.findViewById(R.id.txtTime);
            holder.txtAddress = (CustomTextView) convertView.findViewById(R.id.txtAddress);
            holder.txtDescription = (CustomTextView) convertView.findViewById(R.id.txtDescription);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String profileImg = item.getCustomerDetails().get(0).getProfilePhoto();
        profileImg = profileImg.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, profileImg);
        Picasso.with(getContext()).load(url).transform(new CircleTransform()).fit().into(holder.imgProfile);
        holder.txtName.setText(item.getCustomerDetails().get(0).getUsername());
        holder.txtTime.setText(Global.getDate(item.getRatingTime(), "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy"));
        holder.txtAddress.setText(item.getCustomerDetails().get(0).getAddress());
        holder.txtDescription.setText(item.getComment());
        holder.ratingBar.setRating(Float.parseFloat(item.getRatingOverall()));

        return convertView;
    }

    public class ViewHolder {
        ImageView imgProfile;
        RatingBar ratingBar;
        CustomTextView txtName, txtTime, txtAddress, txtDescription;
    }
}
