package com.oostajob.adapter.customer;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ConHireAdapter extends ArrayAdapter<GetCustDetailsModel.Contractorlist> {

    LayoutInflater inflater;
    HireListener listener;

    public ConHireAdapter(Context context, List<GetCustDetailsModel.Contractorlist> objects) {
        super(context, R.layout.item_contractor_hire, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final GetCustDetailsModel.Contractorlist item = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_contractor_hire, parent, false);
            holder.imgConProfile = (ImageView) convertView.findViewById(R.id.imgConProfile);
            holder.txtConName = (CustomTextView) convertView.findViewById(R.id.txtConName);
            holder.txtAddress = (CustomTextView) convertView.findViewById(R.id.txtAddress);
            holder.txtAmount = (CustomTextView) convertView.findViewById(R.id.txtAmount);
            holder.hireLay = (LinearLayout) convertView.findViewById(R.id.hireLay);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String profileImg = item.getProfilePhoto();
        profileImg = profileImg.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, profileImg);
        Picasso.with(getContext()).load(url).transform(new CircleTransform()).fit().into(holder.imgConProfile);
        holder.txtConName.setText(item.getName());
        holder.txtAddress.setText(item.getAddress());
        holder.txtAmount.setText(String.format("$%s", item.getBidAmount()));

        if (TextUtils.isEmpty(item.getTotalRatingbycategory()))
            item.setTotalRatingbycategory("0.0");
        holder.ratingBar.setRating(Float.parseFloat(item.getTotalRatingbycategory()));

        holder.hireLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onHireClick(item);
                }
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position);
                }
            }
        });

        return convertView;
    }

    public void setHireListener(HireListener listener) {
        this.listener = listener;
    }

    public interface HireListener {
        void onHireClick(GetCustDetailsModel.Contractorlist item);

        void onItemClick(int position);
    }

    public class ViewHolder {
        ImageView imgConProfile;
        CustomTextView txtConName, txtAddress, txtAmount;
        LinearLayout hireLay;
        RatingBar ratingBar;
    }
}
