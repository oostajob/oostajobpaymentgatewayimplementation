package com.oostajob.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import com.Green.customfont.CustomCheckBox;
import com.Green.customfont.CustomCheckedTextView;
import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.model.JobSubTypeCharModel;
import com.oostajob.model.ListModel;

import java.util.List;


public class ListCommonAdapter extends ArrayAdapter<ListModel> {

    LayoutInflater inflater;

    public ListCommonAdapter(Context context, List<ListModel> objects) {
        super(context, R.layout.item__dialog_list, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListModel model = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item__dialog_list, parent, false);
            holder.text = (CustomCheckedTextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(model.getName());
//        convertView.setBackgroundResource(R.drawable.menu_item_selector);
        return convertView;
    }

    public class ViewHolder {
        CustomCheckedTextView text;
//        CustomCheckBox checkbox;
    }
}
