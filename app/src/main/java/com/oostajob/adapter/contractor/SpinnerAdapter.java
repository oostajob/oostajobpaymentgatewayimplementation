package com.oostajob.adapter.contractor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.model.JobTypeModel.JobtypeDetails;

import java.util.List;


public class SpinnerAdapter extends ArrayAdapter<JobtypeDetails> {

    LayoutInflater inflater;

    public SpinnerAdapter(Context context, List<JobtypeDetails> objects) {
        super(context, R.layout.item_spinner, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        JobtypeDetails details = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_spinner, parent, false);
            holder.txtSpinnerItem = (CustomTextView) convertView.findViewById(R.id.txtSpinnerItem);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtSpinnerItem.setText(details.getJob_name());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        JobtypeDetails details = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_spinner, parent, false);
            holder.txtSpinnerItem = (CustomTextView) convertView.findViewById(R.id.txtSpinnerItem);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtSpinnerItem.setText(details.getJob_name());

        return convertView;
    }

    public class ViewHolder {
        CustomTextView txtSpinnerItem;
    }
}
