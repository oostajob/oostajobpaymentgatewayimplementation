package com.oostajob.adapter.contractor;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.DiscussionlistModel.DiscustionDetails;
import com.oostajob.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;


public class DiscussionAdapter extends ArrayAdapter<DiscustionDetails> {

    LayoutInflater inflater;

    public DiscussionAdapter(Context context, List<DiscustionDetails> objects) {
        super(context, R.layout.item_discussion, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiscustionDetails details = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_discussion, parent, false);
            holder.imgConProfile = (ImageView) convertView.findViewById(R.id.imgConProfile);
            holder.imgCusProfile = (ImageView) convertView.findViewById(R.id.imgCusProfile);
            holder.answerLay = (LinearLayout) convertView.findViewById(R.id.answerLay);
            holder.txtConName = (CustomTextView) convertView.findViewById(R.id.txtConName);
            holder.txtConTime = (CustomTextView) convertView.findViewById(R.id.txtConTime);
            holder.txtConText = (CustomTextView) convertView.findViewById(R.id.txtConText);
            holder.txtCusName = (CustomTextView) convertView.findViewById(R.id.txtCusName);
            holder.txtCusTime = (CustomTextView) convertView.findViewById(R.id.txtCusTime);
            holder.txtCusText = (CustomTextView) convertView.findViewById(R.id.txtCusText);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String CON_URL = String.format("%s/%s", Global.BASE_URL, details.getContractor_profile());
        Picasso.with(getContext()).load(CON_URL).transform(new CircleTransform()).fit().into(holder.imgConProfile);
        holder.txtConName.setText(details.getContractor_name());
        holder.txtConTime.setText(Global.getDate(details.getQuestionTime(), Global.SERVER_DATE_FORMAT,
                "MM/dd/yyyy, hh:mm aa"));
        holder.txtConText.setText(details.getQuestionName());

        if (!TextUtils.isEmpty(details.getAnwser())) {
            holder.answerLay.setVisibility(View.VISIBLE);
            String CUS_URL = String.format("%s/%s", Global.BASE_URL, details.getCustomer_profile());
            Picasso.with(getContext()).load(CUS_URL).transform(new CircleTransform()).fit().into(holder.imgCusProfile);
            holder.txtCusName.setText(details.getCustomer_name());
            holder.txtCusTime.setText(Global.getDate(details.getRespondTime(), Global.SERVER_DATE_FORMAT,
                    "MM/dd/yyyy, hh:mm aa"));
            holder.txtCusText.setText(details.getAnwser());
        } else {
            holder.answerLay.setVisibility(View.GONE);
        }

        return convertView;
    }

    public class ViewHolder {
        ImageView imgConProfile, imgCusProfile;
        LinearLayout answerLay;
        CustomTextView txtConName, txtConTime, txtConText, txtCusName, txtCusTime, txtCusText;

    }
}
