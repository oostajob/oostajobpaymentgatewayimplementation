package com.oostajob.adapter.contractor;

import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.Green.customfont.CustomTextView;
import com.Green.customfont.tag.Tag;
import com.Green.customfont.tag.TagView;
import com.oostajob.FileViewActivity;
import com.oostajob.InvitesActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ContractorJobList;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class InvitesAdapter extends ArrayAdapter<ContractorJobList.List> {

    LayoutInflater inflater;
    MediaMetadataRetriever m;
    private SkipJobListener listener;
    private HashMap<Integer, TagView> tagMap = new HashMap<>();
    private HashMap<Integer, LinearLayout> imageMap = new HashMap<>();

    public InvitesAdapter(Context context, List<ContractorJobList.List> objects) {
        super(context, R.layout.item_con_find_jobs, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ContractorJobList.List item = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_con_find_jobs, parent, false);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgMarkerIcon);
            holder.txtTitle = (CustomTextView) convertView.findViewById(R.id.txtTitle);
            holder.txtPostCode = (CustomTextView) convertView.findViewById(R.id.txtPostCode);
            holder.txtDistance = (CustomTextView) convertView.findViewById(R.id.txtDistance);
            holder.txtDescription = (CustomTextView) convertView.findViewById(R.id.txtDescription);
            holder.txtCloses = (CustomTextView) convertView.findViewById(R.id.txtCloses);
            holder.txtLowestBid = (CustomTextView) convertView.findViewById(R.id.txtLowestBid);
            holder.menuLay = (LinearLayout) convertView.findViewById(R.id.menuLay);
            holder.chipView = (ChipView) convertView.findViewById(R.id.chipView);
            holder.filesLay = (LinearLayout) convertView.findViewById(R.id.filesLay);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String icon = item.getJob_icon();
        icon = icon.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, icon);
        Picasso.with(getContext()).load(url).fit().into(holder.imgIcon);
        holder.txtTitle.setText(item.getJobtypeName());
        holder.txtPostCode.setText(item.getZipcode());

        if (item.getMiles() == null) item.setMiles("0");

        if (Integer.parseInt(item.getMiles()) > 1) {
            holder.txtDistance.setText(String.format("%s miles from here", item.getMiles()));
        } else {
            holder.txtDistance.setText(String.format("< %s mile from here", "1"));
        }
        holder.txtCloses.setText(String.format("Job closes in %s", item.getRemaining_time()));
        holder.txtDescription.setText(item.getDescription());

        refreshImageLay(holder.filesLay, item.getJobmedialist(), position);

        List<Chip> chipList = new ArrayList<>();
        for (ContractorJobList.Answer answer : item.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s",  answer.getTag_keyword(),answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        if (item.getMinimumbidamount() == null) {
            //holder.txtLowestBid.setVisibility(View.GONE);
            holder.txtLowestBid.setText(String.format("Lowest bid as of now $%s", "0"));
        } else {
            holder.txtLowestBid.setText(String.format("Lowest bid as of now $%s", item.getMinimumbidamount()));
        }
        holder.chipView.setLineSpacing(10);
        holder.chipView.setChipSpacing(10);
        holder.chipView.setChipLayoutRes(R.layout.chip_lay);
        holder.chipView.setChipBackgroundRes(R.drawable.tag);
        holder.chipView.setChipList(chipList);

        setListener(convertView, holder, position);

        return convertView;
    }

    private void setTags(TagView tagView, final ContractorJobList.List item, int position) {
        tagView.removeAllTags();
        ArrayList<ContractorJobList.Answer> answerList = item.getJobanwserlist();
        for (ContractorJobList.Answer answer : item.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s",  answer.getTag_keyword(),answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                Tag tag = new Tag(ans);
                tag.tagTextColor = getContext().getResources().getColor(R.color.black);
                tag.layoutColor = getContext().getResources().getColor(R.color.white);
                tag.layoutColorPress = getContext().getResources().getColor(R.color.lightGrey);
                tag.radius = 20f;
                tag.tagTextSize = 14f;
                tag.layoutBorderSize = 1f;
                tag.layoutBorderColor = getContext().getResources().getColor(R.color.black);
                tag.isDeletable = false;
                tagView.addTag(tag);
            }
        }
    }

    private void setListener(View convertView, ViewHolder holder, final int position) {
        holder.menuLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_bid:
                                sendIntent(position);
                                return true;
                            case R.id.action_skip:
                                if (listener != null)
                                    listener.onSkipJob(getItem(position));
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.inflate(R.menu.skip);
                popup.show();
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIntent(position);
            }
        });
    }

    private void sendIntent(int position) {
        ContractorJobList.List details = getItem(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("details", details);
        Intent intent = new Intent(getContext(), InvitesActivity.class);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getContext().startActivity(intent);
    }

    private void refreshImageLay(LinearLayout scrollContent, ArrayList<ContractorJobList.Media> fileList, int position) {

        scrollContent.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());

        for (final ContractorJobList.Media model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.item_sub_image, scrollContent, false);
            final ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            ImageView imgClose = (ImageView) view.findViewById(R.id.imgClose);
            imgClose.setVisibility(View.GONE);
            if (model.getMediaType().equals("1")) {
                Picasso.with(getContext()).load(url).fit().into(imgChosen);
            } else {
                Picasso.with(getContext()).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FileViewActivity.class);
                    intent.putExtra("fileType", model.getMediaType());
                    intent.putExtra("filePath", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getContext().startActivity(intent);
                }
            });
            scrollContent.addView(view);
        }
    }

    public void setSkipJobListener(SkipJobListener listener) {
        this.listener = listener;
    }


    public interface SkipJobListener {
        void onSkipJob(ContractorJobList.List details);
    }

    public class ViewHolder {
        ImageView imgIcon;
        CustomTextView txtTitle;
        CustomTextView txtPostCode;
        CustomTextView txtDistance;
        CustomTextView txtCloses;
        CustomTextView txtDescription, txtLowestBid;
        LinearLayout filesLay;
        LinearLayout menuLay;
        ChipView chipView;
    }
}
