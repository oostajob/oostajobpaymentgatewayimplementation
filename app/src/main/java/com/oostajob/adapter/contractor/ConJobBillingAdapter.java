package com.oostajob.adapter.contractor;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.Green.customfont.CustomTextView;
import com.oostajob.FileViewActivity;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ConPaymentHistoryJobResponse;
import com.oostajob.model.ContDetailsForPaymentHistory;
import com.oostajob.model.CustomerDetailsForPaymentHistory;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.CusPaymentHistoryJobResponse;
import com.oostajob.model.ResponseItem;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConJobBillingAdapter extends ArrayAdapter<ConPaymentHistoryJobResponse> {

    private LayoutInflater inflater;
    private int CURRENT_CHECKED;

    public static final int BIDDING = 0;
    public static final int HIRE = 1;
    public static final int RATE = 2;
    public static final int CLOSE = 3;


    private String[] menuItem = {"Bidding", "Hire", "In Progress", "Closed"};

    List<ConPaymentHistoryJobResponse> objects;

    Context context;

    public ConJobBillingAdapter(Context context, List<ConPaymentHistoryJobResponse> objects) {
        super(context, R.layout.item_con_billing_fragment, objects);
        this.objects = objects;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ConPaymentHistoryJobResponse details = getItem(position);
        ResponseItem responseItem = details.getResponse().get(0);

        CustomerDetailsForPaymentHistory contDetailsForPaymentHistory = details.getCustomerDetailsForPaymentHistory();

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_con_billing_fragment, parent, false);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgMarkerIcon);
            holder.txtTitle = (CustomTextView) convertView.findViewById(R.id.txtTitle);
            holder.txtPostCode = (CustomTextView) convertView.findViewById(R.id.txtPostCode);
            holder.txtTime = (CustomTextView) convertView.findViewById(R.id.txtTime);
            //   holder.txtCloses = (CustomTextView) convertView.findViewById(R.id.txtCloses);
            holder.txtDescription = (CustomTextView) convertView.findViewById(R.id.txtDescription);
            holder.txtMenuItem = (CustomTextView) convertView.findViewById(R.id.txtMenuItem);
            holder.txtNewMsg = (CustomTextView) convertView.findViewById(R.id.txtNewMsg);
            //holder.menuLay = (LinearLayout) convertView.findViewById(R.id.menuLay);
            holder.ratelayout = (RelativeLayout) convertView.findViewById(R.id.ratelayout);
            holder.timeLay = (LinearLayout) convertView.findViewById(R.id.timeLay);
//            holder.tagView = (TagView) convertView.findViewById(R.id.tagView);
            holder.filesLay = (LinearLayout) convertView.findViewById(R.id.filesLay);
          /*  holder.imgMsgIcon = (ImageView) convertView.findViewById(R.id.imgMsgIcon);
            holder.imgDropDown = (ImageView) convertView.findViewById(R.id.imgDropDown);
            holder.imgNotifIcon = (ImageView) convertView.findViewById(R.id.imgNotifIcon);*/
          /*  holder.txtMsgCount = (CustomTextView) convertView.findViewById(R.id.txtMsgCount);
            holder.txtNotifCount = (CustomTextView) convertView.findViewById(R.id.txtNotifCount);*/
            holder.chipView = (ChipView) convertView.findViewById(R.id.chipView);
            holder.txtAmount = (CustomTextView) convertView.findViewById(R.id.txtAmount);
            holder.txtDateTime = (CustomTextView) convertView.findViewById(R.id.txtDateTime);
            holder.fmJobCompletedStatus = (FrameLayout) convertView.findViewById(R.id.fmJobCompletedStatus);
            holder.btnCompleted = (CustomTextView) convertView.findViewById(R.id.btnCompleted);
            holder.btnCancelForJob = (CustomTextView) convertView.findViewById(R.id.btnCancelForJob);
            holder.linJObCancel = convertView.findViewById(R.id.linJObCancel);
            holder.chipViewCancel = convertView.findViewById(R.id.chipViewCancel);
            holder.tvEarnsAvaliableDate = convertView.findViewById(R.id.tvEarnsAvaliableDate);


            String icon = details.getJobIconForPaymentHistory().getJobicon();
            icon = icon.replaceAll(" ", "%20");
            String url = String.format("%s/%s", Global.BASE_URL, icon);
            Picasso.with(getContext()).load(url).fit().into(holder.imgIcon);


            holder.tvJobEarningsAfterDecduction = convertView.findViewById(R.id.tvJobEarningsAfterDecduction);
            holder.tvJobValue = convertView.findViewById(R.id.tvJobValue);
            holder.tvAmount = convertView.findViewById(R.id.tvAmount);
            holder.tvPaymentStatusType = convertView.findViewById(R.id.tvPaymentStatusType);
            holder.tvContractorName = convertView.findViewById(R.id.tvContractorName);
            holder.tvCustomerAddress = convertView.findViewById(R.id.tvCustomerAddress);
            holder.tvJobEarningsAfterDecduction.setText("$" + responseItem.getAmountAfterDeduction());

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (!(responseItem.getDescription().trim().length() > 0)) {
            holder.txtDescription.setVisibility(View.GONE);
        } else {
            holder.txtDescription.setVisibility(View.VISIBLE);
        }

        holder.txtTitle.setText(responseItem.getJobtypeName());
        holder.tvEarnsAvaliableDate.setText(responseItem.getEarning_available_date());
        holder.txtPostCode.setText(responseItem.getZipcode());
        holder.txtDescription.setText(responseItem.getDescription());
        holder.tvJobValue.setText("$" + responseItem.getAmount());
        if (responseItem.getPaymentStatus().equalsIgnoreCase("pending")) {
            holder.tvPaymentStatusType.setText("Pending");
            holder.tvPaymentStatusType.setTextColor(context.getResources().getColor(R.color.orange));
        } else {
            holder.tvPaymentStatusType.setText("Paid");
            holder.tvPaymentStatusType.setTextColor(context.getResources().getColor(R.color.green_dark));
        }

        // holder.tvAmount.setText(responseItem.getAmount());
        // holder.tvPaymentStatusType.setText(responseItem.getTransactionFor() + " payment");
        holder.txtTime.setText("" + changeDateFromat(responseItem.getJobpostTime()));

        List<Chip> chipList = new ArrayList<>();
        for (GetCustDetailsModel.JobAnwserlist answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                chipList.add(new com.oostajob.utils.Tag(ans));
            }
        }
        holder.chipView.setLineSpacing(10);
        holder.chipView.setChipSpacing(10);
        holder.chipView.setChipLayoutRes(R.layout.chip_lay);
        holder.chipView.setChipBackgroundRes(R.drawable.tag);
        holder.chipView.setChipList(chipList);


        holder.tvContractorName.setText(contDetailsForPaymentHistory.getName());
        holder.tvCustomerAddress.setText((details.getJobIconForPaymentHistory().getAddress()));
        refreshImageLay(holder.filesLay, details.getJobmedialist(), position);


        List<Chip> arrayListCancelReasons = new ArrayList<>();
        if ((responseItem.getReason() != null) && !(responseItem.getReason().equalsIgnoreCase("null"))) {
            String[] chipListCancel = responseItem.getReason().split(",");

            if (chipListCancel.length > 0) {
                holder.linJObCancel.setVisibility(View.VISIBLE);
                for (String s : chipListCancel) {

                    if (s.trim().length() > 0 && !(s.equalsIgnoreCase("null"))) {
                        arrayListCancelReasons.add(new com.oostajob.utils.Tag(s));
                    }

                }

            }


            holder.chipViewCancel.setLineSpacing(10);
            holder.chipViewCancel.setChipSpacing(10);
            holder.chipViewCancel.setChipLayoutRes(R.layout.chip_lay_cancel);
            holder.chipViewCancel.setChipBackgroundRes(R.drawable.tag_cancel);
            holder.chipViewCancel.setChipList(arrayListCancelReasons);


        } else {
            holder.linJObCancel.setVisibility(View.GONE);
        }


        return convertView;
    }

    private void refreshImageLay(LinearLayout scrollContent, final ArrayList<GetCustDetailsModel.Jobmedialist> fileList, int position) {

//        scrollContent.removeAllViews();
       /* LinearLayout layout = imageMap.get(position);
        if (layout != null) {
            scrollContent = layout;
        } else {*/
        scrollContent.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());

        for (final GetCustDetailsModel.Jobmedialist model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.item_sub_image, scrollContent, false);
            final ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            ImageView imgClose = (ImageView) view.findViewById(R.id.imgClose);
            imgClose.setVisibility(View.GONE);
            if (model.getMediaType().equals("1")) {
                Picasso.with(getContext()).load(url).fit().into(imgChosen);
            } else {
                Picasso.with(getContext()).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FileViewActivity.class);
                    intent.putExtra("fileType", model.getMediaType());
                    intent.putExtra("filePath", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getContext().startActivity(intent);
                }
            });
            scrollContent.addView(view);
        }
            /*imageMap.put(position, scrollContent);
        }*/
    }


    public class ViewHolder {
        ImageView imgIcon;

        CustomTextView txtTitle, txtPostCode, txtMenuItem, txtTime, txtDescription, txtNewMsg, txtAmount, txtDateTime, tvJobEarningsAfterDecduction, tvJobValue, tvAmount, tvPaymentStatusType;
        LinearLayout filesLay;
        //  LinearLayout menuLay;
        LinearLayout timeLay;
        RelativeLayout ratelayout;
        FrameLayout fmJobCompletedStatus;
        //        TagView tagView;
        ChipView chipView, chipViewCancel;
        LinearLayout linJObCancel;
        CustomTextView btnCompleted, btnCancelForJob;

        CustomTextView tvContractorName, tvCustomerAddress, tvEarnsAvaliableDate;
    }

    private String changeDateFromat(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("MM/dd/yyyy");
        date = spf.format(newDate);
        //System.out.println(date);

        return date;
    }

}
