package com.oostajob.adapter.contractor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.Green.customfont.CustomTextView;
import com.Green.customfont.tag.Tag;
import com.Green.customfont.tag.TagView;
import com.oostajob.ConJobsDetailActivity;
import com.oostajob.FileViewActivity;
import com.oostajob.R;
import com.oostajob.adapter.customer.CustomerJobsAdapter;
import com.oostajob.fragment.contractor.ConJobsFragment;
import com.oostajob.global.Global;
import com.oostajob.model.ContractorJobList;
import com.oostajob.model.GetCustDetailsModel;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ContractorJobsAdapter extends ArrayAdapter<ContractorJobList.List> {

    public static final int INVITES = 0;
    public static final int BID = 1;
    public static final int HIRE = 2;
    public static final int CLOSE = 3;
    private LayoutInflater inflater;
    private int CURRENT_CHECKED;
    private String[] menuItem = {"Invites", "Bids", "Hired", "Closed"};
    private HashMap<Integer, TagView> tagMap = new HashMap<>();
    private HashMap<Integer, LinearLayout> imageMap = new HashMap<>();
    JobCompeletedOrCancledListner jobCompeletedOrCancledListner;

    public ContractorJobsAdapter(Context context, List<ContractorJobList.List> objects, int filter) {
        super(context, R.layout.item_con_my_jobs, objects);
        inflater = LayoutInflater.from(context);
        CURRENT_CHECKED = filter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ContractorJobList.List item = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_con_my_jobs, parent, false);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgMarkerIcon);
            holder.txtTitle = (CustomTextView) convertView.findViewById(R.id.txtTitle);
            holder.txtPostCode = (CustomTextView) convertView.findViewById(R.id.txtPostCode);
            holder.txtTime = (CustomTextView) convertView.findViewById(R.id.txtTime);
            holder.txtDescription = (CustomTextView) convertView.findViewById(R.id.txtDescription);
            holder.txtMenuItem = (CustomTextView) convertView.findViewById(R.id.txtMenuItem);
            holder.menuLay = (LinearLayout) convertView.findViewById(R.id.menuLay);
            holder.chipView = (ChipView) convertView.findViewById(R.id.chipView);
            holder.filesLay = (LinearLayout) convertView.findViewById(R.id.filesLay);
            holder.imgMsgIcon = (ImageView) convertView.findViewById(R.id.imgMsgIcon);
            holder.txtMsgCount = (CustomTextView) convertView.findViewById(R.id.txtMsgCount);
            holder.txtNotifCount = (CustomTextView) convertView.findViewById(R.id.txtNotifCount);
            holder.txtNewMsg = (CustomTextView) convertView.findViewById(R.id.txtNewMsg);
            // holder.txtHired = (CustomTextView) convertView.findViewById(R.id.txtHired);
            holder.txtDateTime = (CustomTextView) convertView.findViewById(R.id.txtDateTime);
            holder.txtLowestBid = (CustomTextView) convertView.findViewById(R.id.txtLowestBid);
            holder.timeLay = (LinearLayout) convertView.findViewById(R.id.timeLay);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            holder.btnCompleted = (CustomTextView) convertView.findViewById(R.id.btnCompleted);
            holder.chipViewCancel = convertView.findViewById(R.id.chipViewCancel);
            holder.linJObCancel = convertView.findViewById(R.id.linJObCancel);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String icon = item.getJob_icon();
        icon = icon.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, icon);
        Picasso.with(getContext()).load(url).fit().into(holder.imgIcon);
        holder.txtTitle.setText(item.getJobtypeName());
        holder.txtPostCode.setText(item.getZipcode());
        holder.txtDescription.setText(item.getDescription());

        Date date = getDate(item);

        long difference = System.currentTimeMillis() - date.getTime();

        long hours = TimeUnit.MILLISECONDS.toHours(difference);

        if (hours == 0) {
            long minu = TimeUnit.MILLISECONDS.toMinutes(difference);
            if (minu > 1)
                holder.txtTime.setText(String.format("%d minutes ago", minu));
            else holder.txtTime.setText(String.format("%d minute ago", minu));
        } else if (hours >= 24) {
            long days = TimeUnit.MILLISECONDS.toDays(difference);
            if (days > 1)
                holder.txtTime.setText(String.format("%d days ago", days));
            else holder.txtTime.setText(String.format("%d day ago", days));
        } else if (hours < 24) {
            if (hours > 1)
                holder.txtTime.setText(String.format("%d hours ago", hours));
            else holder.txtTime.setText(String.format("%d hour ago", hours));
        }

        holder.txtNewMsg.setVisibility(View.GONE);

        if (!item.getMessageCount().equals("0")) {
            holder.txtMsgCount.setText(item.getMessageCount());
            holder.txtMsgCount.setTextColor(getContext().getResources().getColor(R.color.blue));
            holder.txtNewMsg.setText(item.getNotificationCont());
            Picasso.with(getContext()).load(R.drawable.mail_blue).fit().into(holder.imgMsgIcon);
        } else {
            Picasso.with(getContext()).load(R.drawable.mail).fit().into(holder.imgMsgIcon);
        }

        holder.txtNotifCount.setText(String.format("$%s", item.getBidAmount()));

        refreshImageLay(holder.filesLay, item.getJobmedialist(), position);

        List<Chip> chipList = new ArrayList<>();

        if (CLOSE == CURRENT_CHECKED) {
            List<Chip> arrayListCancelReasons = new ArrayList<>();

            if ((item.reason != null) && !(item.reason.equalsIgnoreCase("null"))) {
                String[] chipListCancel = item.reason.split(",");

                if (chipListCancel.length > 0) {
                    holder.linJObCancel.setVisibility(View.VISIBLE);
                    for (String s : chipListCancel) {

                        if (s.trim().length() > 0 && !(s.equalsIgnoreCase("null"))) {
                            arrayListCancelReasons.add(new com.oostajob.utils.Tag(s));
                        }

                    }

                }


            } else {
                holder.linJObCancel.setVisibility(View.GONE);
            }


            for (ContractorJobList.Answer answer : item.getJobanwserlist()) {
                if (answer.getTag_key().equalsIgnoreCase("1")) {
                    String ans = "";
                    if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                        ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                    } else {
                        ans = answer.getAnswer();
                    }
                    chipList.add(new com.oostajob.utils.Tag(ans));
                }
            }

            holder.chipViewCancel.setLineSpacing(10);
            holder.chipViewCancel.setChipSpacing(10);
            holder.chipViewCancel.setChipLayoutRes(R.layout.chip_lay_cancel);
            holder.chipViewCancel.setChipBackgroundRes(R.drawable.tag_cancel);
            holder.chipViewCancel.setChipList(arrayListCancelReasons);
        }

        holder.chipView.setLineSpacing(10);
        holder.chipView.setChipSpacing(10);
        holder.chipView.setChipLayoutRes(R.layout.chip_lay);
        holder.chipView.setChipBackgroundRes(R.drawable.tag);
        holder.chipView.setChipList(chipList);


        //holder.btnCompleted.setVisibility(View.VISIBLE);

        if (item.is_contractor_completed != null) {
            if (!item.is_contractor_completed.equalsIgnoreCase("1")) {
                holder.btnCompleted.setVisibility(View.GONE);
            } else {
                holder.btnCompleted.setVisibility(View.GONE);
            }
        }


        setMenu(holder, position);

        setListener(convertView, holder, position);
        setVisibility(holder);
        if (CURRENT_CHECKED == BID) {
            holder.txtLowestBid.setText(String.format("Lowest bid as of now $%s", item.getMinimumbidamount()));
        }

        try {
            if (CURRENT_CHECKED == HIRE) {
                if ((item.getMeetingDate() == null && item.getMeetingTime() == null) || item.getMeetingDate().startsWith("0000")) {
                    holder.txtDateTime.setText("Meeting not scheduled");
                } else {
                    String dateTime = String.format("%s, %s", Global.getDate(item.getMeetingDate(), "yyyy-MM-dd", "MM/dd/yyyy"), item.getMeetingTime());
                    holder.txtDateTime.setText(dateTime);
                }

                holder.btnCompleted.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (jobCompeletedOrCancledListner != null) {
                            jobCompeletedOrCancledListner.onJobCompleted(item);
                        }

                    }
                });
            } else if (CURRENT_CHECKED == CLOSE && item.getCustomerDetails().getTotalRating() != null) {
                holder.ratingBar.setRating(Float.parseFloat(item.getCustomerDetails().getTotalRating()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private Date getDate(ContractorJobList.List item) {
        switch (CURRENT_CHECKED) {
            case BID:
                return Global.getDate(item.getDateofbid());
            case HIRE:
                return Global.getDate(item.getHiredTimed());
            case CLOSE:
                return Global.getDate(item.getClosedTime());
            default:
                return new Date();
        }
    }

    private void setMenu(ViewHolder holder, int position) {
        switch (CURRENT_CHECKED) {
            case BID:
                holder.txtMenuItem.setText(menuItem[BID]);
                holder.menuLay.setBackgroundResource(R.drawable.blue_rectangle_curve);
                holder.btnCompleted.setVisibility(View.GONE);
                break;
            case HIRE:
                holder.txtMenuItem.setText(menuItem[HIRE]);
                holder.menuLay.setBackgroundResource(R.drawable.rose_rectangle_curved);
                holder.btnCompleted.setVisibility(View.VISIBLE);
                break;
            case CLOSE:
                holder.txtMenuItem.setText(menuItem[CLOSE]);
                holder.menuLay.setBackgroundResource(R.drawable.blue_rectangle_curve);
                holder.btnCompleted.setVisibility(View.GONE);
                break;
        }
    }

    private void setTags(TagView tagView, final ContractorJobList.List details, int position) {
        tagView.removeAllTags();
        ArrayList<ContractorJobList.Answer> answerList = details.getJobanwserlist();
        for (ContractorJobList.Answer answer : details.getJobanwserlist()) {
            if (answer.getTag_key().equalsIgnoreCase("1")) {
                String ans = "";
                if (!TextUtils.isEmpty(answer.getTag_keyword())) {
                    ans = String.format("%s %s", answer.getTag_keyword(), answer.getAnswer());
                } else {
                    ans = answer.getAnswer();
                }
                Tag tag = new Tag(ans);
                tag.tagTextColor = getContext().getResources().getColor(R.color.black);
                tag.layoutColor = getContext().getResources().getColor(R.color.white);
                tag.layoutColorPress = getContext().getResources().getColor(R.color.lightGrey);
                tag.radius = 20f;
                tag.tagTextSize = 14f;
                tag.layoutBorderSize = 1f;
                tag.layoutBorderColor = getContext().getResources().getColor(R.color.black);
                tag.isDeletable = false;
                tagView.addTag(tag);
            }
        }
    }

    private void setListener(View convertView, ViewHolder holder, final int position) {
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContractorJobList.List item = getItem(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("details", item);
                bundle.putInt("currentItem", CURRENT_CHECKED);
//                startActivity(bundle);
                Intent intent = new Intent(getContext(), ConJobsDetailActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getContext().startActivity(intent);
            }
        });
    }

    private void refreshImageLay(LinearLayout scrollContent, final ArrayList<ContractorJobList.Media> fileList, int position) {

       /* LinearLayout layout = imageMap.get(position);
        if (layout != null) {
            scrollContent = layout;
        } else {*/
        scrollContent.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());

        for (final ContractorJobList.Media model : fileList) {
            final String url = Global.BASE_URL + "/" + model.getMediaContent();
            View view = inflater.inflate(R.layout.item_sub_image, scrollContent, false);
            final ImageView imgChosen = (ImageView) view.findViewById(R.id.imgChosen);
            ImageView imgClose = (ImageView) view.findViewById(R.id.imgClose);
            imgClose.setVisibility(View.GONE);
            if (model.getMediaType().equals("1")) {
                Picasso.with(getContext()).load(url).fit().into(imgChosen);
            } else {
                Picasso.with(getContext()).load(R.drawable.play).fit().into(imgChosen);
            }
            imgChosen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FileViewActivity.class);
                    intent.putExtra("fileType", model.getMediaType());
                    intent.putExtra("filePath", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getContext().startActivity(intent);
                }
            });
            scrollContent.addView(view);
        }
          /*  imageMap.put(position, scrollContent);
        }*/
    }

    private void setVisibility(ViewHolder holder) {
        switch (CURRENT_CHECKED) {
            case BID:
                //holder.txtHired.setVisibility(View.GONE);
                holder.timeLay.setVisibility(View.GONE);
                holder.ratingBar.setVisibility(View.GONE);
                break;
            case HIRE:
                //  holder.txtHired.setVisibility(View.VISIBLE);
                holder.timeLay.setVisibility(View.VISIBLE);
                holder.txtLowestBid.setVisibility(View.GONE);
                holder.ratingBar.setVisibility(View.GONE);
                break;
            case CLOSE:
                //holder.txtHired.setText("Congratulations you have been rated");
                //  holder.txtHired.setVisibility(View.VISIBLE);
                holder.timeLay.setVisibility(View.GONE);
                holder.txtLowestBid.setVisibility(View.GONE);
                holder.ratingBar.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void setJobCompeletedOrCancledListner(JobCompeletedOrCancledListner jobCompeletedOrCancledListner) {
        this.jobCompeletedOrCancledListner = jobCompeletedOrCancledListner;
    }


    public class ViewHolder {
        ImageView imgIcon, imgMsgIcon;
        CustomTextView txtTitle, txtPostCode, txtMenuItem,
                txtTime, txtDescription, txtMsgCount, txtNotifCount, txtNewMsg, txtHired, txtDateTime, txtLowestBid, btnCompleted;
        LinearLayout filesLay, timeLay, menuLay;
        ChipView chipView, chipViewCancel;
        LinearLayout linJObCancel;
        RatingBar ratingBar;


    }

    public interface JobCompeletedOrCancledListner {
        void onJobCompleted(ContractorJobList.List list);
    }
}
