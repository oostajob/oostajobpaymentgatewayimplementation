package com.oostajob.adapter.contractor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.ConReviewModel;
import com.oostajob.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ConReviewAdapter extends ArrayAdapter<ConReviewModel.RatingDetails> {

    LayoutInflater inflater;

    public ConReviewAdapter(Context context, List<ConReviewModel.RatingDetails> objects) {
        super(context, R.layout.item_review, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ConReviewModel.RatingDetails item = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_review, parent, false);
            holder.imgProfile = (ImageView) convertView.findViewById(R.id.imgProfile);
            holder.txtName = (CustomTextView) convertView.findViewById(R.id.txtName);
            holder.txtTime = (CustomTextView) convertView.findViewById(R.id.txtTime);
            holder.txtAddress = (CustomTextView) convertView.findViewById(R.id.txtAddress);
            holder.txtDescription = (CustomTextView) convertView.findViewById(R.id.txtDescription);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (item.getCustomerDetails().size() > 0) {
            ConReviewModel.CustomerDetails info = item.getCustomerDetails().get(0);
            String profileImg = info.getProfilePhoto();
            profileImg = profileImg.replaceAll(" ", "%20");
            String url = String.format("%s/%s", Global.BASE_URL, profileImg);
            Picasso.with(getContext()).load(url).transform(new CircleTransform()).fit().into(holder.imgProfile);
            holder.txtName.setText(info.getUsername());
            holder.txtAddress.setText(info.getAddress());
        } else {
            holder.imgProfile.setImageDrawable(null);
            holder.txtName.setText("");
            holder.txtAddress.setText("");
        }

        holder.txtTime.setText(Global.getDate(item.getRatingTime(), "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy hh:mm aa"));
        holder.txtDescription.setText(item.getComment());
        holder.ratingBar.setRating(Float.parseFloat(item.getRatingOverall()));


        return convertView;
    }

    public class ViewHolder {
        ImageView imgProfile;
        RatingBar ratingBar;
        CustomTextView txtName, txtTime, txtAddress, txtDescription;
    }
}
