package com.oostajob.adapter.contractor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.JobTypeModel.JobtypeDetails;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ExpertiseAdapter extends ArrayAdapter<JobtypeDetails> {

    LayoutInflater inflater;

    public ExpertiseAdapter(Context context, List<JobtypeDetails> objects) {
        super(context, R.layout.item_category_dialog, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        JobtypeDetails jobtypeDetails = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_category_dialog, parent, false);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgMarkerIcon);
            holder.txtItem = (CustomTextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String jobIncon = jobtypeDetails.getJob_icon();
        jobIncon = jobIncon.replaceAll(" ", "%20");
        String url = String.format("%s/%s", Global.BASE_URL, jobIncon);
        Picasso.with(getContext()).load(url).fit().into(holder.imgIcon);
        holder.txtItem.setText(jobtypeDetails.getJob_name());

//        convertView.setBackgroundResource(R.drawable.menu_item_selector);
        return convertView;
    }

    public class ViewHolder {
        ImageView imgIcon;
        CustomTextView txtItem;
    }
}
