package com.oostajob.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.model.MenuModel;

import java.util.List;

public class DrawerMenuAdapter extends ArrayAdapter<MenuModel> {

    LayoutInflater inflater;

    public DrawerMenuAdapter(Context context, List<MenuModel> objects) {
        super(context, R.layout.item_menu, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuModel model = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_menu, parent, false);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.title = (CustomTextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.icon.setImageResource(model.getIcon());
        holder.title.setText(model.getTitle());
        convertView.setBackgroundResource(R.drawable.blue_drawer_selector);
        return convertView;
    }

    public class ViewHolder {
        ImageView icon;
        CustomTextView title;
    }
}
