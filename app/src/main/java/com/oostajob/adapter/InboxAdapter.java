package com.oostajob.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.Green.customfont.CustomTextView;
import com.oostajob.R;
import com.oostajob.global.Global;
import com.oostajob.model.InboxModel;

import java.util.List;


public class InboxAdapter extends ArrayAdapter<InboxModel.Inbox> {

    private LayoutInflater inflater;
    private RemoveListener removeListener;

    public InboxAdapter(Context context, List<InboxModel.Inbox> objects) {
        super(context, R.layout.item_inbox_list, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InboxModel.Inbox model = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_inbox_list, parent, false);
            holder.txtSubject = (CustomTextView) convertView.findViewById(R.id.txtSubject);
            holder.txtTime = (CustomTextView) convertView.findViewById(R.id.txtTime);
            holder.txtMessage = (CustomTextView) convertView.findViewById(R.id.txtMessage);
            holder.imgRemove = (ImageView) convertView.findViewById(R.id.imgRemove);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtSubject.setText(model.getSubject());
        holder.txtTime.setText(Global.getDate(model.getCreatedTime(), Global.SERVER_DATE_FORMAT, "MM/dd/yyyy hh:mm aa"));
        holder.txtMessage.setText(model.getMessage());
        setMenu(holder.imgRemove, model.getNotifiyID());
        return convertView;
    }

    private void setMenu(ImageView imgRemove, final String id) {
        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_cancel:
                                removeListener.onRemove(id);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.inflate(R.menu.remove);
                popup.show();
            }
        });
    }

    public void setRemoveListener(RemoveListener removeListener) {
        this.removeListener = removeListener;
    }

    public interface RemoveListener {
        void onRemove(String id);
    }

    public class ViewHolder {
        CustomTextView txtSubject, txtTime, txtMessage;
        ImageView imgRemove;
    }
}
