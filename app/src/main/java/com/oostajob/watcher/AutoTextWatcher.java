package com.oostajob.watcher;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Admin on 09-10-2015.
 */
public class AutoTextWatcher implements TextWatcher {
    TextWatcher textWatcher;
    int id;

    public AutoTextWatcher(int id, TextWatcher textWatcher) {
        this.id = id;
        this.textWatcher = textWatcher;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (textWatcher != null) {
            textWatcher.afterTextChanged(id, s);
        }
    }

    public interface TextWatcher {
        public void afterTextChanged(int view, Editable s);
    }
}
