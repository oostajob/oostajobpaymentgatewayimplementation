package com.oostajob.utils;

import java.util.ArrayList;
import java.util.List;

public class CalHolder {
    boolean isFullNotAvailable;
    boolean[] time = new boolean[5];

    public boolean[] getTime() {
        return time;
    }

    public void setTime(boolean[] time) {
        this.time = time;
        int i = 0;
        for (boolean val : time) {
            if (val) {
                i = i + 1;
            }
        }
        if (i == 5) {
            setFullNotAvailable(true);
        } else setFullNotAvailable(false);
    }

    public boolean isFullNotAvailable() {
        return isFullNotAvailable;
    }

    public void setFullNotAvailable(boolean fullNotAvailable) {
        isFullNotAvailable = fullNotAvailable;
    }


}