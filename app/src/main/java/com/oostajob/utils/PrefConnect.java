package com.oostajob.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import java.util.Set;


public class PrefConnect {
    public static final String PREF_NAME = "MY_PREF";
    public static final int MODE = Context.MODE_PRIVATE;
    public static final String GCM_TOKEN = "GCM_TOKEN";
    public static final String APP_VERSION = "APP_VERSION";

    public static final String PAGE = "page";

    public static final String ABOUT_US = "ABOUT_US";
    public static final String FAQ = "FAQ";
    public static final String CUSTOMER_TERMS = "CUSTOMER_TERMS";
    public static final String CONTRACTOR_TERMS = "CONTRACTOR_TERMS";
    public static final String CUSTOMER_STORIES = "CUSTOMER_STORIES";
    public static final String CUSTOMER_PAYMENT_TERMS = "CUSTOMER_PAYMENT_TERMS";
    public static final String CONTRACTOR_PAYMENT_TERMS = "CONTRACTOR_PAYMENT_TERMS";

    //************** USER DETAILS **************
    public static final String USER_ID = "userID";
    public static final String EMAIL = "Email";
    public static final String PASSWORD = "Password";
    public static final String USER_TYPE = "userType";
    public static final String NAME = "Name";
    public static final String ADDRESS = "Address";
    public static final String PHONE = "Phone";
    public static final String PROFILE_PHOTO = "profilePhoto";
    public static final String ZIPCODE = "Zipcode";
    public static final String LAT = "Lat";
    public static final String LNG = "Lng";
    public static final String SSN = "SSN";
    public static final String EXPERTISE_AT = "expertiseAT";
    public static final String LICENSE = "Licence";
    public static final String HOURLY_RATE = "hourlyRate";
    public static final String BILL_STATUS = "billStatus";
    public static final String SKIP = "skip";
    public static final String JOB_POSTAL = "job_postal";
    public static final String SUB_TITLE = "sub_title";
    public static final String IMG_URL = "img_url";
    public static final String JOB_ID = "job_id";
    public static final String LIST = "list";
    public static final String BRAIN_TREE_CUSTOMER_ID = "brain_tree_customer_id";

    /*bank details for the contractor*/

    public static final String BANK_NAME = "BANK_NAME";
    public static final String ACCOUNT_HOLDER_NAME = "ACCOUNT_HOLDER_NAME";
    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String ROUTING = "ROUTING";


    public static void clearAllPrefs(Context context) {
        getEditor(context).clear().commit();
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    /**
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static boolean readBoolean(Context context, String key,
                                      boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    /**
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    /**
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /**
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static String readString_(Context context, String key, String defValue) {
        return getDefaultPreferences(context).getString(key, defValue);
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    /**
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    /**
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }


    /**
     * @param context
     * @return
     */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /**
     * @param context
     * @return
     */
    public static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    /**
     * @param context
     * @return
     */
    public static SharedPreferences getDefaultPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
