package com.oostajob.utils;

import android.app.Activity;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class Tracker implements ConnectionCallbacks, OnConnectionFailedListener, ResultCallback<LocationSettingsResult>,
        LocationListener {

    public static final int REQUEST_CHECK_SETTINGS = 7531;
    private static final String TAG = "LocationTracker";
    private LocationCallback locationCallback;
    private Activity activity;
    private LocationSettingsRequest.Builder builder;
    private PendingResult<LocationSettingsResult> result;
    private GoogleApiClient googleAPIClient;
    private Location location;
    private LocationRequest mLocationRequest;


    public Tracker(Activity activity, LocationCallback locationCallback) {
        this.activity = activity;
        this.locationCallback = locationCallback;
        if (googleAPIClient == null) {
            googleAPIClient = new GoogleApiClient.Builder(activity)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            createLocationRequest();
        }
    }

    public void connectClient() {
        googleAPIClient.connect();
    }

    public void disConnectClient() {
        googleAPIClient.disconnect();
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Google API Connected");
        /*if (location == null) {
            setLocation(LocationServices.FusedLocationApi.getLastLocation(googleAPIClient));
        }*/

        if (locationCallback != null) {
            locationCallback.loadDefaultLocation();
        }

        builder = new LocationSettingsRequest.Builder()
                .setAlwaysShow(true)
                .addLocationRequest(mLocationRequest);
        result = LocationServices.SettingsApi.checkLocationSettings(googleAPIClient,
                builder.build());
        result.setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Google API Suspended");
        googleAPIClient.connect();
        Log.i(TAG, "Google API Re-Connecting");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Google API Failed: " + connectionResult.getErrorMessage());
    }

    //
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(4000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdate() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        LocationServices.FusedLocationApi.requestLocationUpdates(googleAPIClient, mLocationRequest, this);
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        LocationServices.FusedLocationApi.removeLocationUpdates(googleAPIClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            setLocation(location);
            stopLocationUpdates();
            if (locationCallback != null) {
                locationCallback.onLocationReceived(location);
            }
        }
    }

    @Override
    public void onResult(LocationSettingsResult result) {
        final Status status = result.getStatus();
        final LocationSettingsStates settingsStates = result.getLocationSettingsStates();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can
                // initialize location requests here.
                startLocationUpdate();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    status.startResolutionForResult(
                            activity,
                            REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    // Ignore the error.
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way
                // to fix the settings so we won't show the dialog.
                break;
        }
    }

    public interface LocationCallback {
        public void onLocationReceived(Location location);
        public void loadDefaultLocation();
    }

}
