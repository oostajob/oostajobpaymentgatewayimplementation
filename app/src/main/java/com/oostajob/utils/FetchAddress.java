package com.oostajob.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class FetchAddress {

    private static final String TAG = "FetchAddressService";
    Context context;
    LatLng latLng;
    String latitude, longitude;
    private String postalCode;
    private String locality;

    public FetchAddress(Context context, LatLng latLng) {
        this.context = context;
        this.latLng = latLng;
        setLatitude(String.valueOf(latLng.latitude));
        setLongitude(String.valueOf(latLng.longitude));
    }

    public String getAddress() {
        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);

        String errorMessage = "";
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    latLng.latitude,
                    latLng.longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = "Service not Available";
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = "Invalid Lat Lng Used";
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + latLng.latitude +
                    ", Longitude = " +
                    latLng.longitude, illegalArgumentException);
        } catch (NullPointerException e) {
            Log.e(TAG, "Location null", e);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "No Address Found";
                Log.e(TAG, errorMessage);
                return "";
            }
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();
            setPostalCode(address.getPostalCode());
            setLocality(address.getLocality());
            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            Calendar calendar = Calendar.getInstance();
            TimeZone timeZone = calendar.getTimeZone();
            timeZone.getDisplayName();

            if (address.getMaxAddressLineIndex() > 0) {
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
            } else {
                try {
                    addressFragments.add(address.getAddressLine(0));
                } catch (Exception ignored) {}
            }
            Log.i(TAG, "Address Found");
            String adrs = TextUtils.join(System.getProperty("line.separator"),
                    addressFragments);
            return adrs;
        }
        return null;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }
}
