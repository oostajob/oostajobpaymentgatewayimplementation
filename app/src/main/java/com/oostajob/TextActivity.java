package com.oostajob;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.Green.customfont.CustomEditText;
import com.Green.customfont.CustomTextView;

public class TextActivity extends AppCompatActivity {

    private String TITLE, SELECTED;
    private int POSITION;
    private Toolbar toolbar;
    private CustomEditText text;
    private ActionBar.LayoutParams lp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        getIntentValues();
        init();
        setUpCustomToolbar();
        setListener();
        loaValues();
    }

    private void getIntentValues() {
        TITLE = getIntent().getStringExtra("title");
        SELECTED = getIntent().getStringExtra("selected");
        POSITION = getIntent().getIntExtra("position", -1);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        text = (CustomEditText) findViewById(R.id.text);

        //Params
        lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void setUpCustomToolbar() {
        if (toolbar != null) {
            toolbar.removeAllViews();
            View toolView = LayoutInflater.from(this).inflate(R.layout.toolbar_cus, toolbar, false);
            CustomTextView txtTitle = (CustomTextView) toolView.findViewById(R.id.txtTitle);
            CustomTextView txtJobTitle = (CustomTextView) toolView.findViewById(R.id.txtJobTitle);
            txtTitle.setVisibility(View.GONE);
            txtJobTitle.setText(TITLE);
            txtJobTitle.setTextSize(14.0f);
            txtJobTitle.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
            toolbar.addView(toolView, lp);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black_trans1));
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loaValues() {
        toolbar.setTitle(TITLE);
        if (!TITLE.equalsIgnoreCase(SELECTED)) {
            text.setText(SELECTED);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        CustomTextView textView = (CustomTextView) menu.findItem(R.id.action_done).getActionView();
        textView.setText(R.string.done);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.action_done) {
            return super.onOptionsItemSelected(item);
        }
        sendResult();
        return true;
    }

    public void sendResult() {
        Intent intent = new Intent();
        SELECTED = TextUtils.isEmpty(text.getText().toString()) ? "" : text.getText().toString();
        intent.putExtra("selected", SELECTED);
        intent.putExtra("position", POSITION);
        setResult(RESULT_OK, intent);
        finish();
    }
}
