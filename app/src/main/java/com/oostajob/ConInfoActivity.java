package com.oostajob;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.Green.customfont.CustomButton;
import com.Green.customfont.CustomTextView;
import com.oostajob.adapter.customer.ReviewAdapter;
import com.oostajob.dialog.CusCalendarDialog;
import com.oostajob.fragment.ImageFragment;
import com.oostajob.global.Global;
import com.oostajob.model.GetCustDetailsModel;
import com.oostajob.model.JobTypeModel;
import com.oostajob.webservice.APIService;
import com.oostajob.webservice.RetrofitSingleton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConInfoActivity extends AppCompatActivity implements OnPageChangeListener {

    private ArrayList<JobTypeModel.JobtypeDetails> jobTypesList = new ArrayList<>();
    private APIService apiService;
    private ProgressDialog p;
    private ArrayList<GetCustDetailsModel.Contractorlist> contractorlist;
    private ViewPager pager;
    private CustomTextView txtName, txtExpertise, txtAddress, txtHourly;
    private GetCustDetailsModel.Details details;
    private int currentItem;
    private CustomButton btnHire;
    private ConInfoAdapter infoAdapter;
    private Toolbar toolbar;
    private ListView reviewsView;
    private ArrayList<String> expertiseList = new ArrayList<>();
    private ReviewAdapter reviewAdapter;
    private ImageView imgBack;
    private AppCompatRatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_info);

        getFromIntent();

        init();
        setUpToolbar();
        setListener();
        setAdapter();

        loadValues(contractorlist.get(currentItem));
        getExpertise(contractorlist.get(currentItem).getExpertiseAT());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                gotoHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void gotoHome() {
        Intent homeIntent = new Intent(ConInfoActivity.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void getFromIntent() {
        Intent intent = getIntent();
        details = (GetCustDetailsModel.Details) intent.getSerializableExtra("details");
        currentItem = intent.getIntExtra("position", 0);
        contractorlist = details.getContractorlist();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        pager = (ViewPager) findViewById(R.id.pager);
        txtName = (CustomTextView) findViewById(R.id.txtName);
        txtExpertise = (CustomTextView) findViewById(R.id.txtExpertise);
        txtAddress = (CustomTextView) findViewById(R.id.txtAddress);
        txtHourly = (CustomTextView) findViewById(R.id.txtHourly);
        btnHire = (CustomButton) findViewById(R.id.btnHire);
        reviewsView = (ListView) findViewById(R.id.reviewsView);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        ratingBar = (AppCompatRatingBar) findViewById(R.id.ratingBar);
        p = new ProgressDialog(this);
        p.setIndeterminate(false);
        p.setMessage(getString(R.string.loading));
        p.setCancelable(false);
        Global.setBackground(imgBack, getResources(), R.drawable.background_dim);
        apiService = RetrofitSingleton.createService(APIService.class);
    }

    private void setUpToolbar() {
        try {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_home);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        pager.addOnPageChangeListener(this);
//        pager.setPageTransformer(false, new ViewPager.PageTransformer() {
//            @Override
//            public void transformPage(View page, float position) {
//                float normalizedposition = Math.abs(Math.abs(position) - 1);
//                page.setScaleX(normalizedposition / 2 + 0.5f);
//                page.setScaleY(normalizedposition / 2 + 0.5f);
//            }
//        });
        btnHire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("postId", details.getJobpostID());
                bundle.putString("userId", contractorlist.get(pager.getCurrentItem()).getUserID());
                CusCalendarDialog dialog = new CusCalendarDialog();
                dialog.setArguments(bundle);
                dialog.show(getSupportFragmentManager(), "Hire Dialog");
            }
        });
    }

    private void setAdapter() {
        infoAdapter = new ConInfoAdapter(getSupportFragmentManager());
//        pager.setClipToPadding(false);
//        pager.setPageMargin(10);
        pager.setHorizontalFadingEdgeEnabled(true);
        pager.setFadingEdgeLength(10);
        pager.setAdapter(infoAdapter);
        pager.setCurrentItem(currentItem);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        loadValues(contractorlist.get(position));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void loadValues(GetCustDetailsModel.Contractorlist contract) {
        toolbar.setTitle(contract.getName());
        txtName.setText(contract.getName());
        txtAddress.setText(contract.getAddress());
        //txtHourly.setText(String.format("Hourly Rates:$%s/hr", contract.getHourlyRate()));
        btnHire.setText(String.format("Not to exceed bid amount $%s", contract.getBidAmount()));
        if (!TextUtils.isEmpty(contract.getTotalRating())) {
            ratingBar.setRating(Float.parseFloat(contract.getTotalRating()));
        }else{
            ratingBar.setRating(0);
        }
        setReview(contract);
        setExpertise(contract.getExpertiseAT());
    }

    private void setReview(GetCustDetailsModel.Contractorlist contract) {
        if (contract.getReviewlist().size() > 0) {
            reviewAdapter = new ReviewAdapter(ConInfoActivity.this, contract.getReviewlist());
            reviewsView.setAdapter(reviewAdapter);
        } else {
            reviewsView.setAdapter(null);
        }
    }

    private void getExpertise(final String expertiseId) {
        p.show();
        Call<JobTypeModel> call = apiService.getExpertiseAt();
        call.enqueue(new Callback<JobTypeModel>() {
            @Override
            public void onResponse(Call<JobTypeModel> call, Response<JobTypeModel> response) {
                Global.dismissProgress(p);
                if (response.isSuccessful()) {
                    JobTypeModel model = response.body();
                    if (model.getResult().equalsIgnoreCase("success")) {
                        Global.dismissProgress(p);
                        jobTypesList = model.getJobtypeDetails();
                        setExpertise(expertiseId);
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTypeModel> call, Throwable t) {
                Global.dismissProgress(p);
            }
        });
    }

    public void setExpertise(String expertiseId) {
        expertiseList = new ArrayList<>();
        String[] arr = expertiseId.split(",");
        for (String anArr : arr) {
            for (int j = 0; j < jobTypesList.size(); j++) {
                if (jobTypesList.get(j).getJobtype_id().equals(anArr.trim())) {
                    expertiseList.add(jobTypesList.get(j).getJob_name());
                }
            }
        }
        String exp = expertiseList.toString().replace("[", "").replace("]", "");
        txtExpertise.setText(exp);
    }

    public class ConInfoAdapter extends FragmentPagerAdapter {


        public ConInfoAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            String profileImg = contractorlist.get(position).getProfilePhoto();
            profileImg = profileImg.replaceAll(" ", "%20");
            String url = String.format("%s/%s", Global.BASE_URL, profileImg);
            Bundle bundle = new Bundle();
            bundle.putString("url", url);
            ImageFragment imageFragment = new ImageFragment();
            imageFragment.setArguments(bundle);
            return imageFragment;
        }

        @Override
        public int getCount() {
            return contractorlist.size();
        }

        @Override
        public float getPageWidth(int position) {
            if (contractorlist.size() == 1) {
                return 1f;
            }
            return 0.8f;
        }
    }

}
