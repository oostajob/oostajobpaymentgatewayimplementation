package com.Green.customfont.calendar;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Green.customfont.CustomCheckedTextView;
import com.Green.customfont.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class CalendarView extends LinearLayout {
    // for logging
    private static final String LOGTAG = "Calendar View";

    // how many days to show, defaults to six weeks, 42 days
    private static final int DAYS_COUNT = 42;

    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";
    ArrayList<Date> cells;
    // date format
    private String dateFormat;
    // current displayed month
    private Calendar currentCal = Calendar.getInstance();
    //event handling
    private DateEventHandler dateEventHandler = null;
    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context, attrs);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control_calendar, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar();
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarView);

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CalendarView_dateFormat);
            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
    }

    private void assignClickHandlers() {
        // add one month and refresh UI
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentCal.add(Calendar.MONTH, 1);
                if (dateEventHandler != null) {
                    dateEventHandler.updateCalendar();
                }
            }
        });

        // subtract one month and refresh UI
        btnPrev.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                if (cal.before(currentCal)) {
                    currentCal.add(Calendar.MONTH, -1);
                    if (dateEventHandler != null) {
                        dateEventHandler.updateCalendar();
                    }
                }
            }
        });

        // long-pressing a day
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (dateEventHandler != null) {
                    dateEventHandler.onDayPress((Date) parent.getItemAtPosition(position));
                }
            }
        });
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {
        updateCalendar(null);
    }

    /**
     * Display dates correctly in grid
     *
     * @param events
     */
    public void updateCalendar(HashMap<Date, Boolean> events) {
        cells = new ArrayList<>();
        Calendar startCal = (Calendar) currentCal.clone();
        Calendar endCal = (Calendar) currentCal.clone();

        // determine the cell for current month's beginning
        startCal.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = startCal.get(Calendar.DAY_OF_WEEK) - 1;

        // determine the cell for current month's Ending
        endCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
        int monthEndingCell = 7 - endCal.get(Calendar.DAY_OF_WEEK);

        // move calendar backwards to the beginning of the week
        startCal.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);
        endCal.add(Calendar.DAY_OF_MONTH, +monthEndingCell);

        // fill cells
//        while (cells.size() < DAYS_COUNT) {
//            cells.add(startCal.getTime());
//            startCal.add(Calendar.DAY_OF_MONTH, 1);
//        }

        while (startCal.before(endCal) || startCal.equals(endCal)) {
            cells.add(startCal.getTime());
            startCal.add(Calendar.DAY_OF_MONTH, 1);
        }

        // update grid
        grid.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
        grid.setAdapter(new CalendarAdapter(getContext(), cells, events));

        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(currentCal.getTime()));
    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setDateEventHandler(DateEventHandler dateEventHandler) {
        this.dateEventHandler = dateEventHandler;
    }

    public void
    setItemChecked(String dateTime) {
        for (int i = 0; i < cells.size(); i++) {
            if (dateTime.equals(getDate(cells.get(i)))) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    grid.setItemChecked(i, true);
                }
            }
        }

    }

    public String getDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    public interface DateEventHandler {
        void onDayPress(Date date);

        void updateCalendar();
    }

    private class CalendarAdapter extends ArrayAdapter<Date> {

        //Animation
        Animation anim = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);
        // days with events
        private HashMap<Date, Boolean> eventDays;

        // for view inflation
        private LayoutInflater inflater;

        public CalendarAdapter(Context context, ArrayList<Date> days, HashMap<Date, Boolean> eventDays) {
            super(context, R.layout.control_calendar_day, days);
            this.eventDays = eventDays;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            // day in question
            Date date = getItem(position);
            int day = date.getDate();
            int month = date.getMonth();
            int year = date.getYear();

            // Current Month
            Date currentDate = currentCal.getTime();
            //Today
            Date today = new Date();

            // inflate item if it does not exist yet
            ViewHolder holder = null;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.control_calendar_day, parent, false);
                holder.itemDate = (CustomCheckedTextView) view.findViewById(R.id.itemDate);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            // if this day has an event, specify event image
            view.setBackgroundResource(R.drawable.item_grid_selector);
            if (eventDays != null) {
                for (Map.Entry<Date, Boolean> entry : eventDays.entrySet()) {
                    Date eventDate = entry.getKey();
                    if ((eventDate.getDate() == day &&
                            eventDate.getMonth() == month &&
                            eventDate.getYear() == year) && entry.getValue()) {
                        // mark this day for event
                        view.setBackgroundResource(R.drawable.item_grid_selector_grey);
                        break;
                    }
                }
            }

            if (date.after(today) ||
                    (day == today.getDate() + 1 &&
                            month == today.getMonth() &&
                            year == today.getYear())) {
                holder.itemDate.setEnabled(true);
                view.setClickable(false);
            } else {
                view.setClickable(true);
                holder.itemDate.setEnabled(false);
            }

            // set text
            holder.itemDate.setText(String.valueOf(date.getDate()));
            view.startAnimation(anim);
            return view;
        }

        public class ViewHolder {
            CustomCheckedTextView itemDate;
        }
    }
}
