package com.Green.customfont.tag;

/**
 * listener for tag delete
 */
public interface OnTagDeleteListener {
	void onTagDeleted(Tag tag, int position);
}