package com.Green.customfont.tag;

/**
 * listener for tag delete
 */
public interface OnTagClickListener {
	void onTagClick(Tag tag, int position);
}