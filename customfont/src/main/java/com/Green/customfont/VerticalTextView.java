package com.Green.customfont;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;

/**
 * Created by Admin on 13-06-2015.
 */
public class VerticalTextView extends AppCompatTextView {

    //Support for AppCompat V23
    boolean topDown = false, isVertical = false;
    int direction;

    public VerticalTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public VerticalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);


    }

    public VerticalTextView(Context context) {
        super(context);
        init(null);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.VerticalTextView);
            String fontName = a.getString(R.styleable.VerticalTextView_fontName);
            isVertical = a.getBoolean(R.styleable.VerticalTextView_isVertical, false);
            direction = a.getInt(R.styleable.VerticalTextView_direction, -1);
            setDirection(direction);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        TextPaint textPaint = getPaint();
        textPaint.setColor(getCurrentTextColor());
        textPaint.drawableState = getDrawableState();

        canvas.save();
        if (isVertical) {
            if (topDown) {
                canvas.translate(getWidth(), 0);
                canvas.rotate(90);
            } else {
                canvas.translate(0, getHeight());
                canvas.rotate(-90);
            }
        }

        canvas.translate(getCompoundPaddingLeft(), getExtendedPaddingTop());
        getLayout().draw(canvas);
        canvas.restore();
    }

    public void setDirection(int direction) {
        final int gravity = getGravity();
        switch (direction) {
            case 0:
                setGravity((gravity & Gravity.HORIZONTAL_GRAVITY_MASK) | Gravity.CENTER_VERTICAL);
                topDown = true;
                break;
            case 1:
                setGravity((gravity & Gravity.HORIZONTAL_GRAVITY_MASK) | Gravity.CENTER_VERTICAL);
                topDown = false;
                break;
        }
    }

    public void setFontFace(String fontFace) {
        if (fontFace != null) {
            Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontFace);
            setTypeface(myTypeface);
        }
    }
}
